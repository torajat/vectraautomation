package com.mlab.endpoint.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.InstanceStateChange;
import com.amazonaws.services.ec2.model.StartInstancesRequest;
import com.amazonaws.services.ec2.model.StartInstancesResult;
import com.amazonaws.services.ec2.model.StopInstancesRequest;
import com.mlab.endpoint.Model.CustomerVO;
import com.mlab.endpoint.Model.OrganizationVO;
import com.mlab.endpoint.Model.ProductVO;
import com.mlab.endpoint.Model.TableVO;
import com.mlab.endpoint.Model.UserVO;
import com.mlab.endpoint.Service.CustomerInformationImpl;
import com.mlab.endpoint.Service.SchedulerImp;
import com.mlab.endpoint.Service.SchedulerImp.InputConsumer;
import com.mlab.endpoint.exception.ServiceException;
import com.mlab.endpoint.interfaces.ProductInformationView;
import com.mlab.endpoint.interfaces.SystemSettings;
import com.mlab.endpoint.util.Menumodel;

@Controller
public class CustomerAdminController {
	
	private static final Logger logger = LoggerFactory.getLogger(CustomerAdminController.class);
	  
	

	@Autowired
    private CustomerInformationImpl customerinformtion;
	@Autowired
	private SystemSettings systemSettings;

	@Autowired
    private ProductInformationView productinformationView;
    
	
	@ModelAttribute("CustomerVO")
    private CustomerVO constructCustomerVO()
    {
     return new CustomerVO();	
    }
	
	
	@ModelAttribute("userVO")
	private UserVO constractUserVO() {

		return new UserVO();
	}

	

	@ModelAttribute("TaleVO")
	private TableVO constractTableVO() {

		return new TableVO();
	}

	
	
	@ModelAttribute("OrganizationVO")
	private OrganizationVO constractOrganizationVO() {

		return new OrganizationVO();
	}


	  @ModelAttribute("productVO")
	    private ProductVO constractProductVO() {

	        return new ProductVO();
	    }
	    

	
	
	Menumodel mobject=new Menumodel();
	

@Value("${accesskey}")
private String accesskey;

@Value("${sckreetKey}")
private String sckreetKey;


	@SuppressWarnings("deprecation")
	public String StopInstance(String instanceId, String region) {

		
		
		try{
		AWSCredentials credentials = new BasicAWSCredentials(accesskey,
				sckreetKey);

		String regionval = region;

		//AmazonEC2 ec2 = AmazonEC2ClientBuilder.standard()
          //      .withRegion(Regions.valueOf(regionval))
            //    .build();
		
	AmazonEC2 ec2 = new AmazonEC2Client(credentials);
		
	Region ec2region = Region.getRegion(Regions.valueOf(regionval));
		
	ec2.setRegion(ec2region);
		
		//System.out.println("Request to Stop Instance");

	List<String> instancesToStop = new ArrayList<String>();
		instancesToStop.add(instanceId);

		// i-06c8f0d1f584511d3
		StopInstancesRequest stoptr = new StopInstancesRequest();

		stoptr.setInstanceIds(instancesToStop);

		ec2.stopInstances(stoptr);

		} catch (Exception e)
		{
			System.out.println("Error for stopping instance"+e.getMessage());
			return e.getMessage();
		}
		
		return "Stopping the instance,please click refresh button to get status";

	}



	

	@RequestMapping(value = "/vc/manage/instance/stop", method = RequestMethod.POST)
	public String Stop_Instance( Model model,HttpSession session,
			@ModelAttribute("CustomerVO") CustomerVO customerVO,
			@ModelAttribute("UserVO") UserVO userVO,
			@ModelAttribute("productVO") ProductVO productVO,RedirectAttributes rmodel
			)
	{
	
	 if (session.getAttribute("customerid")==null){return "login";}   

	 String css = (String)model.asMap().get("css");
	 String msg = (String)model.asMap().get("msg");
	 
	 
	    if (!"".equals(msg))
	    {
	   	 model.addAttribute("css",css);
	   	 model.addAttribute("msg",msg);
	    }

	    
		 String actionuser=(String) session.getAttribute("username");
		 String customerid=(String) session.getAttribute("customerid");
		 
		 model.addAttribute("username",session.getAttribute("username").toString());
		
		 try 
		 {
			String region="US_EAST_1";
			String instanceid=productVO.getInstanceid();
			String rowid=productVO.getJobrowid();
			
			
			StopInstance(instanceid, region);
			productinformationView.UpdateJobStopEvent(instanceid,rowid);
			rmodel.addFlashAttribute("css","success");
			rmodel.addFlashAttribute("msg","Successfully Stopped");
		 
			System.out.println("Stopping the instance:"+instanceid);

		 } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			rmodel.addFlashAttribute("css","danger");
			rmodel.addFlashAttribute("msg","Error:"+e.getMessage());
		 
		}
		 
		 return "redirect:/vc/job/dashboard";	
	}

	

	@RequestMapping(value = "/vc/manage/instance/transferjob", method = RequestMethod.POST)
	public String TransferJob_Instance( Model model,HttpSession session,
			@ModelAttribute("CustomerVO") CustomerVO customerVO,
			@ModelAttribute("UserVO") UserVO userVO,
			@ModelAttribute("productVO") ProductVO productVO,RedirectAttributes rmodel
			)
	{
	
	 if (session.getAttribute("customerid")==null){return "login";}   

	 String css = (String)model.asMap().get("css");
	 String msg = (String)model.asMap().get("msg");
	 
	 
	    if (!"".equals(msg))
	    {
	   	 model.addAttribute("css",css);
	   	 model.addAttribute("msg",msg);
	    }

	    
		 String actionuser=(String) session.getAttribute("username");
		 String customerid=(String) session.getAttribute("customerid");
		 
		 model.addAttribute("username",session.getAttribute("username").toString());
		
		 try 
		 {
			String region="US_EAST_1";
			
			
			String oldinstanceid=productVO.getInstanceid();
			String companyname=productVO.getCompanyname();
			String designerworkstation=productVO.getDesignerworkstation();
			String toolname=productVO.getToolname();
			
			String rowid=productVO.getJobrowid();
			String olddestionation=productVO.getCsdestionation();
			
			System.out.println("Row Id value"+rowid);
			
			String processingstatus="NeedToCopy";
			String operatingflg="Starting";
			String ipaddress="";
			String cs_destionation="";
			String newinstanceid="";
			
			ArrayList<ProductVO> instancelist = productinformationView.getAllInstanceListWithFlg();
			ArrayList<ProductVO> stoppedinstancelist=new ArrayList<ProductVO>();
	
			StringBuilder inslitstname = new StringBuilder();
			
			BufferedReader br = new BufferedReader(new FileReader("c:/SFTP_Root/"+companyname+"/Merged_Data/"+designerworkstation+"/"+"SERVER_SCHEDULE_MAPPING.txt"));
			String line;
			String flg="1";
			while ((line = br.readLine()) != null) 
			{
			    // Do steps 2 and 3 here
				System.out.println("Lines in text ffile"+line);
				
				if (!line.equals(""))
				{
					inslitstname.append(line);
				}	
			}
			br.close();
			
			System.out.println("InstanceList in file"+inslitstname.toString());
						
			
			for (ProductVO instanceidval : instancelist)
			{
				if ("1".equals(instanceidval.getStatusid()))
				{
				ProductVO info=new ProductVO();
				newinstanceid=instanceidval.getInstanceid();
				
				if (!inslitstname.toString().contains(instanceidval.getInstancename()))
				{
				
				try
				{
					ArrayList<ProductVO> rst = getInstanceDetails(newinstanceid, region);
					String state=rst.get(0).getState();
					ipaddress=rst.get(0).getPrivateip();
					if(state.equalsIgnoreCase("stopped"))
					{
						info.setInstanceid(newinstanceid);
						info.setState(state);
						stoppedinstancelist.add(info);
							
					}
				}catch(Exception e)
				{
					//System.err.println(newinstanceid+ "Error"+ e.getMessage());
				}

				
				}
					
				}
				
			}
			
			System.out.println("Stopped Instance List-->"+stoppedinstancelist.size());
			
		if(stoppedinstancelist.size()>0)
		{
			String newinstancejobid=stoppedinstancelist.get(0).getInstanceid();
		
			System.out.println("New instance id"+newinstancejobid);


			String collectionsource="\\"+olddestionation;
        	
			String coldestionation="C:/SFTP_Root/"+companyname+"/transferjob/"+rowid+"/"+designerworkstation+"/Auto2D_Output_Files";

        	try
        	{	
        		//FileUtils.copyDirectoryToDirectory(srcDir, destDir);
            	FileUtils.copyDirectory(new File(collectionsource),new File(coldestionation));
        				


				File folder = new File(coldestionation);
				File[] listOfFiles = folder.listFiles();

				    for (int i = 0; i < listOfFiles.length; i++)
				    {
				      if (listOfFiles[i].isFile()) 
				      {
				        System.out.println("File " + listOfFiles[i].getName());
				      
				        String name=listOfFiles[i].getName();
				        if (name.contains("Compute"))
				        {
				        	String path=listOfFiles[i].getPath();
				        	System.out.println("Path Name   is  "+path );
				  

				    		File n=new File(path);
				    		
				    		n.deleteOnExit();
				    		
				        }
				      
				      }
				      else if (listOfFiles[i].isDirectory()) {
				        System.out.println("Directory " + listOfFiles[i].getName());
				      }
				    }

			} catch (Exception e) 
        	{
				// TODO: handle exception
			
				rmodel.addFlashAttribute("css","danger");
				rmodel.addFlashAttribute("msg","Error:"+e.getMessage());
			
				return "redirect:/vc/job/dashboard";	

			}
        	
        	
			String rest=StartInstance(newinstancejobid, region);
			
        	
			if (rest.contains("Success"))
			{
				ArrayList<ProductVO> rst = getInstanceDetails(newinstancejobid, region);
				String state=rst.get(0).getState();
				ipaddress=rst.get(0).getPrivateip();
				
				cs_destionation="\\"+ipaddress+"\\Vectra\\Auto2D_Output_Files\\";
	        	
				
				
				System.out.println("Row Id"+rowid);
				String updatestatus=productinformationView.UpdateTransferjobStatus(newinstancejobid, processingstatus, operatingflg, ipaddress, cs_destionation, rowid,coldestionation);
				
	        	if (updatestatus.equals("Success"))
	        	{
	        		StopInstance(oldinstanceid, region);
	    			rmodel.addFlashAttribute("css","success");
	    			rmodel.addFlashAttribute("msg","Transfer Processing Started, it will take sometime to complete the process");
	    		 	        		
	        	}else
	        	{
	        		rmodel.addFlashAttribute("css","danger");
	    			rmodel.addFlashAttribute("msg","Failed transfer..");
	    		 
	    	        		
	        	}
			}else
			{
				
			}
			
		}else
		{
			rmodel.addFlashAttribute("css","danger");
			rmodel.addFlashAttribute("msg","No Instance is available,please try again later");
		 
	       
		}
			
		 } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			rmodel.addFlashAttribute("css","danger");
			rmodel.addFlashAttribute("msg","Error:"+e.getMessage());
		 
		}
		 
		 return "redirect:/vc/job/dashboard";	
	}

	


	
	@RequestMapping(value = "/vc/instances/list", method = RequestMethod.GET)
		public String Get_All_InstanceList( Model model,HttpSession session,
				@ModelAttribute("CustomerVO") CustomerVO customerVO,
				@ModelAttribute("UserVO") UserVO userVO,
				@ModelAttribute("productVO") ProductVO productVO
				)
		{
		
		 if (session.getAttribute("customerid")==null){return "login";}   

		 String css = (String)model.asMap().get("css");
		 String msg = (String)model.asMap().get("msg");
		 
		 
		    if (!"".equals(msg))
		    {
		   	 model.addAttribute("css",css);
		   	 model.addAttribute("msg",msg);
		    }

		    
			 String actionuser=(String) session.getAttribute("username");
			 String customerid=(String) session.getAttribute("customerid");
			 
			 model.addAttribute("username",session.getAttribute("username").toString());
			
			 try 
			 {
				model.addAttribute("instancelist",productinformationView.getAllInstanceListWithFlg());
				model.addAttribute("instatus", InstanceStatus());
			 
			 } catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 
			 model.addAttribute("id",customerVO.getCustomeridentificationo());
			
			 model.addAttribute("modelVOval","productVO");
			 
			 model.addAttribute("actionval","/vc/instances/editinstance");
			 

			 model.addAttribute("actionvalnew","/vc/instances/addinstance");
			 
			 //model.addAttribute(getConfigMenuItem(model));
			 
			 Menumodel menuitem=new Menumodel();
			 
			 model.addAttribute(menuitem.getConfigMenuItem(model));
			
		 				 
		return "customeradmin/instances";
		
		}

	
	@RequestMapping(value = "/vc/instances/editinstance", method = RequestMethod.GET)
		public String EditInstance_GET( Model model,HttpSession session,
				@ModelAttribute("CustomerVO") CustomerVO customerVO,
				@ModelAttribute("UserVO") UserVO userVO,
				@ModelAttribute("productVO") ProductVO productVO
				)
		{
		
		 if (session.getAttribute("customerid")==null){return "login";}   

		 String css = (String)model.asMap().get("css");
		 String msg = (String)model.asMap().get("msg");
		 
		 
		    if (!"".equals(msg))
		    {
		   	 model.addAttribute("css",css);
		   	 model.addAttribute("msg",msg);
		    }

		    
			 String actionuser=(String) session.getAttribute("username");
			 String customerid=(String) session.getAttribute("customerid");
			 
			 model.addAttribute("username",session.getAttribute("username").toString());

			 model.addAttribute("instatus", InstanceStatus());
		 
			 model.addAttribute("modelVOval","productVO");
			 
			 model.addAttribute("actionval","/vc/instances/editinstance");
			 	
			 //model.addAttribute(getConfigMenuItem(model));
			 
			 Menumodel menuitem=new Menumodel();
			 
			 model.addAttribute(menuitem.getConfigMenuItem(model));
			
		 				 
		return "customeradmin/editinstances";
		
		}

	
	
	@RequestMapping(value = "/vc/instances/editinstance", method = RequestMethod.POST)
		public String EditInstance_POST( Model model,HttpSession session,
				@ModelAttribute("CustomerVO") CustomerVO customerVO,
				@ModelAttribute("UserVO") UserVO userVO,
				@ModelAttribute("productVO") ProductVO productVO
				)
		{
		
		 if (session.getAttribute("customerid")==null){return "login";}   

		 String css = (String)model.asMap().get("css");
		 String msg = (String)model.asMap().get("msg");
		 
		 
		    if (!"".equals(msg))
		    {
		   	 model.addAttribute("css",css);
		   	 model.addAttribute("msg",msg);
		    }

		    
			 String actionuser=(String) session.getAttribute("username");
			 String customerid=(String) session.getAttribute("customerid");
			 
			 model.addAttribute("username",session.getAttribute("username").toString());

			 model.addAttribute("instatus", InstanceStatus());
		 
			 model.addAttribute("modelVOval","productVO");
			 
			 model.addAttribute("actionval","/vc/instances/editinstance");
			 
			 model.addAttribute("actionvalnew","/vc/instances/addinstance");
			 
			 //model.addAttribute(getConfigMenuItem(model));

			 try 
			 {
			
				String res=productinformationView.EditInstance(productVO);
				model.addAttribute("msg",res);
				model.addAttribute("css","info");
				
			 } catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 
			 Menumodel menuitem=new Menumodel();
			 
			 model.addAttribute(menuitem.getConfigMenuItem(model));
			
		 				 
		return "customeradmin/editinstances";
		
		}


	

	@RequestMapping(value = "/vc/instances/addinstance", method = RequestMethod.POST)
		public String AddInstance_POST( Model model,HttpSession session,
				@ModelAttribute("CustomerVO") CustomerVO customerVO,
				@ModelAttribute("UserVO") UserVO userVO,
				@ModelAttribute("productVO") ProductVO productVO,RedirectAttributes rmodel
				)
		{
		
		 if (session.getAttribute("customerid")==null){return "login";}   

		 String css = (String)model.asMap().get("css");
		 String msg = (String)model.asMap().get("msg");
		 
		 
		    if (!"".equals(msg))
		    {
		   	 model.addAttribute("css",css);
		   	 model.addAttribute("msg",msg);
		    }

		    
			 String actionuser=(String) session.getAttribute("username");
			 String customerid=(String) session.getAttribute("customerid");
			 
			 model.addAttribute("username",session.getAttribute("username").toString());

			 model.addAttribute("instatus", InstanceStatus());
		 
			 model.addAttribute("modelVOval","productVO");
			 
			 model.addAttribute("actionval","/vc/instances/editinstance");
			 
			 model.addAttribute("actionvalnew","/vc/instances/addinstance");
			 
			 
			 //model.addAttribute(getConfigMenuItem(model));

			 try 
			 {
				String res=productinformationView.AddInstance(productVO);
			
				rmodel.addFlashAttribute("msg",res);
				rmodel.addFlashAttribute("css","info");
				
			 } catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 
		 				 
		return "redirect:/vc/instances/list";
		
		}

	
	
//customer management
	
	
	@RequestMapping(value = "/vc/customer/list", method = RequestMethod.GET)
		public String GetCustomerDetails_GET( Model model,HttpSession session,
				@ModelAttribute("CustomerVO") CustomerVO customerVO,
				@ModelAttribute("UserVO") UserVO userVO,
				@ModelAttribute("productVO") ProductVO productVO
				)
		{
		
		 if (session.getAttribute("customerid")==null){return "login";}   

		 String css = (String)model.asMap().get("css");
		 String msg = (String)model.asMap().get("msg");
		 
		 
		    if (!"".equals(msg))
		    {
		   	 model.addAttribute("css",css);
		   	 model.addAttribute("msg",msg);
		    }

		    
			 String actionuser=(String) session.getAttribute("username");
			 String customerid=(String) session.getAttribute("customerid");
			 
			 model.addAttribute("username",session.getAttribute("username").toString());
			
			 try 
			 {
				model.addAttribute("instancelist",productinformationView.GetAllCustomerList());
			 
			 } catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 
			 model.addAttribute("id",customerVO.getCustomeridentificationo());
			
			 model.addAttribute("modelVOval","productVO");
			 
			 model.addAttribute("actionval","/vc/customer/editcustomer");
				
			 model.addAttribute("actionvalnew","/vc/customer/addnew");
			 
			 Menumodel menuitem=new Menumodel();
			 
			 model.addAttribute(menuitem.getConfigMenuItem(model));
			
		 				 
		return "customeradmin/customers";
		
		}

	


	@RequestMapping(value = "/vc/customer/addnew", method = RequestMethod.POST)
		public String Create_Customer( Model model,HttpSession session,
				@ModelAttribute("CustomerVO") CustomerVO customerVO,
				@ModelAttribute("UserVO") UserVO userVO,
				@ModelAttribute("productVO") ProductVO productVO,RedirectAttributes rmodel
				)
		{
		
		 if (session.getAttribute("customerid")==null){return "login";}   

		 String css = (String)model.asMap().get("css");
		 String msg = (String)model.asMap().get("msg");
		 
		 
		    if (!"".equals(msg))
		    {
		   	 model.addAttribute("css",css);
		   	 model.addAttribute("msg",msg);
		    }

		    
			 String actionuser=(String) session.getAttribute("username");
			 String customerid=(String) session.getAttribute("customerid");
			 
			 model.addAttribute("username",session.getAttribute("username").toString());
			 

			 try 
			 {
				String res=productinformationView.AddCustomer(productVO);
			
				rmodel.addFlashAttribute("msg",res);
				rmodel.addFlashAttribute("css","info");
				
			 } catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 			 
		return "redirect:/vc/customer/list";

		
		}


	@RequestMapping(value = "/vc/customer/editcustomer", method = RequestMethod.GET)
		public String EditCustomer_GET( Model model,HttpSession session,
				@ModelAttribute("CustomerVO") CustomerVO customerVO,
				@ModelAttribute("UserVO") UserVO userVO,
				@ModelAttribute("productVO") ProductVO productVO
				)
		{
		
		 if (session.getAttribute("customerid")==null){return "login";}   

		 String css = (String)model.asMap().get("css");
		 String msg = (String)model.asMap().get("msg");
		 
		 
		    if (!"".equals(msg))
		    {
		   	 model.addAttribute("css",css);
		   	 model.addAttribute("msg",msg);
		    }

		    
			 String actionuser=(String) session.getAttribute("username");
			 String customerid=(String) session.getAttribute("customerid");
			 
			 model.addAttribute("username",session.getAttribute("username").toString());

			 model.addAttribute("instatus", getCustomerStatusList());
		 
			 model.addAttribute("modelVOval","productVO");
			 
			 model.addAttribute("actionval","/vc/customer/editcustomer");
			 	
			 //model.addAttribute(getConfigMenuItem(model));
			 
			 Menumodel menuitem=new Menumodel();
			 
			 model.addAttribute(menuitem.getConfigMenuItem(model));
			
		 				 
		return "customeradmin/editcustomer";
		
		}

	

	@RequestMapping(value = "/vc/customer/editcustomer", method = RequestMethod.POST)
	public String EditCustomer_POST( Model model,HttpSession session,
			@ModelAttribute("CustomerVO") CustomerVO customerVO,
			@ModelAttribute("UserVO") UserVO userVO,
			@ModelAttribute("productVO") ProductVO productVO,RedirectAttributes rmodel
			)
	{
	
	 if (session.getAttribute("customerid")==null){return "login";}   

	 String css = (String)model.asMap().get("css");
	 String msg = (String)model.asMap().get("msg");
	 
	 
	    if (!"".equals(msg))
	    {
	   	 model.addAttribute("css",css);
	   	 model.addAttribute("msg",msg);
	    }

	    
		 String actionuser=(String) session.getAttribute("username");
		 String customerid=(String) session.getAttribute("customerid");
		 
		 model.addAttribute("username",session.getAttribute("username").toString());

		 model.addAttribute("instatus", getCustomerStatusList());
	 
		 model.addAttribute("modelVOval","productVO");
		 
		 model.addAttribute("actionval","/vc/customer/editcustomer");
		 	
		 //model.addAttribute(getConfigMenuItem(model));
		 
		 Menumodel menuitem=new Menumodel();
		 
		 model.addAttribute(menuitem.getConfigMenuItem(model));
		
	

		 try 
		 {
			String res=productinformationView.UpdateCustomer(productVO);
		
			rmodel.addFlashAttribute("msg",res);
			rmodel.addFlashAttribute("css","info");
			
		 } catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
	 				 
	return "redirect:/vc/customer/list";

		 
//	return "customeradmin/editcustomer";
	
	}

	//user managemenegt
	
	

	@RequestMapping(value = "/vc/userlist", method = RequestMethod.GET)
	public String Get_UsersList( Model model,HttpSession session,
			@ModelAttribute("CustomerVO") CustomerVO customerVO,
			@ModelAttribute("UserVO") UserVO userVO
			)
 
	{
	
	
	 if (session.getAttribute("customerid")==null){return "login";}   

	 String css = (String)model.asMap().get("css");
	 String msg = (String)model.asMap().get("msg");
	    
	    if (!"".equals(msg))
	    {
	   	 model.addAttribute("css",css);
	   	 model.addAttribute("msg",msg);
	    }
	
	 
		
		 model.addAttribute("username",session.getAttribute("username").toString());
		 
		 model.addAttribute("id",customerVO.getCustomeridentificationo());
		 
		 model.addAttribute("modelVOval","UserVO");
		 
		 model.addAttribute("actionval","/vc/updateuser");

		 model.addAttribute("statuslist",getStatusList());
		 	
		 model.addAttribute(mobject.getConfigMenuItem(model));
		 
		 String actionuser=(String) session.getAttribute("username");
		 String customerid=(String) session.getAttribute("customerid");
		 
		// model.addAttribute("customerids",getCustomerIdMap(actionuser));
		 
		 try {
			 
			model.addAttribute("userslist",systemSettings.GetAllCustomerUsers(customerid));
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		 model.addAttribute("actionvalpassreset","/vc/user/resetpassword");
		 

	 
	return "customeradmin/user/c_userslist";
	}


	
	

	@RequestMapping(value = "/vc/createuser", method = RequestMethod.GET)
	public String Create_User( Model model,HttpSession session,
			@ModelAttribute("CustomerVO") CustomerVO customerVO,
			@ModelAttribute("UserVO") UserVO userVO
			)
 
	{
	
	
	 if (session.getAttribute("customerid")==null){return "login";}   

	 String css = (String)model.asMap().get("css");
	 String msg = (String)model.asMap().get("msg");
	    
	    if (!"".equals(msg))
	    {
	   	 model.addAttribute("css",css);
	   	 model.addAttribute("msg",msg);
	    }
	
	 model.addAttribute("username",session.getAttribute("username").toString());
	 
	 model.addAttribute("id",customerVO.getCustomeridentificationo());
	 
	 model.addAttribute("modelVOval","UserVO");
	 
	 model.addAttribute("actionval","/vc/createuser");

	 model.addAttribute("statuslist",getStatusList());
	 
	 model.addAttribute("rolelist",getUserRoles());
	 	
	 model.addAttribute(mobject.getConfigMenuItem(model));
	 
	 String actionuser=(String) session.getAttribute("username");
	 
	 //model.addAttribute("customerids",getCustomerIdMap(actionuser));
	 
	return "customeradmin/user/c_createuser";
	}


	@RequestMapping(value = "/vc/createuser", method = RequestMethod.POST)
	public String Create_Customer_Admin_POST( Model model,HttpSession session,
			@ModelAttribute("CustomerVO") CustomerVO customerVO,
			@ModelAttribute("UserVO") UserVO userVO,RedirectAttributes rmodel
			)
 
	{
	
	
	 if (session.getAttribute("customerid")==null){return "login";}   

	 String css = (String)model.asMap().get("css");
	 String msg = (String)model.asMap().get("msg");
	    
	    if (!"".equals(msg))
	    {
	   	 model.addAttribute("css",css);
	   	 model.addAttribute("msg",msg);
	    }
	
	 model.addAttribute("username",session.getAttribute("username").toString());
	 
	 model.addAttribute("id",customerVO.getCustomeridentificationo());
	 
	 model.addAttribute("modelVOval","UserVO");
	 
	 model.addAttribute("actionval","/vc/updateuser");

	 model.addAttribute("statuslist",getStatusList());
	 model.addAttribute("rolelist",getUserRoles());
	 model.addAttribute(mobject.getConfigMenuItem(model));
	 

	 String actionuser=(String) session.getAttribute("username");
	 
	 String customerid=(String) session.getAttribute("customerid");
	 try
	 {
		String result = systemSettings.createResturantUser(userVO, actionuser,customerid);
	
		if (result.equals("Success"))
		{
		rmodel.addFlashAttribute("css","success");
		rmodel.addFlashAttribute("msg","User  create succesfully");
		
		return "redirect:/vc/userlist";
		}else
		{
			model.addAttribute("css","danger");
			model.addAttribute("msg",result);
			
		}
		
	} catch (NoSuchAlgorithmException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();

		model.addAttribute("css","danger");
		model.addAttribute("msg",e.getMessage());
	
	} catch (InvalidKeySpecException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		
		model.addAttribute("css","danger");
		model.addAttribute("msg",e.getMessage());
		
	} catch (ServiceException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		model.addAttribute("css","danger");
		model.addAttribute("msg",e.getMessage());
	}
	 
	 
	// model.addAttribute("customerids",getCustomerIdMap(actionuser));
	 
	 
	 return "customeradmin/user/c_createuser";
	 
	
	
	}

	

	@RequestMapping(value = "/vc/updateuser", method = RequestMethod.GET)
		public String Update_Customer_Admin_GET( Model model,HttpSession session,
				@ModelAttribute("CustomerVO") CustomerVO customerVO,
				@ModelAttribute("UserVO") UserVO userVO
				)
	 
	{
		
		
		 if (session.getAttribute("customerid")==null){return "login";}   

		 String css = (String)model.asMap().get("css");
		 String msg = (String)model.asMap().get("msg");
		    
		    if (!"".equals(msg))
		    {
		   	 model.addAttribute("css",css);
		   	 model.addAttribute("msg",msg);
		    }
		
		 model.addAttribute("username",session.getAttribute("username").toString());
		 
		 model.addAttribute("id",customerVO.getCustomeridentificationo());
		 
		 model.addAttribute("modelVOval","UserVO");
		 
		 model.addAttribute("actionval","/vc/updateuser");

		 model.addAttribute("statuslist",getCustomerStatusList());
		 	
		// model.addAttribute(getConfigMenuItem(model));
		 
		 String actionuser=(String) session.getAttribute("username");
		 
		 //model.addAttribute("customerids",getCustomerIdMap(actionuser));
		 
		return "customeradmin/user/g_updateadmin";
		}


	

	@RequestMapping(value = "/vc/updateuser", method = RequestMethod.POST)
	public String Update_Customer_Admin_POST( Model model,HttpSession session,
			@ModelAttribute("CustomerVO") CustomerVO customerVO,
			@ModelAttribute("UserVO") UserVO userVO,RedirectAttributes rmodel
			)
	{
	
	
	 if (session.getAttribute("customerid")==null){return "login";}   

	 String css = (String)model.asMap().get("css");
	 String msg = (String)model.asMap().get("msg");
	    
	    if (!"".equals(msg))
	    {
	   	 model.addAttribute("css",css);
	   	 model.addAttribute("msg",msg);
	    }
	
	 model.addAttribute("username",session.getAttribute("username").toString());
	 
	 model.addAttribute("id",customerVO.getCustomeridentificationo());
	 
	 model.addAttribute("modelVOval","UserVO");
	 
	 model.addAttribute("actionval","/vc/updateuser");

	 model.addAttribute("statuslist",getCustomerStatusList());
	 	
	// model.addAttribute(getConfigMenuItem(model));
	 

	 String actionuser=(String) session.getAttribute("username");
	 
	 try
	 {
		String result = systemSettings.UpdateCustomerAdminUser(userVO, actionuser);
	
		if (result.equals("Success"))
		{
		rmodel.addFlashAttribute("css","success");
		rmodel.addFlashAttribute("msg","Customer admin update succesfully");
		
		return "redirect:/vc/userlist";
		}else
		{
			model.addAttribute("css","danger");
			model.addAttribute("msg",result);
			
		}
		
	} catch (NoSuchAlgorithmException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();

		model.addAttribute("css","danger");
		model.addAttribute("msg",e.getMessage());
	
	} catch (InvalidKeySpecException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		
		model.addAttribute("css","danger");
		model.addAttribute("msg",e.getMessage());
		
	} catch (ServiceException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		model.addAttribute("css","danger");
		model.addAttribute("msg",e.getMessage());
	}
	 
	 
	 //model.addAttribute("customerids",getCustomerIdMap(actionuser));
	 
	 
	return "customeradmin/user/g_updateadmin";
	}


	

	@RequestMapping(value = "/vc/user/resetpassword", method = RequestMethod.POST)
	public String Resetpassword_Customer_Admin_POST( Model model,HttpSession session,
			@ModelAttribute("CustomerVO") CustomerVO customerVO,
			@ModelAttribute("UserVO") UserVO userVO,RedirectAttributes rmodel)

	{
	
	
	 if (session.getAttribute("customerid")==null){return "login";}   

	 String css = (String)model.asMap().get("css");
	 String msg = (String)model.asMap().get("msg");
	    
	    if (!"".equals(msg))
	    {
	   	 model.addAttribute("css",css);
	   	 model.addAttribute("msg",msg);
	    }
	
	 model.addAttribute("username",session.getAttribute("username").toString());
	 
	 model.addAttribute("id",customerVO.getCustomeridentificationo());
	 
	 model.addAttribute("modelVOval","UserVO");
	 
	 model.addAttribute("actionval","/vc/user/resetpassword");

	 model.addAttribute("statuslist",getStatusList());
	 

	 String actionuser=(String) session.getAttribute("username");
	 
	 try
	 {
		String result = systemSettings.UpdateUserPassword(userVO, actionuser);
	
		if (result.equals("Success"))
		{
			rmodel.addFlashAttribute("css","success");
			rmodel.addFlashAttribute("msg","Password update succesfully");
			return "redirect:/vc/userlist";

		}else
		{
			rmodel.addFlashAttribute("css","danger");
			rmodel.addFlashAttribute("msg","Failed to reset password");
		
			return "redirect:/vc/userlist";
		}
		
	} catch (NoSuchAlgorithmException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();

		rmodel.addFlashAttribute("css","danger");
		rmodel.addFlashAttribute("msg",e.getMessage());
		return "redirect:/vc/userlist";

	
	
	} catch (InvalidKeySpecException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		
		rmodel.addFlashAttribute("css","danger");
		rmodel.addFlashAttribute("msg",e.getMessage());
	
		return "redirect:/vc/userlist";
		
	} catch (ServiceException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		rmodel.addFlashAttribute("css","danger");
		rmodel.addFlashAttribute("msg",e.getMessage());
	
		return "redirect:/vc/userlist";
	}
	
	
}


	@RequestMapping(value = "/vc/zipfile", method = RequestMethod.GET)
	public String CreateZipFile( Model model,HttpSession session,
			@ModelAttribute("CustomerVO") CustomerVO customerVO,
			@ModelAttribute("UserVO") UserVO userVO,
			@ModelAttribute("productVO") ProductVO productVO
			)
			{
	
	
		String foldermappingpath="";
		String workstationname="HBIW3_1";
		String companyname="vectra";
		String sftproot="C:/SFTP_Root/";
		
		 foldermappingpath=sftproot+companyname+"/"+"Merged_Data";
		
		
		
	   ProcessBuilder pb = new ProcessBuilder(System.getenv("ProgramFiles") + "/7-Zip/7z.exe",
		            "a",
		            "-tzip",
		            foldermappingpath+"/"+workstationname+".7z",
		            foldermappingpath+"/"+workstationname+"/*"
		    );
		    pb.redirectError();
		   

				
		    	
		    	////System.out.println("Process output of archive --->"+pb);
		    	
		        Process p;
				try {
					p = pb.start();
					new Thread(new InputConsumer(p.getInputStream())).start();
			        ////System.out.println("Exited with: " + p.waitFor());
			        
			        int code = 4;
					try {
						code = p.waitFor();
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			         
			    
		        //System.out.println("Successfully zip with code: " + p.waitFor());
		      
		        if (code==0)
		        {
		        	FileUtils.deleteDirectory(new File(foldermappingpath+"/"+workstationname));
		        	
		        	
			      /*  //String filenameval = FilenameUtils.getBaseName(filename);

		        	 // thread to sleep for 1000 milliseconds
		            Thread.sleep(30 *   // minutes to sleep
		                    60 *   // seconds to a minute
		                    1000);
		            */
				

	        		File zipfileloca = new File(sftproot+"/"+companyname+"/"+"Merged_Data"+"/"+workstationname+"/"+workstationname+".7z");
					
	        		File moveziplocation = new File(sftproot+"/"+companyname+"/"+"Merged_Data"+"/"+workstationname+".7z");
				
		        	
		        	try
		        	{
		        		
		        		FileUtils.copyFile(zipfileloca, moveziplocation);
		        		
		        		System.out.println("File move success:"+zipfileloca.getPath().toString());
		        		
		        		
		        	}catch(Exception e)
		        	{
		        		
		        		System.out.println("Failed to remove:->" +e.getMessage()+zipfileloca.getPath().toString()+"dest:--->"+moveziplocation.getPath().toString());
		        	}
		        }
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				System.out.println("Done...");
		    
		       return "customeradmin/dashboard"; 
	
		        }	
	
	
/*

	
	
	
	@RequestMapping(value = "/restaurants/myrestaurants/list", method = RequestMethod.GET)
		public String ManageResturarnt_GET( Model model,HttpSession session,
				@ModelAttribute("CustomerVO") CustomerVO customerVO,
				@ModelAttribute("UserVO") UserVO userVO,
				@ModelAttribute("OrganizationVO") OrganizationVO OrganizationVO
				)
		
	{
		
		 if (session.getAttribute("customerid")==null){return "login";}   

		 String css = (String)model.asMap().get("css");
		 String msg = (String)model.asMap().get("msg");
		    
		    if (!"".equals(msg))
		    {
		   	 model.addAttribute("css",css);
		   	 model.addAttribute("msg",msg);
		    }

			 String actionuser=(String) session.getAttribute("username");
			 String customerid=(String) session.getAttribute("customerid");
			    
		 try
		 {
			model.addAttribute("organizationlist", customerinformtion.GetCustomerOrganizationList(customerid,actionuser));
		
		 } catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		 
		 model.addAttribute("username",session.getAttribute("username").toString());
		 model.addAttribute("id",customerVO.getCustomeridentificationo());
		 model.addAttribute("modelVOval","OrganizationVO");
		 model.addAttribute("actionval","/global/manage/updateorganization");

		 model.addAttribute("statuslist",mobject.getStatusList());
		 	
		 //model.addAttribute(getConfigMenuItem(model));
		 
		 Menumodel menuitem=new Menumodel();
		 model.addAttribute(menuitem.getConfigMenuItem(model));
		 
		 
		return "customeradmin/organization/customer_organizationlist";
		}


//Manage table
	
	@RequestMapping(value = "/restaurants/myrestaurants/createtable", method = RequestMethod.GET)
		public String CreateTable_GET( Model model,HttpSession session,
				@ModelAttribute("CustomerVO") CustomerVO customerVO,
				@ModelAttribute("UserVO") UserVO userVO,
				@ModelAttribute("OrganizationVO") OrganizationVO OrganizationVO,
				@ModelAttribute("TableVO") TableVO tableVO
				)
		
	{
		
		 if (session.getAttribute("customerid")==null){return "login";}   

		 String css = (String)model.asMap().get("css");
		 String msg = (String)model.asMap().get("msg");
		    
		    if (!"".equals(msg))
		    {
		   	 model.addAttribute("css",css);
		   	 model.addAttribute("msg",msg);
		    }

			 String actionuser=(String) session.getAttribute("username");
			 String customerid=(String) session.getAttribute("customerid");
			    
		 model.addAttribute("organizationlist",getMyResturantMap(customerid, actionuser) );
	
		 
		 model.addAttribute("username",session.getAttribute("username").toString());
		 model.addAttribute("id",customerVO.getCustomeridentificationo());
		 model.addAttribute("modelVOval","TableVO");
		 model.addAttribute("actionval","/restaurants/myrestaurants/createtable");

		 model.addAttribute("statuslist",mobject.getStatusList());
		 	
		 //model.addAttribute(getConfigMenuItem(model));
		 
		 Menumodel menuitem=new Menumodel();
		 model.addAttribute(menuitem.getConfigMenuItem(model));
		 
		 
		return "customeradmin/organization/customer_createtable";
		}



	

	//Manage table
		@RequestMapping(value = "/restaurants/myrestaurants/createtable", method = RequestMethod.POST)
			public String CreateTable_POST( Model model,HttpSession session,
					@ModelAttribute("CustomerVO") CustomerVO customerVO,
					@ModelAttribute("UserVO") UserVO userVO,
					@ModelAttribute("OrganizationVO") OrganizationVO OrganizationVO,
					@ModelAttribute("TableVO") TableVO tableVO,
					RedirectAttributes rmodel
					)
			
		{
			
			 if (session.getAttribute("customerid")==null){return "login";}   

			 String css = (String)model.asMap().get("css");
			 String msg = (String)model.asMap().get("msg");
			    
			    if (!"".equals(msg))
			    {
			   	 model.addAttribute("css",css);
			   	 model.addAttribute("msg",msg);
			    }

				 String actionuser=(String) session.getAttribute("username");
				 String customerid=(String) session.getAttribute("customerid");
				 
				 
				 	try
				 	
				 	
				 	{
						 String result=customerinformtion.createTable(tableVO, customerid, actionuser);
					
						 if (result.equals("Success"))
						 {
							 rmodel.addFlashAttribute("css","success");
							 rmodel.addFlashAttribute("msg","Successfully Created");
							 
							 return "redirect:/restaurants/myrestaurants/createtable";
					
						 }else
						 {
							 model.addAttribute("css","danger");
							 model.addAttribute("msg",result);
							 
						 }
						 
				 	} catch (ServiceException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();

						 model.addAttribute("css","danger");
						 model.addAttribute("msg",e.getMessage());
						
					}

				 
			 model.addAttribute("organizationlist",getMyResturantMap(customerid, actionuser) );
		
			 
			 model.addAttribute("username",session.getAttribute("username").toString());
			 model.addAttribute("id",customerVO.getCustomeridentificationo());
			 model.addAttribute("modelVOval","TableVO");
			 model.addAttribute("actionval","/restaurants/myrestaurants/createtable");

			 model.addAttribute("statuslist",mobject.getStatusList());
			 	
			 //model.addAttribute(getConfigMenuItem(model));
			 
			 Menumodel menuitem=new Menumodel();
			 model.addAttribute(menuitem.getConfigMenuItem(model));
			 
			 
			return "customeradmin/organization/customer_createtable";
			}

		

		
		
		
		
		@RequestMapping(value = "/restaurants/myrestaurants/edittable", method = RequestMethod.GET)
		public String EditTable_GET( Model model,HttpSession session,
				@ModelAttribute("CustomerVO") CustomerVO customerVO,
				@ModelAttribute("UserVO") UserVO userVO,
				@ModelAttribute("OrganizationVO") OrganizationVO OrganizationVO,
				@ModelAttribute("TableVO") TableVO tableVO
				)
		
	{
		
		 if (session.getAttribute("customerid")==null){return "login";}   

		 String css = (String)model.asMap().get("css");
		 String msg = (String)model.asMap().get("msg");
		    
		    if (!"".equals(msg))
		    {
		   	 model.addAttribute("css",css);
		   	 model.addAttribute("msg",msg);
		    }

			 String actionuser=(String) session.getAttribute("username");
			 String customerid=(String) session.getAttribute("customerid");
			    
		 model.addAttribute("organizationlist",getMyResturantMap(customerid, actionuser) );
	
		 
		 model.addAttribute("username",session.getAttribute("username").toString());
		 model.addAttribute("id",customerVO.getCustomeridentificationo());
		 model.addAttribute("modelVOval","TableVO");
		 model.addAttribute("actionval","/restaurants/myrestaurants/edittable");

		 model.addAttribute("statuslist",mobject.getStatusList());
		 	
		 //model.addAttribute(getConfigMenuItem(model));
		 
		 Menumodel menuitem=new Menumodel();
		 model.addAttribute(menuitem.getConfigMenuItem(model));
		 
		 
		return "customeradmin/organization/customer_edittable";
		}
		
		@RequestMapping(value = "/restaurants/myrestaurants/edittable", method = RequestMethod.POST)
		public String EditTable_POST( Model model,HttpSession session,
				@ModelAttribute("CustomerVO") CustomerVO customerVO,
				@ModelAttribute("UserVO") UserVO userVO,
				@ModelAttribute("OrganizationVO") OrganizationVO OrganizationVO,
				@ModelAttribute("TableVO") TableVO tableVO, RedirectAttributes rmodel
				)
		
	{
		
		 if (session.getAttribute("customerid")==null){return "login";}   

		 String css = (String)model.asMap().get("css");
		 String msg = (String)model.asMap().get("msg");
		    
		    if (!"".equals(msg))
		    {
		   	 model.addAttribute("css",css);
		   	 model.addAttribute("msg",msg);
		    }

			 String actionuser=(String) session.getAttribute("username");
			 String customerid=(String) session.getAttribute("customerid");
			    
			 model.addAttribute("organizationlist",getMyResturantMap(customerid, actionuser) );
				
		 
		 model.addAttribute("username",session.getAttribute("username").toString());
		 model.addAttribute("id",customerVO.getCustomeridentificationo());
		 model.addAttribute("modelVOval","TableVO");
		 model.addAttribute("actionval","/restaurants/myrestaurants/edittable");

		 model.addAttribute("statuslist",mobject.getStatusList());
		 	
		 //model.addAttribute(getConfigMenuItem(model));
		 
		 Menumodel menuitem=new Menumodel();
		 model.addAttribute(menuitem.getConfigMenuItem(model));
		 
		 

		 	try
		 	{
				 String result=customerinformtion.UpdateTable(tableVO, customerid, actionuser);
			
				 if (result.equals("Success"))
				 {
					 rmodel.addFlashAttribute("css","success");
					 rmodel.addFlashAttribute("msg","Updated Successfully");
					 
					 return "redirect:/restaurants/myrestaurants/tablellist";
			
				 }else
				 {
					 model.addAttribute("css","danger");
					 model.addAttribute("msg",result);
					 
				 }
				 
		 	} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

				 model.addAttribute("css","danger");
				 model.addAttribute("msg",e.getMessage());
				
			}

		 
		return "customeradmin/organization/customer_edittable";
		}



		
		
		
		

		//Manage table
		
			@RequestMapping(value = "/restaurants/myrestaurants/tablellist", method = RequestMethod.GET)
				public String GetTableList_GET( Model model,HttpSession session,
						@ModelAttribute("CustomerVO") CustomerVO customerVO,
						@ModelAttribute("UserVO") UserVO userVO,
						@ModelAttribute("OrganizationVO") OrganizationVO OrganizationVO,
						@ModelAttribute("TableVO") TableVO tableVO
						)
				
			{
				
				 if (session.getAttribute("customerid")==null){return "login";}   

				 String css = (String)model.asMap().get("css");
				 String msg = (String)model.asMap().get("msg");
				 
				 model.addAttribute("addactionval","/restaurants/myrestaurants/managedevice");
				 model.addAttribute("removeactionval","/restaurants/myrestaurants/managedevice");
				 model.addAttribute("editactionval","/restaurants/myrestaurants/edittable");
				 
				 
				    if (!"".equals(msg))
				    {
				   	 model.addAttribute("css",css);
				   	 model.addAttribute("msg",msg);
				    }

					 String actionuser=(String) session.getAttribute("username");
					 String customerid=(String) session.getAttribute("customerid");
					model.addAttribute("mydevicelist", getMyDeviceMap(customerid, actionuser))  ;
					 
				 try {
					model.addAttribute("tablevaluelist",customerinformtion.GetCustomerTableList(customerid, "null", actionuser));
				} catch (ServiceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
				 
				 model.addAttribute("username",session.getAttribute("username").toString());
				 model.addAttribute("id",customerVO.getCustomeridentificationo());
				 model.addAttribute("modelVOval","TableVO");
				 model.addAttribute("actionval","/restaurants/myrestaurants/createtable");

				 model.addAttribute("statuslist",mobject.getStatusList());
				 	
				 //model.addAttribute(getConfigMenuItem(model));
				 
				 Menumodel menuitem=new Menumodel();
				 model.addAttribute(menuitem.getConfigMenuItem(model));
				 
				 
				return "customeradmin/organization/customer_tablelist";
				}


		



			@RequestMapping(value = "/restaurants/myrestaurants/devices", method = RequestMethod.GET)
				public String CreateDevices_GET( Model model,HttpSession session,
						@ModelAttribute("CustomerVO") CustomerVO customerVO,
						@ModelAttribute("UserVO") UserVO userVO,
						@ModelAttribute("OrganizationVO") OrganizationVO OrganizationVO,
						@ModelAttribute("TableVO") TableVO tableVO
						)
					
			{
				
				 if (session.getAttribute("customerid")==null){return "login";}   

				 String css = (String)model.asMap().get("css");
				 String msg = (String)model.asMap().get("msg");
				    
				    if (!"".equals(msg))
				    {
				   	 model.addAttribute("css",css);
				   	 model.addAttribute("msg",msg);
				    }

					 String actionuser=(String) session.getAttribute("username");
					 String customerid=(String) session.getAttribute("customerid");
					    
				 model.addAttribute("organizationlist",getMyResturantMap(customerid, actionuser) );
			
				 
				 model.addAttribute("username",session.getAttribute("username").toString());
				 model.addAttribute("id",customerVO.getCustomeridentificationo());
				 model.addAttribute("modelVOval","TableVO");
				 model.addAttribute("actionval","/restaurants/myrestaurants/devices");

				 model.addAttribute("statuslist",mobject.getStatusList());
				 //model.addAttribute(getConfigMenuItem(model));
				 
				 Menumodel menuitem=new Menumodel();
				 
				 model.addAttribute(menuitem.getConfigMenuItem(model));
				 				 
				return "customeradmin/organization/customer_createdevices";
				}

			


			@RequestMapping(value = "/restaurants/myrestaurants/devices", method = RequestMethod.POST)
				public String CreateDevice_POST( Model model,HttpSession session,
						@ModelAttribute("CustomerVO") CustomerVO customerVO,
						@ModelAttribute("UserVO") UserVO userVO,
						@ModelAttribute("OrganizationVO") OrganizationVO OrganizationVO,
						@ModelAttribute("TableVO") TableVO tableVO,
						RedirectAttributes rmodel
						)
				
			{
				
				 if (session.getAttribute("customerid")==null){return "login";}   

				 String css = (String)model.asMap().get("css");
				 String msg = (String)model.asMap().get("msg");
				    
				    if (!"".equals(msg))
				    {
				   	 model.addAttribute("css",css);
				   	 model.addAttribute("msg",msg);
				    }

					 String actionuser=(String) session.getAttribute("username");
					 String customerid=(String) session.getAttribute("customerid");
					 
					 
					 
					 	try
					 	{
							 String result=customerinformtion.createDevices(tableVO, customerid, actionuser);
						
							 if (result.equals("Success"))
							 {
								 rmodel.addFlashAttribute("css","success");
								 rmodel.addFlashAttribute("msg","Successfully Created");
								 
								 return "redirect:/restaurants/myrestaurants/devices";
						
							 }else
							 {
								 model.addAttribute("css","danger");
								 model.addAttribute("msg",result);
								 
							 }
							 
					 	} catch (ServiceException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();

							 model.addAttribute("css","danger");
							 model.addAttribute("msg",e.getMessage());
							
						}

					 
				 model.addAttribute("organizationlist",getMyResturantMap(customerid, actionuser) );
			
				 
				 model.addAttribute("username",session.getAttribute("username").toString());
				 model.addAttribute("id",customerVO.getCustomeridentificationo());
				 model.addAttribute("modelVOval","TableVO");
				 model.addAttribute("actionval","/restaurants/myrestaurants/devices");
				 model.addAttribute("statuslist",mobject.getStatusList());
				 	
				 //model.addAttribute(getConfigMenuItem(model));
				 
				 Menumodel menuitem=new Menumodel();
				 model.addAttribute(menuitem.getConfigMenuItem(model));
				 
				 
				return "customeradmin/organization/customer_createdevices";
				}
			
			
			
			
			

			@RequestMapping(value = "/restaurants/myrestaurants/mydeviceslist", method = RequestMethod.GET)
				public String Get_DeviceList( Model model,HttpSession session,
						@ModelAttribute("CustomerVO") CustomerVO customerVO,
						@ModelAttribute("UserVO") UserVO userVO,
						@ModelAttribute("OrganizationVO") OrganizationVO OrganizationVO,
						@ModelAttribute("TableVO") TableVO tableVO
						)
				
			{
				
				 if (session.getAttribute("customerid")==null){return "login";}   

				 String css = (String)model.asMap().get("css");
				 String msg = (String)model.asMap().get("msg");
				    
				    if (!"".equals(msg))
				    {
				   	 model.addAttribute("css",css);
				   	 model.addAttribute("msg",msg);
				    }

					 String actionuser=(String) session.getAttribute("username");
					 String customerid=(String) session.getAttribute("customerid");
					    
				 try {
					model.addAttribute("listofmydevices",customerinformtion.GetCustomerDeviceList(customerid, "null", actionuser));
				} catch (ServiceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
				 
				 model.addAttribute("username",session.getAttribute("username").toString());
				 model.addAttribute("id",customerVO.getCustomeridentificationo());
				 model.addAttribute("modelVOval","TableVO");
				 model.addAttribute("actionval","/restaurants/myrestaurants/devices");

				 model.addAttribute("statuslist",mobject.getStatusList());
				 	
				 //model.addAttribute(getConfigMenuItem(model));
				 
				 Menumodel menuitem=new Menumodel();
				 model.addAttribute(menuitem.getConfigMenuItem(model));
				 
				return "customeradmin/organization/customer_deviceslist";
				
			
			}


			
			@RequestMapping(value = "/restaurants/myrestaurants/managedevice	", method = RequestMethod.POST)
			public String ManageDeviceOperation_POST( Model model,HttpSession session,
					@ModelAttribute("CustomerVO") CustomerVO customerVO,
					@ModelAttribute("UserVO") UserVO userVO,
					@ModelAttribute("OrganizationVO") OrganizationVO OrganizationVO,
					@ModelAttribute("TableVO") TableVO tableVO,
					RedirectAttributes rmodel
					)
			
		{
			
			 if (session.getAttribute("customerid")==null){return "login";}   

			 String css = (String)model.asMap().get("css");
			 String msg = (String)model.asMap().get("msg");
			    
			    if (!"".equals(msg))
			    {
			   	 model.addAttribute("css",css);
			   	 model.addAttribute("msg",msg);
			    }

				 String actionuser=(String) session.getAttribute("username");
				 String customerid=(String) session.getAttribute("customerid");
				 
				 
				 
				 	try
				 	{
						 String result=customerinformtion.ManageDeviceOperationForTable(tableVO, customerid, actionuser);
					
						 if (result.equals("Success"))
						 {
							 rmodel.addFlashAttribute("css","success");
							 rmodel.addFlashAttribute("msg",tableVO.getOpsflg()+" Successfully");
							 
							 return "redirect:/restaurants/myrestaurants/tablellist";
								
						 }else
						 {
							 rmodel.addAttribute("css","danger");
							 rmodel.addAttribute("msg",tableVO.getOpsflg()+" Failed :"+result);
						 return "redirect:/restaurants/myrestaurants/tablellist";
								
							 
						 }
						 
				 	} catch (ServiceException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();

						 model.addAttribute("css","danger");
						 model.addAttribute("msg",e.getMessage());
						
					}

				 
			 model.addAttribute("organizationlist",getMyResturantMap(customerid, actionuser) );
		
			 
			 model.addAttribute("username",session.getAttribute("username").toString());
			 model.addAttribute("id",customerVO.getCustomeridentificationo());
			 model.addAttribute("modelVOval","TableVO");
			 model.addAttribute("actionval","/restaurants/myrestaurants/devices");
			 model.addAttribute("statuslist",mobject.getStatusList());
			 	
			 //model.addAttribute(getConfigMenuItem(model));
			 
			 Menumodel menuitem=new Menumodel();
			 model.addAttribute(menuitem.getConfigMenuItem(model));
			 
			 
			return "customeradmin/organization/customer_createdevices";
			}
		
		
	
			
			
			//Manage table
			
			@RequestMapping(value = "/restaurants/kitchen/orderdetailslist", method = RequestMethod.GET)
				public String GetKitchenOrderDetails_GET( Model model,HttpSession session,
						@ModelAttribute("CustomerVO") CustomerVO customerVO,
						@ModelAttribute("UserVO") UserVO userVO,
						@ModelAttribute("OrganizationVO") OrganizationVO OrganizationVO,
						@ModelAttribute("TableVO") TableVO tableVO,
						@ModelAttribute("productVO") ProductVO productVO,
						@RequestParam("flg") String flg
						)
				{
				
				 if (session.getAttribute("customerid")==null){return "login";}   

				 String css = (String)model.asMap().get("css");
				 String msg = (String)model.asMap().get("msg");
				 
				 System.out.println("Order Flg====>"+flg);
				 
				 
				 model.addAttribute("addactionval","/restaurants/myrestaurants/managedevice");
				 model.addAttribute("removeactionval","/restaurants/myrestaurants/managedevice");
				 model.addAttribute("editactionval","/restaurants/myrestaurants/edittable");
				 
				 
				    if (!"".equals(msg))
				    {
				   	 model.addAttribute("css",css);
				   	 model.addAttribute("msg",msg);
				    }

					 String actionuser=(String) session.getAttribute("username");
					 String customerid=(String) session.getAttribute("customerid");
					model.addAttribute("mydevicelist", getMyDeviceMap(customerid, actionuser))  ;
				

					 model.addAttribute("username",session.getAttribute("username").toString());
					 model.addAttribute("id",customerVO.getCustomeridentificationo());
					 model.addAttribute("modelVOval","productVO");
					 model.addAttribute("actionval","/restaurants/order/forwardtokitchen");

					 model.addAttribute("statuslist",mobject.getStatusList());
					 	
					 //model.addAttribute(getConfigMenuItem(model));
					 
					 Menumodel menuitem=new Menumodel();
					 model.addAttribute(menuitem.getConfigMenuItem(model));
					

				 try
				 {
				
					 if ("queue".equals(flg))
					 {
					
						 String flgval="Pending";
						 
						 model.addAttribute("decisionflg","1");
						 model.addAttribute("orderlist",productinformationView.GetOrderDetailsByFlg(customerid, flgval));
						model.addAttribute("tabletitle","Pending Order");
						 return "customeradmin/kitchen/vieworder" ;
					 }
				
					 if ("ongoing".equals(flg))
					 {
					
						 String flgval="Preparation Started";
						 model.addAttribute("orderlist",productinformationView.GetOrderDetailsByFlg(customerid,flgval));
						 model.addAttribute("decisionflg","2");
						
						 model.addAttribute("tabletitle",flgval);
						 
						 return "customeradmin/kitchen/vieworder" ;
						 
					 }
				
					 
					 if ("completed".equals(flg))
					 {
					
						 model.addAttribute("decisionflg","3");
						 String flgval="Completed";
						 model.addAttribute("orderlist",productinformationView.GetOrderDetailsByFlg(customerid,flgval));
				
						 model.addAttribute("tabletitle",flgval);
						 return "customeradmin/kitchen/completeorder";
					 }
				
				 
				 } catch (ServiceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
				 				 
				return "customeradmin/order/vieworder";
				
				}


			
			
			

			//Manage table
			
				@RequestMapping(value = "/restaurants/orderdetailslist", method = RequestMethod.GET)
					public String GetOrderDetails_GET( Model model,HttpSession session,
							@ModelAttribute("CustomerVO") CustomerVO customerVO,
							@ModelAttribute("UserVO") UserVO userVO,
							@ModelAttribute("OrganizationVO") OrganizationVO OrganizationVO,
							@ModelAttribute("TableVO") TableVO tableVO,
							@ModelAttribute("productVO") ProductVO productVO
							)
					{
					
					 if (session.getAttribute("customerid")==null){return "login";}   

					 String css = (String)model.asMap().get("css");
					 String msg = (String)model.asMap().get("msg");
					 
					 model.addAttribute("addactionval","/restaurants/myrestaurants/managedevice");
					 model.addAttribute("removeactionval","/restaurants/myrestaurants/managedevice");
					 model.addAttribute("editactionval","/restaurants/myrestaurants/edittable");
					 
					 
					    if (!"".equals(msg))
					    {
					   	 model.addAttribute("css",css);
					   	 model.addAttribute("msg",msg);
					    }

						 String actionuser=(String) session.getAttribute("username");
						 String customerid=(String) session.getAttribute("customerid");
						model.addAttribute("mydevicelist", getMyDeviceMap(customerid, actionuser))  ;
						 
					 try {
						model.addAttribute("orderlist",productinformationView.GetOrderDetails(customerid));
					} catch (ServiceException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
					 
					 model.addAttribute("username",session.getAttribute("username").toString());
					 model.addAttribute("id",customerVO.getCustomeridentificationo());
					 model.addAttribute("modelVOval","productVO");
					 model.addAttribute("actionval","/restaurants/order/forwardtokitchen");

					 model.addAttribute("statuslist",mobject.getStatusList());
					 	
					 //model.addAttribute(getConfigMenuItem(model));
					 
					 Menumodel menuitem=new Menumodel();
					 model.addAttribute(menuitem.getConfigMenuItem(model));
					 
					 
					return "customeradmin/order/vieworder";
					}



				
				
				

				//Manage table
				
					@RequestMapping(value = "/restaurants/analytics", method = RequestMethod.GET)
						public String GetAnalytics_GET( Model model,HttpSession session,
								@ModelAttribute("CustomerVO") CustomerVO customerVO,
								@ModelAttribute("UserVO") UserVO userVO,
								@ModelAttribute("OrganizationVO") OrganizationVO OrganizationVO,
								@ModelAttribute("TableVO") TableVO tableVO,
								@ModelAttribute("productVO") ProductVO productVO
								)
						{
						
						 if (session.getAttribute("customerid")==null){return "login";}   

						 String css = (String)model.asMap().get("css");
						 String msg = (String)model.asMap().get("msg");
						 
						 model.addAttribute("addactionval","/restaurants/myrestaurants/managedevice");
						 model.addAttribute("removeactionval","/restaurants/myrestaurants/managedevice");
						 model.addAttribute("editactionval","/restaurants/myrestaurants/edittable");
						 
						 
						    if (!"".equals(msg))
						    {
						   	 model.addAttribute("css",css);
						   	 model.addAttribute("msg",msg);
						    }

							 String actionuser=(String) session.getAttribute("username");
							 String customerid=(String) session.getAttribute("customerid");
							model.addAttribute("mydevicelist", getMyDeviceMap(customerid, actionuser))  ;
							 
						 try 
						 
						 {
							model.addAttribute("orderlist",productinformationView.GetAnalytics(customerid));
						}
						 catch (ServiceException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					
						 ArrayList<ProductVO> assetconfigs;
						 
						try {
							assetconfigs = productinformationView.GetAnalytics(customerid);
							
							List<ProductVO> pending = assetconfigs.stream()                        // Convert to steam
						    		  .filter(x ->"Pending".equals(x.getStatus()))        // find only request method
						      		 .collect(Collectors.toList());
							
						model.addAttribute("pending",pending.size());
							
							List<ProductVO> completed = assetconfigs.stream()                        // Convert to steam
						    		  .filter(x ->"Completed".equals(x.getStatus()))        // find only request method
						      		 .collect(Collectors.toList());
							model.addAttribute("completed",completed.size());
							
							
							List<ProductVO> pstart = assetconfigs.stream()                        // Convert to steam
						    		  .filter(x ->"Preparation Started".equals(x.getStatus()))        // find only request method
						      		 .collect(Collectors.toList());
							
							model.addAttribute("pstart",pstart.size());
							
							
							model.addAttribute("total",assetconfigs.size());
							
						} catch (ServiceException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				
						  
						 
						 
						 model.addAttribute("username",session.getAttribute("username").toString());
						 model.addAttribute("id",customerVO.getCustomeridentificationo());
						 model.addAttribute("modelVOval","productVO");
						 model.addAttribute("actionval","/restaurants/order/forwardtokitchen");

						 model.addAttribute("statuslist",mobject.getStatusList());
						 	
						 //model.addAttribute(getConfigMenuItem(model));
						 
						 Menumodel menuitem=new Menumodel();
						 model.addAttribute(menuitem.getConfigMenuItem(model));
						 
						 
						return "customeradmin/analytics/vieworder";
						}



				

				//Manage table
				
					@RequestMapping(value = "/restaurants/order/forwardtokitchen", method = RequestMethod.POST)
						public String ManageOrderStatus_GET( Model model,HttpSession session,
								@ModelAttribute("CustomerVO") CustomerVO customerVO,
								@ModelAttribute("UserVO") UserVO userVO,
								@ModelAttribute("OrganizationVO") OrganizationVO OrganizationVO,
								@ModelAttribute("TableVO") TableVO tableVO,
								@ModelAttribute("productVO") ProductVO productVO
								)
						{
						
						 if (session.getAttribute("customerid")==null){return "login";}   

						 String css = (String)model.asMap().get("css");
						 String msg = (String)model.asMap().get("msg");
						 
						 model.addAttribute("addactionval","/restaurants/myrestaurants/managedevice");
						 model.addAttribute("removeactionval","/restaurants/myrestaurants/managedevice");
						 model.addAttribute("editactionval","/restaurants/myrestaurants/edittable");
						 
						 
						    if (!"".equals(msg))
						    {
						   	 model.addAttribute("css",css);
						   	 model.addAttribute("msg",msg);
						    }

							 String actionuser=(String) session.getAttribute("username");
							 String customerid=(String) session.getAttribute("customerid");
							model.addAttribute("mydevicelist", getMyDeviceMap(customerid, actionuser))  ;
							 

							 if ("Pending".equals(productVO.getStatus()))
							 {

								 try {
									productinformationView.ManageOrderStauts(productVO, "Preparation Started", customerid, actionuser);
								} catch (ServiceException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								 
								 
							 }else if ("Preparation Started".equals(productVO.getStatus()))
							 {

								 try {
									productinformationView.ManageOrderStauts(productVO, "Completed", customerid, actionuser);
								} catch (ServiceException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								 
							 }else if ("4".equals(productVO.getStatus()))
							 {
								 
							 }
							 
					
									 
						return "redirect:/restaurants/orderdetailslist";
						}


			
			
			
// User Management of resturnat
					
					
					
					@RequestMapping(value = "/restaurants/userlist", method = RequestMethod.GET)
					public String Get_UsersList( Model model,HttpSession session,
							@ModelAttribute("CustomerVO") CustomerVO customerVO,
							@ModelAttribute("UserVO") UserVO userVO
							)
				 
					{
					
					
					 if (session.getAttribute("customerid")==null){return "login";}   

					 String css = (String)model.asMap().get("css");
					 String msg = (String)model.asMap().get("msg");
					    
					    if (!"".equals(msg))
					    {
					   	 model.addAttribute("css",css);
					   	 model.addAttribute("msg",msg);
					    }
					
					 
						
						 model.addAttribute("username",session.getAttribute("username").toString());
						 
						 model.addAttribute("id",customerVO.getCustomeridentificationo());
						 
						 model.addAttribute("modelVOval","UserVO");
						 
						 model.addAttribute("actionval","/global/customer/updateadmin");

						 model.addAttribute("statuslist",getStatusList());
						 	
						 model.addAttribute(mobject.getConfigMenuItem(model));
						 
						 String actionuser=(String) session.getAttribute("username");
						 String customerid=(String) session.getAttribute("customerid");
						 
						// model.addAttribute("customerids",getCustomerIdMap(actionuser));
						 
						 try {
							 
							model.addAttribute("userslist",systemSettings.GetAllCustomerUsers(customerid));
						} catch (ServiceException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						 
						 model.addAttribute("actionvalpassreset","/global/customer/resetpassword");
						 

					 
					return "customeradmin/user/c_userslist";
					}



					
					

					@RequestMapping(value = "/restaurants/bookingdetails", method = RequestMethod.GET)
						public String BookingDetails_GET( Model model,HttpSession session,
								@ModelAttribute("CustomerVO") CustomerVO customerVO,
								@ModelAttribute("UserVO") UserVO userVO,
								@ModelAttribute("OrganizationVO") OrganizationVO OrganizationVO,
								@ModelAttribute("TableVO") TableVO tableVO,
								@ModelAttribute("productVO") ProductVO productVO
								)
						{
						
						 if (session.getAttribute("customerid")==null){return "login";}   

						 String css = (String)model.asMap().get("css");
						 String msg = (String)model.asMap().get("msg");
						 
						 model.addAttribute("addactionval","/restaurants/myrestaurants/managedevice");
						 model.addAttribute("removeactionval","/restaurants/myrestaurants/managedevice");
						 model.addAttribute("editactionval","/restaurants/myrestaurants/edittable");
						 
						 
						    if (!"".equals(msg))
						    {
						   	 model.addAttribute("css",css);
						   	 model.addAttribute("msg",msg);
						    }

							 String actionuser=(String) session.getAttribute("username");
							 String customerid=(String) session.getAttribute("customerid");
							model.addAttribute("mydevicelist", getMyDeviceMap(customerid, actionuser))  ;
							 
						 try {
							model.addAttribute("bookinglist",customerinformtion.Get_BookingDetails(customerid));
						} catch (ServiceException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					
						 
						 model.addAttribute("username",session.getAttribute("username").toString());
						 model.addAttribute("id",customerVO.getCustomeridentificationo());
						 model.addAttribute("modelVOval","OrganizationVO");
						 model.addAttribute("actionval","/restaurants/order/forwardtokitchen");

						 model.addAttribute("statuslist",mobject.getStatusList());
						 	
						 //model.addAttribute(getConfigMenuItem(model));
						 
						 Menumodel menuitem=new Menumodel();
						 model.addAttribute(menuitem.getConfigMenuItem(model));
						 model.addAttribute("tabletitle","List of Booking");
						 
						 
						return "customeradmin/booking/booking";
						}


					

					@RequestMapping(value = "/restaurants/createuser", method = RequestMethod.GET)
					public String Create_User( Model model,HttpSession session,
							@ModelAttribute("CustomerVO") CustomerVO customerVO,
							@ModelAttribute("UserVO") UserVO userVO
							)
				 
					{
					
					
					 if (session.getAttribute("customerid")==null){return "login";}   

					 String css = (String)model.asMap().get("css");
					 String msg = (String)model.asMap().get("msg");
					    
					    if (!"".equals(msg))
					    {
					   	 model.addAttribute("css",css);
					   	 model.addAttribute("msg",msg);
					    }
					
					 model.addAttribute("username",session.getAttribute("username").toString());
					 
					 model.addAttribute("id",customerVO.getCustomeridentificationo());
					 
					 model.addAttribute("modelVOval","UserVO");
					 
					 model.addAttribute("actionval","/restaurants/createuser");

					 model.addAttribute("statuslist",getStatusList());
					 
					 model.addAttribute("rolelist",getUserRoles());
					 	
					 model.addAttribute(mobject.getConfigMenuItem(model));
					 
					 String actionuser=(String) session.getAttribute("username");
					 
					 //model.addAttribute("customerids",getCustomerIdMap(actionuser));
					 
					return "customeradmin/user/c_createuser";
					}

					
					
			
					
					
			
					

					@RequestMapping(value = "/restaurants/createuser", method = RequestMethod.POST)
					public String Create_Customer_Admin_POST( Model model,HttpSession session,
							@ModelAttribute("CustomerVO") CustomerVO customerVO,
							@ModelAttribute("UserVO") UserVO userVO,RedirectAttributes rmodel
							)
				 
					{
					
					
					 if (session.getAttribute("customerid")==null){return "login";}   

					 String css = (String)model.asMap().get("css");
					 String msg = (String)model.asMap().get("msg");
					    
					    if (!"".equals(msg))
					    {
					   	 model.addAttribute("css",css);
					   	 model.addAttribute("msg",msg);
					    }
					
					 model.addAttribute("username",session.getAttribute("username").toString());
					 
					 model.addAttribute("id",customerVO.getCustomeridentificationo());
					 
					 model.addAttribute("modelVOval","UserVO");
					 
					 model.addAttribute("actionval","/restaurants/createuser");

					 model.addAttribute("statuslist",getStatusList());
					 model.addAttribute("rolelist",getUserRoles());
					 model.addAttribute(mobject.getConfigMenuItem(model));
					 

					 String actionuser=(String) session.getAttribute("username");
					 
					 String customerid=(String) session.getAttribute("customerid");
					 try
					 {
						String result = systemSettings.createResturantUser(userVO, actionuser,customerid);
					
						if (result.equals("Success"))
						{
						rmodel.addFlashAttribute("css","success");
						rmodel.addFlashAttribute("msg","User  create succesfully");
						
						return "redirect:/restaurants/createuser";
						}else
						{
							model.addAttribute("css","danger");
							model.addAttribute("msg",result);
							
						}
						
					} catch (NoSuchAlgorithmException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();

						model.addAttribute("css","danger");
						model.addAttribute("msg",e.getMessage());
					
					} catch (InvalidKeySpecException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						
						model.addAttribute("css","danger");
						model.addAttribute("msg",e.getMessage());
						
					} catch (ServiceException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						model.addAttribute("css","danger");
						model.addAttribute("msg",e.getMessage());
					}
					 
					 
					// model.addAttribute("customerids",getCustomerIdMap(actionuser));
					 
					 
					 return "customeradmin/user/c_createuser";
					 
					
					
					}


					
					
					*/
			

private  Map<String,String> getMyResturantMap(String customerid,String actionuser)
{
	
Map<String,String> organizationlist=new HashMap<>();

try {


ArrayList<OrganizationVO> reslist = customerinformtion.GetCustomerOrganizationList(customerid,actionuser);

	for (OrganizationVO item : reslist) 
	{
		organizationlist.put(item.getOrId(),item.getOrganizationame());
		
	}
	
	
} catch (ServiceException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}

return organizationlist;

}

	
	

private  Map<String,String> getMyDeviceMap(String customerid,String actionuser)
{
	
Map<String,String> organizationlist=new HashMap<>();

try {

ArrayList<TableVO> reslist = customerinformtion.GetCustomerDeviceList(customerid, "null", actionuser);

	for (TableVO item : reslist) 
	{
		organizationlist.put(item.getDevicemac(),item.getDevicemac());
		
	}
	
	
} catch (ServiceException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}

return organizationlist;

}

	
	





private  Map<String,String> getUserRoles()
{
	
Map<String,String> organizationlist=new HashMap<>();
organizationlist.put("1","Administrator");



return organizationlist;

}




private  Map<String,String> getCustomerStatusList()
{
	

Map<String,String> statusList=new HashMap<>();

statusList.put("1","Active");
statusList.put("0", "Inactive");


return statusList;
}



	
private  Map<String,String> getStatusList()
{
	

Map<String,String> statusList=new HashMap<>();

statusList.put("Active","Active");
statusList.put("Inactive", "Inactive");


return statusList;
}



private  Map<String,String> InstanceStatus()
{


Map<String,String> InstancestatusList=new HashMap<>();

InstancestatusList.put("1","In Use");
InstancestatusList.put("0", "Not in Use");


return InstancestatusList;
}

	
	

@SuppressWarnings("deprecation")
public ArrayList<ProductVO> getInstanceDetails(String instanceId, String region)

	{
	//System.out.println("Entering transfer instance id:"+ instanceId);
	
	ArrayList<ProductVO> instancedetails = new ArrayList<ProductVO>();

		ProductVO info=new ProductVO();
		
		try {

			AWSCredentials credentials = new BasicAWSCredentials(accesskey,
					sckreetKey);

			String regionval = region;

			AmazonEC2 ec2 = new AmazonEC2Client(credentials);
			Region ec2region = Region.getRegion(Regions.valueOf(regionval));
			ec2.setRegion(ec2region);

		//	AmazonEC2 ec2 = AmazonEC2ClientBuilder.standard()
	      //          .withRegion(Regions.valueOf(regionval))
	        //        .build();


			DescribeInstancesRequest describeInstanceRequest = new DescribeInstancesRequest()
					.withInstanceIds(instanceId);
			DescribeInstancesResult describeInstanceResult = ec2
					.describeInstances(describeInstanceRequest);
			////System.out.println("Ec2out:"+ec2.toString());
			String state = describeInstanceResult.getReservations()
					.get(0).getInstances().get(0).getState().getName();

		String privateip = describeInstanceResult.getReservations()
					.get(0).getInstances().get(0).getPrivateIpAddress();
					
		info.setState(state.toString());
		info.setPrivateip(privateip);
		instancedetails.add(info);
				
				} 
		catch (Exception e) 
		{
			//System.out.println("Instance state Exception ------------->"+e.getMessage());
			//return e.getMessage();

		}
	
		//System.out.println("Returning instance status"+instancedetails);
		return instancedetails;
	}


private String StartInstance(String instanceId, String region)
{
	try{
			AWSCredentials credentials = new BasicAWSCredentials(accesskey,
				sckreetKey);

		String regionval = region;

	AmazonEC2 ec2 = new AmazonEC2Client(credentials);
		Region ec2region = Region.getRegion(Regions.valueOf(regionval));

		ec2.setRegion(ec2region);

//		AmazonEC2 ec2 = AmazonEC2ClientBuilder.standard()
        //        .withRegion(Regions.valueOf(regionval)).withCredentials((AWSCredentialsProvider) credentials)
          //      .build();

		StartInstancesRequest startRequest = new StartInstancesRequest()
				.withInstanceIds(instanceId);
		StartInstancesResult startResult = ec2.startInstances(startRequest);

		List<InstanceStateChange> stateChangeList = startResult
				.getStartingInstances();

		return "Success: "+stateChangeList.get(0).getCurrentState().getName().toString()+" "+"Please referesh the page to get the current status";

	}catch(Exception e)
	{
		return "Error:"+e.getMessage();
	}
	
}


	
}
