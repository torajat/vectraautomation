package com.mlab.endpoint.controller;


import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.owasp.esapi.ESAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mlab.endpoint.Model.CustomerVO;
import com.mlab.endpoint.Model.OrganizationVO;
import com.mlab.endpoint.Model.ProductVO;
import com.mlab.endpoint.Model.TableVO;
import com.mlab.endpoint.Model.UserVO;
import com.mlab.endpoint.Service.CustomerInformationImpl;
import com.mlab.endpoint.exception.ServiceException;
import com.mlab.endpoint.interfaces.ProductInformationView;
import com.mlab.endpoint.interfaces.Report;
import com.mlab.endpoint.interfaces.SystemSettings;
import com.mlab.endpoint.util.Menumodel;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	@Autowired
	private SystemSettings systemSettings;

	@Autowired
	private Report report;


    @Autowired
    private ProductInformationView productinformationView;
    
    
    @Autowired
    private CustomerInformationImpl customerinformtion;
	
    
    
    @ModelAttribute("productVO")
    private ProductVO constractProductVO() {

        return new ProductVO();
    }


	@ModelAttribute("OrganizationVO")
	private OrganizationVO constractOrganizationVO() {

		return new OrganizationVO();
	}

    
	@RequestMapping(value = "/")
	public String home(HttpSession session,Model model,@ModelAttribute("productVO") ProductVO productVO)
	{	
		return "redirect:/login/user-login";
	}

	
	
	@RequestMapping(value = "/dashbord/viewDashBord")
	public String login() {
//		logger.info("Welcome home! The client locale is {}.", locale);
//
//		Date date = new Date();
//		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
//
//		String formattedDate = dateFormat.format(date);
//
//		model.addAttribute("serverTime", formattedDate);

		return "dashboard";
	}

	
	
	
	

	@RequestMapping(value = "/supplier/dashboard")
	public String supplierdashbord() {

		return "supplier/supplierdashboard";
	}

	
	
	@RequestMapping(value = "/dashbord")
	public String dashbroad() {

		return "dashboard";
	}
	
	
	@RequestMapping(value = "/global/mydashbord")
	public String Globaldashbroad(HttpSession session,Model model) 
	{

		
		if (session.getAttribute("customerid")==null){return "login";}   
		
		
		String actionuser=(String) session.getAttribute("actionuser");
		
			model.addAttribute("username",actionuser);
		
		try {
			
			model.addAttribute("customerinfolist", customerinformtion.getListofCustomer(actionuser).size());
			
			model.addAttribute("organizationlist", customerinformtion.getListOfOrganization(actionuser).size());

			model.addAttribute("listofmydevices",customerinformtion.ListOfGlobalAdminDevices( actionuser).size());
			
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		model.addAttribute(ESAPI.encoder().encodeForHTML("customerlist"),ESAPI.encoder().encodeForHTML("/global/customerlist"));
		
		model.addAttribute("username",actionuser);
		
		 
		return "dashboard";
	}
	
	


	
	

	@RequestMapping(value = "/kitchen/order/dashboard")
	public String KitchenDashboard(HttpSession session,Model model,@ModelAttribute("productVO") ProductVO productVO) 
	{

		if (session.getAttribute("customerid")==null){return "login";}   
		
		
		model.addAttribute("username",(String)session.getAttribute("username").toString());
		
		model.addAttribute(ESAPI.encoder().encodeForHTML("customerlist"),ESAPI.encoder().encodeForHTML("/global/customerlist"));
		
		 try {
			 
			 String customerid=(String) session.getAttribute("customerid");
			 
				model.addAttribute("orderlist",productinformationView.GetOrderDetails(customerid));
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		 Menumodel mobject=new Menumodel();
		 
		 
		 
		 model.addAttribute("username",session.getAttribute("username").toString());
		 //model.addAttribute("id",customerVO.getCustomeridentificationo());
		 model.addAttribute("modelVOval","productVO");
		 model.addAttribute("actionval","/restaurants/order/forwardtokitchen");

		 model.addAttribute("statuslist",mobject.getStatusList());
		 	
		 //model.addAttribute(getConfigMenuItem(model));
		 
		 Menumodel menuitem=new Menumodel();
		 model.addAttribute(menuitem.getConfigMenuItem(model));

		 
		Menumodel menu=new Menumodel();
		
		model.addAttribute(menu.getConfigMenuItem(model));
		 
		return "customeradmin/kitchen/vieworder";
	}


	
	@RequestMapping(value = "/vc/job/dashboard")
	public String Resturnatdashbroad(HttpSession session,Model model,
			@ModelAttribute("productVO") ProductVO productVO) 
	{

		
		if (session.getAttribute("customerid")==null){return "login";}   
		
		String css = (String)model.asMap().get("css");
		 String msg = (String)model.asMap().get("msg");
		 
		 
		    if (!"".equals(msg))
		    {
		   	 model.addAttribute("css",css);
		   	 model.addAttribute("msg",msg);
		    }

		model.addAttribute("username",(String)session.getAttribute("username").toString());
		model.addAttribute(ESAPI.encoder().encodeForHTML("customerlist"),ESAPI.encoder().encodeForHTML("/global/customerlist"));
		
		Menumodel menu=new Menumodel();
		
		model.addAttribute(menu.getConfigMenuItem(model));
		model.addAttribute("modelVOval","productVO");
		model.addAttribute("actionval","/vc/manage/instance/stop");
		model.addAttribute("transferaction","/vc/manage/instance/transferjob");
		
		 ArrayList<ProductVO> assetconfigs = null;
		 
			try {
				
				
				String customerid=(String) session.getAttribute("customerid");
				
				
				ArrayList<ProductVO> allcustomerid = productinformationView.GetAllCustomerList();
				
				
				Map<String ,String>customers=new HashMap<>();
				
				for (ProductVO productVO2 : allcustomerid) 
				{
					customers.put(productVO2.getCompanyname(), productVO2.getCompanyname());
				}
				
				model.addAttribute("companyname",customers);
				
				if (productVO.getCompanyname()==null)
				{

					assetconfigs = productinformationView.GetDashboardJobs();
					
				}else
				{
					//System.out.println("Company Name---->"+productVO.getCompanyname());
					
					assetconfigs = productinformationView.GetDashboardJobs(productVO.getCompanyname());
				}
				
				
				
				List<ProductVO> pending = assetconfigs.stream()                        // Convert to steam
			    		  .filter(x ->"Pending".equals(x.getStatus()))        // find only request method
			      		 .collect(Collectors.toList());
				
			model.addAttribute("pending",pending.size());
				
				List<ProductVO> completed = assetconfigs.stream()                        // Convert to steam
			    		  .filter(x ->"Completed".equals(x.getStatus()))        // find only request method
			      		 .collect(Collectors.toList());
				model.addAttribute("completed",completed.size());
				
				
				List<ProductVO> pstart = assetconfigs.stream()                        // Convert to steam
			    		  .filter(x ->"Preparation Started".equals(x.getStatus()))        // find only request method
			      		 .collect(Collectors.toList());
				
				model.addAttribute("pstart",pstart.size());
				
				
				
				List<ProductVO> runningcompleted = assetconfigs.stream()                        // Convert to steam
	    		  .filter(x ->"ProcessCompleted".equals(x.getRunning()))        // find only request method
	      		 .collect(Collectors.toList());
		
			
				
				List<ProductVO> running = assetconfigs.stream()                        // Convert to steam
	    		  .filter(x ->"Running".equals(x.getOperationflg()))        // find only request method
	      		 .collect(Collectors.toList());
		
				
				model.addAttribute("totalr",running.size());
				
				model.addAttribute("runningcompleted",running.size());
		
				
				
				
				
				List<ProductVO> pendingrun = assetconfigs.stream()                        // Convert to steam
	    		  .filter(x ->"Starting".equals(x.getOperationflg()))        // find only request method
	      		 .collect(Collectors.toList());
		
				model.addAttribute("pendingrun",pendingrun.size());
				
				model.addAttribute("total",assetconfigs.size());
				
				model.addAttribute("assetconfigs",assetconfigs);
				
				try{
				model.addAttribute("ftp",productinformationView.getFTPServiceStatus());
				}catch(Exception e){}
				
				try{
					
					StringBuilder upload=new StringBuilder();
					
					ArrayList<ProductVO> allcustomers = productinformationView.GetAllCustomerList();
					
					for (ProductVO customerList : allcustomers) 
					{
						System.out.println("Getting list of");
						
						String path=customerList.getFtpmappingpath();
						String companyname=customerList.getCompanyname();
						
						File[] zipfileslist=new File(path).listFiles(File::isFile);
						
						String filename="";
						
						for (File file : zipfileslist) 
						{
							String inputZIPFile=file.getAbsolutePath();
							
							
							filename=file.getName();
							
							if( filename.contains(".filepart"))
							{
								upload.append("Company Name: "+companyname + "File Name: "+filename +"- "+ file.getTotalSpace()+"\n");
							}
						
						
							
						}
						
						
						
					}
					
					model.addAttribute("files",upload.toString());
				
				}catch(Exception e){}
				
				
				
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	

			
		 
		return "customeradmin/dashboard";
	}

	
	@RequestMapping(value = "/mydashbord")
	public String mydashbroad() {

		return "dashboard";
	}
	
	
	
	
	
	

	@RequestMapping(value = "/restaurantsList", method = RequestMethod.GET)
		public String ListOfResturant_GET( Model model,HttpSession session,
				@ModelAttribute("CustomerVO") CustomerVO customerVO,
				@ModelAttribute("UserVO") UserVO userVO,
				@ModelAttribute("OrganizationVO") OrganizationVO OrganizationVO
				)
		
	{
		
		 if (session.getAttribute("customerid")==null){return "login";}   

		 String css = (String)model.asMap().get("css");
		 String msg = (String)model.asMap().get("msg");
		    
		    if (!"".equals(msg))
		    {
		   	 model.addAttribute("css",css);
		   	 model.addAttribute("msg",msg);
		    }

		    String actionuser="fontpage";
		    
		    model.addAttribute("modelval", "OrganizationVO");
		    model.addAttribute("actionval","/restaurantsList");
			
			
			model.addAttribute("organizationlist",getAllResturant(actionuser));

			model.addAttribute("citymap",getCityareaMap(actionuser));
			
			try {
				
				ArrayList<com.mlab.endpoint.Model.OrganizationVO> reslist = customerinformtion.GetResturantByLocation(OrganizationVO.getLocid(), actionuser);
				


				  List <String> valmodelList = new ArrayList<String>();
				  
				  //valmodelList.add(validationview);
					 
			//	  model.addAttribute("vallist",valresult); 
				
				  String vs ="";

				  
				  
					ArrayList<OrganizationVO> organizationList = new ArrayList<OrganizationVO>();
					  
				  
				for (OrganizationVO orgitem : reslist)
				{
					 OrganizationVO orgdetais=new OrganizationVO();
					 
					 orgdetais.setOrganizationame(orgitem.getOrganizationame());
					 
					 /*
					 vs = vs.concat( "<tr>"
				                +"<td>"+orgitem.getOrganizationame()+"</td>");
*/
					 Map<String,String> customeridlist=new HashMap<>();

					ArrayList<TableVO> result = customerinformtion.GetCustomerTableList(orgitem.getRef_customerid(), orgitem.getOrId(), actionuser);
					
					String select="";
					
					for (TableVO tableVO : result)
					{
						//select
						//<option value="4">ldkfj sldjflsdfj12323</option>
						
						select=select.concat("<option value="+ tableVO.getTableno()+">"+tableVO.getCapacity() +"</option>");
						
						customeridlist.put(tableVO.getTableno(), tableVO.getCapacity());
						
					}
					
					/* vs = vs.concat( "<tr>"
				                +"<td>"+orgitem.getOrganizationame()+"</td>"+"<td>"+select+"</td>");
					*/
					
					 orgdetais.setOrganizationame(orgitem.getOrganizationame());
					 orgdetais.setAddress(orgitem.getAddress());
					 orgdetais.setCustomerid(orgitem.getRef_customerid());
					 
					 orgdetais.setCapacity(select);
					 
					 orgdetais.setOrId(orgitem.getOrId());
					 
					 
					 organizationList.add(orgdetais);
					
					System.out.println("result--------->"+organizationList);
					
				}
				
				System.out.println("Auto Gent Organization-->"+organizationList);
				
				model.addAttribute("autogen",organizationList);
				  	
				
				
				model.addAttribute("restlist",customerinformtion.GetResturantByLocation(OrganizationVO.getLocid(), actionuser));
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


			  return "/resutrantfontpage/fontpage";
	
	}
	
	
	
	


	@RequestMapping(value = "/restaurantsList", method = RequestMethod.POST)
		public String BookingNow( Model model,HttpSession session,
				@ModelAttribute("CustomerVO") CustomerVO customerVO,
				@ModelAttribute("UserVO") UserVO userVO,
				@ModelAttribute("OrganizationVO") OrganizationVO OrganizationVO
				)
		
	{
		
		 if (session.getAttribute("customerid")==null){return "login";}   

		 String css = (String)model.asMap().get("css");
		 String msg = (String)model.asMap().get("msg");
		    
		    if (!"".equals(msg))
		    {
		   	 model.addAttribute("css",css);
		   	 model.addAttribute("msg",msg);
		    }

		    String actionuser="fontpage";
		    
		    model.addAttribute("modelval", "OrganizationVO");
		    model.addAttribute("actionval","/restaurantsList");
			
		    
		    System.out.println("Booking details --->"+OrganizationVO);
			
		    
			model.addAttribute("organizationlist",getAllResturant(actionuser));

			model.addAttribute("citymap",getCityareaMap(actionuser));
			
			try {
				
				customerinformtion.CreateBooking(OrganizationVO, OrganizationVO.getCustomerid());
				
				
				ArrayList<com.mlab.endpoint.Model.OrganizationVO> reslist = customerinformtion.GetResturantByLocation(OrganizationVO.getLocid(), actionuser);
				


				  List <String> valmodelList = new ArrayList<String>();
				  
				  //valmodelList.add(validationview);
					 
			//	  model.addAttribute("vallist",valresult); 
				
				  String vs ="";

				  
				  
					ArrayList<OrganizationVO> organizationList = new ArrayList<OrganizationVO>();
					  
				  
				for (OrganizationVO orgitem : reslist)
				{
					 OrganizationVO orgdetais=new OrganizationVO();
					 
					 orgdetais.setOrganizationame(orgitem.getOrganizationame());
					 
					 /*
					 vs = vs.concat( "<tr>"
				                +"<td>"+orgitem.getOrganizationame()+"</td>");
*/
					 Map<String,String> customeridlist=new HashMap<>();

					ArrayList<TableVO> result = customerinformtion.GetCustomerTableList(orgitem.getRef_customerid(), orgitem.getOrId(), actionuser);
					
					String select="";
					
					for (TableVO tableVO : result)
					{
						//select
						//<option value="4">ldkfj sldjflsdfj12323</option>
						
						select=select.concat("<option value="+ tableVO.getTableno()+">"+tableVO.getCapacity() +"</option>");
						
						customeridlist.put(tableVO.getTableno(), tableVO.getCapacity());
						
					}
					
					/* vs = vs.concat( "<tr>"
				                +"<td>"+orgitem.getOrganizationame()+"</td>"+"<td>"+select+"</td>");
					*/
					
					 orgdetais.setOrganizationame(orgitem.getOrganizationame());
					 orgdetais.setAddress(orgitem.getAddress());
					 orgdetais.setCustomerid(orgitem.getRef_customerid());
					 
					 orgdetais.setCapacity(select);
					 
					 organizationList.add(orgdetais);
					
					System.out.println("result--------->"+organizationList);
					
				}
				
				System.out.println("Auto Gent Organization-->"+organizationList);
				
				model.addAttribute("autogen",organizationList);
				  	
				
				
				model.addAttribute("restlist",customerinformtion.GetResturantByLocation(OrganizationVO.getLocid(), actionuser));
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


			  return "/resutrantfontpage/fontpage";
	
	}
	
	
	

	

private  Map<String,String> getAllResturant(String actionuser)
{
	
Map<String,String> customeridlist=new HashMap<>();

try {


	List<OrganizationVO> customerids = customerinformtion.getListOfOrganization(actionuser);

	System.out.println("City Area Map----->:"+customerids);
	
	
	for (OrganizationVO customer : customerids) 
	{
		customeridlist.put(customer.getOrId(),customer.getOrganizationame());
		
	}
	
	
} catch (ServiceException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}

return customeridlist;

}




private  Map<String,String> getCityareaMap(String actionuser)
{
	
Map<String,String> customeridlist=new HashMap<>();

try {


	List<OrganizationVO> customerids = customerinformtion.GetListOfLocations(actionuser);

	System.out.println("City Area Map----->:"+customerids);
	
	
	for (OrganizationVO customer : customerids) 
	{
		customeridlist.put(customer.getLocid(),customer.getArea());
		
	}
	
	
} catch (ServiceException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}

return customeridlist;

}
	


	//
//	@RequestMapping(value = "/", method = RequestMethod.POST)
//	public String validateUser(@ModelAttribute("userVO") UserVO userVO, Locale locale, Model model,HttpSession session)
//			throws ServiceException {
//
//		String returnPage = "";
//		 
//		systemSettings.validateUser(userVO);
//		session.setAttribute("user_designation", userVO.getDesignation());
//		session.setAttribute("user_battalion_id", userVO.getBattalionId());
//		model.addAttribute("user_designation", userVO.getDesignation());
//
//		userVO.setDesignation("admin");
//		if (userVO.getDesignation() == "admin") {
//			 
//			returnPage = "redirect:/viewDashBoard";
//		} else if (userVO.getDesignation() == "super admin") {
//
//			returnPage = "redirect:/allCase";
//		} else if (userVO.getDesignation() == "batallion"){
//			returnPage = "redirect:/allCase";
//		}
//
//		return returnPage;
//	}
//	
//	@RequestMapping(value = "/logoutUser", method = RequestMethod.GET)
//	public String logoutUser(@ModelAttribute("userVO") UserVO userVO, Locale locale, Model model,HttpSession session)
//			throws ServiceException {
//
//		 session.invalidate();
//		 
//		return "login";
//	}

}
