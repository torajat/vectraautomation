package com.mlab.endpoint.controller;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.owasp.esapi.ESAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mlab.endpoint.Model.CustomerVO;
import com.mlab.endpoint.Model.OrganizationVO;
import com.mlab.endpoint.Model.ProductVO;
import com.mlab.endpoint.Model.SalesReport;
import com.mlab.endpoint.Model.UserVO;
import com.mlab.endpoint.Service.CustomerInformationImpl;
import com.mlab.endpoint.exception.ServiceException;
import com.mlab.endpoint.interfaces.SystemSettings;
import com.mlab.endpoint.util.Menumodel;

@Controller
public class GlobalAdminController {
	
	private static final Logger logger = LoggerFactory.getLogger(GlobalAdminController.class);
	  
	@Autowired
    private CustomerInformationImpl customerinformtion;
	@Autowired
	private SystemSettings systemSettings;

	
	@ModelAttribute("CustomerVO")
    private CustomerVO constructCustomerVO()
    {
     return new CustomerVO();	
    }
	
	
	@ModelAttribute("userVO")
	private UserVO constractUserVO() {

		return new UserVO();
	}

	

	@ModelAttribute("OrganizationVO")
	private OrganizationVO constractOrganizationVO() {

		return new OrganizationVO();
	}


	@RequestMapping(value = "/success", method = RequestMethod.GET)
	public String SuccessMsg( Model model,HttpSession session,
			@ModelAttribute("CustomerVO") CustomerVO customerVO)
 
 {
		
	 if (session.getAttribute("customerid")==null){return "login";}   
	 
		 String css = (String)model.asMap().get("css");
		 String msg = (String)model.asMap().get("msg");
		    
		    if (!"".equals(msg))
		    {
		   	 model.addAttribute("css",css);
		   	 model.addAttribute("msg",msg);
		    }


	    return "";

 }
	
	
	
	
	
	
	
	

	
	@RequestMapping(value = "/global/customer/createcustomer", method = RequestMethod.GET)
		public String Create_Customer_GET( Model model,HttpSession session,
				@ModelAttribute("CustomerVO") CustomerVO customerVO)
	 
	 {
			
		 if (session.getAttribute("customerid")==null){return "login";}   

		 

		 String css = (String)model.asMap().get("css");
		 String msg = (String)model.asMap().get("msg");
		    
		    if (!"".equals(msg))
		    {
		   	 model.addAttribute("css",css);
		   	 model.addAttribute("msg",msg);
		    }
		
		 model.addAttribute("username",session.getAttribute("username").toString());
		 
		 model.addAttribute("modelVOval","CustomerVO");
		 model.addAttribute("actionval","/global/customer/createcustomer");
			
		 model.addAttribute(getConfigMenuItem(model));
		 
		return "globaladmin/g_createcustomer";
		}


	
	

	@RequestMapping(value = "/global/customer/createcustomer", method = RequestMethod.POST)
		public String Create_Customer_POST( Model model,HttpSession session,
				@ModelAttribute("CustomerVO") CustomerVO customerVO,RedirectAttributes rmodel)
	 
	 {
			
		 if (session.getAttribute("customerid")==null){return "login";}   
		 
		 System.out.println("Username-->"+session.getAttribute("username").toString());
		
		 model.addAttribute("username",session.getAttribute("username").toString());
		 
		 model.addAttribute("modelVOval","CustomerVO");
		 model.addAttribute("actionval","/global/customer/createcustomer");
			
		 model.addAttribute(getConfigMenuItem(model));
		 
		 System.out.println("creating customer"+customerVO);
	
		 String actionuser=(String) session.getAttribute("username");
		 
		 	try
		 	{
				 String result=customerinformtion.createCustomer(customerVO, customerVO.getCustomerid(), actionuser);
			
				 if (result.equals("Success"))
				 {
					 rmodel.addFlashAttribute("css","success");
					 rmodel.addFlashAttribute("msg","Successfully Created");
					 
					 return "redirect:/global/customer/createcustomer";
			
				 }else
				 {
					 model.addAttribute("css","danger");
					 model.addAttribute("msg",result);
					 
				 }
				 
		 	} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

				 model.addAttribute("css","danger");
				 model.addAttribute("msg",e.getMessage());
				
			}
			
		 
		return "globaladmin/g_createcustomer";
		}

	
	@RequestMapping(value = "/global/customerlist", method = RequestMethod.GET)
	public String CustomerList_GET(@ModelAttribute("CustomerVO") CustomerVO customerVO,Model model,HttpSession session)throws ServiceException
	{
		

		 if (session.getAttribute("customerid")==null){return "login";}   
		 

		 String css = (String)model.asMap().get("css");
		 String msg = (String)model.asMap().get("msg");
		    
		    if (!"".equals(msg))
		    {
		   	 model.addAttribute("css",css);
		   	 model.addAttribute("msg",msg);
		    }

		    
		model.addAttribute("title","Customer");
	    model.addAttribute("actionvaledit","/global/customer/edit");
		model.addAttribute("methodval", "POST");
		model.addAttribute("modelVOval","CustomerVO");
		model.addAttribute("btntitle","Create Customer");
		model.addAttribute("customer","/customer/createcustomer.do");
		
		model.addAttribute(getConfigMenuItem(model));
		 
		model.addAttribute("username",session.getAttribute("username").toString());
		model.addAttribute("menutitle","List of Customer");		
		String actionuser=(String) session.getAttribute("username");
		
		model.addAttribute("customerinfolist", customerinformtion.getListofCustomer(actionuser));
			 
			return "globaladmin/g_customerlist";
		 
	   
		}


	

	@RequestMapping(value = "/global/customer/edit", method = RequestMethod.GET)
		public String Edit_Customer_GET( Model model,HttpSession session,
				@ModelAttribute("CustomerVO") CustomerVO customerVO)
	 
	 {
			
		 if (session.getAttribute("customerid")==null){return "login";}   

		 

		 String css = (String)model.asMap().get("css");
		 String msg = (String)model.asMap().get("msg");
		    
		    if (!"".equals(msg))
		    {
		   	 model.addAttribute("css",css);
		   	 model.addAttribute("msg",msg);
		    }
		
		 model.addAttribute("username",session.getAttribute("username").toString());
		 
		 model.addAttribute("id",customerVO.getCustomeridentificationo());
		 
		 model.addAttribute("modelVOval","CustomerVO");
		 
		 model.addAttribute("actionval","/global/customer/edit");

		 model.addAttribute("statuslist",getStatusList());
		 	
		 model.addAttribute(getConfigMenuItem(model));
		 
		return "globaladmin/g_editcustomer";
		}





	@RequestMapping(value = "/global/customer/edit", method = RequestMethod.POST)
		public String Edit_Customer_POST( Model model,HttpSession session,
				@ModelAttribute("CustomerVO") CustomerVO customerVO,RedirectAttributes rmodel)
	 
	 {
			
		 if (session.getAttribute("customerid")==null){return "login";}   

		 

		 String css = (String)model.asMap().get("css");
		 String msg = (String)model.asMap().get("msg");
		    
		    if (!"".equals(msg))
		    {
		   	 model.addAttribute("css",css);
		   	 model.addAttribute("msg",msg);
		    }
		    
		
		    String actionuser=(String) session.getAttribute("username");
		    
		 model.addAttribute("username",session.getAttribute("username").toString());
		 
		 model.addAttribute("id",customerVO.getCustomeridentificationo());
		 
		 model.addAttribute("modelVOval","CustomerVO");
		 
		 model.addAttribute("actionval","/global/customer/edit");
			
		 
		 model.addAttribute(getConfigMenuItem(model));
		 
		 model.addAttribute("statuslist",getStatusList());
		 
		 
		 System.out.println("Customer Edit---->"+customerVO.toString());
	
		 String result;
		try {
			result = customerinformtion.UpdateCustomer(customerVO, customerVO.getCustomerid(), actionuser);

			 if (result.equals("Success"))
			 {
				 rmodel.addFlashAttribute("css","success");
				 rmodel.addFlashAttribute("msg","Successfully Created");
				 
				 return "redirect:/global/customerlist";
		
			 }else
			 {
				 model.addAttribute("css","danger");
				 model.addAttribute("msg",result);
				 
			 }
		
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			 model.addAttribute("css","danger");
			 model.addAttribute("msg",e.getMessage());
		
			
		}
				
		 
		 
		 
		return "globaladmin/g_editcustomer";
		}



//###########################################create customer admin user ####################################
	
	@RequestMapping(value = "/global/customer/createadmin", method = RequestMethod.GET)
		public String Create_Customer_Admin_GET( Model model,HttpSession session,
				@ModelAttribute("CustomerVO") CustomerVO customerVO,
				@ModelAttribute("UserVO") UserVO userVO
				)
	 
		{
		
		
		 if (session.getAttribute("customerid")==null){return "login";}   

		 String css = (String)model.asMap().get("css");
		 String msg = (String)model.asMap().get("msg");
		    
		    if (!"".equals(msg))
		    {
		   	 model.addAttribute("css",css);
		   	 model.addAttribute("msg",msg);
		    }
		
		 model.addAttribute("username",session.getAttribute("username").toString());
		 
		 model.addAttribute("id",customerVO.getCustomeridentificationo());
		 
		 model.addAttribute("modelVOval","UserVO");
		 
		 model.addAttribute("actionval","/global/customer/createadmin");

		 model.addAttribute("statuslist",getStatusList());
		 	
		 model.addAttribute(getConfigMenuItem(model));
		 
		 String actionuser=(String) session.getAttribute("username");
		 
		 model.addAttribute("customerids",getCustomerIdMap(actionuser));
		 
		return "globaladmin/g_createadmin";
		}



	
	
	
	@RequestMapping(value = "/global/customer/createadmin", method = RequestMethod.POST)
	public String Create_Customer_Admin_POST( Model model,HttpSession session,
			@ModelAttribute("CustomerVO") CustomerVO customerVO,
			@ModelAttribute("UserVO") UserVO userVO,RedirectAttributes rmodel
			)
 
	{
	
	
	 if (session.getAttribute("customerid")==null){return "login";}   

	 String css = (String)model.asMap().get("css");
	 String msg = (String)model.asMap().get("msg");
	    
	    if (!"".equals(msg))
	    {
	   	 model.addAttribute("css",css);
	   	 model.addAttribute("msg",msg);
	    }
	
	 model.addAttribute("username",session.getAttribute("username").toString());
	 
	 model.addAttribute("id",customerVO.getCustomeridentificationo());
	 
	 model.addAttribute("modelVOval","UserVO");
	 
	 model.addAttribute("actionval","/global/customer/createadmin");

	 model.addAttribute("statuslist",getStatusList());
	 	
	 model.addAttribute(getConfigMenuItem(model));
	 

	 String actionuser=(String) session.getAttribute("username");
	 
	 try
	 {
		String result = systemSettings.createCustomerAdminUser(userVO, actionuser);
	
		if (result.equals("Success"))
		{
		rmodel.addFlashAttribute("css","success");
		rmodel.addFlashAttribute("msg","Customer admin create succesfully");
		
		return "redirect:/global/customer/createadmin";
		}else
		{
			model.addAttribute("css","danger");
			model.addAttribute("msg",result);
			
		}
		
	} catch (NoSuchAlgorithmException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();

		model.addAttribute("css","danger");
		model.addAttribute("msg",e.getMessage());
	
	} catch (InvalidKeySpecException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		
		model.addAttribute("css","danger");
		model.addAttribute("msg",e.getMessage());
		
	} catch (ServiceException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		model.addAttribute("css","danger");
		model.addAttribute("msg",e.getMessage());
	}
	 
	 
	 model.addAttribute("customerids",getCustomerIdMap(actionuser));
	 
	 
	 
	 
	return "globaladmin/g_createadmin";
	}


	//get admin
	
	@RequestMapping(value = "/global/customer/editadmin", method = RequestMethod.GET)
	public String Get_Customar_Admin( Model model,HttpSession session,
			@ModelAttribute("CustomerVO") CustomerVO customerVO,
			@ModelAttribute("UserVO") UserVO userVO
			)
 
	{
	
	
	 if (session.getAttribute("customerid")==null){return "login";}   

	 String css = (String)model.asMap().get("css");
	 String msg = (String)model.asMap().get("msg");
	    
	    if (!"".equals(msg))
	    {
	   	 model.addAttribute("css",css);
	   	 model.addAttribute("msg",msg);
	    }
	
	 model.addAttribute("username",session.getAttribute("username").toString());
	 
	 model.addAttribute("id",customerVO.getCustomeridentificationo());
	 
	 model.addAttribute("modelVOval","UserVO");
	 
	 model.addAttribute("actionval","/global/customer/updateadmin");

	 model.addAttribute("statuslist",getStatusList());
	 	
	 model.addAttribute(getConfigMenuItem(model));
	 
	 String actionuser=(String) session.getAttribute("username");
	 
	 model.addAttribute("customerids",getCustomerIdMap(actionuser));
	 
	 try {
		model.addAttribute("userslist",systemSettings.getAllCustomarAdminUsers());
	} catch (ServiceException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	 
	 model.addAttribute("actionvalpassreset","/global/customer/resetpassword");
	 
	return "globaladmin/g_adminuserslist";
	}


	
	
	

	@RequestMapping(value = "/global/customer/updateadmin", method = RequestMethod.GET)
		public String Update_Customer_Admin_GET( Model model,HttpSession session,
				@ModelAttribute("CustomerVO") CustomerVO customerVO,
				@ModelAttribute("UserVO") UserVO userVO
				)
	 
	{
		
		
		 if (session.getAttribute("customerid")==null){return "login";}   

		 String css = (String)model.asMap().get("css");
		 String msg = (String)model.asMap().get("msg");
		    
		    if (!"".equals(msg))
		    {
		   	 model.addAttribute("css",css);
		   	 model.addAttribute("msg",msg);
		    }
		
		 model.addAttribute("username",session.getAttribute("username").toString());
		 
		 model.addAttribute("id",customerVO.getCustomeridentificationo());
		 
		 model.addAttribute("modelVOval","UserVO");
		 
		 model.addAttribute("actionval","/global/customer/updateadmin");

		 model.addAttribute("statuslist",getStatusList());
		 	
		 model.addAttribute(getConfigMenuItem(model));
		 
		 String actionuser=(String) session.getAttribute("username");
		 
		 model.addAttribute("customerids",getCustomerIdMap(actionuser));
		 
		return "globaladmin/g_updateadmin";
		}


	

	@RequestMapping(value = "/global/customer/updateadmin", method = RequestMethod.POST)
	public String Update_Customer_Admin_POST( Model model,HttpSession session,
			@ModelAttribute("CustomerVO") CustomerVO customerVO,
			@ModelAttribute("UserVO") UserVO userVO,RedirectAttributes rmodel
			)
	{
	
	
	 if (session.getAttribute("customerid")==null){return "login";}   

	 String css = (String)model.asMap().get("css");
	 String msg = (String)model.asMap().get("msg");
	    
	    if (!"".equals(msg))
	    {
	   	 model.addAttribute("css",css);
	   	 model.addAttribute("msg",msg);
	    }
	
	 model.addAttribute("username",session.getAttribute("username").toString());
	 
	 model.addAttribute("id",customerVO.getCustomeridentificationo());
	 
	 model.addAttribute("modelVOval","UserVO");
	 
	 model.addAttribute("actionval","/global/customer/createadmin");

	 model.addAttribute("statuslist",getStatusList());
	 	
	 model.addAttribute(getConfigMenuItem(model));
	 

	 String actionuser=(String) session.getAttribute("username");
	 
	 try
	 {
		String result = systemSettings.UpdateCustomerAdminUser(userVO, actionuser);
	
		if (result.equals("Success"))
		{
		rmodel.addFlashAttribute("css","success");
		rmodel.addFlashAttribute("msg","Customer admin update succesfully");
		
		return "redirect:/global/customer/editadmin";
		}else
		{
			model.addAttribute("css","danger");
			model.addAttribute("msg",result);
			
		}
		
	} catch (NoSuchAlgorithmException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();

		model.addAttribute("css","danger");
		model.addAttribute("msg",e.getMessage());
	
	} catch (InvalidKeySpecException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		
		model.addAttribute("css","danger");
		model.addAttribute("msg",e.getMessage());
		
	} catch (ServiceException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		model.addAttribute("css","danger");
		model.addAttribute("msg",e.getMessage());
	}
	 
	 
	 model.addAttribute("customerids",getCustomerIdMap(actionuser));
	 
	 
	return "globaladmin/g_updateadmin";
	}

	

	
	@RequestMapping(value = "/global/customer/resetpassword", method = RequestMethod.POST)
	public String Resetpassword_Customer_Admin_POST( Model model,HttpSession session,
			@ModelAttribute("CustomerVO") CustomerVO customerVO,
			@ModelAttribute("UserVO") UserVO userVO,RedirectAttributes rmodel)

	{
	
	
	 if (session.getAttribute("customerid")==null){return "login";}   

	 String css = (String)model.asMap().get("css");
	 String msg = (String)model.asMap().get("msg");
	    
	    if (!"".equals(msg))
	    {
	   	 model.addAttribute("css",css);
	   	 model.addAttribute("msg",msg);
	    }
	
	 model.addAttribute("username",session.getAttribute("username").toString());
	 
	 model.addAttribute("id",customerVO.getCustomeridentificationo());
	 
	 model.addAttribute("modelVOval","UserVO");
	 
	 model.addAttribute("actionval","/global/customer/createadmin");

	 model.addAttribute("statuslist",getStatusList());
	 	
	 model.addAttribute(getConfigMenuItem(model));
	 

	 String actionuser=(String) session.getAttribute("username");
	 
	 try
	 {
		String result = systemSettings.UpdateUserPassword(userVO, actionuser);
	
		if (result.equals("Success"))
		{
			rmodel.addFlashAttribute("css","success");
			rmodel.addFlashAttribute("msg","Password update succesfully");
			
			return "redirect:/global/customer/editadmin";
		
		}else
		{
			rmodel.addFlashAttribute("css","danger");
			rmodel.addFlashAttribute("msg","Failed to reset password");
		
		return "redirect:/global/customer/editadmin";		
		}
		
	} catch (NoSuchAlgorithmException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();

		rmodel.addFlashAttribute("css","danger");
		rmodel.addFlashAttribute("msg",e.getMessage());
	
		return "redirect:/global/customer/editadmin";		

	
	
	} catch (InvalidKeySpecException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		
		rmodel.addFlashAttribute("css","danger");
		rmodel.addFlashAttribute("msg",e.getMessage());
	
		return "redirect:/global/customer/editadmin";		
		
	} catch (ServiceException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		rmodel.addFlashAttribute("css","danger");
		rmodel.addFlashAttribute("msg",e.getMessage());
	
		return "redirect:/global/customer/editadmin";		
	}
	
	
}



	// manage resturant
	


	@RequestMapping(value = "/global/restaurantsList", method = RequestMethod.GET)
		public String ManageResturarnt_GET( Model model,HttpSession session,
				@ModelAttribute("CustomerVO") CustomerVO customerVO,
				@ModelAttribute("UserVO") UserVO userVO,
				@ModelAttribute("OrganizationVO") OrganizationVO OrganizationVO
				)
		
	{
		
		 if (session.getAttribute("customerid")==null){return "login";}   

		 String css = (String)model.asMap().get("css");
		 String msg = (String)model.asMap().get("msg");
		    
		    if (!"".equals(msg))
		    {
		   	 model.addAttribute("css",css);
		   	 model.addAttribute("msg",msg);
		    }

			 String actionuser=(String) session.getAttribute("username");
			    
		 try
		 {
			model.addAttribute("organizationlist", customerinformtion.getListOfOrganization(actionuser));
		
		 } catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		 
		 model.addAttribute("username",session.getAttribute("username").toString());
		 model.addAttribute("id",customerVO.getCustomeridentificationo());
		 model.addAttribute("modelVOval","OrganizationVO");
		 model.addAttribute("actionval","/global/manage/updateorganization");

		 model.addAttribute("statuslist",getStatusList());
		 	
		 //model.addAttribute(getConfigMenuItem(model));
		 
		 Menumodel menuitem=new Menumodel();
		 model.addAttribute(menuitem.getConfigMenuItem(model));
		 
		 
		return "globaladmin/organization/g_organizationlist";
		}



	@RequestMapping(value = "/global/manage/createorganization", method = RequestMethod.GET)
		public String Create_Organization_GET( Model model,HttpSession session,
				@ModelAttribute("CustomerVO") CustomerVO customerVO,
				@ModelAttribute("UserVO") UserVO userVO,
				@ModelAttribute("OrganizationVO") OrganizationVO OrganizationVO
				)
		
			{
		
		 if (session.getAttribute("customerid")==null){return "login";}   

		 String css = (String)model.asMap().get("css");
		 String msg = (String)model.asMap().get("msg");
		    
		    if (!"".equals(msg))
		    {
		   	 model.addAttribute("css",css);
		   	 model.addAttribute("msg",msg);
		    }

			 String actionuser=(String) session.getAttribute("username");
			    
		 try
		 {
			model.addAttribute("organizationlist", customerinformtion.getListOfOrganization(actionuser));
		
		 } catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		 
		 model.addAttribute("username",session.getAttribute("username").toString());
		 model.addAttribute("id",customerVO.getCustomeridentificationo());
		 model.addAttribute("modelVOval","OrganizationVO");
		 model.addAttribute("actionval","/global/manage/createorganization");

		 model.addAttribute("statuslist",getStatusList());
		 	
		 //model.addAttribute(getConfigMenuItem(model));
		 
		 model.addAttribute("citymap",getCityareaMap(actionuser));
		 
		 Menumodel menuitem=new Menumodel();
		 model.addAttribute(menuitem.getConfigMenuItem(model));
		 model.addAttribute("customerids",getCustomerIdMap(actionuser));
		 model.addAttribute("menutitle","Create Restaurant");
		 
		return "globaladmin/organization/g_createorganization";
		}

	

	

	@RequestMapping(value = "/global/manage/createorganization", method = RequestMethod.POST)
		public String Create_Organization_POST( Model model,HttpSession session,
				@ModelAttribute("CustomerVO") CustomerVO customerVO,
				@ModelAttribute("UserVO") UserVO userVO,
				@ModelAttribute("OrganizationVO") OrganizationVO OrganizationVO,RedirectAttributes rmodel
				)
		
			{
		
		 if (session.getAttribute("customerid")==null){return "login";}   

		 String css = (String)model.asMap().get("css");
		 String msg = (String)model.asMap().get("msg");
		    
		    if (!"".equals(msg))
		    {
		   	 model.addAttribute("css",css);
		   	 model.addAttribute("msg",msg);
		    }

			 String actionuser=(String) session.getAttribute("username");
			    
		 try
		 {
			 
			 
			String result= customerinformtion.createOrganization(OrganizationVO, OrganizationVO.getCustomerid(), actionuser);
	
			if (result.equals("Success"))
			{
				rmodel.addFlashAttribute("css","success");
				rmodel.addFlashAttribute("msg","Created Successfully.");
				
				return "redirect:/global/manage/createorganization";
			
			}else
			{
				model.addAttribute("css","danger");
				model.addAttribute("msg","Failed to create "+result);
			}
			
		 } catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			model.addAttribute("css","danger");
			model.addAttribute("msg","Failed to create "+e.getMessage());
		
		}
	
		 
		 model.addAttribute("username",session.getAttribute("username").toString());
		 model.addAttribute("id",customerVO.getCustomeridentificationo());
		 model.addAttribute("modelVOval","OrganizationVO");
		 model.addAttribute("actionval","/global/manage/createorganization");

		 model.addAttribute("statuslist",getStatusList());
		 	
		 //model.addAttribute(getConfigMenuItem(model));
		 
		 model.addAttribute("citymap",getCityareaMap(actionuser));
		 
		 Menumodel menuitem=new Menumodel();
		 model.addAttribute(menuitem.getConfigMenuItem(model));
		 model.addAttribute("customerids",getCustomerIdMap(actionuser));
		 model.addAttribute("menutitle","Create Restaurant");
		 
		 System.out.println("Resturant---->"+OrganizationVO);
		 
		return "globaladmin/organization/g_createorganization";
		}



	

	@RequestMapping(value = "/global/manage/updateorganization", method = RequestMethod.GET)
		public String Update_Organization_GET( Model model,HttpSession session,
				@ModelAttribute("CustomerVO") CustomerVO customerVO,
				@ModelAttribute("UserVO") UserVO userVO,
				@ModelAttribute("OrganizationVO") OrganizationVO OrganizationVO
				)
		
			{
		
		 if (session.getAttribute("customerid")==null){return "login";}   

		 String css = (String)model.asMap().get("css");
		 String msg = (String)model.asMap().get("msg");
		    
		    if (!"".equals(msg))
		    {
		   	 model.addAttribute("css",css);
		   	 model.addAttribute("msg",msg);
		    }

			 String actionuser=(String) session.getAttribute("username");
			    
		 try
		 {
			model.addAttribute("organizationlist", customerinformtion.getListOfOrganization(actionuser));
		
		 } catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		 
		 model.addAttribute("username",session.getAttribute("username").toString());
		 model.addAttribute("id",customerVO.getCustomeridentificationo());
		 model.addAttribute("modelVOval","OrganizationVO");
		 model.addAttribute("actionval","/global/manage/updateorganization");

		 model.addAttribute("statuslist",getStatusList());
		 	
		 model.addAttribute("previouname",OrganizationVO.getOrganizationame());
		 
		 //model.addAttribute(getConfigMenuItem(model));
		 
		 model.addAttribute("citymap",getCityareaMap(actionuser));
		 
		 Menumodel menuitem=new Menumodel();
		 model.addAttribute(menuitem.getConfigMenuItem(model));
		 model.addAttribute("customerids",getCustomerIdMap(actionuser));
		 model.addAttribute("menutitle","Create Restaurant");
		 
		return "globaladmin/organization/g_editorganization";
		}



	@RequestMapping(value = "/global/manage/updateorganization", method = RequestMethod.POST)
		public String Update_Organization_POST( Model model,HttpSession session,
				@ModelAttribute("CustomerVO") CustomerVO customerVO,
				@ModelAttribute("UserVO") UserVO userVO,
				@ModelAttribute("OrganizationVO") OrganizationVO OrganizationVO,RedirectAttributes rmodel
				)
		
			{
		
		 if (session.getAttribute("customerid")==null){return "login";}   

		 String css = (String)model.asMap().get("css");
		 String msg = (String)model.asMap().get("msg");
		    
		    if (!"".equals(msg))
		    {
		   	 model.addAttribute("css",css);
		   	 model.addAttribute("msg",msg);
		    }

			 String actionuser=(String) session.getAttribute("username");
			    
		 try
		 {
			 
			 
			String result= customerinformtion.UpdateOrganization(OrganizationVO, OrganizationVO.getCustomerid(), actionuser);
	
			if (result.equals("Success"))
			{
				rmodel.addFlashAttribute("css","success");
				rmodel.addFlashAttribute("msg","Update Successfully.");
				
				return "redirect:/global/restaurantsList";
			
			}else
			{
				model.addAttribute("css","danger");
				model.addAttribute("msg","Failed to update "+result);
			}
			
		 } catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			model.addAttribute("css","danger");
			model.addAttribute("msg","Failed to update "+e.getMessage());
		
		}
	
		 
		 model.addAttribute("username",session.getAttribute("username").toString());
		 model.addAttribute("id",customerVO.getCustomeridentificationo());
		 model.addAttribute("modelVOval","OrganizationVO");
		 model.addAttribute("actionval","/global/manage/updateorganization");

		 model.addAttribute("statuslist",getStatusList());
		 	
		 //model.addAttribute(getConfigMenuItem(model));
		 
		 model.addAttribute("citymap",getCityareaMap(actionuser));
		 
		 Menumodel menuitem=new Menumodel();
		 model.addAttribute(menuitem.getConfigMenuItem(model));
		 model.addAttribute("customerids",getCustomerIdMap(actionuser));
		 model.addAttribute("menutitle","Update Restaurant");
		 
		 System.out.println("Resturant---->"+OrganizationVO);
		 
		return "globaladmin/organization/g_editorganization";
		}

	
	
	
private Model getConfigMenuItem( Model model)
{
	model.addAttribute(ESAPI.encoder().encodeForHTML("createsiteservers"),ESAPI.encoder().encodeForHTML("Create Site Server"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("createsiteserversaction"),ESAPI.encoder().encodeForHTML("/global/siteserver/createsiteserver"));
	

	model.addAttribute(ESAPI.encoder().encodeForHTML("editsiteservers"),ESAPI.encoder().encodeForHTML("List of Site Server"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("editsiteserversaction"),ESAPI.encoder().encodeForHTML("/global/siteserver/editsiteserver"));



	model.addAttribute(ESAPI.encoder().encodeForHTML("customerlist"),ESAPI.encoder().encodeForHTML("/global/customerlist"));
	
	
	model.addAttribute(ESAPI.encoder().encodeForHTML("createcustomer"),ESAPI.encoder().encodeForHTML("Create Customer"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("createcustomeraction"),ESAPI.encoder().encodeForHTML("/global/customer/createcustomer"));



	model.addAttribute(ESAPI.encoder().encodeForHTML("editcustomer"),ESAPI.encoder().encodeForHTML("Edit Customer"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("editcustomeraction"),ESAPI.encoder().encodeForHTML("/global/customer/listofcustomer"));


	model.addAttribute(ESAPI.encoder().encodeForHTML("createadmin"),ESAPI.encoder().encodeForHTML("Create Customer Admin"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("createadminaction"),ESAPI.encoder().encodeForHTML("/global/customer/createadmin"));

	model.addAttribute(ESAPI.encoder().encodeForHTML("editadmin"),ESAPI.encoder().encodeForHTML("Edit Customer Admin"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("editadminaction"),ESAPI.encoder().encodeForHTML("/global/customer/editadmin"));

	model.addAttribute(ESAPI.encoder().encodeForHTML("resetadminpass"),ESAPI.encoder().encodeForHTML("Reset Password"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("resetadminpassaction"),ESAPI.encoder().encodeForHTML("/global/customer/resetpassword"));

	model.addAttribute(ESAPI.encoder().encodeForHTML("createassetfamily"),ESAPI.encoder().encodeForHTML("Create Asset Family"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("createassetfamilyaction"),ESAPI.encoder().encodeForHTML("/global/assetfamily/create"));


	model.addAttribute(ESAPI.encoder().encodeForHTML("editassetfamily"),ESAPI.encoder().encodeForHTML("Edit Asset Family"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("editassetfamilyaction"),ESAPI.encoder().encodeForHTML("/global/assetfamily/edit"));
	


	model.addAttribute(ESAPI.encoder().encodeForHTML("createprofile"),ESAPI.encoder().encodeForHTML("Create Profile"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("createprofileaction"),ESAPI.encoder().encodeForHTML("/global/profile/create"));


	model.addAttribute(ESAPI.encoder().encodeForHTML("editprofile"),ESAPI.encoder().encodeForHTML("Edit Profile"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("editprofileaction"),ESAPI.encoder().encodeForHTML("/global/profile/edit"));


	model.addAttribute(ESAPI.encoder().encodeForHTML("profilelist"),ESAPI.encoder().encodeForHTML("List of Profile"));

	model.addAttribute(ESAPI.encoder().encodeForHTML("profilelistaction"),ESAPI.encoder().encodeForHTML("/global/profile/list"));

	
	model.addAttribute(ESAPI.encoder().encodeForHTML("createsection"),ESAPI.encoder().encodeForHTML("Create Section"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("createsectionaction"),ESAPI.encoder().encodeForHTML("/global/section/create"));

	model.addAttribute(ESAPI.encoder().encodeForHTML("editsection"),ESAPI.encoder().encodeForHTML("Edit Section"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("editsectionaction"),ESAPI.encoder().encodeForHTML("/global/section/edit"));

	model.addAttribute(ESAPI.encoder().encodeForHTML("rulelistaction"),ESAPI.encoder().encodeForHTML("/global/rule/list"));

	model.addAttribute(ESAPI.encoder().encodeForHTML("menurulelist"),ESAPI.encoder().encodeForHTML("Edit Rule"));

	
	
	model.addAttribute(ESAPI.encoder().encodeForHTML("createrule"),ESAPI.encoder().encodeForHTML("Create Rule"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("createruleaction"),ESAPI.encoder().encodeForHTML("/global/rule/create"));



	model.addAttribute(ESAPI.encoder().encodeForHTML("editrule"),ESAPI.encoder().encodeForHTML("Edit Rule"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("editruleaction"),ESAPI.encoder().encodeForHTML("/global/rule/edit"));
	
	

	model.addAttribute(ESAPI.encoder().encodeForHTML("createrulemap"),ESAPI.encoder().encodeForHTML("Create Rule mapping"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("createrulemapaction"),ESAPI.encoder().encodeForHTML("/global/rule/map/create"));

	model.addAttribute(ESAPI.encoder().encodeForHTML("editrulemap"),ESAPI.encoder().encodeForHTML("Edit Rule mapping"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("editrulemapaction"),ESAPI.encoder().encodeForHTML("/global/rule/map/edit"));

	
	return model;
	
}


private  Map<String,String> getStatusList()
{
	

Map<String,String> statusList=new HashMap<>();

statusList.put("Active","Active");
statusList.put("Inactive", "Inactive");


return statusList;
}



private  Map<String,String> getCustomerIdMap(String actionuser)
{
	
Map<String,String> customeridlist=new HashMap<>();

try {


	List<CustomerVO> customerids = customerinformtion.getListofCustomer(actionuser);

	for (CustomerVO customer : customerids) 
	{
		customeridlist.put(customer.getCustomeridentificationo(),customer.getCustomerName());
		
	}
	
	
} catch (ServiceException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}

return customeridlist;

}



private  Map<String,String> getCityareaMap(String actionuser)
{
	
Map<String,String> customeridlist=new HashMap<>();

try {


	List<OrganizationVO> customerids = customerinformtion.GetListOfLocations(actionuser);

	System.out.println("City Area Map----->:"+customerids);
	
	
	for (OrganizationVO customer : customerids) 
	{
		customeridlist.put(customer.getLocid(),customer.getArea());
		
	}
	
	
} catch (ServiceException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}

return customeridlist;

}




}