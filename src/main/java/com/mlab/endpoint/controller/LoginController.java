/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mlab.endpoint.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author ADMIN
 */
@Controller
public class LoginController {

    @RequestMapping("/login/user-login")
    private String login(@RequestParam(value = "logout", required = false) String logout, HttpServletRequest request,
			HttpServletResponse response,Model model) {
    	HttpSession session = request.getSession();
    	if (logout != null) {
        	
        	
        	Authentication auth = SecurityContextHolder.getContext()
    				.getAuthentication();
    		if (auth != null) 
    		{
    			new SecurityContextLogoutHandler().logout(request, response, auth);
    		}
    		
    		try{
    		session.invalidate(); 
    		}catch( Exception e)
    		{
    
    		}
            model.addAttribute("msg", "You've been logged out successfully.");
        }
        
        return "login";
    }

    @RequestMapping("/403")
    private String accesssDenied(Model model) {
        //check if user is login
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            if (!(auth instanceof AnonymousAuthenticationToken)) {
                UserDetails userDetail = (UserDetails) auth.getPrincipal();
                model.addAttribute("username", userDetail.getUsername());
            }
        }

        return "403";
    }
    

    @ExceptionHandler(NoHandlerFoundException.class)
    public ResponseEntity<Error> handle(NoHandlerFoundException ex){
        String message = "HTTP " + ex.getHttpMethod() + " for " + ex.getRequestURL() + " is not supported.";
        Error error = new Error(message);
        return new ResponseEntity<Error>(error, HttpStatus.NOT_FOUND);
    }
    
    
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<Error> handle(RuntimeException ex){
        System.out.println("controller local exception handling @ExceptionHandler");
        
        Error error = new Error(ex.getMessage());
        
        return new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    
    @ResponseStatus(value=HttpStatus.NOT_FOUND, reason="IOException occured")
	@ExceptionHandler(IOException.class)
	public String handleIOException(){
		//logger.error("IOException handler executed");
		//returning 404 error code
    	return "redirect:/login/user-login";
	}
}
