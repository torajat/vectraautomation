package com.mlab.endpoint.controller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Locale;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mlab.endpoint.Model.AttachedFile;
import com.mlab.endpoint.Model.CustomerVO;
import com.mlab.endpoint.Model.ProductVO;
import com.mlab.endpoint.Model.SalesReport;
import com.mlab.endpoint.Service.CustomerInformationImpl;
import com.mlab.endpoint.exception.ServiceException;
import com.mlab.endpoint.interfaces.DropDownMenuData;
import com.mlab.endpoint.interfaces.ProductInformationView;
import com.mlab.endpoint.interfaces.RegistrationI;
import com.mlab.endpoint.util.Menumodel;

@Controller
public class ProductController {

    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);

    private static String actionuser="suman";
    private static String customerId="1";

    
    @Autowired
    private ProductInformationView productinformationView;
    
    @Autowired
    private CustomerInformationImpl customerinformtion;
    
    
    @Value("${upload.folder}")
    private String uploadfilesothers;
    
    @Autowired
    private DropDownMenuData menuData;

    @ModelAttribute("SalesReport")
    private SalesReport constructComplainReport()
    {
     return new SalesReport();	
    }
    
	Menumodel mobject=new Menumodel();
   
    
    @ModelAttribute("attachedFile")
    private AttachedFile constractAttachedFile() {

        return new AttachedFile();
    }


    @ModelAttribute("customerVO")
    private CustomerVO constractCustomerVO() {

        return new CustomerVO();
    }
    

    @ModelAttribute("productVO")
    private ProductVO constractProductVO() {

        return new ProductVO();
    }
    

    
 @RequestMapping(value = " /product/manageproductcategory.do", method = RequestMethod.POST)
 public String ManageProductCategory(@ModelAttribute("productVO") ProductVO productVO, Locale locale, Model model,HttpSession session)
		throws ServiceException {
	 
	 
	 
			return "productsCategory";
    
}

 
 @RequestMapping(value = "/product/manageproductcategory.do", method = RequestMethod.GET)

 public String ManaageCat(@ModelAttribute("productVO") ProductVO productVO, Locale locale, Model model,HttpSession session)
		throws ServiceException {
	 
	 	model.addAttribute("title","Product Category");
	 	model.addAttribute("activityReport", productinformationView.getProductCategory("1"));
	 
	 return "productsCategory";
    
}

 
 @RequestMapping(value = "/product/createproductcategory.Do", method = RequestMethod.POST)
 public String CreateProductcategory(@ModelAttribute("productVO") ProductVO productVO,Model model,HttpSession session)throws ServiceException {
	 
	 System.out.println("Entering here"+productVO);
	 String customerId="1";
	 String res= productinformationView.createProductCategory(productVO, customerId,"a");

	 System.out.println("Product category funtion return:"+res);
	 
	 model.addAttribute(mobject.getConfigMenuItem(model));
		
	 model.addAttribute("title","Product Category");

	 if (res.equals("Success"))
	 {
		model.addAttribute("css", "success");
     	model.addAttribute("msg", "Created Successfully");
     //	model.addAttribute("activityReport", productinformationView.getProductCategory(1));
 		return "managecategory";

	 }
	 else
	 {
		 
		model.addAttribute("css", "danger");
     	model.addAttribute("msg", res);
     	
		 return "managecategory";
		 
	 }
	 
    
}
 
 
 
 @RequestMapping(value = "/product/createproductcategory.do", method = RequestMethod.GET)
 public String CreateProductcategoryGET(Locale locale, Model model,HttpSession session)throws ServiceException {
	 	 
	 model.addAttribute("title","Product Category");
	 	//model.addAttribute("activityReport", productinformationView.getProductCategory(1));
	 
	 return "managecategory";
    
}
 
 
 
 @RequestMapping(value = "/product/CreateProduct.Do", method = RequestMethod.POST)
 public String CreateProduct(@ModelAttribute("productVO") ProductVO productVO,Model model,HttpSession session)throws ServiceException {


	 if (session.getAttribute("customerid")==null){return "login";}   

	 System.out.println("Create Product"+productVO);
	 
	 String customerId=(String) session.getAttribute("customerid");
			 
	 String res= productinformationView.createProduct(productVO, customerId,(String) session.getAttribute("username"));
	 model.addAttribute(mobject.getConfigMenuItem(model));

	 System.out.println("Create Product:"+res);
	 
	 model.addAttribute("title","Create Product");
	 model.addAttribute("btntitle","Create Product");
	 model.addAttribute("actionval","/product/CreateProduct.Do");
	 model.addAttribute("methodval", "POST");
	 model.addAttribute("productVOval","productVO");


	 if (res.equals("Success"))
	 {
		model.addAttribute("css", "success");
     	model.addAttribute("msg", "Created Successfully");
     //	model.addAttribute("activityReport", productinformationView.getProductCategory(1));
 		return "createproduct";

	 }
	 else
	 {
		 
		model.addAttribute("css", "danger");
     	model.addAttribute("msg", res);
     	
		 return "createproduct";
		 
	 }
	 
    
}
 

 
 @RequestMapping(value = "/product/CreateProduct.Do", method = RequestMethod.GET)
 public String CreateProduct_GET(@ModelAttribute("productVO") ProductVO productVO,Model model,HttpSession session)throws ServiceException
 {
	 
	model.addAttribute("title","Create Product");
    model.addAttribute("actionval","/product/CreateProduct.Do");
	model.addAttribute("methodval", "POST");
	model.addAttribute("productVOval","productVO");
	model.addAttribute("btntitle","Create Product");
	
	 model.addAttribute(mobject.getConfigMenuItem(model));

	 return "createproduct";
	 
    
 	}

 

 @RequestMapping(value = "/product/viewprodcutlist.do", method = RequestMethod.GET)
 public String ViewProductList_GET(@ModelAttribute("productVO") ProductVO productVO,Model model,HttpSession session)throws ServiceException
 {
	 

	 if (session.getAttribute("customerid")==null){return "login";}   
	 
	 String css = (String)model.asMap().get("css");
	 String msg = (String)model.asMap().get("msg");
	    
	    if (!"".equals(msg))
	    {
	   	 model.addAttribute("css",css);
	   	 model.addAttribute("msg",msg);
	    }
		 model.addAttribute(mobject.getConfigMenuItem(model));

	 
	 String customerid=(String) session.getAttribute("customerid");
		
		model.addAttribute("productlist", productinformationView.getProductList(customerid));
		 
		 model.addAttribute("modelVOval","productVO");
			model.addAttribute("unitaction","/product/updateprice");
			model.addAttribute("namaction","/product/productname");
			model.addAttribute("descriptionaction","/product/productdescription");
			model.addAttribute("statusaction","/product/productstatus");
			model.addAttribute("codeaction","/product/productcode");
		
		return "viewproductlist";
	 
    
 	}



@RequestMapping(value = "/product/updateprice", method = RequestMethod.POST)
 public String Product_Updateprice_POST(@ModelAttribute("productVO") ProductVO productVO,
		  Model model,HttpSession session,RedirectAttributes rmodel)throws ServiceException
 		{

			System.out.println("Product NO"+productVO.getProductNo());
		
			System.out.println("It is entering here"+productVO);
			
			String customerid=(String) session.getAttribute("customerid");
			 model.addAttribute(mobject.getConfigMenuItem(model));

			String res=productinformationView.UpdateProductByField(productVO, customerid,"unitprice",productVO.getUnitPrice(), actionuser);
			
			 if (res.equals("Success"))
			 {
				rmodel.addFlashAttribute("css", "success");
		     	rmodel.addFlashAttribute("msg", "Created Successfully");
		     	return "redirect:/product/viewprodcutlist.do";

			 }
			 else
			 {
				 
				rmodel.addFlashAttribute("css", "danger");
		     	rmodel.addFlashAttribute("msg", res);
		     	
		     	return "redirect:/product/viewprodcutlist.do";
				 
			 }
			
			
			

    
 	}



@RequestMapping(value = "/product/productcode", method = RequestMethod.POST)
 public String Product_ProductCode_POST(@ModelAttribute("productVO") ProductVO productVO,
		  Model model,HttpSession session,RedirectAttributes rmodel)throws ServiceException
 		{

			System.out.println("Product NO"+productVO.getProductNo());
		
			System.out.println("It is entering here"+productVO);
			
			String customerid=(String) session.getAttribute("customerid");
			 model.addAttribute(mobject.getConfigMenuItem(model));

			
			String res=productinformationView.UpdateProductByField(productVO, customerid,"code",productVO.getProductDescription(), actionuser);
			
			 if (res.equals("Success"))
			 {
				rmodel.addFlashAttribute("css", "success");
		     	rmodel.addFlashAttribute("msg", "Update Successfully");
		     	return "redirect:/product/viewprodcutlist.do";

			 }
			 else
			 {
				 
				rmodel.addFlashAttribute("css", "danger");
		     	rmodel.addFlashAttribute("msg", res);
		     	
		     	return "redirect:/product/viewprodcutlist.do";
				 
			 }
			
			
			

    
 	}




@RequestMapping(value = "/product/productdescription", method = RequestMethod.POST)
 public String Product_ProductDescription_POST(@ModelAttribute("productVO") ProductVO productVO,
		  Model model,HttpSession session,RedirectAttributes rmodel)throws ServiceException
 		{

			System.out.println("Product NO"+productVO.getProductNo());
		
			System.out.println("It is entering here"+productVO);
			String customerid=(String) session.getAttribute("customerid");
			
			 model.addAttribute(mobject.getConfigMenuItem(model));

			String res=productinformationView.UpdateProductByField(productVO, customerid,"description",productVO.getProductDescription(), actionuser);
			
			 if (res.equals("Success"))
			 {
				rmodel.addFlashAttribute("css", "success");
		     	rmodel.addFlashAttribute("msg", "Update Successfully");
		     	return "redirect:/product/viewprodcutlist.do";

			 }
			 else
			 {
				 
				rmodel.addFlashAttribute("css", "danger");
		     	rmodel.addFlashAttribute("msg", res);
		     	
		     	return "redirect:/product/viewprodcutlist.do";
				 
			 }
			
			
			

    
 	}



@RequestMapping(value = "/product/productname", method = RequestMethod.POST)
public String Product_ProductName_POST(@ModelAttribute("productVO") ProductVO productVO,
		  Model model,HttpSession session,RedirectAttributes rmodel)throws ServiceException
		{

			System.out.println("Product NO"+productVO.getProductNo());
		
			System.out.println("It is entering here"+productVO);
			String customerid=(String) session.getAttribute("customerid");
			 model.addAttribute(mobject.getConfigMenuItem(model));

			
			String res=productinformationView.UpdateProductByField(productVO, customerid,"name",productVO.getProductname(), actionuser);
			
			 if (res.equals("Success"))
			 {
				rmodel.addFlashAttribute("css", "success");
		     	rmodel.addFlashAttribute("msg", "Update Successfully");
		     	return "redirect:/product/viewprodcutlist.do";

			 }
			 else
			 {
				 
				rmodel.addFlashAttribute("css", "danger");
		     	rmodel.addFlashAttribute("msg", res);
		     	
		     	return "redirect:/product/viewprodcutlist.do";
				 
			 }
			
			
			

   
	}


 
@RequestMapping(value = "/product/StockInProduct.Do", method = RequestMethod.GET)
 public String Product_StockIn_GET(@ModelAttribute("productVO") ProductVO productVO,
		 
		 @RequestParam("productno") String productNo,
		 @RequestParam("currentunitprice") String unitPrice,
		 @RequestParam("currentStock") String stockinHand,
		 @RequestParam("productname") String productName,
		 @RequestParam("productcode") String productcode,
		 @RequestParam("flg") String flg,
		 		 
		 Model model,HttpSession session)throws ServiceException
 		{
	 
			model.addAttribute("title","Create Product");
		    model.addAttribute("actionval","/product/StockInProduct.Do");
			model.addAttribute("methodval", "POST");
			
			model.addAttribute("productVOval","productVO");
			model.addAttribute("btntitle","Create Product");
			model.addAttribute("productNo",productNo);
			model.addAttribute("productName",productName);
			model.addAttribute("productcode",productcode);
			model.addAttribute("currentstock",stockinHand);
			model.addAttribute("currentprice",unitPrice);
			
	 return "stockin";
	 
    
 	}




@RequestMapping(value = "/product/addtocart", method = RequestMethod.POST)
 public String Product_AddToCart(@ModelAttribute("productVO") ProductVO productVO,
		  Model model,HttpSession session)throws ServiceException
 		{

		

			   session.setAttribute("mobile", "1");
			   session.setAttribute("cartid", UUID.randomUUID().toString());
			  // session.setAttribute("carti", "1");
			   	
			
			String res=productinformationView.StackIn(productVO, customerId, actionuser);
			
			 if (res.equals("Success"))
			 {
				model.addAttribute("css", "success");
		     	model.addAttribute("msg", "Created Successfully");
		     //	model.addAttribute("activityReport", productinformationView.getProductCategory(1));
		     	return "redirect:/product/viewprodcutlist.do";

			 }
			 else
			 {
				 
				model.addAttribute("css", "danger");
		     	model.addAttribute("msg", res);
		     	
				 return "stockin";
				 
			 }
			
			

    
 	}




@RequestMapping(value = "/product/StockInProduct.Do", method = RequestMethod.POST)
 public String Product_StockIn_POST(@ModelAttribute("productVO") ProductVO productVO,
		  Model model,HttpSession session)throws ServiceException
 		{

			System.out.println("Product NO"+productVO.getProductNo());
		
			System.out.println("It is entering here"+productVO);
			
			
			String res=productinformationView.StackIn(productVO, customerId, actionuser);
			
			 if (res.equals("Success"))
			 {
				model.addAttribute("css", "success");
		     	model.addAttribute("msg", "Created Successfully");
		     //	model.addAttribute("activityReport", productinformationView.getProductCategory(1));
		     	return "redirect:/product/viewprodcutlist.do";

			 }
			 else
			 {
				 
				model.addAttribute("css", "danger");
		     	model.addAttribute("msg", res);
		     	
				 return "stockin";
				 
			 }
			
			
			/*	model.addAttribute("title","Create Product");
		    model.addAttribute("actionval","/product/CreateProduct.Do");
			model.addAttribute("methodval", "POST");
			model.addAttribute("productVOval","productVO");
			model.addAttribute("btntitle","Create Product");
		*/
			//redirect:// "viewproductlist";
			
			

    
 	}


//upload product


@RequestMapping(value = "/product/uploadimage.Do", method = RequestMethod.GET)
public String UploadProductImage_GET(@ModelAttribute("productVO") ProductVO productVO,
		 @RequestParam("productno") String productNo,
		 @RequestParam("flg") String flg,
		 Model model,HttpSession session)throws ServiceException
{
	 
	 model.addAttribute(mobject.getConfigMenuItem(model));

	model.addAttribute("productno",productNo);
	model.addAttribute("title","Upload");
	model.addAttribute("actionval","/product/uploadimage.Do");
	model.addAttribute("methodval", "POST");
	model.addAttribute("modelVOval","productVO");
	model.addAttribute("btntitle","Upload Product");
	
	model.addAttribute("actiondeleteval","/product/deleteimage");
	 String customerid=(String) session.getAttribute("customerid");
		
	model.addAttribute("productimagelist", productinformationView.getProductImageList(productNo,customerid));
	
	/* File serverFile = new File("I:\\logo\\" +"1.PNG");

		System.out.println("read complete");
		
		
		byte[] img = null;
		try {
			img = Files.readAllBytes(serverFile.toPath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	  String encodedString = new String(img);

		ModelMap map = new ModelMap();
		map.put("img", encodedString);
		
*/		

	
	 return "uploadimage";
	 
   
	}





@RequestMapping(value = "/product/uploadimage.Do", method = RequestMethod.POST)
public String UploadProductImage_POST(@ModelAttribute("productVO") ProductVO productVO,
		 Model model,HttpSession session,RedirectAttributes rmodel)throws ServiceException
{

	 if (session.getAttribute("customerid")==null){return "login";}   
	 model.addAttribute(mobject.getConfigMenuItem(model));

	
	model.addAttribute("productno",productVO.getProductNo());
	model.addAttribute("title","Upload Image");
	model.addAttribute("actionval","/product/uploadimage.Do");
	model.addAttribute("methodval", "POST");
	model.addAttribute("modelVOval","productVO");
	model.addAttribute("btntitle","Upload Product");
	
	 String customerid=(String) session.getAttribute("customerid");
		
	
	String res=productinformationView.UploadAttachment(productVO, actionuser,customerid);

	model.addAttribute("image",res);
	
	model.addAttribute("productimagelist", productinformationView.getProductImageList(productVO.getProductNo(),customerid));
	

	
	 if (res.equals("Success"))
	 {
		rmodel.addFlashAttribute("css", "success");
    	rmodel.addFlashAttribute("msg", "Deleted Successfully");
    	return "redirect:/product/viewprodcutlist.do";

	 }
	 else
	 {
		 
		model.addAttribute("css", "danger");
    	model.addAttribute("msg", res);
    	
    	 return "uploadimage";
    			 
	 }
	
	 
   
	}



@RequestMapping(value = "/product/image.Do")
@ResponseBody
public byte[] getImage() throws IOException {

    File serverFile = new File(uploadfilesothers + '1' + ".jpg");

    return Files.readAllBytes(serverFile.toPath());
}



@RequestMapping(value="/getUserImage/{id}")
public void getUserImage(HttpServletResponse response , @PathVariable("id") int tweetID) throws IOException{

	response.setContentType("image/jpeg");
  File serverFile = new File(uploadfilesothers + '1' + ".jpg");
  byte[] buffer = Files.readAllBytes(serverFile.toPath());
  InputStream in1 = new ByteArrayInputStream(buffer);
  IOUtils.copy(in1, response.getOutputStream());        
}



@RequestMapping(value = "/product/productimagelist.do/{id}", method = RequestMethod.GET)
public String ProductImageList_GET(@ModelAttribute("productVO") ProductVO productVO,
		 @PathVariable("id") String productid, Model model,HttpSession session)throws ServiceException
{
	
	model.addAttribute("title","Customer");
    model.addAttribute("actionval","/product/viewprodcutlist.do");
	model.addAttribute("methodval", "POST");
	model.addAttribute("productVOval","customerVO");
	model.addAttribute("btntitle","Create Customer");
	model.addAttribute("customer","/product/viewprodcutlist.do");
	 String customerid=(String) session.getAttribute("customerid");
	 model.addAttribute(mobject.getConfigMenuItem(model));

		
	
	model.addAttribute("productimagelist", productinformationView.getProductImageList(productid,customerid));
		 
		return "viewproductimage";
	 
   
	}



@RequestMapping(value = "/customer/viewlistofcustomer.do", method = RequestMethod.GET)
public String Transection_GET(@ModelAttribute("productVO") ProductVO productVO,Model model,HttpSession session)throws ServiceException
{
	
	model.addAttribute("title","Customer");
    model.addAttribute("actionval","/product/CreateProduct.Do");
	model.addAttribute("methodval", "POST");
	model.addAttribute("productVOval","customerVO");
	model.addAttribute("btntitle","Create Customer");
	model.addAttribute("customer","/customer/createcustomer.do");
	
	model.addAttribute("customerlist", customerinformtion.getListofCustomer(customerId));
	 model.addAttribute(mobject.getConfigMenuItem(model));

		return "viewlistcustomer";
	 
   
	}



@RequestMapping(value = "/customer/createcustomer.do", method = RequestMethod.GET)
public String createCustomer_GET(@ModelAttribute("productVO") ProductVO productVO,Model model,HttpSession session)throws ServiceException
{
	
	model.addAttribute("title","Create Customer");
    model.addAttribute("actionval","/customer/createcustomer.do");
	model.addAttribute("methodval", "POST");
	model.addAttribute("voName","customerVO");
	model.addAttribute("btntitle","Create Customer");
	model.addAttribute("customer","/customer/createcustomer.do");

	 model.addAttribute(mobject.getConfigMenuItem(model));

		//model.addAttribute("productlist", productinformationView.getProductList(1));
		 
		return "createcustomer";
	 
   
}



@RequestMapping(value = "/customer/createcustomer.do", method = RequestMethod.POST)
public String createCustomer_POST(@ModelAttribute("customerVO") CustomerVO customerVO,Model model,HttpSession session)throws ServiceException
{
	
	
	ModelAndView mv = new ModelAndView();
	 
	 logger.info("Entering Here"+customerVO);
	 
	 System.out.println("Here customerVO"+customerVO);
	 
	
	model.addAttribute("title","Create Customer");
    model.addAttribute("actionval","/customer/createcustomerInformation.do");
	model.addAttribute("methodval", "POST");
	model.addAttribute("voName","customerVO");
	model.addAttribute("btntitle","Create Customer");
	model.addAttribute("customer","/customer/createcustomer.do");
	
	String mobileNumber=customerVO.getMobileNumber();
	
	String customeridentificationno=customerVO.getCustomeridentificationo();
	
	ArrayList<CustomerVO> customerdetails=customerinformtion.checkCustomer(customerId, mobileNumber, customeridentificationno);
	 model.addAttribute(mobject.getConfigMenuItem(model));

	
	if(customerdetails.size()>0)
	{

		model.addAttribute("customerlist",customerdetails);
		return "viewlistcustomer";
	}
	
	else
	{
	
	///	System.out.println("RamNumber"+customerinformtion.GetRandomNumber());
		
		//model.addAttribute("customeridentificationnumber",customerinformtion.GetRandomNumber());
		
		model.addAttribute("mobileNo",mobileNumber);
		
		return "createcustomerinfo";
		
	}
	
	
	//model.addAttribute("productlist", productinformationView.getProductList(1));
		 
	 
   
}





@RequestMapping(value = "/customer/createcustomerInformation.do", method = RequestMethod.POST)
public String createCustomerInformation_POST(@ModelAttribute("customerVO") CustomerVO customerVO,Model model,HttpSession session)throws ServiceException
{
	
	
	ModelAndView mv = new ModelAndView();
	 
	 logger.info("Entering Here"+customerVO);
	 
	 System.out.println("Here customerVO"+customerVO);
	 
	
	model.addAttribute("title","Create Customer");
    model.addAttribute("actionval","/customer/createcustomerInformation.do");
	model.addAttribute("methodval", "POST");
	model.addAttribute("voName","customerVO");
	model.addAttribute("btntitle","Create Customer");
	model.addAttribute("customer","/customer/createcustomer.do");
		
	//model.addAttribute("productlist", productinformationView.getProductList(1));
	String res=customerinformtion.createCustomer(customerVO, customerId,actionuser);
	
	
	 if (res.equals("Success"))
	 {
		model.addAttribute("css", "success");
    	model.addAttribute("msg", "Created Successfully");
    //	model.addAttribute("activityReport", productinformationView.getProductCategory(1));
    	return "redirect://POS/Transection.do";

	 }
	 else
	 {
		 
		model.addAttribute("css", "danger");
    	model.addAttribute("msg", res);
    	
    	model.addAttribute("mobileNo",	customerVO.getMobileNumber());
		
		return "createcustomerinfo";
		 
	 }
	 

   
}





@RequestMapping(value = "/employee/todaysmenu.do", method = RequestMethod.GET)
public String ViewTodaysMenu_GET(@ModelAttribute("productVO") ProductVO productVO,Model model,HttpSession session)throws ServiceException
{
	 
	 
		model.addAttribute("productlist", productinformationView.getProductList("1"));
		 
		return "viewproductlist";
	 
   
	}





///product/manageproduct?productno=${report.productNo}&flg=#manage


@RequestMapping(value = "/product/manageproduct/{id}", method = RequestMethod.GET)
public String Manage_Delete_Product(@ModelAttribute("productVO") ProductVO productVO,
		 @PathVariable("id") String productid,RedirectAttributes rmodel,
		
		 Model model,HttpSession session)throws ServiceException
{


	String customerid=(String) session.getAttribute("customerid");
	String actionuser=(String) session.getAttribute("username");
	
	ProductVO myproductVO = new ProductVO();
	myproductVO.setProductNo(productid);
	 model.addAttribute(mobject.getConfigMenuItem(model));

	
	String res=productinformationView.UpdateProductByField(productVO, customerid,"delete",productid, actionuser);
	
	 if (res.equals("Success"))
	 {
		rmodel.addFlashAttribute("css", "success");
    	rmodel.addFlashAttribute("msg", "Deleted Successfully");
    	return "redirect:/product/viewprodcutlist.do";

	 }
	 else
	 {
		 
		rmodel.addFlashAttribute("css", "danger");
    	rmodel.addFlashAttribute("msg", res);
    	
    	return "redirect:/product/viewprodcutlist.do";
		 
	 }
	 
   
	}




@RequestMapping(value = "/product/deleteimage", method = RequestMethod.POST)
public String Manage_DeleteImage_Product(@ModelAttribute("productVO") ProductVO productVO,
		RedirectAttributes rmodel,
 Model model,HttpSession session)throws ServiceException
	{


	String customerid=(String) session.getAttribute("customerid");
	String actionuser=(String) session.getAttribute("username");
	 model.addAttribute(mobject.getConfigMenuItem(model));

	String res=productinformationView.DeleteImage(productVO, customerid, actionuser);
	
	 if (res.equals("Success"))
	 {
		rmodel.addFlashAttribute("css", "success");
    	rmodel.addFlashAttribute("msg", "Deleted Successfully");
    	return "redirect:/product/viewprodcutlist.do";

	 }
	 else
	 {
		 
		rmodel.addFlashAttribute("css", "danger");
    	rmodel.addFlashAttribute("msg", res);
    	
    	return "redirect:/product/viewprodcutlist.do";
		 
	 }
	 
   
	}



@RequestMapping(value = "/POS/Transection.do", method = RequestMethod.GET)

public String Transection_GET(Model model,HttpSession session)throws ServiceException
{
	
	
	ModelAndView mv = new ModelAndView();
	 
	
	model.addAttribute("title","Create Customer");
    model.addAttribute("actionval","/customer/createcustomerInformation.do");
	model.addAttribute("methodval", "POST");
	model.addAttribute("voName","customerVO");
	model.addAttribute("btntitle","Create Customer");
	model.addAttribute("customer","/customer/createcustomer.do");
		
	return "checkout_viewproductlist";/*
	//model.addAttribute("productlist", productinformationView.getProductList(1));
	String res=customerinformtion.createCustomer(customerVO, customerId,actionuser);
	
	
	 if (res.equals("Success"))
	 {
		model.addAttribute("css", "success");
    	model.addAttribute("msg", "Created Successfully");
    //	model.addAttribute("activityReport", productinformationView.getProductCategory(1));
    	return "checkout_viewproductlist";

	 }
	 else
	 {
		 
		model.addAttribute("css", "danger");
    	model.addAttribute("msg", res);
    	
    	model.addAttribute("mobileNo",	customerVO.getMobileNumber());
		
		return "createcustomerinfo";
		 
	 }
*/	 

   
}




}
