package com.mlab.endpoint.Model;

public class UserVO {

	private String middleName;
	private String lastName;
	private String username;
	private String password; 

	private String phoneNo;
	private String type;
	private String designation;
	private String uuid;
	private String roleId;
	private String flg;
	private String email;
	private String status;
	private String customerid;
	private String firstName;
	
	private String displayname;
	private String customeraname;
	
	private String statusid;
	
	
	
	public String getStatusid() {
		return statusid;
	}
	public void setStatusid(String statusid) {
		this.statusid = statusid;
	}
	public String getCustomeraname() {
		return customeraname;
	}
	public void setCustomeraname(String customeraname) {
		this.customeraname = customeraname;
	}
	public String getDisplayname() {
		return displayname;
	}
	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getFlg() {
		return flg;
	}
	public void setFlg(String flg) {
		this.flg = flg;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCustomerid() {
		return customerid;
	}
	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}
	@Override
	public String toString() {
		return "UserVO [middleName=" + middleName + ", lastName=" + lastName + ", username=" + username + ", password="
				+ password + ", phoneNo=" + phoneNo + ", type=" + type + ", designation=" + designation + ", uuid="
				+ uuid + ", roleId=" + roleId + ", flg=" + flg + ", email=" + email + ", status=" + status
				+ ", customerid=" + customerid + ", firstName=" + firstName + ", displayname=" + displayname
				+ ", customeraname=" + customeraname + ", statusid=" + statusid + "]";
	}
	
	
	
	
}
