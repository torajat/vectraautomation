package com.mlab.endpoint.Model;

public class TableVO {
	
	
	private String tablename;
	private String tableno;
	private String capacity;
	private String customerId;
	private String deviceId;
	private String devicestatus;
	private String orgId;
	private String organizationame;
	private String createdby,createdate,lasteditby,lasteditdate;
	private String devicemodel;
	private String devicemac;
	private String vendor;
	private String devicesupplier;
	private String warrenty;
	private String registrationdate;
	private String devicerowid;
	private String opsflg;
	
	
	
	
	
	
	public String getOpsflg() {
		return opsflg;
	}
	public void setOpsflg(String opsflg) {
		this.opsflg = opsflg;
	}
	public String getDevicemodel() {
		return devicemodel;
	}
	public void setDevicemodel(String devicemodel) {
		this.devicemodel = devicemodel;
	}
	public String getDevicemac() {
		return devicemac;
	}
	public void setDevicemac(String devicemac) {
		this.devicemac = devicemac;
	}
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	public String getDevicesupplier() {
		return devicesupplier;
	}
	public void setDevicesupplier(String devicesupplier) {
		this.devicesupplier = devicesupplier;
	}
	public String getWarrenty() {
		return warrenty;
	}
	public void setWarrenty(String warrenty) {
		this.warrenty = warrenty;
	}
	public String getRegistrationdate() {
		return registrationdate;
	}
	public void setRegistrationdate(String registrationdate) {
		this.registrationdate = registrationdate;
	}
	public String getDevicerowid() {
		return devicerowid;
	}
	public void setDevicerowid(String devicerowid) {
		this.devicerowid = devicerowid;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public String getCreatedate() {
		return createdate;
	}
	public void setCreatedate(String createdate) {
		this.createdate = createdate;
	}
	public String getLasteditby() {
		return lasteditby;
	}
	public void setLasteditby(String lasteditby) {
		this.lasteditby = lasteditby;
	}
	public String getLasteditdate() {
		return lasteditdate;
	}
	public void setLasteditdate(String lasteditdate) {
		this.lasteditdate = lasteditdate;
	}
	public String getOrganizationame() {
		return organizationame;
	}
	public void setOrganizationame(String organizationame) {
		this.organizationame = organizationame;
	}
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getDevicestatus() {
		return devicestatus;
	}
	public void setDevicestatus(String devicestatus) {
		this.devicestatus = devicestatus;
	}
	public String getTablename() {
		return tablename;
	}
	public void setTablename(String tablename) {
		this.tablename = tablename;
	}
	public String getTableno() {
		return tableno;
	}
	public void setTableno(String tableno) {
		this.tableno = tableno;
	}
	public String getCapacity() {
		return capacity;
	}
	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}
	
	
	
	@Override
	public String toString() {
		return "TableVO [tablename=" + tablename + ", tableno=" + tableno + ", capacity=" + capacity + ", customerId="
				+ customerId + ", deviceId=" + deviceId + ", devicestatus=" + devicestatus + ", orgId=" + orgId
				+ ", organizationame=" + organizationame + ", createdby=" + createdby + ", createdate=" + createdate
				+ ", lasteditby=" + lasteditby + ", lasteditdate=" + lasteditdate + ", devicemodel=" + devicemodel
				+ ", devicemac=" + devicemac + ", vendor=" + vendor + ", devicesupplier=" + devicesupplier
				+ ", warrenty=" + warrenty + ", registrationdate=" + registrationdate + ", devicerowid=" + devicerowid
				+ ", opsflg=" + opsflg + "]";
	}
	
	

}
