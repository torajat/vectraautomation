package com.mlab.endpoint.Model;

public class OrganizationVO {
	
	private String organizationame;
	private String orId,licenseno,orgstatus,ref_customerid,createdby,createdate,lasteditby,lasteditdate;
	
	private String area,address,foodtype;
	private String locid;
	private String cityid;
	private String cityname;
	private String customerid;
	private String previousname;
	private String areaid;
	private String tableid;
	private String tableno;
	private String orgimageurl;
	private String capacity;
	
	private String bookingdate;
	private String startime;
	private String endtime;
	private String mobileno;
	private String name;
	private String id;
	private String tablename;
	private String status;
	
	
	
	
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTablename() {
		return tablename;
	}
	public void setTablename(String tablename) {
		this.tablename = tablename;
	}
	public String getMobileno() {
		return mobileno;
	}
	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBookingdate() {
		return bookingdate;
	}
	public void setBookingdate(String bookingdate) {
		this.bookingdate = bookingdate;
	}
	public String getStartime() {
		return startime;
	}
	public void setStartime(String startime) {
		this.startime = startime;
	}
	public String getEndtime() {
		return endtime;
	}
	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}
	public String getCapacity() {
		return capacity;
	}
	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}
	public String getTableid() {
		return tableid;
	}
	public void setTableid(String tableid) {
		this.tableid = tableid;
	}
	public String getTableno() {
		return tableno;
	}
	public void setTableno(String tableno) {
		this.tableno = tableno;
	}
	public String getOrgimageurl() {
		return orgimageurl;
	}
	public void setOrgimageurl(String orgimageurl) {
		this.orgimageurl = orgimageurl;
	}
	public String getAreaid() {
		return areaid;
	}
	public void setAreaid(String areaid) {
		this.areaid = areaid;
	}
	public String getPreviousname() {
		return previousname;
	}
	public void setPreviousname(String previousname) {
		this.previousname = previousname;
	}
	public String getCustomerid() {
		return customerid;
	}
	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}
	public String getCityid() {
		return cityid;
	}
	public void setCityid(String cityid) {
		this.cityid = cityid;
	}
	public String getCityname() {
		return cityname;
	}
	public void setCityname(String cityname) {
		this.cityname = cityname;
	}
	public String getLocid() {
		return locid;
	}
	public void setLocid(String locid) {
		this.locid = locid;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getFoodtype() {
		return foodtype;
	}
	public void setFoodtype(String foodtype) {
		this.foodtype = foodtype;
	}
	public String getOrganizationame() 
	{
		return organizationame;
	}
	public void setOrganizationame(String organizationame) {
		this.organizationame = organizationame;
	}
	public String getOrId() {
		return orId;
	}
	public void setOrId(String orId) {
		this.orId = orId;
	}
	public String getLicenseno() {
		return licenseno;
	}
	public void setLicenseno(String licenseno) {
		this.licenseno = licenseno;
	}
	public String getOrgstatus() {
		return orgstatus;
	}
	public void setOrgstatus(String orgstatus) {
		this.orgstatus = orgstatus;
	}
	public String getRef_customerid() {
		return ref_customerid;
	}
	public void setRef_customerid(String ref_customerid) {
		this.ref_customerid = ref_customerid;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public String getCreatedate() {
		return createdate;
	}
	public void setCreatedate(String createdate) {
		this.createdate = createdate;
	}
	public String getLasteditby() {
		return lasteditby;
	}
	public void setLasteditby(String lasteditby) {
		this.lasteditby = lasteditby;
	}
	public String getLasteditdate() {
		return lasteditdate;
	}
	public void setLasteditdate(String lasteditdate) {
		this.lasteditdate = lasteditdate;
	}
	
	@Override
	public String toString() {
		return "OrganizationVO [organizationame=" + organizationame + ", orId=" + orId + ", licenseno=" + licenseno
				+ ", orgstatus=" + orgstatus + ", ref_customerid=" + ref_customerid + ", createdby=" + createdby
				+ ", createdate=" + createdate + ", lasteditby=" + lasteditby + ", lasteditdate=" + lasteditdate
				+ ", area=" + area + ", address=" + address + ", foodtype=" + foodtype + ", locid=" + locid
				+ ", cityid=" + cityid + ", cityname=" + cityname + ", customerid=" + customerid + ", previousname="
				+ previousname + ", areaid=" + areaid + ", tableid=" + tableid + ", tableno=" + tableno
				+ ", orgimageurl=" + orgimageurl + ", capacity=" + capacity + ", bookingdate=" + bookingdate
				+ ", startime=" + startime + ", endtime=" + endtime + ", mobileno=" + mobileno + ", name=" + name
				+ ", id=" + id + ", tablename=" + tablename + ", status=" + status + "]";
	}
	
	
	

}
