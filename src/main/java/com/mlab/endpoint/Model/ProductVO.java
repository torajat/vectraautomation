package com.mlab.endpoint.Model;

import org.springframework.web.multipart.MultipartFile;

public class ProductVO {
	
	
	private String companyname;
	private String instanceid;
	private String crId,ftpmappingpath;
	private String wId,workstationame;
	private String InrId,region,hostname,privateip,state;
	private String jId,ref_instanceid,designerworkstation,toolname,workingunit,ref_companyid,processingstatus,duration,operationflg,jobdate,ref_customerid,inque,schedule,running,complete,ready;
	private String parentpath;
	
	private String qjobid, qstatus,requiredinstance,processingpath,sweepdatapath,quedate;
	private String textfilepath;
	private String flg;
	private String datafilename;
	private String orderid;
	private String joblistIdval;
	
	private String jobrowid;
	
	private String tooldatapath;
	private String csdestionation;
	private String joborder;
	private String copysourcefolder;
	private String passtime;
	private String transferbtn;
	private String instancename;
	
	private String adalg;
	private String unitalg;
	private String corealg;
	private String processname;
	private String auto2d;
	
	private String nullproc;
	
	private String transferflg;
	
	private String publicip;
	
	
public String getPublicip() {
		return publicip;
	}
	public void setPublicip(String publicip) {
		this.publicip = publicip;
	}
public String getTransferflg() {
		return transferflg;
	}
	public void setTransferflg(String transferflg) {
		this.transferflg = transferflg;
	}
public String getNullproc() {
		return nullproc;
	}
	public void setNullproc(String nullproc) {
		this.nullproc = nullproc;
	}
public String getAuto2d() {
		return auto2d;
	}
	public void setAuto2d(String auto2d) {
		this.auto2d = auto2d;
	}
public String getAdalg() {
		return adalg;
	}
	public void setAdalg(String adalg) {
		this.adalg = adalg;
	}
	public String getUnitalg() {
		return unitalg;
	}
	public void setUnitalg(String unitalg) {
		this.unitalg = unitalg;
	}
	public String getCorealg() {
		return corealg;
	}
	public void setCorealg(String corealg) {
		this.corealg = corealg;
	}
	public String getProcessname() {
		return processname;
	}
	public void setProcessname(String processname) {
		this.processname = processname;
	}
public String getInstancename() {
		return instancename;
	}
	public void setInstancename(String instancename) {
		this.instancename = instancename;
	}
public String getTransferbtn() {
		return transferbtn;
	}
	public void setTransferbtn(String transferbtn) {
		this.transferbtn = transferbtn;
	}
public String getPasstime() {
		return passtime;
	}
	public void setPasstime(String passtime) {
		this.passtime = passtime;
	}
public String getCopysourcefolder() {
		return copysourcefolder;
	}
	public void setCopysourcefolder(String copysourcefolder) {
		this.copysourcefolder = copysourcefolder;
	}
public String getCsdestionation() {
		return csdestionation;
	}
	public void setCsdestionation(String csdestionation) {
		this.csdestionation = csdestionation;
	}
	public String getJoborder() {
		return joborder;
	}
	public void setJoborder(String joborder) {
		this.joborder = joborder;
	}
public String getTooldatapath() {
		return tooldatapath;
	}
	public void setTooldatapath(String tooldatapath) {
		this.tooldatapath = tooldatapath;
	}
public String getJobrowid() {
		return jobrowid;
	}
	public void setJobrowid(String jobrowid) {
		this.jobrowid = jobrowid;
	}
public String getDatafilename() {
		return datafilename;
	}
	public void setDatafilename(String datafilename) {
		this.datafilename = datafilename;
	}
	public String getOrderid() {
		return orderid;
	}
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	public String getJoblistIdval() {
		return joblistIdval;
	}
	public void setJoblistIdval(String joblistIdval) {
		this.joblistIdval = joblistIdval;
	}
public String getFlg() {
		return flg;
	}
	public void setFlg(String flg) {
		this.flg = flg;
	}
public String getTextfilepath() {
		return textfilepath;
	}
	public void setTextfilepath(String textfilepath) {
		this.textfilepath = textfilepath;
	}
public String getQjobid() {
		return qjobid;
	}
	public void setQjobid(String qjobid) {
		this.qjobid = qjobid;
	}
	public String getQstatus() {
		return qstatus;
	}
	public void setQstatus(String qstatus) {
		this.qstatus = qstatus;
	}
	public String getRequiredinstance() {
		return requiredinstance;
	}
	public void setRequiredinstance(String requiredinstance) {
		this.requiredinstance = requiredinstance;
	}
	public String getProcessingpath() {
		return processingpath;
	}
	public void setProcessingpath(String processingpath) {
		this.processingpath = processingpath;
	}
	public String getSweepdatapath() {
		return sweepdatapath;
	}
	public void setSweepdatapath(String sweepdatapath) {
		this.sweepdatapath = sweepdatapath;
	}
	public String getQuedate() {
		return quedate;
	}
	public void setQuedate(String quedate) {
		this.quedate = quedate;
	}
public String getParentpath() {
		return parentpath;
	}
	public void setParentpath(String parentpath) {
		this.parentpath = parentpath;
	}
public String getCompanyname() {
		return companyname;
	}
	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}
	public String getInstanceid() {
		return instanceid;
	}
	public void setInstanceid(String instanceid) {
		this.instanceid = instanceid;
	}
	public String getCrId() {
		return crId;
	}
	public void setCrId(String crId) {
		this.crId = crId;
	}
	public String getFtpmappingpath() {
		return ftpmappingpath;
	}
	public void setFtpmappingpath(String ftpmappingpath) {
		this.ftpmappingpath = ftpmappingpath;
	}
	public String getwId() {
		return wId;
	}
	public void setwId(String wId) {
		this.wId = wId;
	}
	public String getWorkstationame() {
		return workstationame;
	}
	public void setWorkstationame(String workstationame) {
		this.workstationame = workstationame;
	}
	public String getInrId() {
		return InrId;
	}
	public void setInrId(String inrId) {
		InrId = inrId;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getHostname() {
		return hostname;
	}
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	public String getPrivateip() {
		return privateip;
	}
	public void setPrivateip(String privateip) {
		this.privateip = privateip;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getjId() {
		return jId;
	}
	public void setjId(String jId) {
		this.jId = jId;
	}
	public String getRef_instanceid() {
		return ref_instanceid;
	}
	public void setRef_instanceid(String ref_instanceid) {
		this.ref_instanceid = ref_instanceid;
	}
	public String getDesignerworkstation() {
		return designerworkstation;
	}
	public void setDesignerworkstation(String designerworkstation) {
		this.designerworkstation = designerworkstation;
	}
	public String getToolname() {
		return toolname;
	}
	public void setToolname(String toolname) {
		this.toolname = toolname;
	}
	public String getWorkingunit() {
		return workingunit;
	}
	public void setWorkingunit(String workingunit) {
		this.workingunit = workingunit;
	}
	public String getRef_companyid() {
		return ref_companyid;
	}
	public void setRef_companyid(String ref_companyid) {
		this.ref_companyid = ref_companyid;
	}
	public String getProcessingstatus() {
		return processingstatus;
	}
	public void setProcessingstatus(String processingstatus) {
		this.processingstatus = processingstatus;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getOperationflg() {
		return operationflg;
	}
	public void setOperationflg(String operationflg) {
		this.operationflg = operationflg;
	}
	public String getJobdate() {
		return jobdate;
	}
	public void setJobdate(String jobdate) {
		this.jobdate = jobdate;
	}
	public String getRef_customerid() {
		return ref_customerid;
	}
	public void setRef_customerid(String ref_customerid) {
		this.ref_customerid = ref_customerid;
	}
	public String getInque() {
		return inque;
	}
	public void setInque(String inque) {
		this.inque = inque;
	}
	public String getSchedule() {
		return schedule;
	}
	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}
	public String getRunning() {
		return running;
	}
	public void setRunning(String running) {
		this.running = running;
	}
	public String getComplete() {
		return complete;
	}
	public void setComplete(String complete) {
		this.complete = complete;
	}
	public String getReady() {
		return ready;
	}
	public void setReady(String ready) {
		this.ready = ready;
	}
private String productNo;
private String productCode;
private String productDescription;
private String barcode;
private String unitPrice;
private String stocksOnHand;
private String categoryNo;
private String categoryName;
private String catstatus;
private String categoryDescription;
private String productname;
private String productwarrenty;
private String suppilername;
private String supplieruid;
private String newquantity;
private String totalstock;
private String status;
private String statusid;
private String locationid;
private String locationname;
private String menuname;
private String menuid;
private String starttime;
private String endtime;
private String cartid;
private String cartproductid;
private String cartquantity;
private String totalprice;
private String id;

private String customerid;

private String virtualid;

private String macid;
private String tableno;
private String orderdate;

private String quantity;

private String orderflg;



public String getOrderflg() {
	return orderflg;
}
public void setOrderflg(String orderflg) {
	this.orderflg = orderflg;
}
public String getQuantity() {
	return quantity;
}
public void setQuantity(String quantity) {
	this.quantity = quantity;
}
public String getMacid() {
	return macid;
}
public void setMacid(String macid) {
	this.macid = macid;
}
public String getTableno() {
	return tableno;
}
public void setTableno(String tableno) {
	this.tableno = tableno;
}
public String getOrderdate() {
	return orderdate;
}
public void setOrderdate(String orderdate) {
	this.orderdate = orderdate;
}
public String getVirtualid() {
	return virtualid;
}
public void setVirtualid(String virtualid) {
	this.virtualid = virtualid;
}
public String getCustomerid() {
	return customerid;
}
public void setCustomerid(String customerid) {
	this.customerid = customerid;
}
private MultipartFile file;

private String docid;
private String doctype;
private String docdescription;
private String filename;
private String fileextentsion;
private String filepath;
private String contenttype;






public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public MultipartFile getFile() {
	return file;
}
public void setFile(MultipartFile file) {
	this.file = file;
}
public String getDocid() {
	return docid;
}
public void setDocid(String docid) {
	this.docid = docid;
}
public String getDoctype() {
	return doctype;
}
public void setDoctype(String doctype) {
	this.doctype = doctype;
}
public String getDocdescription() {
	return docdescription;
}
public void setDocdescription(String docdescription) {
	this.docdescription = docdescription;
}
public String getFilename() {
	return filename;
}
public void setFilename(String filename) {
	this.filename = filename;
}
public String getFileextentsion() {
	return fileextentsion;
}
public void setFileextentsion(String fileextentsion) {
	this.fileextentsion = fileextentsion;
}
public String getFilepath() {
	return filepath;
}
public void setFilepath(String filepath) {
	this.filepath = filepath;
}
public String getContenttype() {
	return contenttype;
}
public void setContenttype(String contenttype) {
	this.contenttype = contenttype;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public String getStatusid() {
	return statusid;
}
public void setStatusid(String statusid) {
	this.statusid = statusid;
}
public String getLocationid() {
	return locationid;
}
public void setLocationid(String locationid) {
	this.locationid = locationid;
}
public String getLocationname() {
	return locationname;
}
public void setLocationname(String locationname) {
	this.locationname = locationname;
}
public String getMenuname() {
	return menuname;
}
public void setMenuname(String menuname) {
	this.menuname = menuname;
}
public String getMenuid() {
	return menuid;
}
public void setMenuid(String menuid) {
	this.menuid = menuid;
}
public String getStarttime() {
	return starttime;
}
public void setStarttime(String starttime) {
	this.starttime = starttime;
}
public String getEndtime() {
	return endtime;
}
public void setEndtime(String endtime) {
	this.endtime = endtime;
}
public String getCartid() {
	return cartid;
}
public void setCartid(String cartid) {
	this.cartid = cartid;
}
public String getCartproductid() {
	return cartproductid;
}
public void setCartproductid(String cartproductid) {
	this.cartproductid = cartproductid;
}
public String getCartquantity() {
	return cartquantity;
}
public void setCartquantity(String cartquantity) {
	this.cartquantity = cartquantity;
}

public String getTotalprice() {
	return totalprice;
}
public void setTotalprice(String totalprice) {
	this.totalprice = totalprice;
}
public String getNewquantity() {
	return newquantity;
}
public void setNewquantity(String newquantity) {
	this.newquantity = newquantity;
}
public String getTotalstock() {
	return totalstock;
}
public void setTotalstock(String totalstock) {
	this.totalstock = totalstock;
}

public String getProductNo() {
	return productNo;
}
public void setProductNo(String productNo) {
	this.productNo = productNo;
}
public String getProductCode() {
	return productCode;
}
public void setProductCode(String productCode) {
	this.productCode = productCode;
}
public String getProductDescription() {
	return productDescription;
}
public void setProductDescription(String productDescription) {
	this.productDescription = productDescription;
}
public String getBarcode() {
	return barcode;
}
public void setBarcode(String barcode) {
	this.barcode = barcode;
}
public String getUnitPrice() {
	return unitPrice;
}
public void setUnitPrice(String unitPrice) {
	this.unitPrice = unitPrice;
}
public String getStocksOnHand() {
	return stocksOnHand;
}
public void setStocksOnHand(String stocksOnHand) {
	this.stocksOnHand = stocksOnHand;
}
public String getCategoryNo() {
	return categoryNo;
}
public void setCategoryNo(String categoryNo) {
	this.categoryNo = categoryNo;
}
public String getCategoryName() {
	return categoryName;
}
public void setCategoryName(String categoryName) {
	this.categoryName = categoryName;
}
public String getCatstatus() {
	return catstatus;
}
public void setCatstatus(String catstatus) {
	this.catstatus = catstatus;
}
public String getCategoryDescription() {
	return categoryDescription;
}
public void setCategoryDescription(String categoryDescription) {
	this.categoryDescription = categoryDescription;
}
@Override
public String toString() {
	return "ProductVO [companyname=" + companyname + ", instanceid=" + instanceid + ", crId=" + crId
			+ ", ftpmappingpath=" + ftpmappingpath + ", wId=" + wId + ", workstationame=" + workstationame + ", InrId="
			+ InrId + ", region=" + region + ", hostname=" + hostname + ", privateip=" + privateip + ", state=" + state
			+ ", jId=" + jId + ", ref_instanceid=" + ref_instanceid + ", designerworkstation=" + designerworkstation
			+ ", toolname=" + toolname + ", workingunit=" + workingunit + ", ref_companyid=" + ref_companyid
			+ ", processingstatus=" + processingstatus + ", duration=" + duration + ", operationflg=" + operationflg
			+ ", jobdate=" + jobdate + ", ref_customerid=" + ref_customerid + ", inque=" + inque + ", schedule="
			+ schedule + ", running=" + running + ", complete=" + complete + ", ready=" + ready + ", parentpath="
			+ parentpath + ", qjobid=" + qjobid + ", qstatus=" + qstatus + ", requiredinstance=" + requiredinstance
			+ ", processingpath=" + processingpath + ", sweepdatapath=" + sweepdatapath + ", quedate=" + quedate
			+ ", textfilepath=" + textfilepath + ", flg=" + flg + ", datafilename=" + datafilename + ", orderid="
			+ orderid + ", joblistIdval=" + joblistIdval + ", jobrowid=" + jobrowid + ", tooldatapath=" + tooldatapath
			+ ", csdestionation=" + csdestionation + ", joborder=" + joborder + ", copysourcefolder=" + copysourcefolder
			+ ", passtime=" + passtime + ", transferbtn=" + transferbtn + ", instancename=" + instancename + ", adalg="
			+ adalg + ", unitalg=" + unitalg + ", corealg=" + corealg + ", processname=" + processname + ", auto2d="
			+ auto2d + ", nullproc=" + nullproc + ", transferflg=" + transferflg + ", publicip=" + publicip
			+ ", productNo=" + productNo + ", productCode=" + productCode + ", productDescription=" + productDescription
			+ ", barcode=" + barcode + ", unitPrice=" + unitPrice + ", stocksOnHand=" + stocksOnHand + ", categoryNo="
			+ categoryNo + ", categoryName=" + categoryName + ", catstatus=" + catstatus + ", categoryDescription="
			+ categoryDescription + ", productname=" + productname + ", productwarrenty=" + productwarrenty
			+ ", suppilername=" + suppilername + ", supplieruid=" + supplieruid + ", newquantity=" + newquantity
			+ ", totalstock=" + totalstock + ", status=" + status + ", statusid=" + statusid + ", locationid="
			+ locationid + ", locationname=" + locationname + ", menuname=" + menuname + ", menuid=" + menuid
			+ ", starttime=" + starttime + ", endtime=" + endtime + ", cartid=" + cartid + ", cartproductid="
			+ cartproductid + ", cartquantity=" + cartquantity + ", totalprice=" + totalprice + ", id=" + id
			+ ", customerid=" + customerid + ", virtualid=" + virtualid + ", macid=" + macid + ", tableno=" + tableno
			+ ", orderdate=" + orderdate + ", quantity=" + quantity + ", orderflg=" + orderflg + ", file=" + file
			+ ", docid=" + docid + ", doctype=" + doctype + ", docdescription=" + docdescription + ", filename="
			+ filename + ", fileextentsion=" + fileextentsion + ", filepath=" + filepath + ", contenttype="
			+ contenttype + "]";
}
public String getProductname() {
	return productname;
}
public void setProductname(String productname) {
	this.productname = productname;
}
public String getProductwarrenty() {
	return productwarrenty;
}
public void setProductwarrenty(String productwarrenty) {
	this.productwarrenty = productwarrenty;
}
public String getSuppilername() {
	return suppilername;
}
public void setSuppilername(String suppilername) {
	this.suppilername = suppilername;
}
public String getSupplieruid() {
	return supplieruid;
}
public void setSupplieruid(String supplieruid) {
	this.supplieruid = supplieruid;
}
	  
}
