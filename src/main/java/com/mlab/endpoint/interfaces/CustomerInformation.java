package com.mlab.endpoint.interfaces;

import java.util.ArrayList;
import java.util.Map;

import com.mlab.endpoint.Model.AttachedFile;
import com.mlab.endpoint.Model.ProductVO;
import com.mlab.endpoint.Model.TableVO;
import com.mlab.endpoint.Model.CustomerVO;
import com.mlab.endpoint.Model.OrganizationVO;
import com.mlab.endpoint.exception.ServiceException;

public interface CustomerInformation {

	public ArrayList<CustomerVO> checkCustomer(String customerId,String mobilenumber,String cid)
			throws ServiceException;
	/*
	public String createCustomer(CustomerVO customerVO, int customerId,String actionuser)
			throws ServiceException;
	*/
	public ArrayList<CustomerVO> getListofCustomer(String customerId)
			throws ServiceException;
	

	public String GetRandomNumber()throws ServiceException;

	
	public String createCustomer(CustomerVO customerVO, String customerId, String actionuser) throws ServiceException;
	public String UpdateCustomer(CustomerVO customerVO, String customerId, String actionuser) throws ServiceException;
	public ArrayList<OrganizationVO> getListOfOrganization(String actionuser) throws ServiceException;
	public ArrayList<OrganizationVO> GetListOfLocations(String actionuser) throws ServiceException;
	public String createOrganization(OrganizationVO organizationvo, String customerId, String actionuser)
			throws ServiceException;
	public String UpdateOrganization(OrganizationVO organizationvo, String customerId, String actionuser)
			throws ServiceException;
	public ArrayList<OrganizationVO> GetCustomerOrganizationList(String customerid, String actionuser) throws ServiceException;
	public String createTable(TableVO tableVO, String customerId, String actionuser) throws ServiceException;
	public ArrayList<TableVO> GetCustomerTableList(String customerid, String resturtantid, String actionuser)
			throws ServiceException;
	public String createDevices(TableVO tableVO, String customerId, String actionuser) throws ServiceException;
	public ArrayList<TableVO> GetCustomerDeviceList(String customerid, String resturtantid, String actionuser)
			throws ServiceException;
	public String ManageDeviceOperationForTable(TableVO tableVO, String customerId, String actionuser) throws ServiceException;
	public String UpdateTable(TableVO tableVO, String customerId, String actionuser) throws ServiceException;
	public ArrayList<TableVO> ListOfGlobalAdminDevices(String actionuser) throws ServiceException;
	public ArrayList<OrganizationVO> GetResturantByLocation(String locid, String actionuser) throws ServiceException;
	public ArrayList<OrganizationVO> Get_BookingDetails(String customerid) throws ServiceException;
	public String CreateBooking(OrganizationVO organizationvo, String customerId) throws ServiceException;
	

	
}
