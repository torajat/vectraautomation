package com.mlab.endpoint.interfaces;

import java.util.ArrayList;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;
import com.mlab.endpoint.Model.AttachedFile;
import com.mlab.endpoint.Model.CustomerVO;
import com.mlab.endpoint.Model.ProductVO;
import com.mlab.endpoint.exception.ServiceException;

public interface Scheduler {

	public ArrayList<ProductVO> GetAllInstanceStatus() throws ServiceException;

	public ArrayList<ProductVO> GetAllCustomerList() throws ServiceException;

	public String UpdateInstanceInformation(String instanceid, String rowid, String state, String privateIp, String hostname)
			throws ServiceException;

	public ArrayList<ProductVO> GetAllJobQue() throws ServiceException;

	public ArrayList<ProductVO> GetAllStoppedInstanceStatus() throws ServiceException;

	public ArrayList<ProductVO> GetAllJobWaitingForComputeQue() throws ServiceException;

	public ArrayList<ProductVO> GetOngoingJobDetails(String jobid, String instanceid, String customerid, String companyname,
			String designerwks) throws ServiceException;

	public ArrayList<ProductVO> GetExecuting_Jobs(String jobid, String customerid, String companyname, String designerwks)
			throws ServiceException;

	public String UpdateJobStatus(String instanceid, String jId, String customerid, String company, String status,
			String ref_que_id) throws ServiceException;

	public ArrayList<ProductVO> getExpandJob() throws ServiceException;

	public ArrayList<ProductVO> GetAllJobsListForCopyContent() throws ServiceException;

	public String UpdateCopyJobStatus(String instanceid, String jId, String newstatus, String customerid)
			throws ServiceException;

	public ArrayList<ProductVO> GetAllMonitoringJob(String Status) throws ServiceException;

	public ArrayList<ProductVO> get_All_Collection_Jobs() throws ServiceException;

	public ArrayList<ProductVO> GetCollectionJobStatusList(String ref_jobid, String ref_customerid, String operationflg)
			throws ServiceException;

	public ArrayList<ProductVO> Get_InstanceHostName(String instanceid) throws ServiceException;

	public ArrayList<ProductVO> GetllAllRunningJob() throws ServiceException;

	public String UpdateAutoJobTime(String jId, String flg, String time) throws ServiceException;
	

	// Map<Integer, String>  getDistrictByBattalionId(String battalionName)
		//	throws ServiceException;

	

}
