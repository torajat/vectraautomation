package com.mlab.endpoint.interfaces;

import java.util.ArrayList;
import java.util.Map;

import com.mlab.endpoint.Model.AttachedFile;
import com.mlab.endpoint.Model.ProductVO;
import com.mlab.endpoint.Model.CustomerVO;
import com.mlab.endpoint.exception.ServiceException;

public interface ProductInformationView {

	public ArrayList<ProductVO> getProductCategory(String customerId)
			throws ServiceException;
	
	public String createProductCategory(ProductVO productVO, String customerId,String actionuser)
			throws ServiceException;
	

	public String createProduct(ProductVO productVO, String customerId,String actionuser)
			throws ServiceException;
	
	
	
	public ArrayList<ProductVO> getProductList(String customerId)
			throws ServiceException;
	
	public String StackIn(ProductVO productVO, String customerId,String actionuser)
			throws ServiceException;

	
	public String UpdateProductByField(ProductVO productVO, String customerId, String fieldname, String fieldvalue,
			String actionuser) throws ServiceException;

	public String UploadAttachment(ProductVO productVO, String actionuser, String customerid) throws ServiceException;

	public String DeleteImage(ProductVO productVO, String customerId, String actionuser) throws ServiceException;

	public ArrayList<ProductVO> getProductImageList(String productno, String customerid) throws ServiceException;

	public ArrayList<ProductVO> getProductListWithImages(String customerId) throws ServiceException;

	public String AddToCart(ProductVO productVO, String customerId, String actionuser) throws ServiceException;

	public ArrayList<ProductVO> GetOrderDetails(String customerId) throws ServiceException;

	public String ManageOrderStauts(ProductVO productVO, String newstatus, String customerId, String actionuser)
			throws ServiceException;

	public ArrayList<ProductVO> GetOrderDetailsByFlg(String customerId, String flg) throws ServiceException;

	public ArrayList<ProductVO> GetAnalytics(String customerId) throws ServiceException;

	public ArrayList<ProductVO> getAllInstanceListWithFlg() throws ServiceException;

	public String EditInstance(ProductVO productVO) throws ServiceException;

	public String AddInstance(ProductVO productVO) throws ServiceException;

	public ArrayList<ProductVO> GetAllCustomerList() throws ServiceException;

	public String AddCustomer(ProductVO productVO) throws ServiceException;

	public String UpdateCustomer(ProductVO productVO) throws ServiceException;

	public ArrayList<ProductVO> GetDashboardJobs() throws ServiceException;

	public String UpdateJobStopEvent(String instanceid, String rowid) throws ServiceException;

	public String UpdateTransferjobStatus(String instanceid, String procesingstatus, String operationflg, String ipaddress,
			String destionation, String rowid,String newsource) throws ServiceException;

	ArrayList<ProductVO> GetDashboardJobs(String companyname) throws ServiceException;

	String getInstanceDetails(String instanceId);

	String getFTPServiceStatus();

	String startFTPServiceStatus();
	
}
