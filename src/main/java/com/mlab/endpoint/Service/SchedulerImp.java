package com.mlab.endpoint.Service;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.InstanceState;
import com.amazonaws.services.ec2.model.InstanceStateChange;
import com.amazonaws.services.ec2.model.StartInstancesRequest;
import com.amazonaws.services.ec2.model.StartInstancesResult;
import com.amazonaws.services.ec2.model.StopInstancesRequest;
import com.mlab.endpoint.Model.ProductVO;
import com.mlab.endpoint.exception.ServiceException;
import com.mlab.endpoint.exception.StoredProcException;
import com.mlab.endpoint.interfaces.ProductInformationView;
import com.mlab.endpoint.interfaces.Scheduler;
import com.mlab.endpoint.interfaces.StoredProcJdbcDao;
import com.mlab.endpoint.util.Constants;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;

/*import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileOutputStream;
*/
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.util.NumberUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

public class SchedulerImp implements Scheduler
{


@Value("${accesskey}")
private String accesskey;

@Value("${sckreetKey}")
private String sckreetKey;

@Value("${unitdata}")
private String unitdata;


@Value("${mapusername}")
private String mapusername;

@Value("${mappassword}")
private String mappassword;



@Value("${sftproot}")
private String sftproot;

@Value("${auto2dsourcefile}")
private String auto2dsourcefile;




@Value("${nullproc}")
private String nullproc;

@Value("${auto2d}")
private String auto2d;


@Value("${adalg}")
private String adalg;

@Value("${unitalg}")
private String unitalg;

@Value("${coralg}")
private String coralg;

@Value("${remail}")
private String remail;


@Autowired
private ProductInformationView productinformationView;



private static String fileserialnumber="123";
private static final Logger logger = LoggerFactory.getLogger(SchedulerImp.class);
    
    
    private StoredProcJdbcDao storedProcJdbcDao;

    public StoredProcJdbcDao getStoredProcJdbcDao() {
        return storedProcJdbcDao;
    }

    public void setStoredProcJdbcDao(StoredProcJdbcDao storedProcJdbcDao) {
        this.storedProcJdbcDao = storedProcJdbcDao;
    }


    // Transfer job automation
    
    
    @Scheduled(cron="0 0 0 0/1 * ?")
    public void FileDeletion()
    {
    
    	
    	try
    	{

		ArrayList<ProductVO> allcustomers = GetAllCustomerList();
		
		for (ProductVO customerList : allcustomers) 
		{
			
			String path=customerList.getFtpmappingpath();
			String companyname=customerList.getCompanyname();
			
			String clientcopypath=path+"/"+"client_original_copy";
			String backupcopy=path+"/"+"Merged_data_Backup";
			
			
			
			File[] clientcopylist=new File(clientcopypath).listFiles(File::isFile);
		
			
			File[] backupcopylist=new File(backupcopy).listFiles(File::isFile);
		
			
			String filename="";
			
			long purgeTime = System.currentTimeMillis() - (7 * 24 * 60 * 60 * 1000);
			
			for (File file : clientcopylist) 
			{
				String inputZIPFile=file.getAbsolutePath();
				
				if(file.lastModified() < purgeTime) {
		            if(!file.delete()) {
		                System.err.println("Unable to delete file: " + file);
		            }
		         }
				
			}
		

			for (File file : backupcopylist) 
			{
				String inputZIPFile=file.getAbsolutePath();
				
				
				filename=file.getName();
				
				if(file.lastModified() < purgeTime) {
		            if(!file.delete()) {
		                System.err.println("Unable to delete file: " + file);
		            }
		         }
			
				
			}
		
			
			
		}
		
    	}catch (Exception e)
    	{
    		System.out.println("Failed to Delete:"+e.getMessage());
    	}
    	
		
    }
    
    @Scheduled(cron="0 0/1 * * * ?")
    public void SFTPServiceandUploadStatus()
    {
    String status	=productinformationView.getFTPServiceStatus();
    
	    if (!status.equalsIgnoreCase("Running"))
	    {
	    	productinformationView.startFTPServiceStatus();	
	    	
	    	//net start "Windows Firewall"//
	    }
    }
    
    
    
    
    
    
    //cron="0 0/30 * * * ?"
    @Scheduled(cron="0 0/30 * * * ?")
    public void CheckNotification()
    {

    	//smtpusingssl("");
    	System.out.println("Going to call send email");
    	
    	System.out.println("Going to execute job Auto transfer process");
    	
    
		try {
			ArrayList<ProductVO> joblist = GetllAllRunningJob();
			
			//for
			for (ProductVO job : joblist)
			{
				String jobrowid=job.getjId();
				String toolnameval=job.getToolname();
				String designerworkstationname=job.getDesignerworkstation();
				String instanceid=job.getRef_instanceid();
				String  compnaynameval=job.getCompanyname();
				String transferflg=job.getTransferflg();
				String olddestionation=job.getTextfilepath();
				Double passtime=Double.parseDouble(job.getPasstime());
				String publicip=job.getPublicip();
				System.out.println("Pass time: "+passtime);
				
				if (passtime>5)
				{
					
					String details="Following server is running for :"+passtime+" hours \n INSTANCE ID : "+instanceid+"  \n PUBLIC IP "+publicip +"\n TOOL NAME:"+ mytoolname(job.getToolname())+"\n COMPANY NAME :" +compnaynameval +"\n"+"DESIGNER MACHINE NAME: "+designerworkstationname +"\n";
 					
					String subject="RUN TIME: "+passtime +" Hours --- "+instanceid+" - "+mytoolname(job.getToolname()) +" - "+job.getCompanyname()+" - "+job.getDesignerworkstation();
							
								smtpusingssl(details,subject);
				}
				
				
			}
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
    	

		
    }
	
	private String mytoolname(String toolpath)
	{
		
		String[] paths = toolpath.split("\\\\");
		
		return paths[paths.length-1];
	}	
	
    
    @Scheduled(fixedDelay =70000)
    public void Auto2dServer()
    {
    	String inputZIPFile="C:/SFTP_Root/auto2dserver/AUTO2D_SERVER.7z";
    	String outputFolder="C:/Vectra";

    	String del="C:/Vectra/AUTO2D_SERVER";


	
	    	
   if (new File(inputZIPFile).exists())
       {    

	   	try {
			FileUtils.forceDelete(new File(del));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		

    	  ProcessBuilder pb = new ProcessBuilder(System.getenv("ProgramFiles") + "/7-Zip/7z.exe",
		            "x",      inputZIPFile,
		            "-o" + outputFolder,
		            "-r","-y"
		    );
		    pb.redirectError();
		    try {
		        Process p = pb.start();
		        new Thread(new InputConsumer(p.getInputStream())).start();
		        
		         int code = p.waitFor();
		         
		        ////System.out.println("Exited with: " + p.waitFor());
		      
		        if (code==0)
		        {
		        	
			         File file1 = new File(inputZIPFile);
			         
			         file1.delete();
   	
		        	
		        }
		    }catch(Exception e)
		    {
		    	System.out.println("Error Unzip auto2d server"+e.getMessage());
		    }
       }    
    	
    	System.out.println("Auto 2d server file transfer...");
    }

    
    
    
public ArrayList<ProductVO> getInstanceDetails(String instanceId, String region)

	{
	//System.out.println("Entering transfer instance id:"+ instanceId);
	
	ArrayList<ProductVO> instancedetails = new ArrayList<ProductVO>();

		ProductVO info=new ProductVO();
		
		try {

			AWSCredentials credentials = new BasicAWSCredentials(accesskey,
					sckreetKey);

			String regionval = region;

			AmazonEC2 ec2 = new AmazonEC2Client(credentials);
			Region ec2region = Region.getRegion(Regions.valueOf(regionval));
			ec2.setRegion(ec2region);

		//	AmazonEC2 ec2 = AmazonEC2ClientBuilder.standard()
	      //          .withRegion(Regions.valueOf(regionval))
	        //        .build();


			DescribeInstancesRequest describeInstanceRequest = new DescribeInstancesRequest()
					.withInstanceIds(instanceId);
			DescribeInstancesResult describeInstanceResult = ec2
					.describeInstances(describeInstanceRequest);
			////System.out.println("Ec2out:"+ec2.toString());
			String state = describeInstanceResult.getReservations()
					.get(0).getInstances().get(0).getState().getName();

		String privateip = describeInstanceResult.getReservations()
					.get(0).getInstances().get(0).getPrivateIpAddress();
					
		info.setState(state.toString());
		info.setPrivateip(privateip);
		instancedetails.add(info);
				
				} 
		catch (Exception e) 
		{
			//System.out.println("Instance state Exception ------------->"+e.getMessage());
			//return e.getMessage();

		}
	
		//System.out.println("Returning instance status"+instancedetails);
		return instancedetails;
	}


private String StartInstance(String instanceId, String region)
{
	try{
			AWSCredentials credentials = new BasicAWSCredentials(accesskey,
				sckreetKey);

		String regionval = region;

	AmazonEC2 ec2 = new AmazonEC2Client(credentials);
		Region ec2region = Region.getRegion(Regions.valueOf(regionval));

		ec2.setRegion(ec2region);

//		AmazonEC2 ec2 = AmazonEC2ClientBuilder.standard()
        //        .withRegion(Regions.valueOf(regionval)).withCredentials((AWSCredentialsProvider) credentials)
          //      .build();

		StartInstancesRequest startRequest = new StartInstancesRequest()
				.withInstanceIds(instanceId);
		StartInstancesResult startResult = ec2.startInstances(startRequest);

		List<InstanceStateChange> stateChangeList = startResult
				.getStartingInstances();

		return "Success: "+stateChangeList.get(0).getCurrentState().getName().toString()+" "+"Please referesh the page to get the current status";

	}catch(Exception e)
	{
		return "Error:"+e.getMessage();
	}
	
}

    
    private  void TransferJob( String jobrowid, String toolnameval, String designerworkstationname,String instanceid,String compnaynameval,String olddestionation)
    {
    	 
    	try 
		 {
			String region="US_EAST_1";
			
			
			String oldinstanceid=instanceid;//productVO.getInstanceid();
			String companyname=compnaynameval;//productVO.getCompanyname();
			String designerworkstation=designerworkstationname;//productVO.getDesignerworkstation();
			String toolname=toolnameval;//productVO.getToolname();
			
			String rowid=jobrowid;//productVO.getJobrowid();
			
			System.out.println("Row Id value"+rowid);
			
			String processingstatus="NeedToCopy";
			String operatingflg="Starting";
			String ipaddress="";
			String cs_destionation="";
			String newinstanceid="";
			
			ArrayList<ProductVO> instancelist = productinformationView.getAllInstanceListWithFlg();
			ArrayList<ProductVO> stoppedinstancelist=new ArrayList<ProductVO>();
	
			StringBuilder inslitstname = new StringBuilder();
			
			BufferedReader br = new BufferedReader(new FileReader("c:/SFTP_Root/"+companyname+"/Merged_Data/"+designerworkstation+"/"+"SERVER_SCHEDULE_MAPPING.txt"));
			String line;
			String flg="1";
			while ((line = br.readLine()) != null) 
			{
			    // Do steps 2 and 3 here
				//System.out.println("Lines in text ffile"+line);
				
				if (!line.equals(""))
				{
					inslitstname.append(line);
				}	
			}
			br.close();
			
			//System.out.println("InstanceList in file"+inslitstname.toString());
						
			
			for (ProductVO instanceidval : instancelist)
			{
				if ("1".equals(instanceidval.getStatusid()))
				{
				ProductVO info=new ProductVO();
				newinstanceid=instanceidval.getInstanceid();
				
				if (!inslitstname.toString().contains(instanceidval.getInstancename()))
				{
				
				try
				{
					ArrayList<ProductVO> rst = getInstanceDetails(newinstanceid, region);
					String state=rst.get(0).getState();
					ipaddress=rst.get(0).getPrivateip();
					if(state.equalsIgnoreCase("stopped"))
					{
						info.setInstanceid(newinstanceid);
						info.setState(state);
						stoppedinstancelist.add(info);
							
					}
				}catch(Exception e)
				{
					//System.err.println(newinstanceid+ "Error"+ e.getMessage());
				}

				
				}
					
				}
				
			}
			
			System.out.println("Stopped Instance List-->"+stoppedinstancelist.size());
			
		if(stoppedinstancelist.size()>0)
		{
			String newinstancejobid=stoppedinstancelist.get(0).getInstanceid();
		
			System.out.println("New instance id"+newinstancejobid);
		
			


			String collectionsource="\\"+olddestionation;
        	String coldestionation="C:/SFTP_Root/"+companyname+"/transferjob/"+rowid+"/"+designerworkstation+"/Auto2D_Output_Files";

        	try
        	{
				            	//FileUtils.copyDirectoryToDirectory(srcDir, destDir);
            	FileUtils.copyDirectory(new File(collectionsource),new File(coldestionation));
    		

    			
    			String rest=StartInstance(newinstancejobid, region);
    			
    			if (rest.contains("Success"))
    			{
    				ArrayList<ProductVO> rst = getInstanceDetails(newinstancejobid, region);
    				String state=rst.get(0).getState();
    				ipaddress=rst.get(0).getPrivateip();
    				
    				cs_destionation="\\"+ipaddress+"\\Vectra\\Auto2D_Output_Files\\";
    	        	
    				System.out.println("Row Id"+rowid);
    				
    								
    				String updatestatus=productinformationView.UpdateTransferjobStatus(newinstancejobid, processingstatus, operatingflg, ipaddress, cs_destionation, rowid,coldestionation);
    				
    	        	if (updatestatus.equals("Success"))
    	        	{
    	        		StopInstance(oldinstanceid, region);
//    	    			rmodel.addFlashAttribute("css","success");
    	//    			rmodel.addFlashAttribute("msg","Transfer Processing Started, it will take sometime to complete the process");
    	    		 	        		
    	        	}else
    	        	{
    	  //      		rmodel.addFlashAttribute("css","danger");
    	    //			rmodel.addFlashAttribute("msg","Failed transfer..");
    	    		 
    	    	        		
    	        	}
    			}else
    			{
    				
    			}
    		
			} catch (Exception e) 
        	{
        	}

        		
		}else
		{
			//rmodel.addFlashAttribute("css","danger");
	//		rmodel.addFlashAttribute("msg","No Instance is available,please try again later");
		 
	       
		}
			
		 } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		//	rmodel.addFlashAttribute("css","danger");
			//rmodel.addFlashAttribute("msg","Error:"+e.getMessage());
		 
		}
    	 
    	 
    	
    	
    	
    	
    	
    	
    	
    	
    }
    
    
    
    
    
    

    @Scheduled(fixedDelay =70000)
    public void AutoJobTransfer()
    {
    	
    	System.out.println("Going to execute job Auto transfer process");
    	
    
		try {
			ArrayList<ProductVO> joblist = GetllAllRunningJob();
			
			//for
			for (ProductVO job : joblist)
			{
				String jobrowid=job.getjId();
				String toolnameval=job.getToolname();
				String designerworkstationname=job.getDesignerworkstation();
				String instanceid=job.getRef_instanceid();
				String  compnaynameval=job.getCompanyname();
				String transferflg=job.getTransferflg();
				String olddestionation=job.getTextfilepath();
				
				if(job.getProcessname().contains("ADA_Alg"))
				{
					
					
					if (Integer.parseInt(job.getAdalg())>Integer.parseInt(adalg))
					{
						//transfer job
						System.out.println("Transfering Job"+job.getAdalg()+job.getProcessname());;
			
						TransferJob(jobrowid, toolnameval, designerworkstationname, instanceid, compnaynameval,olddestionation);
						
					}else
					{
						int timeval=2;
						timeval=timeval+Integer.parseInt(job.getAdalg());
						UpdateAutoJobTime(job.getjId(), "ada_alg", String.valueOf(timeval));
					
					}
					
				}else if (job.getProcessname().contains("CoreAlg"))
				{
					System.out.println("Job Process Name coralg"+job.getProcessname());
					
					if (Integer.parseInt(job.getCorealg())>Integer.parseInt(coralg))
					{

						TransferJob(jobrowid, toolnameval, designerworkstationname, instanceid, compnaynameval,olddestionation);
			
						//transfer job
						System.out.println("Transfering Job"+job.getCorealg()+job.getProcessname());;
					}else
					{
						int timeval=2;
						timeval=timeval+Integer.parseInt(job.getCorealg());
			
						UpdateAutoJobTime(job.getjId(), "core_alg", String.valueOf(timeval));
					
						
					}
					
				}else if (job.getProcessname().contains("Unit_Alg"))
				{
					//System.out.println("Job Process Name Unit_alg"+job.getProcessname());
					
					if (Integer.parseInt(job.getCorealg())>Integer.parseInt(coralg))
					{

						TransferJob(jobrowid, toolnameval, designerworkstationname, instanceid, compnaynameval,olddestionation);
			
						//transfer job
						System.out.println("Transfering Job"+job.getCorealg()+job.getProcessname());;
					}else
					{
						int timeval=2;
						timeval=timeval+Integer.parseInt(job.getUnitalg());
						UpdateAutoJobTime(job.getjId(), "unit_alg", String.valueOf(timeval));
					}
					
				}else if (job.getProcessname().contains("Vectra_Auto2D_Server"))
				{
					System.out.println("Job Process Name auto2d"+job.getProcessname());
					
					if (Integer.parseInt(job.getAuto2d())>Integer.parseInt(auto2d))
					{

						TransferJob(jobrowid, toolnameval, designerworkstationname, instanceid, compnaynameval,olddestionation);
			
						//transfer job
						System.out.println("Transfering Job"+job.getAuto2d()+job.getProcessname());;
					}else
					{
						int timeval=2;
						timeval=timeval+Integer.parseInt(job.getAuto2d());
						UpdateAutoJobTime(job.getjId(), "auto2d", String.valueOf(timeval));
					
					}
					
				}else if (job.getProcessname().equals(""))
				{
					System.out.println("Job Process Name Null"+job.getProcessname());
					
					if (Integer.parseInt(job.getNullproc())>Integer.parseInt(nullproc))
					{

						TransferJob(jobrowid, toolnameval, designerworkstationname, instanceid, compnaynameval,olddestionation);
			
						//transfer job
						System.out.println("Transfering Job"+job.getAuto2d()+job.getProcessname());;
					}else
					{
						
						int timeval=2;
						timeval=timeval+Integer.parseInt(job.getNullproc());
						UpdateAutoJobTime(job.getjId(), "nullproc", String.valueOf(timeval));
					
					}
				}
				
			}
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
    	

    }
    
    @Scheduled(fixedDelay =5000)
    public void CreateJobQue()
    {
    	////System.out.println("Going to execute job que creation process");
    	
    	try 
    	{	
			ArrayList<ProductVO> stoppedinstancelist = new ArrayList<ProductVO>();
			
			ArrayList<ProductVO> instancelist = GetAllInstanceStatus();
			
			
			
			
			for (ProductVO instance : instancelist)
			{
				ProductVO info = new ProductVO();
				
				String state=instanceStatus(instance.getInstanceid(),instance.getInrId(),instance.getRegion());
				
				if ("running".equalsIgnoreCase(state))
				{
					
				}else if ("stopped".equalsIgnoreCase(state))
				{
					info.setInstanceid(instance.getInstanceid());
					info.setRegion(instance.getRegion());
					
				}
				else
				{
					
				}
				
				stoppedinstancelist.add(info);
			}

			
			for (ProductVO customerList : GetAllCustomerList())
			{
				////System.out.println("Company Name and file map path--->"+customerList.getFtpmappingpath()+ "--->"+customerList.getCompanyname());
				
				CheckJobQue(customerList.getFtpmappingpath(), customerList.getCompanyname(),stoppedinstancelist,customerList.getCustomerid());
				
			}


			
			
			
			// checking list of customer job in que for process
			
		
    	} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	//////System.out.println("Demo method 1 Runing shcedule task...");
    	
    }



    

    @Scheduled(fixedDelay =30000)
    public void jobexpend()
    {
		try
		{
			
			ArrayList<ProductVO> Nextjoblist = getExpandJob();
	
			for (ProductVO productVO : Nextjoblist) 
			{
				String jobid=productVO.getQjobid();
				
				String numberofinstance=productVO.getRequiredinstance();
				String customerid=productVO.getCustomerid();
				
				System.out.println("System Required Number of Instance"+ numberofinstance);
				
				createJobList(jobid, numberofinstance,customerid);
				
			}
			
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    	
    }
    

    

    @Scheduled(fixedDelay =200000)
    public void ProcessAndDistributeJobFromQue()
    {
    //	System.out.println("Job Distribution -->");
    	
    	// going to distribute job details to instance
    	try 
		{
			
			ArrayList<ProductVO> Nextjoblist = GetAllJobQue();
		

	    	//////System.out.println("Job Distribution is started for next job"+ Nextjoblist);
			
	    	
			for (ProductVO productVO : Nextjoblist) 
			{
				//Process();
			//	String numberofinstance=productVO.getRequiredinstance();
				String companyname=productVO.getCompanyname();
				String customerId=productVO.getCustomerid();
				String sweepdata=productVO.getSweepdatapath();
				String tooldata=productVO.getProcessingpath();
				String workstationname=productVO.getDesignerworkstation();
				String ref_jobid=productVO.getQjobid();
				int numberofinstance=Integer.parseInt(productVO.getRequiredinstance());
				
				String textfilepath=productVO.getTextfilepath();
		
				
				//////System.out.println("Number of instance--->"+numberofinstance+" Txt file path-->" +textfilepath );
				
				ArrayList<ProductVO> joballocationlist = Get_all_Job_Allocation(ref_jobid);
				
				//////System.out.println("Next Job Processing--------------->:"+ joballocationlist);
				
				for (ProductVO productVO2 : joballocationlist) 
				{
					
					//////System.out.println("Job Allocation Que Started----------->:"+productVO2.toString());
					
					String datafilename=productVO2.getDatafilename();
					
					String inorder=productVO2.getOrderid();
					
					String joblistdatafileid=productVO2.getjId();
					
					String status=productVO2.getStatus();

					
					//////System.out.println("Getting Status------>"+ status +"In order:--------->"+inorder);
					
					if ("1".equals(status))
					{
					
						//System.out.println("Textfile Path ------------->"+textfilepath);
						
					File f = new File(textfilepath+"/Transfer_Complete - Start Compute Sequence.txt");
					
					//////System.out.println("After checking file exist or not-------->"+status);
					
					
					
					if(f.exists())
						{
							
						 ////System.out.println("File Exist in the following folder:" +textfilepath );
							
						}
						else
						{
							

						ArrayList<ProductVO> allstopinstancelist = GetAllStoppedInstanceStatus();
						
					//System.out.println("All Stopped Instance Size--------------->"+allstopinstancelist.size());
						
	    		
						if(allstopinstancelist.size()>0)
	    				{
	    				
							String instanceid=allstopinstancelist.get(0).getInstanceid();
		    				
		    				String region=allstopinstancelist.get(0).getRegion();
		    	
		    				int inorderval=Integer.parseInt(inorder);
		    				
		    				//////System.out.println("Going to Process Order:"+ instanceid);
		    			
		    				StartProcess(instanceid, region, customerId, sweepdata, tooldata, companyname, workstationname, ref_jobid, textfilepath,inorderval,joblistdatafileid,datafilename);    				
		    	
	    				
	    				}else
	    				{
	    					//////System.out.println("Required Number of Instance is not statisfied-->"+"Required:"+numberofinstance+" but got stopped : "+allstopinstancelist.size());
	    					
	    					
	    				}
					}
					
					
				}
					
			}
				
				
				
			}
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//////System.out.println("Error: while checking file path:"+ e.getMessage());
		}
	
		
    	
    	//////System.out.println("Demo Method 2 Runing shcedule task...");
    	
    }
    
    
    

    @Scheduled(fixedDelay =3000)
    public void ExtractZipFileServices ()
    {
    	
    	
		try {
			
			for (ProductVO customerList : GetAllCustomerList())
			{
				UnZipServices(customerList.getFtpmappingpath(), customerList.getCompanyname());
				
			}
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
  //  	////System.out.println("System Checking  for New Zip Files arival");
    	
    }
    

    

    @Scheduled(fixedDelay =1000)
    public void GetComputeStatus()
    {

/*
    	try
		{
		
			ArrayList<ProductVO> OngoingJobList = GetAllJobWaitingForComputeQue();
	
			for (ProductVO job : OngoingJobList)
			{
				
				String qstatus=job.getQstatus();
				String ref_jobid=job.getjId();
				String ref_customerid=job.getCustomerid();
				String companyname=job.getCompanyname();
				String processingpath=job.getProcessingpath();
				String sweepdatapath=job.getSweepdatapath();
				String designerwks=job.getDesignerworkstation();
			
				if (qstatus.equals("InQ-NotStart"))
				{
					GetInstanceJobStatus(ref_jobid, ref_customerid, designerwks, processingpath, sweepdatapath, companyname);
				}
			}
			
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    	
*/    	

    }
    

    @Scheduled(fixedDelay =50000)
    public void ArhciveForMerged_Data()
    {
    	
		try {
			
			
			ArrayList<ProductVO> collectionjoblist = get_All_Collection_Jobs();
			
			////System.out.println("Collection Job List--->:"+collectionjoblist);
			String operationflg="ProcessCompleted";
	
		for (ProductVO productVO : collectionjoblist)
		{
			String ref_jobid=productVO.getQjobid();
			String workstationname=productVO.getDesignerworkstation();
			String ref_customerid=productVO.getCustomerid();
			String companynameval=productVO.getCompanyname();
			String numberofinstance=productVO.getRequiredinstance();
			
			ArrayList<ProductVO> joblist = GetCollectionJobStatusList(ref_jobid, ref_customerid, operationflg);
			
			System.out.println("All Job Collection Status--->"+joblist.size()+"--->"+numberofinstance);
			
			if(joblist.size()==Integer.parseInt(numberofinstance))
			{
				
				String foldermappingpath=sftproot+companynameval+"/"+"Merged_Data";
			
				//System.out.println("Going to archive:"+joblist.size());
				
				String result = ArchiveZipFileAndMakeMerged_DataReady(foldermappingpath, companynameval, workstationname);
			
						
				        if (result.contains("error"))
				        {
				        	
				        }else
				        {
				        	/*try{
				        	File megerdirectory=new  File(foldermappingpath+"/"+workstationname);
				        	megerdirectory.deleteOnExit();
				        	}catch(Exception e)
				        	{//System.out.println("Failed to delete directory:"+foldermappingpath+"/"+workstationname);
				        		
				        	}*/
				        	
				        	//update que
				        	String newstatus="JobCompleted";
		
				        	
				        	UpdateJobQue(ref_jobid, workstationname,ref_customerid ,companynameval, newstatus);
				
				        	
				        	for (ProductVO job : joblist) 
				        	{
				        		newstatus="JobCompleted";
				        		String instanceid=job.getInstanceid();
				        		String rowid=job.getQjobid();
				        		String customerid=job.getCustomerid();
				        		String processlist="";
				        		
				        		//FinalJobUpdate(instanceid, rowid, customerid, newstatus, processlist);
								
							}
				        	
				        }
						
						
						
			}else
			
			{
				
			}
			
		}
		
		
		
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	/*
    	
    	
    	try
		{
		
    		ArrayList<ProductVO> customerslist = GetAllCustomerList();
    		
			ArrayList<ProductVO> OngoingJobList = GetAllJobWaitingForComputeQue();
	
			for (ProductVO job : OngoingJobList)
			{
				
				String qstatus=job.getQstatus();
				String ref_jobid=job.getjId();
				String ref_customerid=job.getCustomerid();
				String companyname=job.getCompanyname();
				String processingpath=job.getProcessingpath();
				String sweepdatapath=job.getSweepdatapath();
				String designerwks=job.getDesignerworkstation();
				int numberofinstance=Integer.parseInt(job.getRequiredinstance());
				
				
				if (qstatus.equals("WaitingForComputeResponse"))
				{


					
					ArrayList<ProductVO> executingjobsList = GetExecuting_Jobs(ref_jobid,ref_customerid, companyname, designerwks);
				
					ArrayList<String> processque= new ArrayList<String>();
					
					for (ProductVO exejob : executingjobsList)
					{
						if (exejob.getProcessingstatus().equals("ProcessCompleted"))
						{
							processque.add(exejob.getProcessingstatus());	
						}
					}
					
					
					if (numberofinstance==processque.size())
					{
						for (ProductVO customer : customerslist)
						{
							if(customer.getCustomerid().equals(ref_customerid))
							{
								
								String foldermappingpath=customer.getFtpmappingpath();
								
								String zipres=ArchiveZipFileAndMakeMerged_DataReady(foldermappingpath, companyname, designerwks);
								

						        if (zipres.contains("error"))
						        {
						        	
						        }else
						        {
						        	//update que
						        	String newstatus="JobCompleted";
						        	UpdateJobQue(ref_jobid, designerwks, ref_customerid, companyname, newstatus);


									File f = new File(foldermappingpath+"/"+designerwks+"/Transfer_Complete_InQ.txt");
									
							        File newFile = new File(foldermappingpath+"/"+designerwks+"/"+"Transfer_Complete_ProcessCompleted.txt");
							        							        
							        if( f.renameTo(newFile))
							        {
							        	
							           // ////System.out.println("File rename success");
							            
							        }
							        else
							        {
							            //////System.out.println("File rename failed");
							        }
							        

						        
						        }
							}
							
						}
						
					}
					
					
					
				}
			}
			
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


*/    	
    }

   
    
//300000
    @Scheduled(fixedDelay =100000)
    public void CopyContent ()
    {
    	System.out.println("Copy Job Content");
    	try 
    	{
			ArrayList<ProductVO> copyjoblist = GetAllJobsListForCopyContent();
		
			////System.out.println("All Copy Job->:"+copyjoblist);
			
			for (ProductVO productVO : copyjoblist)
			{
				
				String destionationserver=productVO.getCsdestionation();
				String privateip=productVO.getPrivateip();
				String sourcecopydata=productVO.getCopysourcefolder();
				String datafilename=productVO.getTextfilepath();
				String jId=productVO.getJobrowid();
				String instanceid=productVO.getInstanceid();
				String customerid=productVO.getCustomerid();
				String designerworkstation=productVO.getDesignerworkstation();
				String companyname=productVO.getCompanyname();
				String transferflg=productVO.getTransferflg();
				

				ArrayList<ProductVO> allinstance = GetAllInstanceStatus();
        	
				String region="";
				System.out.println("all instance:"+allinstance);
				
				for (ProductVO instance : allinstance) 
				{
					if (instanceid.equals(instance.getInstanceid()))
					{
						region=instance.getRegion();
						break;
					}
					
				}
				
				while(true) 
            	{
					
				String instancestatus = getInstanceStatus(instanceid, region, customerid);
				
				System.out.println("instancs status+"+instancestatus);
					if (instancestatus.equals("stopped"))
		        	{
						
					   	String insresult=StartInstance(instanceid, region,"Vectra");
					     
            			//////System.out.println("Instance State  --->:"+instancestatus);
		        		
		        	}else if(instancestatus.equals("running"))
		        	{
		        		////System.out.println("Instance is running");
		        		
		        		break;
		        	}
		     
		        	
		        	
		        	else
		        	{
		        		
		        	}
            		
            		
            		
            		
	            }
            	
				
				
				String result=getNatDriveStatus(destionationserver, privateip);
				
				////System.out.println("Net drive status:------------>"+result);
				
				
			if (result.equals("Success"))
				{
				////System.out.println("Going copy file");
				
				File destDir = new File("\\\\"+privateip+"Auto2D_Output_Files"+"\\"+Constants.SCHEDULING_COPY_COMPLETION_FILE);
		
		
				if (destDir.exists())
				{
					//System.out.println("Copy File Exist");
				}
				else
										
				{String rs="";
					
					if (transferflg.equals("0"))
					{

						
						rs=	Transfer_CopyAndReplace(sourcecopydata, destionationserver,datafilename,privateip,companyname,designerworkstation);

					}else
					{

						
						rs=	CopyAndReplace(sourcecopydata, destionationserver,datafilename,privateip,companyname,designerworkstation);
									
					}
					
							
				
					////System.out.println("Copy Paste result---->:"+rs);
					
					
					if(rs.equals("1"))
						{
							String newstatus="CopyComplete";
							
							////System.out.println("Going to update job status _----->:"+newstatus);
							
							
							UpdateCopyJobStatus(instanceid, jId, newstatus, customerid)	;
							
							ArrayList<ProductVO> rst = getInstanceDetails(instanceid, region, customerid);
							
							String insttancename=rst.get(0).getRef_instanceid();
		    				//here need to add the server mapping part..
		    				String fileopspath=sftproot+companyname+"/"+"Merged_Data/"+designerworkstation+"/"+"SERVER_SCHEDULE_MAPPING.txt";
		    				try {
		    			        File file = new File(fileopspath);
		    			        if (!file.exists()) {
		    			            file.createNewFile();
		    			            FileWriter fw = new FileWriter(fileopspath, true);
		    			        	BufferedWriter bw = new BufferedWriter(fw);
		    			        	String content = datafilename+"->"+insttancename+"\n";
		    			        	bw.write(content);
		    			        	bw.newLine();
		    			        	bw.close();
		    			        	fw.close();
		    			        } 
		    			        else
		    			        {
		    			        	FileWriter fw = new FileWriter(fileopspath, true);
		    			        	BufferedWriter bw = new BufferedWriter(fw);
		    			        	String content = datafilename+"->"+insttancename+"\n";
		    			        	//bw.write(content);
		    			        	bw.append(content);
		    			        	bw.newLine();
		    			        	bw.close();
		    			        	fw.close();
		    			        }
		    			    } catch (IOException e) {
		    			        e.printStackTrace();
		    			    }
							
						}
						else
						{
							
							
						}
				}
			}
				else
				{
					////System.out.println("Failed to Copy Folder:"+result);
					
				}
			
			}
    	
    	
    	} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }

    
    
/*
    @Scheduled(fixedDelay =3000)
    public void SchedulerChecker ()
    {
    	
    	
    }
*/
    
    
    
    @Scheduled(fixedDelay =80000)
    public void MonitoringJobStatus ()
    {
    	
    	String Status="CopyComplete";
    	
    	try {
			ArrayList<ProductVO> monitoringjob = GetAllMonitoringJob(Status);
			
			////System.out.println("List of all Monitoring jobs--->:"+monitoringjob);
			
			for (ProductVO productVO : monitoringjob) 
			{
				String ipaddress=productVO.getPrivateip();
				String csdestionation=productVO.getCsdestionation();
				String jobrowid=productVO.getJobrowid();
				String joborderid=productVO.getJoborder();
				String ref_jobque=productVO.getQjobid();
				String customerid=productVO.getCustomerid();
				String instancename=productVO.getInstanceid();
				String operationflg=productVO.getOperationflg();
				String processingstatus=productVO.getProcessingstatus();
				String designerwks=productVO.getDesignerworkstation();
				String companyname=productVO.getCompanyname();
				String region=productVO.getRegion();
				
			////System.out.println("Operation flg------>"+operationflg);
				
			if(!operationflg.equals("processCompleted"))
			{
				manageMonitoring(csdestionation, ipaddress, customerid, jobrowid,ref_jobque,instancename,designerwks,companyname,region);
			}	
				
				
			}
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
    	
    }
    
    
    
    private String manageMonitoring(String csdestionation,String privateip,String customerid,String rowid,String ref_jobid,String instanceid,String desingerwks,String companyname,String region)
    {
////System.out.println("Looking files for compute instance processing finish path-->:"+"\\"+csdestionation);
    	
		File f = new File("\\"+csdestionation+Constants.SCHEDULING_FINISHED_COMPLETION_FILE);
		
		if(f.exists())
		{
			//System.out.println("File exist--->"+f.toPath().toString());
			//compeletion completed;


			//String downalodfolder_destionation=sftproot+companyname+"/"+"Merged_Data"+"/"+desingerwks+"/Auto2D_Output_Files";
			
			String downalodfolder_destionation=sftproot+companyname+"/"+"Merged_Data"+"/"+desingerwks;
			
			
			String result=CollectFileFromComputeInstance(privateip, downalodfolder_destionation);
			
			if (result.equals("1"))
			{
				
				
				String res = StopInstance(instanceid, region);
				
				String newstatus="ProcessCompleted";
				String runningprocess="ProcessCompleted";
				try {
					//Update_Operation_Flg(instanceid, rowid, customerid, newstatus, runningprocess);
					FinalJobUpdate(instanceid, rowid, customerid, newstatus, runningprocess);
				} catch (ServiceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
				////System.out.println("Stopeed Instance Status"+res);
				
			}else
			{
				
			}
		
			
				
			
			
		}else
		{
			String runningprocess="";
			try
			{
				
					String path="\\\\"+privateip+"\\"+"Vectra"+"\\"+"Monitoring"+"\\"+"processmon.txt";	    
				
				InputStream is = new FileInputStream(path);
				
				BufferedReader buf = new BufferedReader(new InputStreamReader(is));
				//String line = buf.readLine(); 
				StringBuilder sb = new StringBuilder();
				
				String line = "1"; 
				while(line != null)
				{ String contents="";
					sb.append(line).append("\n");
					line = buf.readLine();
					
					contents=line;
					if (contents.contains("Vectra_Auto2D_Server"))
					{
						runningprocess=runningprocess+ line;
					}
					else if (contents.contains("Unit_Alg"))
					{
						runningprocess=runningprocess+ line;
					}
				
					else if (contents.contains("ADA_Alg"))
					{
						runningprocess=runningprocess+ line;
					}
				

					else if (contents.contains("CoreAlg"))
					{
						runningprocess=runningprocess+ line;
					}
					
					else if (contents.contains("EXCEL"))
					{
						runningprocess=runningprocess+ line;
					}else
					{
						
					}

				}

				buf.close();				
			} catch( Exception e)
			{
				////System.out.println("Error Failed to read file:"+ e.getMessage());
				
			}
		
			if(runningprocess.equals(""))
			
			{/*
				String newstatus="Running-2";
				runningprocess=runningprocess;
				try {
					Update_Operation_Flg(instanceid, rowid, customerid, newstatus, runningprocess);
				} catch (ServiceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		*/
				
			}else
			{
				String newstatus="Running";
				try
				{
					Update_Operation_Flg(instanceid, rowid, customerid, newstatus, runningprocess);
				} catch (ServiceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
				
				
			
			//completion ongoing
		}
			
		return "Done";
    }
    
   
    private void createJobList(String jobrefid,String instancenumber,String customerid)
    {
    
    	int instance=Integer.parseInt(instancenumber);
    
    	for (int i = 1; i <= instance; i++)
    	{
    		String datafile="Unit_Sweep_Data_List_0"+i+".txt";
    		
    		try
    		{
			
    			InsertExpandJob(jobrefid, datafile, i,customerid);
			
    		} catch (ServiceException e) 
    		{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
    	
    	
    	try
    	{
			updateExpandJob(jobrefid, customerid);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
    
    
    
    private String GetInstanceJobStatus(String ref_jobid,String ref_customerid,String designerwks,String processingpath, String sweepdatapath,String companyname)
    {
    	    	
    	try 
    	{
    		
			ArrayList<ProductVO> executingjobsList = GetExecuting_Jobs(ref_jobid,ref_customerid, companyname, designerwks);
		
			for (ProductVO exejob : executingjobsList) {
			

				String cs_designation=exejob.getTextfilepath();
				String jobjid=exejob.getjId();
				String instanceid=exejob.getInstanceid();
				String customerid=exejob.getCustomerid();
				
				File f = new File(cs_designation+"/Transfer_Complete_ProcessingCompleted.txt");
				
				if(f.exists())
				{


					File srcDir = new File(cs_designation);
					File destDir = new File(sweepdatapath);
					
					
					try 
					{
						
						FileUtils.copyDirectory(srcDir, destDir);
						
						File file = new File(cs_designation);
						
				        File newFile = new File(cs_designation+"/"+"Transfer_Complete_CopyCompleted.txt");
				        
				        if(file.renameTo(newFile))
				        {
				        	
				        	UpdateJobStatus(instanceid, jobjid, ref_customerid, companyname, "ProcessCompleted", ref_jobid);
				        	
				            ////System.out.println("File rename success");
				            
				            
				            
				        }
				        else
				        {
				            ////System.out.println("File rename failed");
				        }
				        
				        
				        
				        
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						
					}

					
				}else
				{
		        	UpdateJobStatus(instanceid, jobjid, ref_customerid, companyname, "Running", ref_jobid);
		        		
				}
				
				
			}
    	} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    
    	
    	
    	
    	return "";
    }
    
    
    
    /*
    
    private String ProcessingJob(ArrayList<ProductVO> productvo)
    {
    	
    	
    	
    	for (ProductVO job : productvo) 
    	{
    		
    		int numberofinstance=Integer.parseInt(job.getRequiredinstance());
			
    		
    		for (int i = 0; i < numberofinstance; i++) 
    		{
    	    	    	
    			try
    			{	
    				ArrayList<ProductVO> instancelist = GetAllStoppedInstanceStatus();
    				
    				String instanceid=instancelist.get(i).getInstanceid();
    				String region=instancelist.get(i).getRegion();
    				
    			
    			
    			} catch (ServiceException e)
    			{
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
    	
    			
				
			}
    		
		}
    	
    	
    	
    	return "";
    	
    }
    */
    
    
    private String StartProcess(String instanceId,String region,String customerId, String sweepdata,String tooldata,String companyname,String workstationname,String ref_jobid,String textfilepath,int index,String rowid,String datafile)
    {
    	
    	
    	ArrayList<ProductVO> instancelist = getInstanceDetails(instanceId, region, customerId);
    	
         String privateip=instancelist.get(0).getPrivateip();
         String state=instancelist.get(0).getState();
         String result="";
        
       // System.out.println("Proccessing Order____"+privateip+state);
         
         if ("stopped".equalsIgnoreCase(state))
         {
        	 result=StartInstance(instanceId, region,"Vectra");
        	 
        	 System.out.println("Start Result:"+ result);
        	 
        	 if (result.contains("Success"))
        	 {
        
        		 ArrayList<ProductVO> instancelist2 = getInstanceDetails(instanceId, region, customerId);
        	    	
                 String privateip2=instancelist2.get(0).getPrivateip();
                
        		 String destionation="\\"+privateip2+"\\Vectra\\Auto2D_Output_Files\\";
        		 
        		// ////System.out.println("Destionation Folder--------->:"+destionation);
	             
        		 try 
        		 {
        			 
					String res=InsertJobList(instanceId, customerId, sweepdata, tooldata, companyname, workstationname, ref_jobid,destionation,index,privateip2,textfilepath,rowid,datafile);
					
					if (res.equals("Success"))
					{
						////System.out.println("Updatting Job List Status----->"+ref_jobid+rowid+customerId);
						String newstatus1="0";
						
	    				String res4=UpdateJobListStatus(ref_jobid, rowid, customerId, newstatus1);
	    	
	    				
	    				//here need to add the server mapping part..
	    				String fileopspath=sftproot+companyname+"/"+"Merged_Data/"+destionation+"/"+"SERVER_SCHEDULE_MAPPING.txt";
	    				try {
	    			        File file = new File(fileopspath);
	    			        if (!file.exists()) {
	    			            file.createNewFile();
	    			            FileWriter fw = new FileWriter(fileopspath, true);
	    			        	BufferedWriter bw = new BufferedWriter(fw);
	    			        	String content = unitdata+"->"+instanceId+"\n";
	    			        	bw.write(content);
	    			        	bw.close();
	    			        	fw.close();
	    			        } 
	    			        else
	    			        {
	    			        	FileWriter fw = new FileWriter(fileopspath, true);
	    			        	BufferedWriter bw = new BufferedWriter(fw);
	    			        	String content = unitdata+"->"+instanceId+"\n";
	    			        	bw.write(content);
	    			        	bw.close();
	    			        	fw.close();
	    			        }
	    			    } catch (IOException e) {
	    			        e.printStackTrace();
	    			    }
					}
					else
					{
						////System.out.println("Failed to Updatting Job List Status----->"+ref_jobid+rowid+customerId);
						
					}
    				
        		 } catch (ServiceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	
        		 
        	 }else
        	 {
        		 
        		 ////System.out.println("Failed to Start the instance:"+result);
        	 }
         }
         
         
         
         
         return result;
    
    }
    
    
    private String getNatDriveStatus(String destionationserver,String privateip)
    {
    	
        
   // 	\172.31.28.90\Vectra\Auto2D_Output_Files\
    	String ckdrive="\\\\"+privateip+"\\Vectra\\Monitoring\\processmon.txt";
    	
    	  File CheckFlie=new File(ckdrive);
	    		
    	  ////System.out.println("Check Drive------------>"+ckdrive);
        	 
	            if(CheckFlie.exists())
	    			{
	            	
	            	System.out.println("Network drive is connected:"+"Vectra_Auto2D_Server.exe");
	    			
	            	// copy auto2d files to server
	            	//copying auto 2d source file
	        	
	            	File vectsrcDir = new File(auto2dsourcefile);
	        		File vectradestDir = new File("\\\\"+privateip+"\\"+"Vectra"+"\\"+"AUTO2D_SERVER");
	        		
	        		System.out.println("Going to copy Auto2d Server file from "+auto2dsourcefile +" to "+vectradestDir.toPath());
	        		//System.out.println("Source file Data path---->"+auto2dsourcefile);
	        		
	        		
	        		File Reference = new File("\\\\"+privateip+"\\"+"Vectra"+"\\"+"AUTO2D_SERVER\\Reference");
	        		File Templates = new File("\\\\"+privateip+"\\"+"Vectra"+"\\"+"AUTO2D_SERVER\\Templates");
	        		File CoreAlg = new File("\\\\"+privateip+"\\"+"Vectra"+"\\"+"AUTO2D_SERVER\\CoreAlg.exe");
	        		File ADA_Alg = new File("\\\\"+privateip+"\\"+"Vectra"+"\\"+"AUTO2D_SERVER\\ADA_Alg.exe");
	        		File Unit_Alg = new File("\\\\"+privateip+"\\"+"Vectra"+"\\"+"AUTO2D_SERVER\\Unit_Alg.exe");
	        		
	        		try{FileUtils.deleteDirectory(Reference);}catch(Exception e){System.out.println("Failed to delete reference"+ Reference +"->"+e.getMessage());}
	        		try{FileUtils.deleteDirectory(Templates);}catch(Exception e){System.out.println("Failed to delete Templates"+ Templates +"->"+e.getMessage());}
	        		try{CoreAlg.delete();}catch(Exception e){System.out.println("Failed to delete CoreAlg"+ CoreAlg +"->"+e.getMessage());}
	        		try{ADA_Alg.delete();}catch(Exception e){System.out.println("Failed to delete ADA_Alg"+ ADA_Alg +"->"+e.getMessage());}
	        		try{Unit_Alg.delete();}catch(Exception e){System.out.println("Failed to delete Unit_Alg"+ Unit_Alg +"->"+e.getMessage());}
			        	;
	        		
	        		try 
	        		{
	        			
	        			FileUtils.copyDirectory(vectsrcDir, vectradestDir);
	        			System.out.println("Auto2D Source Copy Completed: Source file copy to:"+vectradestDir.toPath().toString());
	        		} catch (IOException e1) {
	        			// TODO Auto-generated catch block
	        			System.out.println("Network Error: Failed to Copy Auto2d Vectra source file to destination:"+ "\\"+vectradestDir.getAbsolutePath());
	        			e1.printStackTrace();
	        			return "Failed";
	        		}
	        		
	            	
	            	
	            	
	            	
	            	return "Success";	

	    			}
	            else
	            {
	            	System.out.println("Failed to connect ->Creating new map drive:------------>"+destionationserver);
	            	
	            	List<String> drive = new ArrayList<String>();
            
	            	for(File f : File.listRoots())
	        		{
	            		drive.add(f.getAbsolutePath().toLowerCase());
	            	}
	    
	            	if (drive.size()>23)
	            	{
	            		
	            		return "Failed";
	            	}
	            	
	            	String assignedletter="";
	            	
	            	//////System.out.println("All Drive Letters---------------->:"+drive.toString());
	            	
	            	assignedletter=	getDriveletter();
            		
            		//////System.out.println("Drive Letter----->:"+assignedletter);
            			            		
            		/*if (drive.toString().contains(assignedletter))
		        	{
		        	 ////System.out.println("Match the drive letter:"+ drive.toString() + "--->"+assignedletter );
		        	}
            		else
		        	{
		        		
		        		 ////System.out.println("Does not match the drive letter:"+ drive.toString() + "--->"+assignedletter );
				        		
		        	}
            		*/
	            	int count = 1;
	            	
	            	while(true) 
	            	{
	            	
	            		
	            		////System.out.println("Drive Letter----->:"+assignedletter);
	            			            		
	            		if (drive.toString().contains(assignedletter))
			        	{
	            			////System.out.println("Drive Letter Match  --->:"+assignedletter);
			        		
			        	}else
			        	{
			        		////System.out.println("Assigned Drive letter------->:"+assignedletter);
			        		
			        		break;
			        	}
	            		
	            		
	            		if (count>26)
	            		{
	            			break;
	            		}
	            		
	            		
		            }
	            	
	            	try{
		        		String result = MapNetworkSharedFile(privateip,assignedletter);
		        	
		        		if (result.contains("Error"))
			        		{
			        			return "Failed";	
			        		}
			        		else
			        		{
			        			return "Success";
			        		}
	            	}catch (Exception e)
	            	{
	            		////System.out.println("Error In Mapping Drive:"+ e.getMessage());
	            		return "Error:"+e.getMessage();
	            	}
	            }
    	
    		
    	
    }
    
        
    private String getDriveletter()
    {
        String letter=RandomStringUtils.randomAlphabetic(1).toLowerCase();
        ////System.out.println("Drive Letter------------->"+letter);

        return letter;
    }
    
/*    
    private String TestJob()
    {
    	
		 
		 try {
			 ////System.out.println("Going to sleep the threed to check instance status:"+ result);
			 
	            // thread to sleep for 1000 milliseconds
	            Thread.sleep(30 *   // minutes to sleep
	                    60 *   // seconds to a minute
	                    1000);
	            
	            //G:/Vectra/Auto2D_Output_Files/
	            
	            ////System.out.println("Wake Up Instance-------------->"+instanceId);
	            
	            String letter=RandomStringUtils.randomAlphabetic(1).toLowerCase();
	            
	            ////System.out.println("Drive Letter------------->"+letter);
	            
	        	List<String> drive = new ArrayList<String>();
	        	
	        	
	        	for(File f : File.listRoots())
	    		{
	        		
	        		drive.add(f.getAbsolutePath());
	        		
	    		}  
	        	
	        	
	        	////System.out.println("Drivelist-->"+drive.toString());
	        	
	        	if (letter.contains(drive.toString()))
	        	{
	        		////System.out.println("Yes Already Exist-->"+letter);
	        	}
	        	else
	        	{	            	            
   	            String rs = MapNetworkSharedFile(privateip);
   	            
   	            if( (rs.contains("error")) || (rs.contains("Error")))
   	            {
   	            	
   	            	////System.out.println("Failed to Map Network drive:"+instanceId);
   	            }
   	            
   	            else
   	            {

       	        	try
       	    		{
       	        		
       	            File CheckFlie=new File("\\"+privateip+"Vectra\\AUTO2D_SERVER\\"+"Vectra_Auto2D_Server.exe");
       	    		
       	 
       	            if(CheckFlie.exists())
       	    			{
       	            	////System.out.println("Network drive is connected:"+"Vectra_Auto2D_Server.exe");
       	    				

       	    			//File srcDir = new File("G:/amipro/mlab1/Auto2D_Output_Files/Auto2D_Output_Files_maa54578s_22july2017/Unit_Sweep_Data");
       	    			//File destDir = new File("G:/Vectra/Auto2D_Output_Files/");
       	    			
       	    			String destionation="\\"+privateip+"Vectra\\Auto2D_Output_Files\\";
               	    	
       	    			////System.out.println("Destionation folder is"+ destionation);
       	    			
       	    			String copyresult=CopyAndReplace(sweepdata,destionation,textfilepath);
		        	    				
       	    			String newstatus="WaitingForComputeResponse";
       	    			
		        	    			if (copyresult.equals("1"))
		        	    			{
		        	    			
		        	    				String res=InsertJobList(instanceId, customerId, sweepdata, tooldata, companyname, workstationname, ref_jobid,destionation,index);
		        	    			
		        	    				String res3=UpdateJobQue(ref_jobid, workstationname, customerId, companyname, newstatus);
		        	    				
		        	    				String newstatus1="0";
		        	    				
		        	    				String res4=UpdateJobListStatus(ref_jobid, rowid, customerId, newstatus1);
		        	    			}
       	    			}
       	    		else
       	    			{
       	    			
       	    			
       	    			}
       	    		
       	    		}catch( Exception e)
       	    		{
       	    			
       	    			
       	    			
       	    		}
       	        	

   	            	
   	            }            	            
	        	}
	    
	        
	        	
	         } catch (Exception e) {
	            ////System.out.println(e);
	         }
		 
    }*/
    

	@Override
	public ArrayList<ProductVO> GetAllJobQue() throws ServiceException {
			List<HashMap<String, Object>> resultSet = null;
			List<String> values = new ArrayList<String>();
			
			ArrayList<ProductVO> instanceList = new ArrayList<ProductVO>();
			
			try {
				resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.get_All_Jobs, values);
			} catch (StoredProcException e) 
			{
				throw new ServiceException(e.getMessage(), e);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (resultSet != null) {
				
				
				//qId,designerworkstation,customer_refid,qstatus,requiredinstance,companyname,
				//processingpath,sweepdatapath,quedate
				
					try
					{
	            	
	            	for (Iterator<HashMap<String, Object>> iterator = resultSet
							.iterator(); iterator.hasNext();) {
						HashMap<?, ?> map = iterator.next();
					
						ProductVO info = new ProductVO();
			
						
						info.setQjobid(String.valueOf(map.get("qId")));
						info.setDesignerworkstation(String.valueOf(map.get("designerworkstation")));
						info.setCustomerid(String.valueOf(map.get("customer_refid")));
						info.setQstatus(String.valueOf(map.get("qstatus")));
						info.setRequiredinstance(String.valueOf(map.get("requiredinstance")));
						info.setCompanyname(String.valueOf(map.get("companyname")));
						info.setProcessingpath(String.valueOf(map.get("processingpath")));
						info.setSweepdatapath(String.valueOf(map.get("sweepdatapath")));
						info.setQuedate(String.valueOf(map.get("quedate")));
						info.setTextfilepath(String.valueOf(map.get("foldersweepdatamapping")));
						
	/*					info.setDatafilename(String.valueOf(map.get("datafilename")));
						info.setOrderid(String.valueOf(map.get("inorder")));
						info.setStatus(String.valueOf(map.get("status")));
						
						info.setjId(String.valueOf(map.get("id")));
	*/					
						instanceList.add(info);

	            	}
	            } catch (Exception e) {
	            	logger.info("Error"+e.getMessage());
	                throw new ServiceException(e.getMessage(), e);
	            }

			
			}

			logger.info("Returing list of instances: " + instanceList);

			//////System.out.println("Returing list of instances:"+instanceList);

			return instanceList;
		}


	public ArrayList<ProductVO> Get_all_Job_Allocation(String jobid) throws ServiceException {
			List<HashMap<String, Object>> resultSet = null;
			List<String> values = new ArrayList<String>();
			
			ArrayList<ProductVO> instanceList = new ArrayList<ProductVO>();
			
			values.add(jobid);
			
			try {
				resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.get_All_Jobs_Instance_ready, values);
			} catch (StoredProcException e) 
			{
				throw new ServiceException(e.getMessage(), e);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (resultSet != null) {
				
				
				//qId,designerworkstation,customer_refid,qstatus,requiredinstance,companyname,
				//processingpath,sweepdatapath,quedate
				
					try
					{
	            	
	            	for (Iterator<HashMap<String, Object>> iterator = resultSet
							.iterator(); iterator.hasNext();) {
						HashMap<?, ?> map = iterator.next();
					
						ProductVO info = new ProductVO();
			
						
						info.setQjobid(String.valueOf(map.get("qId")));
						info.setDesignerworkstation(String.valueOf(map.get("designerworkstation")));
						info.setCustomerid(String.valueOf(map.get("customer_refid")));
						info.setQstatus(String.valueOf(map.get("qstatus")));
						info.setRequiredinstance(String.valueOf(map.get("requiredinstance")));
						info.setCompanyname(String.valueOf(map.get("companyname")));
						info.setProcessingpath(String.valueOf(map.get("processingpath")));
						info.setSweepdatapath(String.valueOf(map.get("sweepdatapath")));
						info.setQuedate(String.valueOf(map.get("quedate")));
						info.setTextfilepath(String.valueOf(map.get("textfilepath")));
						
						info.setDatafilename(String.valueOf(map.get("datafilename")));
						info.setOrderid(String.valueOf(map.get("inorder")));
						info.setStatus(String.valueOf(map.get("status")));
						
						info.setjId(String.valueOf(map.get("Id")));
						
						instanceList.add(info);

	            	}
	            } catch (Exception e) {
	            	logger.info("Error"+e.getMessage());
	                throw new ServiceException(e.getMessage(), e);
	            }

			
			}

			logger.info("Returing list of instances: " + instanceList);

			//////System.out.println("Returing list of instances:"+instanceList);

			return instanceList;
		}

    

	//get_All_Collection_Jobs
	
	@Override
	public ArrayList<ProductVO> getExpandJob() throws ServiceException {
			List<HashMap<String, Object>> resultSet = null;
			List<String> values = new ArrayList<String>();
			
			ArrayList<ProductVO> instanceList = new ArrayList<ProductVO>();
			
			try {
				resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.get_All_Jobs_Expand, values);
			} catch (StoredProcException e) 
			{
				throw new ServiceException(e.getMessage(), e);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (resultSet != null) {
				
				
				//qId,designerworkstation,customer_refid,qstatus,requiredinstance,companyname,
				//processingpath,sweepdatapath,quedate
				
					try
					{
	            	
	            	for (Iterator<HashMap<String, Object>> iterator = resultSet
							.iterator(); iterator.hasNext();) {
						HashMap<?, ?> map = iterator.next();
					
						ProductVO info = new ProductVO();
			
						
						info.setQjobid(String.valueOf(map.get("qId")));
						info.setDesignerworkstation(String.valueOf(map.get("designerworkstation")));
						info.setCustomerid(String.valueOf(map.get("customer_refid")));
						info.setQstatus(String.valueOf(map.get("qstatus")));
						info.setRequiredinstance(String.valueOf(map.get("requiredinstance")));
						info.setCompanyname(String.valueOf(map.get("companyname")));
						info.setProcessingpath(String.valueOf(map.get("processingpath")));
						info.setSweepdatapath(String.valueOf(map.get("sweepdatapath")));
						info.setQuedate(String.valueOf(map.get("quedate")));
						info.setTextfilepath(String.valueOf(map.get("textfilepath")));
						
						instanceList.add(info);

	            	}
	            } catch (Exception e) {
	            	logger.info("Error"+e.getMessage());
	                throw new ServiceException(e.getMessage(), e);
	            }

			
			}

			logger.info("Returing list of instances: " + instanceList);

			//////System.out.println("Returing list of instances:"+instanceList);

			return instanceList;
		}



	
	
	@Override
	public ArrayList<ProductVO> get_All_Collection_Jobs() throws ServiceException {
			List<HashMap<String, Object>> resultSet = null;
			List<String> values = new ArrayList<String>();
			
			ArrayList<ProductVO> instanceList = new ArrayList<ProductVO>();
			
			try {
				resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.get_All_Collection_Jobs, values);
			} catch (StoredProcException e) 
			{
				throw new ServiceException(e.getMessage(), e);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (resultSet != null) {
				
				
				//qId,designerworkstation,customer_refid,qstatus,requiredinstance,companyname,
				//processingpath,sweepdatapath,quedate
				
					try
					{
	            	
	            	for (Iterator<HashMap<String, Object>> iterator = resultSet
							.iterator(); iterator.hasNext();) {
						HashMap<?, ?> map = iterator.next();
					
						ProductVO info = new ProductVO();
			
						
						info.setQjobid(String.valueOf(map.get("qId")));
						info.setDesignerworkstation(String.valueOf(map.get("designerworkstation")));
						info.setCustomerid(String.valueOf(map.get("customer_refid")));
						info.setQstatus(String.valueOf(map.get("qstatus")));
						info.setRequiredinstance(String.valueOf(map.get("requiredinstance")));
						info.setCompanyname(String.valueOf(map.get("companyname")));
						info.setProcessingpath(String.valueOf(map.get("processingpath")));
						info.setSweepdatapath(String.valueOf(map.get("sweepdatapath")));
						info.setQuedate(String.valueOf(map.get("quedate")));
						info.setTextfilepath(String.valueOf(map.get("textfilepath")));
						
						instanceList.add(info);

	            	}
	            } catch (Exception e) {
	            	logger.info("Error"+e.getMessage());
	                throw new ServiceException(e.getMessage(), e);
	            }

			
			}

			logger.info("Returing list of instances: " + instanceList);

			//////System.out.println("Returing list of instances:"+instanceList);

			return instanceList;
		}



	

	@Override
	public ArrayList<ProductVO> GetllAllRunningJob() throws ServiceException {
			List<HashMap<String, Object>> resultSet = null;
			List<String> values = new ArrayList<String>();
			
			ArrayList<ProductVO> instanceList = new ArrayList<ProductVO>();
			
			try {
				resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_All_Running_Job, values);
			} catch (StoredProcException e) 
			{
				throw new ServiceException(e.getMessage(), e);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (resultSet != null) {
				
				
				//qId,designerworkstation,customer_refid,qstatus,requiredinstance,companyname,
				//processingpath,sweepdatapath,quedate
				
					try
					{
	            	
	            	for (Iterator<HashMap<String, Object>> iterator = resultSet
							.iterator(); iterator.hasNext();) {
						HashMap<?, ?> map = iterator.next();
					
						ProductVO info = new ProductVO();
					
						info.setjId(String.valueOf(map.get("jId")));
info.setRef_instanceid(String.valueOf(map.get("ref_instanceid")));
info.setToolname(String.valueOf(map.get("toolname")));
info.setWorkingunit(String.valueOf(map.get("workingunit")));

						
						info.setQjobid(String.valueOf(map.get("qId")));
						info.setDesignerworkstation(String.valueOf(map.get("designerworkstation")));
						info.setCustomerid(String.valueOf(map.get("customer_refid")));
						info.setQstatus(String.valueOf(map.get("qstatus")));
						info.setRequiredinstance(String.valueOf(map.get("requiredinstance")));
						info.setCompanyname(String.valueOf(map.get("ref_companyname")));
						info.setProcessingpath(String.valueOf(map.get("processingpath")));
						info.setSweepdatapath(String.valueOf(map.get("sweepdatapath")));
						info.setQuedate(String.valueOf(map.get("quedate")));
						info.setTextfilepath(String.valueOf(map.get("textfilepath")));
						info.setAdalg(String.valueOf(map.get("ad_alg")));
						info.setUnitalg(String.valueOf(map.get("unit_alg")));
						info.setCorealg(String.valueOf(map.get("core_alg")));
						info.setAuto2d(String.valueOf(map.get("auto2d")));
						info.setProcessname(String.valueOf(map.get("running")));
				
						info.setNullproc(String.valueOf(map.get("nullproc")));
						info.setTransferflg(String.valueOf(map.get("transferflg")));
						
						info.setTextfilepath(String.valueOf(map.get("cs_destionation")));
						
						String ptime=String.valueOf(map.get("starttime"));
						
						info.setPasstime(Constants.GetElispTimeNotification(String.valueOf(map.get("starttime"))));
						
						try{
							
							info.setPublicip(getInstanceDetails(String.valueOf(map.get("ref_instanceid"))));
								}catch(Exception e){}
						
						String endtime=String.valueOf(map.get("endtime"));
					/*	
						if (!endtime.equals("null"))
						{
							//System.out.println("endtime-->"+endtime);
							info.setPasstime(Constants.GetElispeTwoDateFormNotifcation(ptime,endtime));
									try{
								
								info.setPublicip(getInstanceDetails(String.valueOf(map.get("ref_instanceid"))));
									}catch(Exception e){}
							
						}else
						{}*/
			
						
						instanceList.add(info);

	            	}
	            } catch (Exception e) {
	            	logger.info("Error"+e.getMessage());
	                throw new ServiceException(e.getMessage(), e);
	            }

			
			}

			logger.info("Returing list of instances: " + instanceList);

			////System.out.println("Returing list of instances:"+instanceList);

			return instanceList;
		}




	public String getInstanceDetails(String instanceId)

	{
	//System.out.println("Entering transfer instance id:"+ instanceId);
	
		String publicip = null;
	ArrayList<ProductVO> instancedetails = new ArrayList<ProductVO>();

		ProductVO info=new ProductVO();
		
		try {

			AWSCredentials credentials = new BasicAWSCredentials(accesskey,
					sckreetKey);

			String regionval = "US_EAST_1";

			AmazonEC2 ec2 = new AmazonEC2Client(credentials);
			Region ec2region = Region.getRegion(Regions.valueOf(regionval));
			ec2.setRegion(ec2region);

		//	AmazonEC2 ec2 = AmazonEC2ClientBuilder.standard()
	      //          .withRegion(Regions.valueOf(regionval))
	        //        .build();


			DescribeInstancesRequest describeInstanceRequest = new DescribeInstancesRequest()
					.withInstanceIds(instanceId);
			DescribeInstancesResult describeInstanceResult = ec2
					.describeInstances(describeInstanceRequest);
			////System.out.println("Ec2out:"+ec2.toString());
			String state = describeInstanceResult.getReservations()
					.get(0).getInstances().get(0).getState().getName();

		String privateip = describeInstanceResult.getReservations()
					.get(0).getInstances().get(0).getPrivateIpAddress();


	 publicip = describeInstanceResult.getReservations()
					.get(0).getInstances().get(0).getPublicIpAddress();

		
		info.setState(state.toString());
		info.setPrivateip(privateip);
		instancedetails.add(info);
				
				} 
		catch (Exception e) 
		{
			//System.out.println("Instance state Exception ------------->"+e.getMessage());
			//return e.getMessage();

		}
	
		//System.out.println("Returning instance status"+instancedetails);
		return publicip;
	}

	
	
	
	@Override
	public ArrayList<ProductVO> GetAllJobWaitingForComputeQue() throws ServiceException {
			List<HashMap<String, Object>> resultSet = null;
			List<String> values = new ArrayList<String>();
			
			ArrayList<ProductVO> instanceList = new ArrayList<ProductVO>();
			
			try {
				resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.get_All_WaitingForCompute, values);
			} catch (StoredProcException e) 
			{
				throw new ServiceException(e.getMessage(), e);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (resultSet != null) {
				
				
				//qId,designerworkstation,customer_refid,qstatus,requiredinstance,companyname,
				//processingpath,sweepdatapath,quedate
				
					try
					{
	            	
	            	for (Iterator<HashMap<String, Object>> iterator = resultSet
							.iterator(); iterator.hasNext();) {
						HashMap<?, ?> map = iterator.next();
					
						ProductVO info = new ProductVO();
			
						
						info.setQjobid(String.valueOf(map.get("qId")));
						info.setDesignerworkstation(String.valueOf(map.get("designerworkstation")));
						info.setCustomerid(String.valueOf(map.get("customer_refid")));
						info.setQstatus(String.valueOf(map.get("qstatus")));
						info.setRequiredinstance(String.valueOf(map.get("requiredinstance")));
						info.setCompanyname(String.valueOf(map.get("companyname")));
						info.setProcessingpath(String.valueOf(map.get("processingpath")));
						info.setSweepdatapath(String.valueOf(map.get("sweepdatapath")));
						info.setQuedate(String.valueOf(map.get("quedate")));
						info.setTextfilepath(String.valueOf(map.get("textfilepath")));
						
						instanceList.add(info);

	            	}
	            } catch (Exception e) {
	            	logger.info("Error"+e.getMessage());
	                throw new ServiceException(e.getMessage(), e);
	            }

			
			}

			logger.info("Returing list of instances: " + instanceList);

			////System.out.println("Returing list of instances:"+instanceList);

			return instanceList;
		}




	@Override
	public ArrayList<ProductVO> GetOngoingJobDetails(String jobid, 
			String instanceid,String customerid,String companyname,String designerwks) throws ServiceException {
			List<HashMap<String, Object>> resultSet = null;
			
			List<String> values = new ArrayList<String>();
			
			values.add(jobid);
			values.add(customerid);
			values.add(companyname);
			values.add(designerwks);
			
			ArrayList<ProductVO> instanceList = new ArrayList<ProductVO>();
			
			try {
				resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_Job_Details_By_Job_Id, values);
			} catch (StoredProcException e) 
			{
				throw new ServiceException(e.getMessage(), e);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (resultSet != null) {
				
				
				//qId,designerworkstation,customer_refid,qstatus,requiredinstance,companyname,
				//processingpath,sweepdatapath,quedate
				
					try
					{
	            	
	            	for (Iterator<HashMap<String, Object>> iterator = resultSet
							.iterator(); iterator.hasNext();) {
						HashMap<?, ?> map = iterator.next();
					
						ProductVO info = new ProductVO();
						
						info.setQjobid(String.valueOf(map.get("ref_que_id")));
						info.setDesignerworkstation(String.valueOf(map.get("designerworkstation")));
						info.setCustomerid(String.valueOf(map.get("customer_refid")));
						info.setCompanyname(String.valueOf(map.get("companyname")));
						info.setTextfilepath(String.valueOf(map.get("cs_destionation")));
						info.setInstanceid(String.valueOf(map.get("ref_instanceid")));
						info.setjId(String.valueOf(map.get("jId")));
						
						instanceList.add(info);

						
						
	            	}
	            } catch (Exception e) {
	            	logger.info("Error"+e.getMessage());
	                throw new ServiceException(e.getMessage(), e);
	            }

			
			}

			logger.info("Returing list of instances: " + instanceList);

			//////System.out.println("Returing list of instances:"+instanceList);

			return instanceList;
		}



	
	

	@Override
	public ArrayList<ProductVO> GetExecuting_Jobs(String jobid, 
			String customerid,String companyname,String designerwks) throws ServiceException {
			List<HashMap<String, Object>> resultSet = null;
			
			List<String> values = new ArrayList<String>();
			
			values.add(jobid);
			values.add(customerid);
			values.add(companyname);
			values.add(designerwks);
			
			ArrayList<ProductVO> instanceList = new ArrayList<ProductVO>();
			
			try {
				resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_Job_Details_By_Job_Id, values);
			} catch (StoredProcException e) 
			{
				throw new ServiceException(e.getMessage(), e);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (resultSet != null) {
				
				
				//qId,designerworkstation,customer_refid,qstatus,requiredinstance,companyname,
				//processingpath,sweepdatapath,quedate
				
					try
					{
	            	
	            	for (Iterator<HashMap<String, Object>> iterator = resultSet
							.iterator(); iterator.hasNext();) {
						HashMap<?, ?> map = iterator.next();
					
						ProductVO info = new ProductVO();
						
						info.setQjobid(String.valueOf(map.get("ref_que_id")));
						info.setDesignerworkstation(String.valueOf(map.get("designerworkstation")));
						info.setCustomerid(String.valueOf(map.get("customer_refid")));
						info.setCompanyname(String.valueOf(map.get("companyname")));
						info.setTextfilepath(String.valueOf(map.get("cs_destionation")));
						info.setInstanceid(String.valueOf(map.get("ref_instanceid")));
						info.setjId(String.valueOf(map.get("jId")));
						info.setProcessingstatus(String.valueOf(map.get("processingstatus")));
						
						
						instanceList.add(info);

						
						
	            	}
	            } catch (Exception e) {
	            	logger.info("Error"+e.getMessage());
	                throw new ServiceException(e.getMessage(), e);
	            }

			
			}

			logger.info("Returing list of instances: " + instanceList);

			//////System.out.println("Returing list of instances:"+instanceList);

			return instanceList;
		}


	
	

	@Override
	public ArrayList<ProductVO> GetCollectionJobStatusList(String ref_jobid,String ref_customerid,String operationflg) throws ServiceException {
			List<HashMap<String, Object>> resultSet = null;
			List<String> values = new ArrayList<String>();
			
			values.add(ref_jobid);
			values.add(operationflg);
			values.add(ref_customerid);

			ArrayList<ProductVO> instanceList = new ArrayList<ProductVO>();
			
			try {
				resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_All_collectionJobLisStatus, values);
			} catch (StoredProcException e) 
			{
				throw new ServiceException(e.getMessage(), e);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (resultSet != null) {
				
				
				//qId,designerworkstation,customer_refid,qstatus,requiredinstance,companyname,
				//processingpath,sweepdatapath,quedate
				
					try
					{
	            	
	            	for (Iterator<HashMap<String, Object>> iterator = resultSet
							.iterator(); iterator.hasNext();) {
						HashMap<?, ?> map = iterator.next();
					
						ProductVO info = new ProductVO();
			
						
					//	jId,ref_instanceid,designerworkstation,toolname,workingunit,ref_companyname,processingstatus,
						//starttime,endtime,duration,operationflg,jobdate,ref_customerid,inque,schedule,
						//running,complete,ready,ref_que_id,sweepdate,tooldata,cs_destionation,jobseqorder
						
						info.setJobrowid(String.valueOf(map.get("jId")));
						
						info.setInstanceid(String.valueOf(map.get("ref_instanceid")));
						info.setDesignerworkstation(String.valueOf(map.get("designerworkstation")));
						info.setToolname(String.valueOf(map.get("toolname")));
						info.setWorkingunit(String.valueOf(map.get("workingunit")));
						info.setCompanyname(String.valueOf(map.get("ref_companyname")));
						info.setProcessingstatus(String.valueOf(map.get("processingstatus")));
						info.setStarttime(String.valueOf(map.get("starttime")));
						info.setEndtime(String.valueOf(map.get("endtime")));
						info.setDuration(String.valueOf(map.get("duration")));
						info.setOperationflg(String.valueOf(map.get("operationflg")));
						info.setJobdate(String.valueOf(map.get("jobdate")));
						info.setJobdate(String.valueOf(map.get("jobdate")));
						info.setCustomerid(String.valueOf(map.get("ref_customerid")));
						info.setSchedule(String.valueOf(map.get("schedule")));	
						info.setRunning(String.valueOf(map.get("running")));	
						info.setComplete(String.valueOf(map.get("complete")));	
						info.setReady(String.valueOf(map.get("ready")));
						info.setQjobid(String.valueOf(map.get("ref_que_id")));
						
						info.setSweepdatapath(String.valueOf(map.get("sweepdate")));
						info.setTooldatapath(String.valueOf(map.get("tooldata")));
						
						info.setCsdestionation(String.valueOf(map.get("cs_destionation")));
						info.setOrderid(String.valueOf(map.get("jobseqorder")));
						info.setPrivateip(String.valueOf(map.get("privateip")));
						info.setCopysourcefolder(String.valueOf(map.get("sourcedata")));
						info.setTextfilepath(String.valueOf(map.get("processingfile")));
						info.setRegion(String.valueOf(map.get("region")));
						instanceList.add(info);

	            	}
	            } catch (Exception e) {
	            	logger.info("Error"+e.getMessage());
	                throw new ServiceException(e.getMessage(), e);
	            }

			
			}

			logger.info("Returing list of instances: " + instanceList);

			////System.out.println("Returing list of instances:"+instanceList);

			return instanceList;
		}

	

	

	
	@Override
	public ArrayList<ProductVO> GetAllMonitoringJob(String Status) throws ServiceException {
			List<HashMap<String, Object>> resultSet = null;
			List<String> values = new ArrayList<String>();
			
			values.add(Status);
			
			ArrayList<ProductVO> instanceList = new ArrayList<ProductVO>();
			
			try {
				resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_All_Monitoring_Job, values);
			} catch (StoredProcException e) 
			{
				throw new ServiceException(e.getMessage(), e);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (resultSet != null) {
				
				
				//qId,designerworkstation,customer_refid,qstatus,requiredinstance,companyname,
				//processingpath,sweepdatapath,quedate
				
					try
					{
	            	
	            	for (Iterator<HashMap<String, Object>> iterator = resultSet
							.iterator(); iterator.hasNext();) {
						HashMap<?, ?> map = iterator.next();
					
						ProductVO info = new ProductVO();
			
						
					//	jId,ref_instanceid,designerworkstation,toolname,workingunit,ref_companyname,processingstatus,
						//starttime,endtime,duration,operationflg,jobdate,ref_customerid,inque,schedule,
						//running,complete,ready,ref_que_id,sweepdate,tooldata,cs_destionation,jobseqorder
						
						info.setJobrowid(String.valueOf(map.get("jId")));
						
						info.setInstanceid(String.valueOf(map.get("ref_instanceid")));
						info.setDesignerworkstation(String.valueOf(map.get("designerworkstation")));
						info.setToolname(String.valueOf(map.get("toolname")));
						info.setWorkingunit(String.valueOf(map.get("workingunit")));
						info.setCompanyname(String.valueOf(map.get("ref_companyname")));
						info.setProcessingstatus(String.valueOf(map.get("processingstatus")));
						info.setStarttime(String.valueOf(map.get("starttime")));
						info.setEndtime(String.valueOf(map.get("endtime")));
						info.setDuration(String.valueOf(map.get("duration")));
						info.setOperationflg(String.valueOf(map.get("operationflg")));
						info.setJobdate(String.valueOf(map.get("jobdate")));
						info.setJobdate(String.valueOf(map.get("jobdate")));
						info.setCustomerid(String.valueOf(map.get("ref_customerid")));
						info.setSchedule(String.valueOf(map.get("schedule")));	
						info.setRunning(String.valueOf(map.get("running")));	
						info.setComplete(String.valueOf(map.get("complete")));	
						info.setReady(String.valueOf(map.get("ready")));
						info.setQjobid(String.valueOf(map.get("ref_que_id")));
						
						info.setSweepdatapath(String.valueOf(map.get("sweepdate")));
						info.setTooldatapath(String.valueOf(map.get("tooldata")));
						
						info.setCsdestionation(String.valueOf(map.get("cs_destionation")));
						info.setOrderid(String.valueOf(map.get("jobseqorder")));
						info.setPrivateip(String.valueOf(map.get("privateip")));
						info.setCopysourcefolder(String.valueOf(map.get("sourcedata")));
						info.setTextfilepath(String.valueOf(map.get("processingfile")));
						info.setRegion(String.valueOf(map.get("region")));
						instanceList.add(info);

	            	}
	            } catch (Exception e) {
	            	logger.info("Error"+e.getMessage());
	                throw new ServiceException(e.getMessage(), e);
	            }

			
			}

			logger.info("Returing list of instances: " + instanceList);

			////System.out.println("Returing list of instances:"+instanceList);

			return instanceList;
		}

	
	
	
	
	
	@Override
	public ArrayList<ProductVO> GetAllJobsListForCopyContent() throws ServiceException {
			List<HashMap<String, Object>> resultSet = null;
			List<String> values = new ArrayList<String>();
			
			ArrayList<ProductVO> instanceList = new ArrayList<ProductVO>();
			
			try {
				resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_AllJobsForCopy, values);
			} catch (StoredProcException e) 
			{
				throw new ServiceException(e.getMessage(), e);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (resultSet != null) {
				
				
				//qId,designerworkstation,customer_refid,qstatus,requiredinstance,companyname,
				//processingpath,sweepdatapath,quedate
				
					try
					{
	            	
	            	for (Iterator<HashMap<String, Object>> iterator = resultSet
							.iterator(); iterator.hasNext();) {
						HashMap<?, ?> map = iterator.next();
					
						ProductVO info = new ProductVO();
			
						
					//	jId,ref_instanceid,designerworkstation,toolname,workingunit,ref_companyname,processingstatus,
						//starttime,endtime,duration,operationflg,jobdate,ref_customerid,inque,schedule,
						//running,complete,ready,ref_que_id,sweepdate,tooldata,cs_destionation,jobseqorder
						
						info.setJobrowid(String.valueOf(map.get("jId")));
						
						info.setInstanceid(String.valueOf(map.get("ref_instanceid")));
						info.setDesignerworkstation(String.valueOf(map.get("designerworkstation")));
						info.setToolname(String.valueOf(map.get("toolname")));
						info.setWorkingunit(String.valueOf(map.get("workingunit")));
						info.setCompanyname(String.valueOf(map.get("ref_companyname")));
						info.setProcessingstatus(String.valueOf(map.get("processingstatus")));
						info.setStarttime(String.valueOf(map.get("starttime")));
						info.setEndtime(String.valueOf(map.get("endtime")));
						info.setDuration(String.valueOf(map.get("duration")));
						info.setOperationflg(String.valueOf(map.get("operationflg")));
						info.setJobdate(String.valueOf(map.get("jobdate")));
						info.setJobdate(String.valueOf(map.get("jobdate")));
						info.setCustomerid(String.valueOf(map.get("ref_customerid")));
						info.setSchedule(String.valueOf(map.get("schedule")));	
						info.setRunning(String.valueOf(map.get("running")));	
						info.setComplete(String.valueOf(map.get("complete")));	
						info.setReady(String.valueOf(map.get("ready")));
						info.setQjobid(String.valueOf(map.get("ref_que_id")));
						
						info.setSweepdatapath(String.valueOf(map.get("sweepdate")));
						info.setTooldatapath(String.valueOf(map.get("tooldata")));
						
						info.setCsdestionation(String.valueOf(map.get("cs_destionation")));
						info.setOrderid(String.valueOf(map.get("jobseqorder")));
						info.setPrivateip(String.valueOf(map.get("privateip")));
						info.setCopysourcefolder(String.valueOf(map.get("sourcedata")));
						info.setTextfilepath(String.valueOf(map.get("processingfile")));
						info.setTransferflg(String.valueOf(map.get("transferflg")));
						
						instanceList.add(info);

	            	}
	            } catch (Exception e) {
	            	logger.info("Error"+e.getMessage());
	                throw new ServiceException(e.getMessage(), e);
	            }

			
			}

			logger.info("Returing list of instances: " + instanceList);

			//////System.out.println("Returing list of instances:"+instanceList);

			return instanceList;
		}


	
    
	@Override
	public ArrayList<ProductVO> GetAllInstanceStatus() throws ServiceException {
			List<HashMap<String, Object>> resultSet = null;
			List<String> values = new ArrayList<String>();
			
			ArrayList<ProductVO> instanceList = new ArrayList<ProductVO>();
			
			try {
				resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_InstanceList, values);
			} catch (StoredProcException e) 
			{
				throw new ServiceException(e.getMessage(), e);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (resultSet != null) {
				
				
					try
					{
	            	
	            	for (Iterator<HashMap<String, Object>> iterator = resultSet
							.iterator(); iterator.hasNext();) {
						HashMap<?, ?> map = iterator.next();
					
						ProductVO info = new ProductVO();
						info.setInstanceid(String.valueOf(map.get("instanceid")));
						info.setInrId(String.valueOf(map.get("InrId")));
						info.setRegion(String.valueOf(map.get("region")));
						
						instanceList.add(info);

	            	}
	            } catch (Exception e) {
	            	logger.info("Error"+e.getMessage());
	                throw new ServiceException(e.getMessage(), e);
	            }

			
			}

			logger.info("Returing list of instances: " + instanceList);

			//////System.out.println("Returing list of instances:"+instanceList);

			return instanceList;
		}



	

	@Override
	public ArrayList<ProductVO> GetAllStoppedInstanceStatus() throws ServiceException {
			List<HashMap<String, Object>> resultSet = null;
			List<String> values = new ArrayList<String>();
			
			ArrayList<ProductVO> instanceList = new ArrayList<ProductVO>();
			
			try {
				resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_InstanceList, values);
			} catch (StoredProcException e) 
			{
				throw new ServiceException(e.getMessage(), e);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (resultSet != null) {
				
				
					try
					{
	            	
	            	for (Iterator<HashMap<String, Object>> iterator = resultSet
							.iterator(); iterator.hasNext();) {
						HashMap<?, ?> map = iterator.next();
						
						String instatus=getInstanceStatus(String.valueOf(map.get("instanceid")),String.valueOf(map.get("region")),"vectra");
					
						if("stopped".equalsIgnoreCase(instatus))
							
						{
						ProductVO info = new ProductVO();
			
						info.setInstanceid(String.valueOf(map.get("instanceid")));
						info.setInrId(String.valueOf(map.get("InrId")));
						info.setRegion(String.valueOf(map.get("region")));
						info.setState(instatus);
						instanceList.add(info);
						}
	            	}
	            } catch (Exception e) {
	            	logger.info("Error"+e.getMessage());
	                throw new ServiceException(e.getMessage(), e);
	            }

			
			}

			logger.info("Returing list of instances: " + instanceList);

			//////System.out.println("Returing list of instances:"+instanceList);

			return instanceList;
		}

	
	@Override
	public ArrayList<ProductVO> GetAllCustomerList() throws ServiceException {
			List<HashMap<String, Object>> resultSet = null;
			List<String> values = new ArrayList<String>();
			
			ArrayList<ProductVO> customerList = new ArrayList<ProductVO>();
			
			try {
				resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_All_Customer_List, values);
			} catch (StoredProcException e) 
			{
				throw new ServiceException(e.getMessage(), e);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (resultSet != null) {
				
				
					try
					{
	            	
	            	for (Iterator<HashMap<String, Object>> iterator = resultSet
							.iterator(); iterator.hasNext();) {
						HashMap<?, ?> map = iterator.next();
					
						ProductVO info = new ProductVO();
						info.setCompanyname(String.valueOf(map.get("companyname")));
						info.setCustomerid(String.valueOf(map.get("customerid")));
						info.setFtpmappingpath(String.valueOf(map.get("ftpmappingpath")));
						info.setCrId(String.valueOf(map.get("crId")));
						customerList.add(info);

	            	}
	            } catch (Exception e) {
	            	logger.info("Error"+e.getMessage());
	                throw new ServiceException(e.getMessage(), e);
	            }

			
			}

			logger.info("Returing list of customer: " + customerList);

			////System.out.println("Returing list of customer:"+customerList);

			return customerList;
		}


	
	
	@Override
	public ArrayList<ProductVO> Get_InstanceHostName(String instanceid) throws ServiceException {
			List<HashMap<String, Object>> resultSet = null;
			List<String> values = new ArrayList<String>();
			
			ArrayList<ProductVO> customerList = new ArrayList<ProductVO>();
			values.add(instanceid);
			try {
				resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.get_instance_HostName, values);
			} catch (StoredProcException e) 
			{
				throw new ServiceException(e.getMessage(), e);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (resultSet != null) {
				
				
					try
					{
	            	
	            	for (Iterator<HashMap<String, Object>> iterator = resultSet
							.iterator(); iterator.hasNext();) {
						HashMap<?, ?> map = iterator.next();
					
						ProductVO info = new ProductVO();
						info.setCompanyname(String.valueOf(map.get("companyname")));
						info.setRef_instanceid(String.valueOf(map.get("hostname")));
						customerList.add(info);

	            	}
	            } catch (Exception e) {
	            	logger.info("Error"+e.getMessage());
	                throw new ServiceException(e.getMessage(), e);
	            }

			
			}

			logger.info("Returing list of customer: " + customerList);

			////System.out.println("Returing list of customer:"+customerList);

			return customerList;
		}

	
	
	private String instanceStatus(String instanceId,String rowid,String region)
	{
		String state="stopped";
		String hostname="ABC";
		String privateIp="";
		
		
		getInstanceStatus(instanceId, region, rowid);
		
		
		try 
		{
			
			UpdateInstanceInformation(instanceId, rowid, state, privateIp, hostname);
	
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//////System.out.println("Access Key"+accesskey +sckreetKey+ "-->"+instanceId+"-->"+rowid +" --"+region);
		return state;
	}
	
	
	
	

	@Override
	public String UpdateInstanceInformation(String instanceid,String rowid,String state, String privateIp,String hostname)
			throws ServiceException {
		// TODO Auto-generated method stub
		
	List<String> values = new ArrayList<String>();
		
		values.add(instanceid);
		values.add(rowid);
		values.add(state);
		values.add(privateIp);
		values.add(hostname);
		
		
		String res=null;
		try {
			  	res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Update_Instance_Status, values);
			  //	logger.info("Update Instance:"+ values);

		} catch (StoredProcException e) {
		  	logger.info("Failed to Update Instance:"+ values +e.getMessage());

			throw new ServiceException(e.getMessage(), e);
		}

		return res;
		
		
	}


	
	


	@Override
	public String UpdateCopyJobStatus(String instanceid,String jId,String newstatus,String customerid)
			throws ServiceException {
		// TODO Auto-generated method stub
		
	List<String> values = new ArrayList<String>();
		
		values.add(instanceid);
		values.add(jId);
		values.add(newstatus);
		values.add(customerid);
		
		String res=null;
		try {
			
			////System.out.println("New update status system:"+values);
			
			  	res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Update_Job_Copy_Status, values);
			  //	logger.info("Update Instance:"+ values);

		} catch (StoredProcException e) {
		  	logger.info("Failed to Update Instance:"+ values +e.getMessage());

			throw new ServiceException(e.getMessage(), e);
		}

		return res;
		
		
	}



	

	@Override
	public String UpdateJobStatus(String instanceid,String jId,String customerid,String company,String status,String ref_que_id)
			throws ServiceException {
		// TODO Auto-generated method stub
		
	List<String> values = new ArrayList<String>();
		
		values.add(instanceid);
		values.add(jId);
		values.add(customerid);
		values.add(company);
		values.add(status);
		values.add(ref_que_id);
		
		String res=null;
		try {
			  	res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Update_Job_Status, values);
			  //	logger.info("Update Instance:"+ values);

		} catch (StoredProcException e) {
		  	logger.info("Failed to Update Instance:"+ values +e.getMessage());

			throw new ServiceException(e.getMessage(), e);
		}

		return res;
		
		
	}

	
	
	
	public String InsertExpandJob(String ref_jobid, String datafileval, int orderval,String customerid)
			throws ServiceException {
		// TODO Auto-generated method stub
		
	List<String> values = new ArrayList<String>();
		
		values.add(ref_jobid);
		values.add(datafileval);
		values.add(String.valueOf(orderval));
		values.add(customerid);
		
		String res=null;
		
		try {
			  	res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.PreparedJobListQue, values);
			  //	logger.info("Update Instance:"+ values);

		} catch (StoredProcException e) {
		  	logger.info("Failed to Update Instance:"+ values +e.getMessage());

			throw new ServiceException(e.getMessage(), e);
		}

		return res;
		
		
	}



	public String updateExpandJob(String ref_jobid, String customerid )
			throws ServiceException {
		// TODO Auto-generated method stub
		
	List<String> values = new ArrayList<String>();
		
		values.add(ref_jobid);
		values.add(customerid);
	
		String res=null;
		try {
			  	res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.updatejobquevalue, values);
			  //	logger.info("Update Instance:"+ values);

		} catch (StoredProcException e) {
		  	logger.info("Failed to Update Instance:"+ values +e.getMessage());

			throw new ServiceException(e.getMessage(), e);
		}

		return res;
		
		
	}


	
	@Override
	public String UpdateAutoJobTime(String jId,String flg,String time)
			throws ServiceException {
		// TODO Auto-generated method stub
		
	List<String> values = new ArrayList<String>();
		
	values.add(time);
			values.add(jId);
		
		
		
		String res=null;
		try {
			
			////System.out.println("New update status system:"+values);
			
			if (flg.equals("ada_alg"))
			{
			  	res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Update_ada_alg, values);
			  //	logger.info("Update Instance:"+ values);
			}else	if (flg.equals("unit_alg"))
			{
			
				System.out.println("Updating Unit_alg"+time+"job id"+jId);
			
			  	res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Update_Unit_alg, values);
			  //	logger.info("Update Instance:"+ values);
			}else if (flg.equals("core_alg"))
			{
			  	res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Update_core_alg, values);
			  //	logger.info("Update Instance:"+ values);
			}
			else if (flg.equals("auto2d"))
			{
				System.out.println("Updating Auto2d"+time+"job id"+jId);
				
			  	res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Update_Auto2D_alg, values);
			  //	logger.info("Update Instance:"+ values);
			}
			else if (flg.equals("nullproc"))
			{
			  	res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Update_NullProce_alg, values);
			  //	logger.info("Update Instance:"+ values);
			}
			
			
			
		} catch (StoredProcException e) {
		  	logger.info("Failed to Update Instance:"+ values +e.getMessage());

			throw new ServiceException(e.getMessage(), e);
		}

		return res;
		
		
	}

	
	

	
	private String InsertJobList(String instanceId,String customerId,
			String sweepdata,
			String tooldata,
			String companyname,
			String workstationname,
			String ref_jobid,
			String destionation,
			int index,
			String privateip,
			String sourcecopyfolder,
			String jobquerowid,String datafile)
			throws ServiceException {
		// TODO Auto-generated method stub
		
	List<String> values = new ArrayList<String>();
		
		values.add(instanceId);
		values.add(customerId);
		values.add(sweepdata);
		values.add(tooldata);
		values.add(companyname);
		values.add(workstationname);
		values.add(ref_jobid);
		values.add(destionation);
		values.add(String.valueOf(index));
		values.add(privateip);
		values.add(sourcecopyfolder);
		values.add(jobquerowid);
		values.add(datafile);
		
		String res=null;
		try
		{
			
				////System.out.println("Inserting job Que Details:"+ values);
				
			  	res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.InsertJobList, values);
			  //	logger.info("Update Instance:"+ values);

		} catch (StoredProcException e)
		{
		  	logger.info("Failed to Update Instance:"+ values +e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}

		return res;
		
		
	}




	private String UpdateJobQue(String jobid,String workstation,String customerid,String companyname,String newstatus)
			throws ServiceException {
		// TODO Auto-generated method stub
		
	List<String> values = new ArrayList<String>();
		
		values.add(jobid);
		values.add(workstation);
		values.add(customerid);
		values.add(companyname);
		values.add(newstatus);
		
		String res=null;
		try {
			  	res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.UpdateStatus_Que, values);
			  //	logger.info("Update Instance:"+ values);

		} catch (StoredProcException e) {
		  	logger.info("Failed to Update Instance:"+ values +e.getMessage());

			throw new ServiceException(e.getMessage(), e);
		}

		return res;
		
		
	}


	
	
	private String UpdateJobListStatus(String jobid,String rowid,String customerid,String newstatus)
			throws ServiceException {
		// TODO Auto-generated method stub
		
	List<String> values = new ArrayList<String>();
		
		values.add(jobid);
		values.add(rowid);
		values.add(customerid);
		values.add(newstatus);
		
		String res=null;
		try 
		{
			////System.out.println("Update JobQueList with values:"+values);
			
			  	res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.UpdateJobListStatusQue, values);
			
			  	//	logger.info("Update Instance:"+ values);

		} catch (StoredProcException e) {
		  	logger.info("Failed to Update Instance:"+ values +e.getMessage());

			throw new ServiceException(e.getMessage(), e);
		}

		return res;
		
		
	}

	

	
	

	private String Update_Operation_Flg(String instanceid,String rowid,String customerid,String newstatus,String processlist)
			throws ServiceException {
		// TODO Auto-generated method stub
		
	List<String> values = new ArrayList<String>();
		
		values.add(instanceid);
		values.add(rowid);
		values.add(newstatus);
		values.add(customerid);
		
		values.add(processlist);
		
		String res=null;
		try 
		{
			////System.out.println("Update JobOperational Status with values:"+values);
			
			  	res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Update_Job_OperationStatus, values);
			
			  	//	logger.info("Update Instance:"+ values);

		} catch (StoredProcException e) {
		  	logger.info("Failed to Update Instance:"+ values +e.getMessage());

			throw new ServiceException(e.getMessage(), e);
		}

		return res;
		
		
	}


	private String FinalJobUpdate(String instanceid,String rowid,String customerid,String newstatus,String processlist)
			throws ServiceException {
		// TODO Auto-generated method stub
		
	List<String> values = new ArrayList<String>();
		
		values.add(instanceid);
		values.add(rowid);
		values.add(newstatus);
		values.add(customerid);
		
		values.add(processlist);
		
		String res=null;
		try 
		{
			////System.out.println("Update JobOperational Status with values:"+values);
			
			  	res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Update_Job_Final, values);
			
			  	//	logger.info("Update Instance:"+ values);

		} catch (StoredProcException e) {
		  	logger.info("Failed to Update Instance:"+ values +e.getMessage());

			throw new ServiceException(e.getMessage(), e);
		}

		return res;
		
		
	}



	
	
	
	//Checking customer Job
	

	private String CheckJobQue(String foldermappingpath,String companyname, ArrayList<ProductVO> stoppedinstancelist,String customerid )
	{
		
		try
		{
		////System.out.println("Total in Job Que:"+foldermappingpath+"-->"+companyname +"--->"+stoppedinstancelist.toString() +"--->"+customerid);
		
		String state="stopped";
		String hostname="ABC";
		String privateIp="";
		String executionpath="";
		String sweepdatapath="";
		String transferfilepath="";
		String transferfilepathnew="";
		String textfilepath="";
		String designerworkstation="";
		//int reqinstance=0;
		
		//////System.out.println("Entering in check job Que+ "+companyname+ foldermappingpath);
		
		File[] directories=new File(foldermappingpath).listFiles(File::isDirectory);
		
		ArrayList<ProductVO> listOfUnitFiles = new ArrayList<ProductVO>();
		
		
		for (File file : directories) 
		{
		
			int reqinstance=0;
			
		//	////System.out.println("File path for transfer checking"+file.getAbsolutePath());
			
			File f = new File(file.getAbsolutePath()+"/Transfer_Complete.txt");
			
			
			if(f.exists())
			{
				String unitsweepdatapath=unitdata;
						
				transferfilepath=f.getAbsolutePath();

				transferfilepathnew=f.getParent();
				
				//////System.out.println("Transfer file path--->"+ transferfilepath + "    -->transferfilepathnew-->"+transferfilepathnew);
				
				designerworkstation=file.getName();	
				
				sweepdatapath=foldermappingpath+"/"+file.getName()+"/"+unitsweepdatapath;
				
				//////System.out.println("Unit SwapData---->"+sweepdatapath);
				
				
				textfilepath=foldermappingpath+"/"+file.getName();
				
				System.out.println("Unit Sweep Data---->"+foldermappingpath+"/"+file.getName()+"/"+unitsweepdatapath);
				
				File[] unitdatadirectory=new File(foldermappingpath+"/"+file.getName()+"/"+unitsweepdatapath).listFiles(File::isDirectory);
				
				
				System.out.println("Files in unit Sweep_Data"+unitdatadirectory.toString());
				
				for (File file2 : unitdatadirectory) 
						{
					
						File[] maadirectories=new File(file2.getAbsolutePath()).listFiles(File::isFile);
					
						
						for (File maafile3 : maadirectories) 
						{
							if (maafile3.getName().contains("Unit_Sweep_Data_List_"))
									{
								if(!maafile3.getName().contains("Unit_Sweep_Data_List_Master"))
									
								{
									ProductVO info=new ProductVO();
									reqinstance=reqinstance+1;
									info.setFilename(maafile3.getName());
									info.setFilepath(maafile3.getAbsolutePath());
									info.setFtpmappingpath(foldermappingpath);
									info.setCompanyname(companyname);
									info.setParentpath(maafile3.getParent().toString());
									info.setTextfilepath(textfilepath);
									
									//////System.out.println("Files in maa directory--->"+maafile3.getName()+"--->"+maafile3.getAbsolutePath()+"Parent---->"+maafile3.getParent());
									
									listOfUnitFiles.add(info);
								}
								}
							
							}
						
						
					
							

						}
				
				
				////System.out.println("List of unit File size----->"+ listOfUnitFiles.size());
				
				if (listOfUnitFiles.size()>0)
				{
				

					try 
					{
						
						String res =CreateJobQueInDB(designerworkstation, customerid, reqinstance, companyname, listOfUnitFiles.get(0).getParentpath().toString(),sweepdatapath,listOfUnitFiles.get(0).getTextfilepath());
					
						
						if ("Success".equals(res))
						{
								
							
							File fileinq = new File(transferfilepath);
							
					        File newFile = new File(transferfilepathnew+"/"+"Transfer_Complete_InQ.txt");
					        
					        if(fileinq.renameTo(newFile))
					        {
					        	
					            ////System.out.println("File rename success");;
					        }
					        else
					        {
					            ////System.out.println("File rename failed");
					        }
							
						
						}
					
					} catch (ServiceException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					/*
					
						for (ProductVO stopinstance : stoppedinstancelist)
						{
							
						//	////System.out.println("InstanceID--->"+stopinstance.getInstanceid() +stopinstance.getRegion());
							
							ArrayList<ProductVO> instancestatus = getInstanceDetails(stopinstance.getInstanceid(), stopinstance.getRegion(), companyname);
					
							if (instancestatus.size()>0)
							{
								
							
								//////System.out.println("Private Ip Address->"+instancestatus.get(0).getPrivateip() );
								//////System.out.println("Current State ->"+instancestatus.get(0).getState() );
								
								try 
								{
									
									//System.out.println("Going to insert intial job que-->"+designerworkstation+"->"+ customerid+"->"+ reqinstance+"->"+ companyname+"->"+ listOfUnitFiles.get(0).getParentpath().toString()+"->"+ sweepdatapath+"->"+ listOfUnitFiles.get(0).getTextfilepath());
									String res =CreateJobQueInDB(designerworkstation, customerid, reqinstance, companyname, listOfUnitFiles.get(0).getParentpath().toString(),sweepdatapath,listOfUnitFiles.get(0).getTextfilepath());
								
									
									if ("Success".equals(res))
									{
											
										
										File fileinq = new File(transferfilepath);
										
								        File newFile = new File(transferfilepathnew+"/"+"Transfer_Complete_InQ.txt");
								        
								        if(fileinq.renameTo(newFile))
								        {
								        	
								            ////System.out.println("File rename success");;
								        }
								        else
								        {
								            ////System.out.println("File rename failed");
								        }
										
									
									}
								
								} catch (ServiceException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								
							}
						}*/
					
					}
					

				
				
			
			
			}
			else
			{
				
			    ////System.out.println("No Such File");
			}
			
			
			//////System.out.println("Directoy Name---->"+ file.getName() );
			
		}
		
		
			
		
		//////System.out.println("All directories----->"+directories.toString());
		
		
		return state;
		}catch( Exception e)
		{
			System.out.println( "Error:"+e.getMessage());
			return "2";
			
		}
	}

	
	
	//designerworkstation,customer_refid,qstatus,requiredinstance,companyname

	
	private String CreateJobQueInDB(String designerworkstation, String customerid, int numberofinstance,
			String companyname,String path,String sweepdatapath,String textfilepath)
			throws ServiceException {
		// TODO Auto-generated method stub
		
	List<String> values = new ArrayList<String>();
		
		values.add(designerworkstation);
		values.add(customerid);
		values.add(String.valueOf(numberofinstance));
		values.add(companyname);
		values.add(path);
		values.add(sweepdatapath);
		values.add(textfilepath);
		
		String res=null;
		try {
			  	res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.CreateJobQue, values);
			  //	logger.info("Update Instance:"+ values);
				////System.out.println("Job Que Created Successfully:"+values);

		} catch (StoredProcException e) {
		  	logger.info("Failed to Update Instance:"+ values +e.getMessage());

			throw new ServiceException(e.getMessage(), e);
		}
		
		return res;
		
		
	}

	
	

	private void UnZipServices(String foldermappingpath,String companyname)
	{	
		
		File[] zipfileslist=new File(foldermappingpath).listFiles(File::isFile);
		
		String filename="";
		
		for (File file : zipfileslist) 
		{
			//duplication check
			
			File[] directorynames=new File(foldermappingpath).listFiles(File::isDirectory);
	
			StringBuilder sb=new StringBuilder();  
			
			for (File file2 : directorynames)
			{

			String dir=file2.getName();
			sb.append(dir);
			// File dir = new File(foldermappingpath+"/"+file.getName());
			 
		//System.out.println("Directory Name----------------->:"+dir);
			}
			
			////System.out.println("File Name of the zip file is---->"+FilenameUtils.getName(file.getName()));
			
			
			//System.out.println("File Names----->:"+FilenameUtils.getBaseName(file.getName()));
			
			 if (sb.toString().contains(FilenameUtils.getBaseName(file.getName())))
			 {
				 //System.out.println("More job is waiting for the que:------>"+foldermappingpath+"/"+file.getName());
			 }
			 
			 
			 else
			 {
			
			String inputZIPFile=file.getAbsolutePath();
			
			 //System.out.println("inputZIPFile:------>"+inputZIPFile);
				
			//String outputFolder=foldermappingpath+"/"+"processque";
			
			String outputFolder=foldermappingpath+"/";
			
			filename=file.getName();
			
			//System.out.println("File Name------>"+file.getName());
			////System.out.println("File Name------>"+file.getAbsolutePath());
		
			if( filename.contains(".filepart"))
			{
			break;
			}
		
			int random = (int )(Math.random() * 50 + 1);
	    	fileserialnumber=String.valueOf(random);
	    
			   ProcessBuilder pb = new ProcessBuilder(System.getenv("ProgramFiles") + "/7-Zip/7z.exe",
				            "x",      inputZIPFile,
				            "-o" + outputFolder,
				            "-r","-y"
				    );
				    pb.redirectError();
				    try {
				        Process p = pb.start();
				        new Thread(new InputConsumer(p.getInputStream())).start();
				        
				         int code = p.waitFor();
				         
				        ////System.out.println("Exited with: " + p.waitFor());
				      
				        if (code==0)
				        {
				        	
				        	try{
				        		
				        		
				        		
				        		   ProcessBuilder pb1 = new ProcessBuilder(System.getenv("ProgramFiles") + "/7-Zip/7z.exe",
								            "x",      inputZIPFile,
								            "-o" + outputFolder+"/Merged_Data/",
								            "-r","-y"
								    );
								    pb1.redirectError();
								        Process p1 = pb1.start();
								        new Thread(new InputConsumer(p1.getInputStream())).start();
								        
								         int code1 = p1.waitFor();
								         
								        ////System.out.println("Exited with: " + p1.waitFor());
				        		        

								        /*String filenameval = FilenameUtils.getBaseName(filename);

										File vectsrcDir = new File(outputFolder+"/"+"Merged_Data"+"/"+filenameval);
										
										try {
											
											FileUtils.deleteDirectory(vectsrcDir);
											
											////System.out.println("Source file copy to:"+vectsrcDir.toPath().toString());
										} catch (IOException e1) {
											// TODO Auto-generated catch block
											////System.out.println("Failed to Delete Directory:"+vectsrcDir);
											e1.printStackTrace();
											
										}
												
									
									File vectsrcDir1 = new File(outputFolder+"/"+"Merged_Data"+"/"+filename+".7z");
										
										try {
											
											vectsrcDir1.delete();
											
											////System.out.println("Source file copy to:"+vectsrcDir.toPath().toString());
										} catch (Exception e1) {
											// TODO Auto-generated catch block
											////System.out.println("Failed to Delete file:"+vectsrcDir);
											e1.printStackTrace();
											
										}
										
								
										
												File vectsrcDir = new File(outputFolder+"/"+"Merged_Data"+"/"+filenameval);
										
										try {
											
											FileUtils.deleteDirectory(vectsrcDir);
											
											////System.out.println("Source file copy to:"+vectsrcDir.toPath().toString());
										} catch (IOException e1) {
											// TODO Auto-generated catch block
											////System.out.println("Failed to Delete Directory:"+vectsrcDir);
											e1.printStackTrace();
											
										}
												
									
								
										
										String filenameval = FilenameUtils.getBaseName(filename)
*/										

								         String filenameval = FilenameUtils.getBaseName(filename);
								        		 System.out.println("File Names are:"+filenameval);
								File auto2ddelete = new File(outputFolder+"/"+"Merged_Data"+"/"+filenameval+"/"+"Auto2D_Output_Files");
									
									try {
										
										FileUtils.deleteDirectory(auto2ddelete);
										
										System.out.println("File detleted successfully:"+auto2ddelete.toPath().toString());
									} catch (IOException e1) {
										// TODO Auto-generated catch block
										System.out.println("Failed to Delete Directory:"+auto2ddelete.toPath().toString());
										e1.printStackTrace();
										
									}
									
								         
								         
								         DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyy");
								         Date date = new Date();
								         
								        String nfilename="_"+fileserialnumber+"_"+dateFormat.format(date).toString(); //2016/11/16 12:08:43
								         
											File filem = new File(sftproot+"/"+companyname+"/"+"client_original_copy/"+nfilename+"_"+filename);
											
								         File file1 = new File(inputZIPFile);
								         FileUtils.copyFile(file1, filem);
								         

				        		if(file1.delete())
				        		{
				        			
				        			////System.out.println(file1.getName() + " is deleted!");
				        		}
				        		else
				        		{
				        			
				        			////System.out.println("Delete operation is failed.");
				        		}

				        	}catch(Exception e){

				        		e.printStackTrace();

				        	}
				        	
				        }
				        
				    } catch (Exception ex) {
				    	
				        ex.printStackTrace();
				    }
			 }
			
			}			
			
			
		
		/*
		////System.out.println("Unzip services of files");
	
		String res="";
		
		File[] directories=new File(foldermappingpath).listFiles(File::isDirectory);
		
		
		for (File file : directories) 
		{
			////System.out.println("Directoy Name---->"+ file.getName() );
			
		}
		
		//////System.out.println("All directories----->"+directories.toString());
		
*/		
		//return res;
	}



	
	private String ArchiveZipFileAndMakeMerged_DataReady(String foldermappingpath, String companyname, String workstationname)
	{
		
		
		String res="";
		
/*
		
		   ProcessBuilder pb = new ProcessBuilder(System.getenv("ProgramFiles") + "/7-Zip/7z.exe",
			            "a ",foldermappingpath+"/"+workstationname,
			            "-r" + foldermappingpath+"/processque/SpecificFolder/*"
			    );
			    pb.redirectError();
*/		
			    

		//creating sftp download folder files
		
		//File uploadfilename = new File(sftproot+"/"+companyname+"/"+"Merged_Data"+"/"+workstationname+"/"+"Transfer_Complete - Copy Completed.txt");
		
		File uploadnewfilename = new File(sftproot+"/"+companyname+"/"+"Merged_Data"+"/"+workstationname+"/"+"Data_Upload_Complete.txt");
		
		try {
			uploadnewfilename.createNewFile();
			//System.out.println("Upload file created successfully"+uploadnewfilename.toPath().toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			res="2";
		}

		
		
		   ProcessBuilder pb = new ProcessBuilder(System.getenv("ProgramFiles") + "/7-Zip/7z.exe",
			            "a",
			            "-tzip",
			            foldermappingpath+"/"+workstationname+".7z",
			            foldermappingpath+"/"+workstationname+"/*"
			    );
			    pb.redirectError();
			   

							    
			    try {
		
			    	
			    	////System.out.println("Process output of archive --->"+pb);
			    	
			        Process p = pb.start();
			        new Thread(new InputConsumer(p.getInputStream())).start();
			        ////System.out.println("Exited with: " + p.waitFor());
			        
			        int code = p.waitFor();
			         
			        //System.out.println("Successfully zip with code: " + p.waitFor());
			      
			        if (code==0)
			        {

				      /*  //String filenameval = FilenameUtils.getBaseName(filename);

			        	 // thread to sleep for 1000 milliseconds
			            Thread.sleep(30 *   // minutes to sleep
			                    60 *   // seconds to a minute
			                    1000);
			            */
					
/*
		        		File zipfileloca = new File(sftproot+"/"+companyname+"/"+"Merged_Data"+"/"+workstationname+"/"+workstationname+".7z");
						
		        		File moveziplocation = new File(sftproot+"/"+companyname+"/"+"Merged_Data"+"/"+workstationname+".7z");
					
			        	
			        	try
			        	{
			        		
			        		FileUtils.copyFile(zipfileloca, moveziplocation);
			        		
			        		System.out.println("File move success:"+zipfileloca.getPath().toString());
			        		
			        		
			        	}catch(Exception e)
			        	{
			        		
			        		System.out.println("Failed to remove:->" +e.getMessage()+zipfileloca.getPath().toString()+"dest:--->"+moveziplocation.getPath().toString());
			        	}
			        	*/
						try 
						{
							
//deleting the original files
							
							File vectsrcDir = new File(sftproot+"/"+companyname+"/"+workstationname);
							
						FileUtils.deleteDirectory(vectsrcDir);
							
							//System.out.println("Delete root folder:"+vectsrcDir.toPath().toString());
							

							
							//copying backup files
							//String foldermappingpath=sftproot+companynameval+"/"+"Merged_Data";
							
							   
						    String mergedatadestionation=sftproot+"/"+companyname+"/"+"Merged_data_Backup";

					         DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyy");
					         Date date = new Date();
					         
					        String nfilename2="_"+fileserialnumber+"_"+dateFormat.format(date).toString(); //2016/11/16 12:08:43
					        
					        File zipfiledir = new File(sftproot+"/"+companyname+"/"+"Merged_data/"+workstationname+".7z");
							
					        File backupzipdir = new File(mergedatadestionation+"/"+nfilename2+workstationname+".7z");
							
					        FileUtils.copyFile(zipfiledir, backupzipdir);
					        
							File mainfile = new File(mergedatadestionation+"/"+workstationname+".7z");
							

							File renamefile = new File(mergedatadestionation+"/"+nfilename2+"_"+workstationname+".7z");
							
							
							
								try {
									mainfile.renameTo(renamefile);
									
										
									
									//System.out.println("Rename file:"+mainfile.toPath().toString()+"--->"+renamefile.toPath().toString());
						
									 File zipfiledirdel = new File(sftproot+"/"+companyname+"/"+"Merged_data"+"/"+workstationname);
										FileUtils.deleteDirectory(zipfiledirdel);
										//System.out.println("Delete file:"+zipfiledirdel.toPath().toString());
										res="1";
								} catch (Exception e1) {
									// TODO Auto-generated catch block
									//System.out.println("Failed to rename file:"+mainfile.toPath().toString()+"--->"+renamefile.toPath().toString());
									
									e1.printStackTrace();
									res="2";
								}
		


							//File vectsrcDir1 = new File(foldermappingpath+"/"+workstationname);
							
							} catch (IOException e1) {
							// TODO Auto-generated catch block
							////System.out.println("Failed to Delete Directory:"+vectsrcDir);
							e1.printStackTrace();
							res="2";
						}
								
								        	
			        	
			        }
			    } catch (Exception ex) 
			    {	
			    	////System.out.println("Error-------------->"+ex.getMessage());
			         ex.printStackTrace();
			         res="2";
			    }
		

			    		return res;
	}

	

	
	

	private String ArchiveZipFile(String foldermappingpath,String companyname,String workstationname)
	{
		String res="";
		
/*
		
		   ProcessBuilder pb = new ProcessBuilder(System.getenv("ProgramFiles") + "/7-Zip/7z.exe",
			            "a ",foldermappingpath+"/"+workstationname,
			            "-r" + foldermappingpath+"/processque/SpecificFolder/*"
			    );
			    pb.redirectError();
*/		
			    
		
		
		   ProcessBuilder pb = new ProcessBuilder(System.getenv("ProgramFiles") + "/7-Zip/7z.exe",
			            "a",
			            "-tzip",
			            foldermappingpath+"/processque/"+workstationname+".7z",
			            foldermappingpath+"/processque/SpecificFolder/*"
			    );
			    pb.redirectError();
			   
			    
			    
			    
			    try {
		
			    	
			    	////System.out.println("Process output of archive --->"+pb);
			    	
			        Process p = pb.start();
			        new Thread(new InputConsumer(p.getInputStream())).start();
			        ////System.out.println("Exited with: " + p.waitFor());
			    } catch (Exception ex) 
			    {
			    	////System.out.println("Error-------------->"+ex.getMessage());
			    	
			        ex.printStackTrace();
			    }
		

			    		return res;
	}

	

	
	private String MapNetworkSharedFile(String privateip,String letter)
	{
		
		String res="";		    
		
	
		
		String url="";
			    String command = "net use "+letter+":"+" \\\\"+privateip+"\\Vectra "+ mappassword+" /USER:"+mapusername;
			    
			    System.err.println("Command of Net Use:"+ command);
			    
			    try 
			    {
			        Process process = Runtime.getRuntime().exec(command);
			        BufferedReader reader = new BufferedReader(
			                new InputStreamReader(process.getInputStream()));
			        String line;
			        while ((line = reader.readLine()) != null) 
			        {
			            ////System.out.println("result output->"+line);
			        }
			     
			        reader.close();
			        
			        res=res+reader.toString();
			        ////System.out.println("Output Reader--->"+ reader.toString());
			        
			        
			        BufferedReader errorReader = new BufferedReader(
			                new InputStreamReader(process.getErrorStream()));
			        
			        while ((line = errorReader.readLine()) != null) 
			        {
			        
			        	////System.out.println("Error Output-->"+line);
			        }
			         
			        errorReader.close();
			        
			        res=res+errorReader.readLine().toString()+"Error";
			        
			        ////System.out.println("Error Reader--->"+ errorReader.readLine().toString());
			     
			    } catch (IOException e) 
			    {
			        e.printStackTrace();
			        res=res+"Error:"+e.getMessage();
			    }
			    

			    ////System.out.println("Returing new map drive result--->"+res);
			    		return res;
	}
	
	

	private String MappSharedFile()
	{
		String res="";
		
/*
		
		   ProcessBuilder pb = new ProcessBuilder(System.getenv("ProgramFiles") + "/7-Zip/7z.exe",
			            "a ",foldermappingpath+"/"+workstationname,
			            "-r" + foldermappingpath+"/processque/SpecificFolder/*"
			    );
			    pb.redirectError();
*/		
			    
		
		String drivelabel="";
		String privateip="";
		String url="";
		
		   ProcessBuilder pb = new ProcessBuilder("net use"+drivelabel+":\\"+privateip,
			            mappassword,
			            mapusername
			            );
		   
		   
			    pb.redirectError();
			   
			    try {
			    	
			    	////System.out.println("Map Process output of Map --->"+pb);
			        Process p = pb.start();
			        
			        new Thread(new InputConsumer(p.getInputStream())).start();
			    
			        ////System.out.println("Map Exists with: " + p.waitFor());
		
			    } catch (Exception ex) 
			    {
			    	////System.out.println("Error-------------->"+ex.getMessage());
			    	
			        ex.printStackTrace();
			    }
		
			    
			    
			    String command = "net use x:"+ "\\172.31.39.86"+"\\AutoCAD2D Grameen123"+ "/USER:Administrator";
			    
			    try {
			        Process process = Runtime.getRuntime().exec(command);
			     
			        BufferedReader reader = new BufferedReader(
			                new InputStreamReader(process.getInputStream()));
			        String line;
			        while ((line = reader.readLine()) != null) 
			        {
			            ////System.out.println("result output->"+line);
			        }
			     
			        reader.close();
			        
			        ////System.out.println("Output Reader--->"+ reader.toString());
			        
			        
			        BufferedReader errorReader = new BufferedReader(
			                new InputStreamReader(process.getErrorStream()));
			        while ((line = errorReader.readLine()) != null) {
			            ////System.out.println("Error Output-->"+line);
			        }
			         
			        errorReader.close();
			        
			        
			        ////System.out.println("Error Reader--->"+ errorReader.readLine().toString());
			     
			    } catch (IOException e) {
			        e.printStackTrace();
			    }
			    

			    		return res;
	}

	
	

	private String CopyAndReplaceWithSAMBA(String sourceUnitSweepdata,String destionationdata,String processingfile,String privateip,String companyname,String designerworkstation)
	{
		

		 NtlmPasswordAuthentication  ntlmPasswordAuthentication = new NtlmPasswordAuthentication(null, mapusername, mappassword); 
		 
			String destionation="smb://"+privateip+"/Vectra";

			String sourcepath="smb://172.31.29.42/"+companyname.toLowerCase()+"/"+designerworkstation+"/"+"Auto2D_Output_Files";
		
			//////System.out.println("");
			try {
			  
				SmbFile source = new SmbFile(sourcepath);
			  
				////System.out.println("************* Source Location Authenticate *****************"+sourcepath);
			 
			   SmbFile destination = new SmbFile(destionation , ntlmPasswordAuthentication);
			   ////System.out.println("************* Destination Location  Authenticate ************"+destionation);
			 
			   //--------- final process to move folder
			   source.copyTo(destination);
			
			   ////System.out.println("**** Content have been Move from one directory to another ****");
			   return "1";
			  } catch (MalformedURLException e) 
			{
			   e.printStackTrace();
			   ////System.out.println("Error1 while copy content-------->"+ e.getMessage());
			   return "2";
			
			} catch (SmbException e)
			{
			   e.printStackTrace();
			   
			   ////System.out.println("Error2 while copy content-------->"+ e.getMessage());
			   return "2";
				
			 }

	
	}
	
	private String Transfer_CopyAndReplace(String sourceUnitSweepdata,String destionationdata,String processingfile,String privateip,String companyname,String designerworkstation)
	{
		//System.out.println("Source 0------->:"+sourceUnitSweepdata);		
		//copying auto 2d source file
	//C:/SFTP_Root/Vectra/transferjob/255/UITTO1DT0158263/Auto2D_Output_Files

		File folder = new File(sourceUnitSweepdata);
		File[] listOfFiles = folder.listFiles();

		    for (int i = 0; i < listOfFiles.length; i++)
		    {
		      if (listOfFiles[i].isFile()) 
		      {
		        System.out.println("File " + listOfFiles[i].getName());
		      
		        String name=listOfFiles[i].getName();
		        if (name.contains("Compute"))
		        {
		        	String path=listOfFiles[i].getPath();
		        	System.out.println("Path Name   is  "+path );
		  

		    		File n=new File(path);
		    		
		    		n.delete();}
		    		
		        
		      
		      }
		      else if (listOfFiles[i].isDirectory()) {
		        System.out.println("Directory " + listOfFiles[i].getName());
		      }
		    }
		    
		//String runningfile=sourceUnitSweepdata+"/"+"Transfer_Complete - Start Compute Sequence - Running.txt";
		   
		
		File vectsrcDir = new File(sourceUnitSweepdata);
		File destDir = new File("\\"+destionationdata);
			
		
		System.out.println("Transfer_Source file Data path---->"+destionationdata);
		
		try {
			FileUtils.copyDirectory(vectsrcDir, destDir);
			
			System.out.println("Transfer file complete "+destDir);
			
			Thread.sleep(10000);
			  
			 
			FileUtils.deleteDirectory(vectsrcDir);

			
			
			String comfilepath="\\"+destionationdata+Constants.SCHEDULING_START_COMPLETION_FILE;
			
			
			////System.out.println("Completion file path-->"+comfilepath);
		    File dstnewFile = new File(comfilepath);
		
		    if(!dstnewFile.exists()){
	       dstnewFile.createNewFile();}


			
			String[] paths = sourceUnitSweepdata.split("/");
			
			String p1=paths[0];
			String p2=paths[1];
			String p3=paths[2];
			String p4=paths[3];
			String p5=paths[4];
			
			String delpath=p1+"/"+p2+"/"+p3+"/"+p4+"/"+p5;
			
			File dl=new File(delpath);
			//C:/SFTP_Root/Vectra/transferjob/255/UITTO1DT0158263/Auto2D_Output_Files

			FileUtils.deleteDirectory(dl);
			
			////System.out.println("Source file copy to:"+vectradestDir.toPath().toString());
		} catch (IOException | InterruptedException e1) {
			// TODO Auto-generated catch block
			////System.out.println("Failed to Copy Vectra source file to destination:"+ "\\"+destionationdata);
		
			System.out.println("Failed to"+e1.getMessage());
			e1.printStackTrace();
			return "2";
		
		}
		
		return "1";
	
	}
	private String cleanOldData(String cleanfilepath)
	{

		File cleandir = new File(cleanfilepath);
		
		try {
			FileUtils.deleteDirectory(cleandir);//(vectsrcDir, vectradestDir);
			//System.out.println("Delete Directory:"+cleandir.toPath().toString());
			return "1";
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			//System.out.println("Failed to Delete directory file:"+ "\\"+cleandir.toPath().toString());
			e1.printStackTrace();
			return "2";
		}
		
				
	}
	
	
	private String CopyAndReplace(String sourceUnitSweepdata,String destionationdata,String processingfile,String privateip,String companyname,String designerworkstation)
	{
		System.out.println("Workstation Name:"+companyname+"-->"+designerworkstation);		
		
		
		String cleandatapath="\\\\"+privateip+"\\"+"Vectra"+"\\"+"Auto2D_Output_Files\\";
		
		if ("2".contentEquals(cleanOldData(cleandatapath)))
				{
					return "2";
				}
		
		
		String textfilefolder=sourceUnitSweepdata;
		
		String res="2";
		
		//copying auto 2d source file
		File vectsrcDir = new File(auto2dsourcefile);
		File vectradestDir = new File("\\\\"+privateip+"\\"+"Vectra"+"\\"+"AUTO2D_SERVER");
		
		
		System.out.println("Source file Data path---->"+auto2dsourcefile);
		
		
		//source file error
/*		
		try {
			FileUtils.copyDirectory(vectsrcDir, vectradestDir);
			////System.out.println("Source file copy to:"+vectradestDir.toPath().toString());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			System.out.println("Copy Error Inside: Failed to Copy Vectra source file to destination:"+ "\\"+destionationdata);
			e1.printStackTrace();
			return "2";
		}*/
		
		
		
					
		System.out.println("Source Unit date--->:"+sourceUnitSweepdata);
		
		File srcDir = new File(sftproot+companyname+"/"+designerworkstation+"/"+"Auto2D_Output_Files");
		File destDir = new File("\\"+destionationdata);
		
		try 
		{
			////System.out.println("Copying file from source: "+ srcDir + " to destionation:"+ destDir);
			
			FileUtils.copyDirectory(srcDir, destDir);
			
			
			File[] unitdatadirectory=new File("\\"+destionationdata+"\\Unit_Sweep_Data").listFiles(File::isDirectory);
			
			////System.out.println("Files in unit Sweep_Data");
			
			for (File file2 : unitdatadirectory) 
					{
				////System.out.println("Data file processing absoulet path"+file2.getAbsolutePath());
				
					File[] maadirectories=new File(file2.getAbsolutePath()).listFiles(File::isFile);
				
					
					for (File maafile3 : maadirectories) 
					{
					
						////System.out.println("Files Names are:"+maafile3.getName());
					
					
						////System.out.println("Processing file name:"+processingfile);
						
						if (maafile3.getName().contains(processingfile))
								{
							

							////System.out.println("Data Processing file name:__---->"+maafile3.getAbsolutePath().toString());
							
						        File filen = new File(maafile3.getAbsolutePath());
						       
						        String renamepath=file2.getAbsolutePath()+"\\"+"Unit_Sweep_Data_List.txt";
						        
						        ////System.out.println("Rename New file path"+renamepath);
						        
						       File newFile = new File(renamepath);
						        
						        //newFile.createNewFile();
						        

						        if(filen.renameTo(newFile))
						        {
						        	
						            ////System.out.println("File rename success:"+ maafile3.getPath().toString());;
						        }
						        else
						        {
						            ////System.out.println("File rename failed:"+ maafile3.getPath().toString());
						        }
													
								}
						
					}
					
					
				
						
					}
			
			
			
			  File ckFile = new File("\\\\"+privateip+"\\"+"Vectra\\"+"Auto2D_Output_Files"+"\\"+Constants.SCHEDULING_COPY_COMPLETION_FILE);
			  
			  ckFile.createNewFile();
			
			String comfilepath="\\"+destionationdata+Constants.SCHEDULING_START_COMPLETION_FILE;
			
			////System.out.println("Completion file path-->"+comfilepath);
			
			
	       
	        File dstnewFile = new File(comfilepath);
		       
	       dstnewFile.createNewFile();
	       	res="1";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			res="2";
			////System.out.println("Error while copy file from source:" +srcDir   +"to Destionation:"+ destDir+e.getMessage());
		}
		
		
		return res;
	}

	
	
	
	
	private String CollectFileFromComputeInstance(String instanceprivateip_soure,String  downalodfolder_destionation)
	{

		String sourcepath="\\\\"+instanceprivateip_soure+"\\"+"Vectra"+"\\"+"Auto2D_Output_Files";
		
		File srcDir = new File(sourcepath);
		File destDir = new File(downalodfolder_destionation);
		
		try 
		{
			////System.out.println("Copying file from source: "+ srcDir + " to destionation:"+ destDir);
			
			FileUtils.copyDirectory(srcDir, destDir);
			
			return "1";
			
		}catch(IOException e)
		{
		
			////System.out.println("Error while copy"+e.getMessage());
		
			return "2";
		}
		
	}
	
	
	private String InitialCheck(String foldermappingpath,String companyname,String customerid)
	{	
		
		String res="";
		
		File[] directories=new File(foldermappingpath).listFiles(File::isDirectory);
		
		for (File file : directories) 
		{
			//////System.out.println("Directoy Name---->"+ file.getName() );
			
			File transferfilestatus=new File(foldermappingpath+"\\"+file.getName()+"\\"+"Transfer_Complete.txt");
		
			if(transferfilestatus.exists())
			{
			
				
				
			}else
			{
				
			}
			
			//File[] filesList=new File(foldermappingpath).listFiles(File::isFile);
			
			
		}
		
		//////System.out.println("All directories----->"+directories.toString());
		
		
		return res;
	}
	
	
	
	private String StartInstance(String instanceId, String region,
			 String customerId)
	{
		try{
				AWSCredentials credentials = new BasicAWSCredentials(accesskey,
					sckreetKey);

			String regionval = region;

		AmazonEC2 ec2 = new AmazonEC2Client(credentials);
			Region ec2region = Region.getRegion(Regions.valueOf(regionval));

			ec2.setRegion(ec2region);

//			AmazonEC2 ec2 = AmazonEC2ClientBuilder.standard()
	        //        .withRegion(Regions.valueOf(regionval)).withCredentials((AWSCredentialsProvider) credentials)
	          //      .build();

			StartInstancesRequest startRequest = new StartInstancesRequest()
					.withInstanceIds(instanceId);
			StartInstancesResult startResult = ec2.startInstances(startRequest);

			List<InstanceStateChange> stateChangeList = startResult
					.getStartingInstances();

			return "Success: "+stateChangeList.get(0).getCurrentState().getName().toString()+" "+"Please referesh the page to get the current status";

		}catch(Exception e)
		{
			return "Error:"+e.getMessage();
		}
		
	}
	
	
	public String StopInstance(String instanceId, String region) {

		
	
		try{
		AWSCredentials credentials = new BasicAWSCredentials(accesskey,
				sckreetKey);

		String regionval = region;

		//AmazonEC2 ec2 = AmazonEC2ClientBuilder.standard()
          //      .withRegion(Regions.valueOf(regionval))
            //    .build();
		
	AmazonEC2 ec2 = new AmazonEC2Client(credentials);
		
	Region ec2region = Region.getRegion(Regions.valueOf(regionval));
		
	ec2.setRegion(ec2region);
		
		////System.out.println("Request to Stop Instance");

	List<String> instancesToStop = new ArrayList<String>();
		instancesToStop.add(instanceId);

		// i-06c8f0d1f584511d3
		StopInstancesRequest stoptr = new StopInstancesRequest();

		stoptr.setInstanceIds(instancesToStop);

		ec2.stopInstances(stoptr);

		} catch (Exception e)
		{
			return e.getMessage();
		}
		return "Stopping the instance,please click refresh button to get status";

	}



	public String getInstanceStatus(String instanceId, String region,
			String customerId)

	{

		
		try {

			AWSCredentials credentials = new BasicAWSCredentials(accesskey,
					sckreetKey);

			String regionval = region;

			AmazonEC2 ec2 = new AmazonEC2Client(credentials);
			Region ec2region = Region.getRegion(Regions.valueOf(regionval));

			ec2.setRegion(ec2region);

			DescribeInstancesRequest describeInstanceRequest = new DescribeInstancesRequest()
					.withInstanceIds(instanceId);
			DescribeInstancesResult describeInstanceResult = ec2
					.describeInstances(describeInstanceRequest);
			InstanceState state = describeInstanceResult.getReservations()
					.get(0).getInstances().get(0).getState();

			//////System.out.println("Returning Instance State:" + state.getName());

			return state.getName();
		} catch (Exception e) {
			
			////System.out.println("Instance status error--->:"+e.getMessage());
			
			return e.getMessage();

		}
	}



	
	
public ArrayList<ProductVO> getInstanceDetails(String instanceId, String region,
			String customerId)

	{
		ArrayList<ProductVO> instancedetails = new ArrayList<ProductVO>();

		ProductVO info=new ProductVO();
		
		try {

			AWSCredentials credentials = new BasicAWSCredentials(accesskey,
					sckreetKey);

			String regionval = region;

			AmazonEC2 ec2 = new AmazonEC2Client(credentials);
			Region ec2region = Region.getRegion(Regions.valueOf(regionval));
			ec2.setRegion(ec2region);

		//	AmazonEC2 ec2 = AmazonEC2ClientBuilder.standard()
	      //          .withRegion(Regions.valueOf(regionval))
	        //        .build();


			DescribeInstancesRequest describeInstanceRequest = new DescribeInstancesRequest()
					.withInstanceIds(instanceId);
			DescribeInstancesResult describeInstanceResult = ec2
					.describeInstances(describeInstanceRequest);
			////System.out.println("Ec2out:"+ec2.toString());
			String state = describeInstanceResult.getReservations()
					.get(0).getInstances().get(0).getState().getName();

		String privateip = describeInstanceResult.getReservations()
					.get(0).getInstances().get(0).getPrivateIpAddress();
		String instancename = describeInstanceResult.getReservations()
				.get(0).getInstances().get(0).getTags().get(0).getValue();
					
		info.setState(state.toString());
		info.setPrivateip(privateip);
		info.setRef_instanceid(instancename);
		instancedetails.add(info);
				
				} 
		catch (Exception e) 
		{
			////System.out.println("Exception ------------->"+e.getMessage());
			//return e.getMessage();

		}
		
		return instancedetails;
	}


private void smtpusingssl( String details,String subject)
{
	
String text=details;
String smtpserver="smtp.gmail.com";
String smtpport="465";
String smtpuser="awsalert@vectraglobal.com";
String smtppass="vectraAWS18";
String emailsender="awsalert@vectraglobal.com";
String emailsubject=subject;
String receipents=remail;

Properties props = new Properties();
	props.put("mail.smtp.host", smtpserver);
	props.put("mail.smtp.socketFactory.port", smtpport);
	props.put("mail.smtp.socketFactory.class",
			"javax.net.ssl.SSLSocketFactory");
	props.put("mail.smtp.auth", "true");
	props.put("mail.smtp.port", smtpport);

	Session session = Session.getDefaultInstance(props,
		new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(smtpuser,smtppass);
			}
		});

	try {

		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress(emailsender));
		message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(receipents));
		message.setSubject(emailsubject);
		
	//	message.setSubject("Welcome to Control Catalyst Inc.-Make Easy to Configuration Validation & Auditing");
		
		
		message.setText(text);

		/*
		message.setText("Dear Mail Crawler," +
				"\n\n No spam to my email, please!");
*/
		Transport.send(message);

		System.out.println("Done");

	} catch (MessagingException e) {
		System.out.println("Failed to Send email"+e.getMessage());
		
		throw new RuntimeException(e);
	}
	
}



	
public static  class InputConsumer implements Runnable {
    private InputStream is;

    public InputConsumer(InputStream is) {
        this.is = is;
    }

    @Override
    public void run() {
        try {
            int value = -1;
            while ((value = is.read()) != -1) {
                System.out.print((char) value);
            }
        } catch (IOException exp) {
            exp.printStackTrace();
        }
        ////System.out.println("");
    }

}	

}


