
package com.mlab.endpoint.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.mlab.endpoint.Model.AttachedFile;
import com.mlab.endpoint.Model.ProductVO;
import com.mlab.endpoint.Model.CustomerVO;
import com.mlab.endpoint.exception.ServiceException;
import com.mlab.endpoint.exception.StoredProcException;
import com.mlab.endpoint.interfaces.ProductInformationView;
import com.mlab.endpoint.interfaces.StoredProcJdbcDao;
import com.mlab.endpoint.util.Constants;

public class ProductInformationViewImpl implements ProductInformationView {
	private static final Logger logger = LoggerFactory.getLogger(ProductInformationViewImpl.class);

	private StoredProcJdbcDao storedProcJdbcDao;

	public StoredProcJdbcDao getStoredProcJdbcDao() {
		return storedProcJdbcDao;
	}

	public void setStoredProcJdbcDao(StoredProcJdbcDao storedProcJdbcDao) {
		this.storedProcJdbcDao = storedProcJdbcDao;
	}

	@Value("${elapsedtime}")
	private int elapsedtime;


@Value("${accesskey}")
private String accesskey;

@Value("${sckreetKey}")
private String sckreetKey;


	
	
	
	@Override
	public String EditInstance(ProductVO productVO)
			throws ServiceException {
		
		// TODO Auto-generated method stub
		
			List<String> values = new ArrayList<String>();
				
				values.add(productVO.getInrId());	
				values.add(productVO.getInstanceid());
				values.add(productVO.getRegion());
				values.add(productVO.getFlg());
				
				String res=null;
				try
				{
					System.out.println("Going to update Instance---------"+values);
					
					res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.EditInstanceList, values);
				
				} catch (StoredProcException e) 
				
				{
					
					
					throw new ServiceException(e.getMessage(), e);
				}

				return res;
	
	}
	

	
	

	@Override
	public String AddInstance(ProductVO productVO)
			throws ServiceException {
		
		// TODO Auto-generated method stub
		
			List<String> values = new ArrayList<String>();
				
				values.add(productVO.getInstanceid());
				values.add(productVO.getRegion());
				values.add(productVO.getFlg());
				
				String res=null;
				try
				{
					System.out.println("Going to update Instance---------"+values);
					
					res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.AddInstance, values);
				
				} catch (StoredProcException e) 
				
				{
					
					
					throw new ServiceException(e.getMessage(), e);
				}

				return res;
	
	}

	
	
	
	@Override
	public ArrayList<ProductVO> getAllInstanceListWithFlg()	throws ServiceException {
		// TODO Auto-generated method stub
		
		List<HashMap<String, Object>> resultSet = null;
		List<String> values = new ArrayList<String>();
		
		ArrayList<ProductVO> productList = new ArrayList<ProductVO>();
		
		try {
			resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_All_InstanceList, values);
		} catch (StoredProcException e) 
		{
			throw new ServiceException(e.getMessage(), e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (resultSet != null) {
			
			
				try
				{
            	
            	for (Iterator<HashMap<String, Object>> iterator = resultSet
						.iterator(); iterator.hasNext();) {
					HashMap<?, ?> map = iterator.next();
				
					ProductVO info = new ProductVO();
					info.setInrId(String.valueOf(map.get("InrId")));
					
					info.setInstanceid(String.valueOf(map.get("instanceid")));
					
					info.setRegion(String.valueOf(map.get("region")));
					
					info.setFlg(InStatus(String.valueOf(map.get("flg"))));
					info.setStatusid(String.valueOf(map.get("flg")));
					info.setInstancename(String.valueOf(map.get("hostname")));
					
					productList.add(info);

            	}
            } catch (Exception e) {
            	logger.info("Error"+e.getMessage());
                throw new ServiceException(e.getMessage(), e);
            }

		
		}

		logger.info("returning Product Category data : " + productList);

		System.out.println("Product List:"+productList);

		return productList;
		
		
	}


	

	

	@Override
	public ArrayList<ProductVO> GetAllCustomerList() throws ServiceException {
			List<HashMap<String, Object>> resultSet = null;
			List<String> values = new ArrayList<String>();
			
			ArrayList<ProductVO> customerList = new ArrayList<ProductVO>();
			
			try {
				resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.get_All_Customer_InformationWebUri, values);
			} catch (StoredProcException e) 
			{
				throw new ServiceException(e.getMessage(), e);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (resultSet != null) {
				
				
					try
					{
	            	
	            	for (Iterator<HashMap<String, Object>> iterator = resultSet
							.iterator(); iterator.hasNext();) {
						HashMap<?, ?> map = iterator.next();
					
						ProductVO info = new ProductVO();
						info.setCompanyname(String.valueOf(map.get("companyname")));
						info.setCustomerid(String.valueOf(map.get("customerid")));
						info.setFtpmappingpath(String.valueOf(map.get("ftpmappingpath")));
						info.setCrId(String.valueOf(map.get("crId")));
						info.setStatus(GetStatusDisplayName(String.valueOf(map.get("status"))));
						info.setStatusid(String.valueOf(map.get("status")));
						
						
						customerList.add(info);

	            	}
	            } catch (Exception e) {
	            	logger.info("Error"+e.getMessage());
	                throw new ServiceException(e.getMessage(), e);
	            }

			
			}

			logger.info("Returing list of customer: " + customerList);

			System.out.println("Returing list of customer:"+customerList);

			return customerList;
		}




	@Override
	public String AddCustomer(ProductVO productVO)
			throws ServiceException {
		
		// TODO Auto-generated method stub
		
			List<String> values = new ArrayList<String>();
				
				values.add(productVO.getCompanyname());
				values.add(productVO.getFtpmappingpath());
				values.add(UUID.randomUUID().toString());
				
				String res=null;
				try
				{
					System.out.println("Going to create customer---------"+values);
					String rs=createfolder(productVO.getFtpmappingpath());
					
					res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.AddCustomer, values);
				
				} catch (StoredProcException e) 
				
				{
					
					
					throw new ServiceException(e.getMessage(), e);
				}

				return res;
	
	}

	private String createfolder(String path)
	{
		File file = new File(path);
        if (!file.exists()) 
        {
            if (file.mkdir())
            {
                System.out.println("Directory is created!");
                
                
                
                File files2 = new File(path+"/client_original_copy");
                if (!files2.exists()) {
                    if (files2.mkdirs()) {
                        System.out.println("Multiple directories are created!");
                    } else {
                        System.out.println("Failed to create multiple directories!");
                    }
                
                }
                
                
                File files3 = new File(path+"/Merged_Data");
                if (!files3.exists()) {
                    if (files3.mkdirs()) {
                        System.out.println("Multiple directories are created!");
                    } else {
                        System.out.println("Failed to create multiple directories!");
                    }
                }
                
                File files4 = new File(path+"/Merged_data_Backup");
                if (!files4.exists()) {
                    if (files4.mkdirs()) {
                        System.out.println("Multiple directories are created!");
                    } else {
                        System.out.println("Failed to create multiple directories!");
                    }
                }
                
                return "1"; 
            
            } 
            else
            {
                System.out.println("Failed to create directory!");
                return "0";
            }
        }
       return "0";
	}
	


	@Override
	public String UpdateCustomer(ProductVO productVO)
			throws ServiceException {
		
		// TODO Auto-generated method stub
		
			List<String> values = new ArrayList<String>();
				
				values.add(productVO.getCompanyname());
				values.add(productVO.getFtpmappingpath());
				values.add(productVO.getCustomerid());
				values.add(productVO.getStatus());
				
				String res=null;
				try
				{
					System.out.println("Going to update customer---------"+values);
					
					res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Edit_Customer, values);
				
				} catch (StoredProcException e) 
				
				{
					
					
					throw new ServiceException(e.getMessage(), e);
				}

				return res;
	
	}

	
	

	

	@Override
	public ArrayList<ProductVO> GetDashboardJobs()
			throws ServiceException {
		// TODO Auto-generated method stub
		
		List<HashMap<String, Object>> resultSet = null;
		List<String> values = new ArrayList<String>();
		
		ArrayList<ProductVO> productList = new ArrayList<ProductVO>();
		
		try {
			
			//System.out.println("Order details"+ values);

			resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_Dashboard_JobList, values);
		} catch (StoredProcException e) 
		{
			throw new ServiceException(e.getMessage(), e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (resultSet != null) {
			
			
				try
				{
            	
            	for (Iterator<HashMap<String, Object>> iterator = resultSet
						.iterator(); iterator.hasNext();) {
					HashMap<?, ?> map = iterator.next();
				
					ProductVO info = new ProductVO();
					
					//ref_instanceid,designerworkstation,toolname,workingunit,ref_companyname,
					//processingstatus,starttime,endtime,duration,operationflg,jobdate,
					//ref_customerid,inque,schedule,running,complete,ready,ref_que_id,sweepdate,tooldata,cs_destionation
					
					//info.setProductNo(String.valueOf(map.get("ref_instanceid")));
					info.setJobrowid(String.valueOf(map.get("jId")));
					info.setInstanceid(String.valueOf(map.get("ref_instanceid")));
					info.setDesignerworkstation(String.valueOf(map.get("designerworkstation")));
					info.setToolname(mytoolname(String.valueOf(map.get("toolname"))));
					info.setWorkingunit(String.valueOf(map.get("workingunit")));
					info.setCompanyname(String.valueOf(map.get("ref_companyname")));
					info.setProcessingstatus(String.valueOf(map.get("processingstatus")));
					info.setStarttime(String.valueOf(map.get("starttime")));
					info.setEndtime(String.valueOf(map.get("endtime")));
					info.setDuration(String.valueOf(map.get("duration")));
					info.setOperationflg(String.valueOf(map.get("operationflg")));
					info.setJobdate(String.valueOf(map.get("jobdate")));
					info.setSweepdatapath(String.valueOf(map.get("sweepdate")));
					info.setTextfilepath(String.valueOf(map.get("cs_destionation")));
					info.setRunning(String.valueOf(map.get("running")));
					info.setDatafilename(String.valueOf(map.get("processingfile")));
					info.setPrivateip(String.valueOf(map.get("privateip")));
					String ptime=String.valueOf(map.get("starttime"));
					info.setPasstime(GetElispTime(String.valueOf(map.get("starttime"))));
					String endtime=String.valueOf(map.get("endtime"));
					if (!endtime.equals("null"))
					{
						//System.out.println("endtime-->"+endtime);
						info.setPasstime(GetElispeTwoDare(ptime,endtime));
						
						
					}else
					{
						//System.out.println("Start time-->"+ptime);
						info.setPasstime(GetElispTime(String.valueOf(map.get("starttime"))));
								
						String hr=Get_Elisp_Hours((String.valueOf(map.get("starttime"))));
						if(Integer.parseInt(hr)>elapsedtime)
						{
							if(String.valueOf(map.get("running")).contains("unit_alg"))
							{
								info.setTransferbtn("1");
							}
						
							if(String.valueOf(map.get("running")).contains(""))
							{
								info.setTransferbtn("1");
							}
						}
						
						try{
					info.setPublicip(getInstanceDetails(String.valueOf(map.get("ref_instanceid"))));
						}catch(Exception e){}
						
						}
					/*
					if("".equals(ptime))
					{
						if("null".equals(ptime))
						{
							
						}else
						{
							
						}
					}else
					{
						if("null".equals(ptime))
						{
							
						}else
						{
							
						}
						
					}
					
					GetElispeTwoDare
					//info.set
					//info.setTextfilepath(String.valueOf(map.get("cs_destionation")));
					*/
					productList.add(info);

            	}
            } catch (Exception e) {
            	logger.info("Error"+e.getMessage());
                throw new ServiceException(e.getMessage(), e);
            }

		
		}

		//System.out.println("Job Details List:"+productList);

		return productList;
		
		
	}
	

	
	@Override
	public ArrayList<ProductVO> GetDashboardJobs(String companyname)
			throws ServiceException {
		// TODO Auto-generated method stub
		
		List<HashMap<String, Object>> resultSet = null;
		List<String> values = new ArrayList<String>();
		
		ArrayList<ProductVO> productList = new ArrayList<ProductVO>();
		
		try {
			values.add(companyname);
			System.out.println("Order details"+ values);

			resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_Dashboard_JobListWithCompanyName, values);
		} catch (StoredProcException e) 
		{
			throw new ServiceException(e.getMessage(), e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (resultSet != null) {
			
			
				try
				{
            	
            	for (Iterator<HashMap<String, Object>> iterator = resultSet
						.iterator(); iterator.hasNext();) {
					HashMap<?, ?> map = iterator.next();
				
					ProductVO info = new ProductVO();
					
					//ref_instanceid,designerworkstation,toolname,workingunit,ref_companyname,
					//processingstatus,starttime,endtime,duration,operationflg,jobdate,
					//ref_customerid,inque,schedule,running,complete,ready,ref_que_id,sweepdate,tooldata,cs_destionation
					
					//info.setProductNo(String.valueOf(map.get("ref_instanceid")));
					info.setJobrowid(String.valueOf(map.get("jId")));
					info.setInstanceid(String.valueOf(map.get("ref_instanceid")));
					info.setDesignerworkstation(String.valueOf(map.get("designerworkstation")));
					info.setToolname(mytoolname(String.valueOf(map.get("toolname"))));
					info.setWorkingunit(String.valueOf(map.get("workingunit")));
					info.setCompanyname(String.valueOf(map.get("ref_companyname")));
					info.setProcessingstatus(String.valueOf(map.get("processingstatus")));
					info.setStarttime(String.valueOf(map.get("starttime")));
					info.setEndtime(String.valueOf(map.get("endtime")));
					info.setDuration(String.valueOf(map.get("duration")));
					info.setOperationflg(String.valueOf(map.get("operationflg")));
					info.setJobdate(String.valueOf(map.get("jobdate")));
					info.setSweepdatapath(String.valueOf(map.get("sweepdate")));
					info.setTextfilepath(String.valueOf(map.get("cs_destionation")));
					info.setRunning(String.valueOf(map.get("running")));
					info.setDatafilename(String.valueOf(map.get("processingfile")));
					info.setPrivateip(String.valueOf(map.get("privateip")));
					
					String ptime=String.valueOf(map.get("starttime"));
					
					info.setPasstime(GetElispTime(String.valueOf(map.get("starttime"))));
					
					String endtime=String.valueOf(map.get("endtime"));
					
					if (!endtime.equals("null"))
					{
						//System.out.println("endtime-->"+endtime);
						info.setPasstime(GetElispeTwoDare(ptime,endtime));
						
						
					}else
					{
						//System.out.println("Start time-->"+ptime);
						info.setPasstime(GetElispTime(String.valueOf(map.get("starttime"))));
								
						String hr=Get_Elisp_Hours((String.valueOf(map.get("starttime"))));
						if(Integer.parseInt(hr)>elapsedtime)
						{
							if(String.valueOf(map.get("running")).contains("unit_alg"))
							{
								info.setTransferbtn("1");
							}
						
							if(String.valueOf(map.get("running")).contains(""))
							{
								info.setTransferbtn("1");
							}
						}
						
						
						
						try{
							info.setPublicip(getInstanceDetails(String.valueOf(map.get("ref_instanceid"))));
								}catch(Exception e){}
						
						
					}
					/*
					if("".equals(ptime))
					{
						if("null".equals(ptime))
						{
							
						}else
						{
							
						}
					}else
					{
						if("null".equals(ptime))
						{
							
						}else
						{
							
						}
						
					}
					
					GetElispeTwoDare
					//info.set
					//info.setTextfilepath(String.valueOf(map.get("cs_destionation")));
					*/
					productList.add(info);

            	}
            } catch (Exception e) {
            	logger.info("Error"+e.getMessage());
                throw new ServiceException(e.getMessage(), e);
            }

		
		}

		System.out.println("Job Details List:"+productList);

		return productList;
		
		
	}

	
	@Override
	public String getInstanceDetails(String instanceId)

	{
	//System.out.println("Entering transfer instance id:"+ instanceId);
	
		String publicip = null;
	ArrayList<ProductVO> instancedetails = new ArrayList<ProductVO>();

		ProductVO info=new ProductVO();
		
		try {

			AWSCredentials credentials = new BasicAWSCredentials(accesskey,
					sckreetKey);

			String regionval = "US_EAST_1";

			AmazonEC2 ec2 = new AmazonEC2Client(credentials);
			Region ec2region = Region.getRegion(Regions.valueOf(regionval));
			ec2.setRegion(ec2region);

		//	AmazonEC2 ec2 = AmazonEC2ClientBuilder.standard()
	      //          .withRegion(Regions.valueOf(regionval))
	        //        .build();


			DescribeInstancesRequest describeInstanceRequest = new DescribeInstancesRequest()
					.withInstanceIds(instanceId);
			DescribeInstancesResult describeInstanceResult = ec2
					.describeInstances(describeInstanceRequest);
			////System.out.println("Ec2out:"+ec2.toString());
			String state = describeInstanceResult.getReservations()
					.get(0).getInstances().get(0).getState().getName();

		String privateip = describeInstanceResult.getReservations()
					.get(0).getInstances().get(0).getPrivateIpAddress();


	 publicip = describeInstanceResult.getReservations()
					.get(0).getInstances().get(0).getPublicIpAddress();

		
		info.setState(state.toString());
		info.setPrivateip(privateip);
		instancedetails.add(info);
				
				} 
		catch (Exception e) 
		{
			//System.out.println("Instance state Exception ------------->"+e.getMessage());
			//return e.getMessage();

		}
	
		//System.out.println("Returning instance status"+instancedetails);
		return publicip;
	}

	
	
	
	public String GetElispTime(String recevieddate)
	{
		

		if("".equals(recevieddate)){return "";}
		
		if("null".equals(recevieddate)){return "";}
		
		
		Date now = new Date();
		
		SimpleDateFormat currentdf = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss");
		String currentdateString=currentdf.format(now);
		
		//System.out.println("MyCurrent datetime-->"+currentdateString);
		
		String fm_recevieddate = recevieddate;
		
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		
		String finalrecevieddate = null;
		try {
			finalrecevieddate = sdf2.format(sdf1.parse(fm_recevieddate));
			//System.out.println(finalrecevieddate); //will be 30/06/2007

		
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		String result="";
		String dateStart = finalrecevieddate;
		String dateStop = currentdateString;

		//HH converts hour in 24 hours format (0-23), day calculation
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		Date d1 = null;
		Date d2 = null;

		try {
			d1 = format.parse(dateStart);
			d2 = format.parse(dateStop);
			
			
			//in milliseconds
			long diff = d2.getTime() - d1.getTime();

			long diffSeconds = diff / 1000 % 60;
			long diffMinutes = diff / (60 * 1000) % 60;
			long diffHours = diff / (60 * 60 * 1000) % 24;
			long diffDays = diff / (24 * 60 * 60 * 1000);
		
			long diffMonths = diffDays / 30;
			long diffYears=diffMonths/12;
			
		result=diffHours + " hrs " + diffMinutes + " Min ";
			
		
		
		//	System.out.print(diffDays + " days, ");
			//System.out.print(diffHours + " hours, ");
			//System.out.print(diffMinutes + " minutes, ");
			//System.out.print(diffSeconds + " seconds.");

//			result=getDateDifferenceInDDMMYYYY(d1, d2);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
/*
		//datetime management.............................
		
		DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		
		LocalDate ldt = LocalDate.parse(recevieddate, dateformatter);
		
		LocalDate today = LocalDate.now();
		//LocalDate birthday = LocalDate.of(1960, Month.JANUARY, 1);
		
		LocalDate birthday =ldt ;//LocalDate.of(ldt);

		Period p = Period.between(birthday, today);
		long p2 = ChronoUnit.DAYS.between(birthday, today);
		
		 result="You are " + p.getYears() + " years, " + p.getMonths() + " months, and " + p.getDays() + " days old. (" + p2 + " days total)";
		*/
		return result;
	}
	
	


	

	public String GetElispeTwoDare(String recevieddate, String enddate)
	{
		

		if("".equals(recevieddate)){return "";}
		
		if("null".equals(recevieddate)){return "";}
		
		
		Date now = new Date();
		
		SimpleDateFormat currentdf = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss");
		String currentdateString=currentdf.format(now);
		
		
		
		String fm_recevieddate = recevieddate;
		
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		
		String finalrecevieddate = null;
		try {
			finalrecevieddate = sdf2.format(sdf1.parse(fm_recevieddate));
			currentdateString = sdf2.format(sdf1.parse(enddate));
			
			
			//System.out.println(finalrecevieddate); //will be 30/06/2007

		
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		String result="";
		String dateStart = finalrecevieddate;
		String dateStop = currentdateString;

		//HH converts hour in 24 hours format (0-23), day calculation
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		Date d1 = null;
		Date d2 = null;

		try {
			d1 = format.parse(dateStart);
			d2 = format.parse(dateStop);
			
			
			//in milliseconds
			long diff = d2.getTime() - d1.getTime();

			long diffSeconds = diff / 1000 % 60;
			long diffMinutes = diff / (60 * 1000) % 60;
			long diffHours = diff / (60 * 60 * 1000) % 24;
			long diffDays = diff / (24 * 60 * 60 * 1000);
		
			long diffMonths = diffDays / 30;
			long diffYears=diffMonths/12;
			
		result=diffHours + " hrs " + diffMinutes + " Min ";
			
		
		
		//	System.out.print(diffDays + " days, ");
			//System.out.print(diffHours + " hours, ");
			//System.out.print(diffMinutes + " minutes, ");
			//System.out.print(diffSeconds + " seconds.");

//			result=getDateDifferenceInDDMMYYYY(d1, d2);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
/*
		//datetime management.............................
		
		DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		
		LocalDate ldt = LocalDate.parse(recevieddate, dateformatter);
		
		LocalDate today = LocalDate.now();
		//LocalDate birthday = LocalDate.of(1960, Month.JANUARY, 1);
		
		LocalDate birthday =ldt ;//LocalDate.of(ldt);

		Period p = Period.between(birthday, today);
		long p2 = ChronoUnit.DAYS.between(birthday, today);
		
		 result="You are " + p.getYears() + " years, " + p.getMonths() + " months, and " + p.getDays() + " days old. (" + p2 + " days total)";
		*/
		return result;
	}
	
	

	public String Get_Elisp_Hours(String recevieddate)
	{
		

		if("".equals(recevieddate)){return "";}
		
		if("null".equals(recevieddate)){return "";}
		
		
		Date now = new Date();
		
		SimpleDateFormat currentdf = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss");
		String currentdateString=currentdf.format(now);
		
		
		
		String fm_recevieddate = recevieddate;
		
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		
		String finalrecevieddate = null;
		try {
			finalrecevieddate = sdf2.format(sdf1.parse(fm_recevieddate));
			//System.out.println(finalrecevieddate); //will be 30/06/2007

		
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		String result="";
		String dateStart = finalrecevieddate;
		String dateStop = currentdateString;

		//HH converts hour in 24 hours format (0-23), day calculation
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		Date d1 = null;
		Date d2 = null;

		try {
			
			//System.out.println("Afert zero start date-->"+dateStart);
			d1 = format.parse(dateStart);
			d2 = format.parse(dateStop);
			
			
			//in milliseconds
			long diff = d2.getTime() - d1.getTime();

			long diffSeconds = diff / 1000 % 60;
			long diffMinutes = diff / (60 * 1000) % 60;
			long diffHours = diff / (60 * 60 * 1000) % 24;
			long diffDays = diff / (24 * 60 * 60 * 1000);
		
			long diffMonths = diffDays / 30;
			long diffYears=diffMonths/12;
			
		result=String.valueOf(diffHours);
			
		
		
		//	System.out.print(diffDays + " days, ");
			//System.out.print(diffHours + " hours, ");
			//System.out.print(diffMinutes + " minutes, ");
			//System.out.print(diffSeconds + " seconds.");

//			result=getDateDifferenceInDDMMYYYY(d1, d2);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
/*
		//datetime management.............................
		
		DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		
		LocalDate ldt = LocalDate.parse(recevieddate, dateformatter);
		
		LocalDate today = LocalDate.now();
		//LocalDate birthday = LocalDate.of(1960, Month.JANUARY, 1);
		
		LocalDate birthday =ldt ;//LocalDate.of(ldt);

		Period p = Period.between(birthday, today);
		long p2 = ChronoUnit.DAYS.between(birthday, today);
		
		 result="You are " + p.getYears() + " years, " + p.getMonths() + " months, and " + p.getDays() + " days old. (" + p2 + " days total)";
		*/
		return result;
	}


	
	private String mytoolname(String toolpath)
	{
		
		String[] paths = toolpath.split("\\\\");
		
		return paths[paths.length-1];
	}	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	public ArrayList<ProductVO> getProductCategory(String customerId) throws ServiceException {
			List<HashMap<String, Object>> resultSet = null;
			List<String> values = new ArrayList<String>();
			values.add(customerId);
			
			ArrayList<ProductVO> productCategoryList = new ArrayList<ProductVO>();
			
			try {
				resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_ProductCategory, values);
			} catch (StoredProcException e) 
			{
				throw new ServiceException(e.getMessage(), e);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (resultSet != null) {
				
				
					try
					{
	            	
	            	for (Iterator<HashMap<String, Object>> iterator = resultSet
							.iterator(); iterator.hasNext();) {
						HashMap<?, ?> map = iterator.next();
					
						ProductVO info = new ProductVO();
						
						//info.setBarcode(Integer.parseInt(String.valueOf(map.get("profileId"))));
						//info.setBarcode(String.valueOf(map.get("profileId")));
						//CategoryNo,CategoryName,Description
		
						info.setCategoryNo(String.valueOf(map.get("CategoryNo")));
						info.setCategoryName(String.valueOf(map.get("CategoryName")));
						info.setCategoryDescription(String.valueOf(map.get("Description")));
						info.setCatstatus(String.valueOf(map.get("catstatus")));

						productCategoryList.add(info);

	            	}
	            } catch (Exception e) {
	            	logger.info("Error"+e.getMessage());
	                throw new ServiceException(e.getMessage(), e);
	            }

			
			}

			logger.info("returning Product Category data : " + productCategoryList);

			//System.out.println("Product List:"+productCategoryList);

			return productCategoryList;
		}

	@Override
	public String createProductCategory(ProductVO productVO, String customerId,String actionuser)
			throws ServiceException {
		// TODO Auto-generated method stub
		
	List<String> values = new ArrayList<String>();
		
		values.add(productVO.getCategoryName());
		values.add(productVO.getCatstatus());
		values.add(productVO.getCategoryDescription());
		values.add(String.valueOf(customerId));
		values.add(actionuser);
		
		
		String res=null;
		try {
			  	res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Create_ProductCategory, values);
			  	logger.info("Product Category Created Successfully by:");

		} catch (StoredProcException e) {
			throw new ServiceException(e.getMessage(), e);
		}

		return res;

		
		
		
	}

	
	@Override
	public String createProduct(ProductVO productVO, String customerId,
			String actionuser) throws ServiceException 
	{
		// TODO Auto-generated method stub
		
	List<String> values = new ArrayList<String>();
		
		values.add(productVO.getCategoryNo());
		values.add(productVO.getBarcode());
		values.add(productVO.getProductCode());
		values.add(productVO.getProductname());
		values.add(productVO.getProductDescription());		
		values.add(productVO.getUnitPrice());
		values.add(productVO.getStocksOnHand());
		values.add(productVO.getSupplieruid());
		values.add(productVO.getProductwarrenty());
		values.add(String.valueOf(customerId));
		values.add(actionuser);
		
		
		String res=null;
		try {
			  	res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Create_Product, values);
			  	logger.info("Product Category Created Successfully by:");

		} catch (StoredProcException e) {
			throw new ServiceException(e.getMessage(), e);
		}

		return res;

		

		
		
			}


	

	@Override
	public String UpdateJobStopEvent(String instanceid,String rowid) throws ServiceException 
	{
		// TODO Auto-generated method stub
		
	List<String> values = new ArrayList<String>();
		
		values.add(instanceid);
		values.add(rowid);
		
		String res=null;
		try {
			  	res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Update_Job_ManuallyStop, values);
			  	logger.info("Product Category Created Successfully by:");

		} catch (StoredProcException e) {
			throw new ServiceException(e.getMessage(), e);
		}

		return res;

		

		
		
			}

	

	

	@Override
	public String UpdateTransferjobStatus(String instanceid,String procesingstatus,
			String operationflg,String ipaddress,String destionation,String rowid,String newsource) throws ServiceException 
	{
		// TODO Auto-generated method stub
		
	List<String> values = new ArrayList<String>();
		
		values.add(instanceid);
		values.add(rowid);
		values.add(procesingstatus);
		values.add(operationflg);
		values.add(ipaddress);
		values.add(destionation);
		values.add(newsource);
		
		String res=null;
		try {
			  	res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Transfer_Job_Manually, values);
			  	logger.info("Transfer job successfully");

		} catch (StoredProcException e) 
		{
			System.out.println("Transfer file error"+e.getMessage());
			throw new ServiceException(e.getMessage(), e);
		}
		System.out.println("Updating transfer job status--->"+res);

		return res;

		

		
		
			}


	
	@Override
	public ArrayList<ProductVO> getProductList(String customerId)
	
			throws ServiceException {
		// TODO Auto-generated method stub
		
		
		List<HashMap<String, Object>> resultSet = null;
		List<String> values = new ArrayList<String>();
		values.add(customerId);
		
		ArrayList<ProductVO> productList = new ArrayList<ProductVO>();
		
		try {
			resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_ProductList, values);
		} catch (StoredProcException e) 
		{
			throw new ServiceException(e.getMessage(), e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (resultSet != null) {
			
			
				try
				{
            	
            	for (Iterator<HashMap<String, Object>> iterator = resultSet
						.iterator(); iterator.hasNext();) {
					HashMap<?, ?> map = iterator.next();
				
					ProductVO info = new ProductVO();
					
					//info.setBarcode(Integer.parseInt(String.valueOf(map.get("profileId"))));
					//info.setBarcode(String.valueOf(map.get("profileId")));
					//CategoryNo,CategoryName,Description
	
					info.setProductNo(String.valueOf(map.get("productno")));
					
					info.setProductCode(String.valueOf(map.get("productcode")));
					info.setProductDescription(String.valueOf(map.get("description")));
					info.setBarcode(String.valueOf(map.get("barcode")));
					info.setUnitPrice(String.valueOf(map.get("unitprice")));
					info.setStocksOnHand(String.valueOf(map.get("stocksonhand")));
					info.setCategoryNo(String.valueOf(map.get("categoryno")));
					info.setSupplieruid(String.valueOf(map.get("supplieruid")));
					info.setProductwarrenty(String.valueOf(map.get("warrenty")));
					info.setCategoryName(String.valueOf(map.get("categoryname")));
					info.setCategoryNo(String.valueOf(map.get("categoryno")));
					info.setProductname(String.valueOf(map.get("productname")));
					info.setStatusid(String.valueOf(map.get("pstatus")));
					info.setStatus(String.valueOf(map.get("pstatus")));
					
					info.setCustomerid(String.valueOf(map.get("customerid")));
					
					
					productList.add(info);

            	}
            } catch (Exception e) {
            	logger.info("Error"+e.getMessage());
                throw new ServiceException(e.getMessage(), e);
            }

		
		}

		logger.info("returning Product Category data : " + productList);

		System.out.println("Product List:"+productList);

		return productList;
		
		
	}


	@Override
	public ArrayList<ProductVO> GetAnalytics(String customerId)
			throws ServiceException {
		// TODO Auto-generated method stub
		
		List<HashMap<String, Object>> resultSet = null;
		List<String> values = new ArrayList<String>();
		values.add(customerId);
		
		
		ArrayList<ProductVO> productList = new ArrayList<ProductVO>();
		
		try {
			
			System.out.println("Order details"+ values);

			resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_Analytics, values);
		} catch (StoredProcException e) 
		{
			throw new ServiceException(e.getMessage(), e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (resultSet != null) {
			
			
				try
				{
            	
            	for (Iterator<HashMap<String, Object>> iterator = resultSet
						.iterator(); iterator.hasNext();) {
					HashMap<?, ?> map = iterator.next();
				
					ProductVO info = new ProductVO();
					
					//info.setBarcode(Integer.parseInt(String.valueOf(map.get("profileId"))));
					//info.setBarcode(String.valueOf(map.get("profileId")));
					//CategoryNo,CategoryName,Description
	
					info.setProductNo(String.valueOf(map.get("ref_productno")));
					
					info.setProductCode(String.valueOf(map.get("productcode")));
					info.setProductDescription(String.valueOf(map.get("description")));
					info.setBarcode(String.valueOf(map.get("barcode")));
					info.setUnitPrice(String.valueOf(map.get("unitprice")));
					info.setStocksOnHand(String.valueOf(map.get("stocksonhand")));
					info.setCategoryNo(String.valueOf(map.get("categoryno")));
					info.setSupplieruid(String.valueOf(map.get("supplieruid")));
					info.setProductwarrenty(String.valueOf(map.get("warrenty")));
					info.setCategoryName(String.valueOf(map.get("categoryname")));
					info.setCategoryNo(String.valueOf(map.get("categoryno")));
					info.setProductname(String.valueOf(map.get("productname")));
					info.setStatusid(String.valueOf(map.get("pstatus")));
					info.setStatus(String.valueOf(map.get("preparationstatus")));
					info.setQuantity(String.valueOf(map.get("quantity")));
					info.setTableno(String.valueOf(map.get("ref_tableno")));
					info.setTotalprice(String.valueOf(map.get("totalprice")));
					info.setOrderdate(String.valueOf(map.get("createdatetime")));
					info.setId(String.valueOf(map.get("Id")));
					
					try{
						info.setFilename(getProductImageList(String.valueOf(map.get("productno")), customerId).get(0).getFilename());
						}catch( Exception e){}
					
					info.setCustomerid(String.valueOf(map.get("customerid")));
					
					
					productList.add(info);

            	}
            } catch (Exception e) {
            	logger.info("Error"+e.getMessage());
                throw new ServiceException(e.getMessage(), e);
            }

		
		}

		logger.info("returning Product Category data : " + productList);

		System.out.println("Order Details List:"+productList);

		return productList;
		
		
	}


	
	
	
	
	
	

	
	
	@Override
	public ArrayList<ProductVO> GetOrderDetails(String customerId)
			throws ServiceException {
		// TODO Auto-generated method stub
		
		List<HashMap<String, Object>> resultSet = null;
		List<String> values = new ArrayList<String>();
		values.add(customerId);
		
		
		ArrayList<ProductVO> productList = new ArrayList<ProductVO>();
		
		try {
			
			System.out.println("Order details"+ values);

			resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_OrderDetails, values);
		} catch (StoredProcException e) 
		{
			throw new ServiceException(e.getMessage(), e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (resultSet != null) {
			
			
				try
				{
            	
            	for (Iterator<HashMap<String, Object>> iterator = resultSet
						.iterator(); iterator.hasNext();) {
					HashMap<?, ?> map = iterator.next();
				
					ProductVO info = new ProductVO();
					
					//info.setBarcode(Integer.parseInt(String.valueOf(map.get("profileId"))));
					//info.setBarcode(String.valueOf(map.get("profileId")));
					//CategoryNo,CategoryName,Description
	
					info.setProductNo(String.valueOf(map.get("ref_productno")));
					
					info.setProductCode(String.valueOf(map.get("productcode")));
					info.setProductDescription(String.valueOf(map.get("description")));
					info.setBarcode(String.valueOf(map.get("barcode")));
					info.setUnitPrice(String.valueOf(map.get("unitprice")));
					info.setStocksOnHand(String.valueOf(map.get("stocksonhand")));
					info.setCategoryNo(String.valueOf(map.get("categoryno")));
					info.setSupplieruid(String.valueOf(map.get("supplieruid")));
					info.setProductwarrenty(String.valueOf(map.get("warrenty")));
					info.setCategoryName(String.valueOf(map.get("categoryname")));
					info.setCategoryNo(String.valueOf(map.get("categoryno")));
					info.setProductname(String.valueOf(map.get("productname")));
					info.setStatusid(String.valueOf(map.get("pstatus")));
					info.setStatus(String.valueOf(map.get("preparationstatus")));
					info.setQuantity(String.valueOf(map.get("quantity")));
					info.setTableno(String.valueOf(map.get("ref_tableno")));
					info.setTotalprice(String.valueOf(map.get("totalprice")));
					info.setOrderdate(String.valueOf(map.get("createdatetime")));
					info.setId(String.valueOf(map.get("Id")));
					
					try{
						info.setFilename(getProductImageList(String.valueOf(map.get("productno")), customerId).get(0).getFilename());
						}catch( Exception e){}
					
					info.setCustomerid(String.valueOf(map.get("customerid")));
					
					
					productList.add(info);

            	}
            } catch (Exception e) {
            	logger.info("Error"+e.getMessage());
                throw new ServiceException(e.getMessage(), e);
            }

		
		}

		logger.info("returning Product Category data : " + productList);

		System.out.println("Order Details List:"+productList);

		return productList;
		
		
	}


	
	

	@Override
	public ArrayList<ProductVO> GetOrderDetailsByFlg(String customerId,String flg)
			throws ServiceException {
		// TODO Auto-generated method stub
		
		List<HashMap<String, Object>> resultSet = null;
		List<String> values = new ArrayList<String>();
		values.add(customerId);
		values.add(flg);
		
		
		ArrayList<ProductVO> productList = new ArrayList<ProductVO>();
		
		try {
			
			System.out.println("Order details"+ values);

			resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_OrderDetailsByFlg, values);
		} catch (StoredProcException e) 
		{
			throw new ServiceException(e.getMessage(), e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (resultSet != null) {
			
			
				try
				{
            	
            	for (Iterator<HashMap<String, Object>> iterator = resultSet
						.iterator(); iterator.hasNext();) {
					HashMap<?, ?> map = iterator.next();
				
					ProductVO info = new ProductVO();
					
					//info.setBarcode(Integer.parseInt(String.valueOf(map.get("profileId"))));
					//info.setBarcode(String.valueOf(map.get("profileId")));
					//CategoryNo,CategoryName,Description
	
					info.setProductNo(String.valueOf(map.get("ref_productno")));
					
					info.setProductCode(String.valueOf(map.get("productcode")));
					info.setProductDescription(String.valueOf(map.get("description")));
					info.setBarcode(String.valueOf(map.get("barcode")));
					info.setUnitPrice(String.valueOf(map.get("unitprice")));
					info.setStocksOnHand(String.valueOf(map.get("stocksonhand")));
					info.setCategoryNo(String.valueOf(map.get("categoryno")));
					info.setSupplieruid(String.valueOf(map.get("supplieruid")));
					info.setProductwarrenty(String.valueOf(map.get("warrenty")));
					info.setCategoryName(String.valueOf(map.get("categoryname")));
					info.setCategoryNo(String.valueOf(map.get("categoryno")));
					info.setProductname(String.valueOf(map.get("productname")));
					info.setStatusid(String.valueOf(map.get("pstatus")));
					info.setStatus(String.valueOf(map.get("preparationstatus")));
					info.setQuantity(String.valueOf(map.get("quantity")));
					info.setTableno(String.valueOf(map.get("ref_tableno")));
					info.setTotalprice(String.valueOf(map.get("totalprice")));
					info.setOrderdate(String.valueOf(map.get("createdatetime")));
					info.setId(String.valueOf(map.get("Id")));
					
					try{
						info.setFilename(getProductImageList(String.valueOf(map.get("productno")), customerId).get(0).getFilename());
						}catch( Exception e){}
					
					info.setCustomerid(String.valueOf(map.get("customerid")));
					
					
					productList.add(info);

            	}
            } catch (Exception e) {
            	logger.info("Error"+e.getMessage());
                throw new ServiceException(e.getMessage(), e);
            }

		
		}

		logger.info("returning Product Category data : " + productList);

		System.out.println("Order Details List:"+productList);

		return productList;
		
		
	}

	
	//uploadimage.Do?productno=10&flg=#upload
	
	@Override
	public ArrayList<ProductVO> getProductListWithImages(String customerId)
			throws ServiceException {
		// TODO Auto-generated method stub
		
		
		List<HashMap<String, Object>> resultSet = null;
		List<String> values = new ArrayList<String>();
		values.add(customerId);
		
		ArrayList<ProductVO> productList = new ArrayList<ProductVO>();
		
		try {
			resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_ProductList, values);
		} catch (StoredProcException e) 
		{
			throw new ServiceException(e.getMessage(), e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (resultSet != null) {
			
			
				try
				{
            	
            	for (Iterator<HashMap<String, Object>> iterator = resultSet
						.iterator(); iterator.hasNext();) {
					HashMap<?, ?> map = iterator.next();
				
					ProductVO info = new ProductVO();
					
					//info.setBarcode(Integer.parseInt(String.valueOf(map.get("profileId"))));
					//info.setBarcode(String.valueOf(map.get("profileId")));
					//CategoryNo,CategoryName,Description
	
					info.setProductNo(String.valueOf(map.get("productno")));
					
					info.setProductCode(String.valueOf(map.get("productcode")));
					info.setProductDescription(String.valueOf(map.get("description")));
					info.setBarcode(String.valueOf(map.get("barcode")));
					info.setUnitPrice(String.valueOf(map.get("unitprice")));
					info.setStocksOnHand(String.valueOf(map.get("stocksonhand")));
					info.setCategoryNo(String.valueOf(map.get("categoryno")));
					info.setSupplieruid(String.valueOf(map.get("supplieruid")));
					info.setProductwarrenty(String.valueOf(map.get("warrenty")));
					info.setCategoryName(String.valueOf(map.get("categoryname")));
					info.setCategoryNo(String.valueOf(map.get("categoryno")));
					info.setProductname(String.valueOf(map.get("productname")));
					info.setStatusid(String.valueOf(map.get("pstatus")));
					info.setStatus(String.valueOf(map.get("pstatus")));
					
					info.setCustomerid(String.valueOf(map.get("customerid")));
					
					try{
					info.setFilename(getProductImageList(String.valueOf(map.get("productno")), customerId).get(0).getFilename());
					}catch( Exception e){}
					
					productList.add(info);

            	}
            } catch (Exception e) {
            	logger.info("Error"+e.getMessage());
                throw new ServiceException(e.getMessage(), e);
            }

		
		}

		logger.info("returning Product Category data : " + productList);

		System.out.println("Product List:"+productList);

		return productList;
		
		
	}


	
	
	
	
/*
	@Override
	public ArrayList<ProductVO> getTodaysMenu(String dayname)	throws ServiceException {
		// TODO Auto-generated method stub
		
		
		List<HashMap<String, Object>> resultSet = null;
		List<String> values = new ArrayList<String>();
		values.add(dayname);
		
		ArrayList<ProductVO> productList = new ArrayList<ProductVO>();
		
		try {
			resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_ProductList, values);
		} catch (StoredProcException e) 
		{
			throw new ServiceException(e.getMessage(), e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (resultSet != null) {
			
			
				try
				{
            	
            	for (Iterator<HashMap<String, Object>> iterator = resultSet
						.iterator(); iterator.hasNext();) {
					HashMap<?, ?> map = iterator.next();
				
					ProductVO info = new ProductVO();
					
					//info.setBarcode(Integer.parseInt(String.valueOf(map.get("profileId"))));
					//info.setBarcode(String.valueOf(map.get("profileId")));
					//CategoryNo,CategoryName,Description
	
					info.setProductNo(String.valueOf(map.get("productno")));
					
					info.setProductCode(String.valueOf(map.get("productcode")));
					info.setProductDescription(String.valueOf(map.get("description")));
					info.setBarcode(String.valueOf(map.get("barcode")));
					info.setUnitPrice(String.valueOf(map.get("unitprice")));
					info.setStocksOnHand(String.valueOf(map.get("stocksonhand")));
					info.setCategoryNo(String.valueOf(map.get("categoryno")));
					info.setSupplieruid(String.valueOf(map.get("supplieruid")));
					info.setProductwarrenty(String.valueOf(map.get("warrenty")));
					info.setCategoryName(String.valueOf(map.get("categoryname")));
					info.setCategoryNo(String.valueOf(map.get("categoryno")));
					info.setProductname(String.valueOf(map.get("productname")));
					info.setStatusid(String.valueOf(map.get("pstatus")));
					info.setStatus(GetStatusDisplayName(String.valueOf(map.get("pstatus"))));
				
					
					productList.add(info);

            	}
            } catch (Exception e) {
            	logger.info("Error"+e.getMessage());
                throw new ServiceException(e.getMessage(), e);
            }

		
		}

		logger.info("returning Product Category data : " + productList);

		System.out.println("Product List:"+productList);

		return productList;
		
		
	}

*/


	
	

	
	@Override
	public String AddToCart(ProductVO productVO, String customerId, String actionuser)
			throws ServiceException {
		
		// TODO Auto-generated method stub
		
		int stockInH=Integer.valueOf( productVO.getStocksOnHand())+Integer.valueOf(productVO.getNewquantity());
		int newstock=Integer.valueOf(productVO.getTotalstock());
		if (newstock==stockInH)
			
		{

			List<String> values = new ArrayList<String>();
				
				values.add(productVO.getProductNo());
				values.add(productVO.getProductCode());
				values.add(productVO.getNewquantity());
				values.add(String.valueOf(stockInH));
				values.add(String.valueOf(customerId));
				values.add(actionuser);
				values.add(productVO.getVirtualid());
				
				
				String res=null;
				try {
					  	
						res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.UpdateStockIn_Product, values);
					  	

				} catch (StoredProcException e) {
					throw new ServiceException(e.getMessage(), e);
				}

				return res;


			
		}
		else
		{
		 return "invalid total Stock";	
		}
					
		
	
	}

	
	
	
	@Override
	public String StackIn(ProductVO productVO, String customerId, String actionuser)
			throws ServiceException {
		
		// TODO Auto-generated method stub
		
		int stockInH=Integer.valueOf( productVO.getStocksOnHand())+Integer.valueOf(productVO.getNewquantity());
		int newstock=Integer.valueOf(productVO.getTotalstock());
		if (newstock==stockInH)
			
		{

			List<String> values = new ArrayList<String>();
				
				values.add(productVO.getProductNo());
				values.add(productVO.getProductCode());
				values.add(productVO.getNewquantity());
				values.add(String.valueOf(stockInH));
				values.add(String.valueOf(customerId));
				values.add(actionuser);
				
				
				String res=null;
				try {
					  	
						res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.UpdateStockIn_Product, values);
					  	

				} catch (StoredProcException e) {
					throw new ServiceException(e.getMessage(), e);
				}

				return res;


			
		}
		else
		{
		 return "invalid total Stock";	
		}
					
		
	
	}

	


	@Override
	public String UpdateProductByField(ProductVO productVO, String customerId, String fieldname, String fieldvalue,String actionuser)
			throws ServiceException {
		
		// TODO Auto-generated method stub
		
			List<String> values = new ArrayList<String>();
				
				values.add(productVO.getProductNo());
				values.add(customerId);
				values.add(fieldname);
				values.add(fieldvalue);
				values.add(actionuser);
				
				
				String res=null;
				try {
					if(fieldname.equals("unitprice"))
					{ 	
						res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Update_Product_UnitPrice, values);
						
					}
					else if(fieldname.equals("name"))
					{
						res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Update_Product_Name, values);
		
					} 	
					else if(fieldname.equals("description"))
					{

						res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Update_Product_Description, values);
					
					} 	
					else if(fieldname.equals("code"))
					{

						res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Update_Product_Code, values);
						
					} 
					else if(fieldname.equals("delete"))
					{

						res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Delet_Product, values);
					}

					else if(fieldname.equals("deleteimage"))
					{

						res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Delet_Product, values);
					}

					
					else
					{
						return "Sorry.Wrong Operation";
					}

				} catch (StoredProcException e) {
					throw new ServiceException(e.getMessage(), e);
				}

				return res;


		
	
	}

	


	@Override
	public String DeleteImage(ProductVO productVO, String customerId,String actionuser)
			throws ServiceException {
		
		// TODO Auto-generated method stub
		
			List<String> values = new ArrayList<String>();
				
				values.add(productVO.getId());	
				values.add(productVO.getFilename());
				values.add(customerId);
				values.add(actionuser);
				
				String res=null;
				try
				{
					res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Delete_Image, values);
				
				} catch (StoredProcException e) {
					throw new ServiceException(e.getMessage(), e);
				}

				return res;
	
	}



	

	@Override
	public String ManageOrderStauts(ProductVO productVO,String newstatus, String customerId,String actionuser)
			throws ServiceException {
		
		// TODO Auto-generated method stub
		
			List<String> values = new ArrayList<String>();
				
				values.add(productVO.getId());	
				values.add(newstatus);
				values.add(productVO.getTableno());
				values.add(customerId);
				values.add(productVO.getProductNo());
				values.add(actionuser);
				 
				String res=null;
				try
				{
					System.out.println("Updating Order---->" +values);
					
					res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Update_OrderStatus, values);
				
				} catch (StoredProcException e) {
					throw new ServiceException(e.getMessage(), e);
				}

				return res;
	
	}


	
	
//upload products
	
	

private static String getFileExtension(String fileName) {
    if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
    return fileName.substring(fileName.lastIndexOf(".")+1);
    else return "";
}


@Value("${upload.folder}")

private String ATTACHMENT_FOLDER;


@Value("${invalidextentions}")

private String invalidextentions;

@Override
public String UploadAttachment(ProductVO productVO, String actionuser,String customerid)
		throws ServiceException {
	// TODO Auto-generated method stub
	
	
	String name = productVO.getFile().getOriginalFilename();
	
	
	String uuid = UUID.randomUUID().toString();
	
	System.out.println("UUUID->"+uuid);
	
	String filextension=getFileExtension(name);
	
	
if (!invalidextentions.contains(filextension))
	
	{
	System.out.println("Invalid file format");
		return "Invalid File format:"+ invalidextentions;
	}
	
    logger.info("file name ... " + name);

    String filename= productVO.getProductNo()+"-"+uuid +name;
    //File file1=new File(name);
    
    String filePath = ATTACHMENT_FOLDER + File.separatorChar + File.separatorChar +filename;
    
    File destFile = new File(filePath);
    
    String res=null;
    logger.info("Attacment File Path:"+filePath);
    
    try {
        if (!destFile.exists()) {
            destFile.mkdirs();
        }
        
        //file.transferTo(destFile);
        
        productVO.getFile().transferTo(destFile);
        
	    System.out.println("File----->"+ productVO.getFile());
        
	    List<String> values = new ArrayList<String>();
	
	    values.add(String.valueOf(productVO.getProductNo()));
		
	    values.add(filename);
		
	    values.add(productVO.getFile().getContentType());
		
	    values.add(filePath);
		
	    values.add(actionuser);
		
	    values.add(filextension);
	    values.add(customerid);
	


	try
	{	res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Upload_Picture, values);
		  	logger.info("upload files:"+productVO.toString()+" created: "+actionuser);

	} catch (StoredProcException e) 
	{	
			return "Error:"+e.getMessage();
	
	}




        
    
    } catch (IllegalStateException e) {
        e.printStackTrace();
        res=e.getMessage();
        System.out.println("Failed to upload file"+res);
        
        logger.info("File uploaded failed:" + name);
        return res;
    } catch (IOException e) {
        e.printStackTrace();
        res=e.getMessage();
        System.out.println("Failed to upload file"+res);
        
        logger.info("File uploaded failed:" + name);
        return res;
    }
    

   // return res;
return filename;
	
}

	
	


@Override
public ArrayList<ProductVO> getProductImageList(String productno,String customerid)
		throws ServiceException {
	// TODO Auto-generated method stub

	
	List<HashMap<String, Object>> resultSet = null;
	List<String> values = new ArrayList<String>();
	values.add(productno);
	values.add(customerid);
	
	ArrayList<ProductVO> customerList = new ArrayList<ProductVO>();
	
	try 
	{ 			
		resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_LIST_OF_ProductImage, values);
	} catch (StoredProcException e) 
	{
		throw new ServiceException(e.getMessage(), e);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	if (resultSet != null) {
		
		
			try
			{
        	
        	for (Iterator<HashMap<String, Object>> iterator = resultSet
					.iterator(); iterator.hasNext();) {
				HashMap<?, ?> map = iterator.next();
			
				ProductVO info = new ProductVO();
				
				info.setFilename(String.valueOf(map.get("filename")));
				info.setId(String.valueOf(map.get("att_Id")));
				info.setProductNo(String.valueOf(map.get("ref_productid")));
				
				
				
				
				customerList.add(info);

        	}
        } catch (Exception e) {
        	logger.info("Error"+e.getMessage());
            throw new ServiceException(e.getMessage(), e);
        }

	
	}

	logger.info("Returing Customer query List : " + customerList);

	System.out.println("Customer List:"+customerList);

	return customerList;
	
}


@Override
	public String getFTPServiceStatus() {

		StringBuffer output = new StringBuffer();
				
		String command="sc query "+'"'+"SolarWinds SFTP Server"+'"';
		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			//p.waitFor();
			BufferedReader reader =new BufferedReader(new InputStreamReader(p.getInputStream()));

//			int flg=0;
                     String line = "";
			while ((line = reader.readLine())!= null) 
			{
				
				output.append(line+",");
			}

			//p.destroy();
			
			
		} catch (Exception e) {
			e.printStackTrace();
			//System.exit(1);
		}
		


		if (output.toString().contains("RUNNING"))
		return "Running";
		else
			return "Stopped";

	}




@Override
public String startFTPServiceStatus() {

	StringBuffer output = new StringBuffer();
			
	String command="net start "+'"'+"SolarWinds SFTP Server"+'"';
	Process p;
	try {
		p = Runtime.getRuntime().exec(command);
		//p.waitFor();
		BufferedReader reader =new BufferedReader(new InputStreamReader(p.getInputStream()));

//		int flg=0;
                 String line = "";
		while ((line = reader.readLine())!= null) 
		{
			
			output.append(line+",");
		}

		//p.destroy();
		
		
	} catch (Exception e) {
		e.printStackTrace();
		//System.exit(1);
	}
	


	if (output.toString().contains("RUNNING"))
	return "Running";
	else
		return "Stopped";

}





	
/*

@Override
public String ProductListImage(String productno,String customerid)
		throws ServiceException {
	// TODO Auto-generated method stub

	
	List<HashMap<String, Object>> resultSet = null;
	List<String> values = new ArrayList<String>();
	values.add(productno);
	values.add(customerid);
	
	ArrayList<ProductVO> customerList = new ArrayList<ProductVO>();
	
	try 
	{ 			
		resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_LIST_OF_ProductImage, values);
	} catch (StoredProcException e) 
	{
		throw new ServiceException(e.getMessage(), e);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
	if (resultSet != null) {
		
		
			try
			{
        	
        	for (Iterator<HashMap<String, Object>> iterator = resultSet
					.iterator(); iterator.hasNext();) {
				HashMap<?, ?> map = iterator.next();
			
				return String.valueOf(map.get("filename"));
	
        	}
        } catch (Exception e) {
        	logger.info("Error"+e.getMessage());
            throw new ServiceException(e.getMessage(), e);
        }

	
	}

	logger.info("Returing Customer query List : " + customerList);

	System.out.println("Customer List:"+customerList);

	return customerList;
	
}
*/


//get product string
	
	private String GetStatusDisplayName(String status)
	{
		if("0".equals(status))
		{
			return "Inactive";
		}else if("1".equals(status))
		{
			return "Active";
		}else
		{
			return "Unknown";
		}
		
		
	}
	
	
	
	private String InStatus(String status)
	{
		if("0".equals(status))
		{
			return "Not in use";
		}else if("1".equals(status))
		{
			return "In Use";
		}else
		{
			return "Unknown";
		}
		
		
	}


}


