
package com.mlab.endpoint.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.mlab.endpoint.Model.AttachedFile;
import com.mlab.endpoint.Model.ProductVO;
import com.mlab.endpoint.Model.TableVO;
import com.mlab.endpoint.Model.CustomerVO;
import com.mlab.endpoint.Model.OrganizationVO;
import com.mlab.endpoint.exception.ServiceException;
import com.mlab.endpoint.exception.StoredProcException;
import com.mlab.endpoint.interfaces.CustomerInformation;
import com.mlab.endpoint.interfaces.ProductInformationView;
import com.mlab.endpoint.interfaces.StoredProcJdbcDao;
import com.mlab.endpoint.util.Constants;
import com.mlab.endpoint.util.Ranumber;

public class CustomerInformationImpl implements CustomerInformation {
	private static final Logger logger = LoggerFactory.getLogger(CustomerInformationImpl.class);

	private static final String Null = null;
	

	private StoredProcJdbcDao storedProcJdbcDao;

	public StoredProcJdbcDao getStoredProcJdbcDao() {
		return storedProcJdbcDao;
	}

	public void setStoredProcJdbcDao(StoredProcJdbcDao storedProcJdbcDao) {
		this.storedProcJdbcDao = storedProcJdbcDao;
	}

	
	@Override
	public ArrayList<CustomerVO> checkCustomer(String customerId, String mobileNumber,String cid)
			throws ServiceException {
		// TODO Auto-generated method stub
		
		List<HashMap<String, Object>> resultSet = null;
		List<String> values = new ArrayList<String>();
		values.add(customerId);
		values.add(mobileNumber);
		values.add(cid);
		
		
		
		ArrayList<CustomerVO> customerList = new ArrayList<CustomerVO>();
		
		try 
		{ 			
			resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Customer_By_Mobile_ID, values);
		} catch (StoredProcException e) 
		{
			throw new ServiceException(e.getMessage(), e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (resultSet != null) {
			
			
				try
				{
            	
            	for (Iterator<HashMap<String, Object>> iterator = resultSet
						.iterator(); iterator.hasNext();) {
					HashMap<?, ?> map = iterator.next();
				
					CustomerVO info = new CustomerVO();
					
					//info.setBarcode(Integer.parseInt(String.valueOf(map.get("profileId"))));
					//info.setBarcode(String.valueOf(map.get("profileId")));
					//CategoryNo,CategoryName,Description
	
					
					//'customer_id,mobile,customer_identificationumber
					
					
					//customer_id,customertype,customer_name,customer_identificationumber,email,phone,mobile,status,createdby,editedby,createdate,lasteditdate
					
					info.setCustomerName(String.valueOf(map.get("customer_name")));
					info.setCid(String.valueOf(map.get("customer_id")));
					info.setCustomeridentificationo(String.valueOf(map.get("customer_identificationumber")));
					info.setMobileNumber(String.valueOf(map.get("mobile")));
					info.setCustomerAddress(String.valueOf(map.get("address")));
					info.setCustomertype(String.valueOf(map.get("customertype")));
					
						
					
					//'ProductNo,ProductCode,Description,Barcode,UnitPrice,StocksOnHand,CategoryNo,supplieruid,warrenty,createby,createdate,customerid

					customerList.add(info);

            	}
            } catch (Exception e) {
            	logger.info("Error"+e.getMessage());
                throw new ServiceException(e.getMessage(), e);
            }

		
		}

		logger.info("Returing Customer query List : " + customerList);

		System.out.println("Customer List:"+customerList);

		return customerList;

	}

	@Override
	public String createCustomer(CustomerVO customerVO, String customerId,
			String actionuser) throws ServiceException {
		// TODO Auto-generated method stub
		
	List<String> values = new ArrayList<String>();
		
		values.add(customerVO.getMobileNumber());
		values.add(customerVO.getCustomerName());
		values.add(UUID.randomUUID().toString());
		values.add(customerVO.getCustomerAddress());
		values.add(customerVO.getEmail());
		values.add(customerVO.getCustomertype());
		values.add(customerId.trim());
		values.add(actionuser);
		
		
		String res=null;
		try {
			  	res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Create_Customer, values);
			  	
			  	System.out.println("create customer result"+res);
			  	
			  	logger.info("Customer Created Successfully by:"+actionuser +"with customer ID:"+customerId);

		} catch (StoredProcException e) {
			
			System.out.println("error while create customer"+ e.getMessage());
			
			
			throw new ServiceException(e.getMessage(), e);
		}

		return res;

		
		
	}


	
	
	

	@Override
	public String UpdateCustomer(CustomerVO customerVO, String customerId,
			String actionuser) throws ServiceException {
		// TODO Auto-generated method stub
		
	List<String> values = new ArrayList<String>();
		
		values.add(customerVO.getMobileNumber());
		values.add(customerVO.getCustomerName());
		values.add(customerVO.getCustomeridentificationo());
		values.add(customerVO.getCustomerAddress());
		values.add(customerVO.getEmail());
		values.add(customerId.trim());
		values.add(customerVO.getStatus());
		values.add(actionuser);
		
		String res=null;
		
		try
		{
			  	res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Update_Customer, values);
			  	
			  	System.out.println("update customer result"+res);
			  	
			  	logger.info("Customer update Successfully by:"+actionuser +"with customer ID:"+customerId);

		} catch (StoredProcException e) {
			
			System.out.println("error while create customer"+ e.getMessage());
			
			
			throw new ServiceException(e.getMessage(), e);
		}

		return res;

		
		
	}


	
	
	@Override
	public ArrayList<CustomerVO> getListofCustomer(String actionuser)
			throws ServiceException {
		// TODO Auto-generated method stub

		
		List<HashMap<String, Object>> resultSet = null;
		List<String> values = new ArrayList<String>();
		values.add(actionuser);
		
		ArrayList<CustomerVO> customerList = new ArrayList<CustomerVO>();
		
		try 
		{ 			
			resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_LIST_OF_CUSTOMER, values);
		} catch (StoredProcException e) 
		{
			throw new ServiceException(e.getMessage(), e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (resultSet != null) {
			
			
				try
				{
            	
            	for (Iterator<HashMap<String, Object>> iterator = resultSet
						.iterator(); iterator.hasNext();) {
					HashMap<?, ?> map = iterator.next();
				
					CustomerVO info = new CustomerVO();
					
					//info.setBarcode(Integer.parseInt(String.valueOf(map.get("profileId"))));
					//info.setBarcode(String.valueOf(map.get("profileId")));
					//CategoryNo,CategoryName,Description
	
					
					//'customer_id,mobile,customer_identificationumber
					
					
					//customer_id,customertype,customer_name,customer_identificationumber,email,phone,mobile,status,createdby,editedby,createdate,lasteditdate
					
					info.setCustomerName(String.valueOf(map.get("customer_name")));
					info.setCid(String.valueOf(map.get("customer_id")));
					info.setCustomeridentificationo(String.valueOf(map.get("customer_identificationumber")));
					info.setMobileNumber(String.valueOf(map.get("mobile")));
					info.setCustomerAddress(String.valueOf(map.get("address")));
					info.setCustomertype(String.valueOf(map.get("customertype")));
					info.setEmail(String.valueOf(map.get("email")));
					info.setStatus(String.valueOf(map.get("status")));
						
					
					//'ProductNo,ProductCode,Description,Barcode,UnitPrice,StocksOnHand,CategoryNo,supplieruid,warrenty,createby,createdate,customerid

					customerList.add(info);

            	}
            } catch (Exception e) {
            	logger.info("Error"+e.getMessage());
                throw new ServiceException(e.getMessage(), e);
            }

		
		}

		logger.info("Returing Customer query List : " + customerList);

		System.out.println("Customer List:"+customerList);

		return customerList;
		
	}



	

	
	
	@Override
	public ArrayList<OrganizationVO> GetListOfLocations(String actionuser)
			throws ServiceException {
		// TODO Auto-generated method stub

		
		List<HashMap<String, Object>> resultSet = null;
		List<String> values = new ArrayList<String>();
		values.add(actionuser);
		
		ArrayList<OrganizationVO> customerList = new ArrayList<OrganizationVO>();
		
		try 
		{ 			
			resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_List_All_Location_Details, values);
		} catch (StoredProcException e) 
		{
			throw new ServiceException(e.getMessage(), e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (resultSet != null) {
			
			
				try
				{
            	
            	for (Iterator<HashMap<String, Object>> iterator = resultSet
						.iterator(); iterator.hasNext();) {
					HashMap<?, ?> map = iterator.next();
				
					OrganizationVO info = new OrganizationVO();
					
					//areaId,areaname,ref_cityid,createdby,createdate,lasteditedby,lasteditdate
			
					info.setArea(String.valueOf(map.get("areaname")));
					info.setLocid(String.valueOf(map.get("areaId")));
					
					info.setCityid(String.valueOf(map.get("ref_cityid")));
					info.setCityname(String.valueOf(map.get("cityname")));
					info.setCreatedby(String.valueOf(map.get("createdby")));
					info.setCreatedate(String.valueOf(map.get("createdate")));
					info.setLasteditby(String.valueOf(map.get("lasteditby")));
					info.setLasteditdate(String.valueOf(map.get("lasteditdate")));
			
					
					
					customerList.add(info);

            	}
            } catch (Exception e) {
            	logger.info("Error"+e.getMessage());
                throw new ServiceException(e.getMessage(), e);
            }

		
		}

		logger.info("Returing Customer query List : " + customerList);

		System.out.println("Customer List:"+customerList);

		return customerList;
		
	}




	
	
	
	
	@Override
	public String createOrganization(OrganizationVO organizationvo, String customerId,
			String actionuser) throws ServiceException {
		// TODO Auto-generated method stub
		
	List<String> values = new ArrayList<String>();
		
		values.add(organizationvo.getCustomerid());
		values.add(organizationvo.getOrganizationame());
		values.add(organizationvo.getLicenseno());
		values.add(organizationvo.getLocid());
		values.add(organizationvo.getAddress());
		values.add(actionuser);
		
		
		String res=null;
		try {
			  	res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Create_Organization, values);
			  	
			  	System.out.println("create customer result"+res);
			  	
			  	logger.info("Customer Created Successfully by:"+actionuser +"with customer ID:"+customerId);

		} catch (StoredProcException e) {
			
			System.out.println("error while create customer"+ e.getMessage());
			
			
			throw new ServiceException(e.getMessage(), e);
		}

		return res;

		
		
	}



	@Override
	public String CreateBooking(OrganizationVO organizationvo,
			String customerId) throws ServiceException {
		// TODO Auto-generated method stub
		
	List<String> values = new ArrayList<String>();
		
		values.add(organizationvo.getCustomerid());
		values.add(organizationvo.getBookingdate());
		values.add(organizationvo.getStartime());
		values.add(organizationvo.getEndtime());
		values.add(organizationvo.getCapacity());
		values.add(organizationvo.getMobileno());
		values.add(organizationvo.getName());
		values.add(organizationvo.getOrId());
		
		
		String res=null;
		try {
			  	res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Create_Booking, values);
			  	
			  	System.out.println("create customer result"+res);
			  	
		} catch (StoredProcException e) {
			
			System.out.println("error while create customer"+ e.getMessage());
			
			
			throw new ServiceException(e.getMessage(), e);
		}

		return res;

		
		
	}


	

	
	
	@Override
	public String UpdateOrganization(OrganizationVO organizationvo, String customerId,
			String actionuser) throws ServiceException {
		// TODO Auto-generated method stub
		
		
	List<String> values = new ArrayList<String>();
		
	
		values.add(organizationvo.getCustomerid());
		values.add(organizationvo.getOrganizationame());
		values.add(organizationvo.getLicenseno());
		values.add(organizationvo.getLocid());
		values.add(organizationvo.getAddress());
		values.add(organizationvo.getOrgstatus());
		values.add(actionuser);
		
		
		String res=null;
		try {

			System.out.println("Get organization previous organization"+organizationvo.getPreviousname());
			
			if (organizationvo.getOrganizationame().equalsIgnoreCase(organizationvo.getPreviousname()))
			{
				res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Update_Organization_WithoutName, values);
				System.out.println("create customer result"+res);
			  	logger.info("Customer Update Successfully by:"+actionuser +"with customer ID:"+customerId);
			  	
				
			}else
			{
				res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Update_Organization, values);
			  	System.out.println("create customer result"+res);
			  	logger.info("Customer Created Successfully by:"+actionuser +"with customer ID:"+customerId);
			}
			
			  	

		} catch (StoredProcException e) {
			
			System.out.println("error while create customer"+ e.getMessage());
			
			
			throw new ServiceException(e.getMessage(), e);
		}

		return res;

		
		
	}

	

	
	
	@Override
	public ArrayList<OrganizationVO> getListOfOrganization(String actionuser)
			throws ServiceException {
		// TODO Auto-generated method stub

		
		List<HashMap<String, Object>> resultSet = null;
		List<String> values = new ArrayList<String>();
		values.add(actionuser);
		
		ArrayList<OrganizationVO> organizationList = new ArrayList<OrganizationVO>();
		
		try 
		{
			
			resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_All_OrganizationList, values);
	
		} catch (StoredProcException e) 
		{
			throw new ServiceException(e.getMessage(), e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (resultSet != null) {
			
			
				try
				{
            	
            	for (Iterator<HashMap<String, Object>> iterator = resultSet
						.iterator(); iterator.hasNext();) {
					HashMap<?, ?> map = iterator.next();
				
					OrganizationVO info = new OrganizationVO();
						
			
					info.setOrganizationame(String.valueOf(map.get("organizationame")));
					info.setOrId(String.valueOf(map.get("orId")));
					info.setLicenseno(String.valueOf(map.get("licenseno")));
					info.setOrgstatus(String.valueOf(map.get("orgstatus")));
					info.setArea(String.valueOf(map.get("area")));
					info.setAddress(String.valueOf(map.get("address")));
					
					info.setRef_customerid(String.valueOf(map.get("ref_customerid")));
					info.setCreatedby(String.valueOf(map.get("createdby")));
					info.setCreatedate(String.valueOf(map.get("createdate")));
					info.setLasteditby(String.valueOf(map.get("lasteditby")));
					info.setLasteditdate(String.valueOf(map.get("lasteditdate")));
					
					organizationList.add(info);

            	}
            } catch (Exception e) {
            	logger.info("Error"+e.getMessage());
                throw new ServiceException(e.getMessage(), e);
            }

		
		}

		logger.info("Returing organization query List : " + organizationList);

		System.out.println("Organization List:"+organizationList);

		return organizationList;
		
	}




	
	@Override
	public ArrayList<OrganizationVO> GetResturantByLocation(String locid,String actionuser)
			throws ServiceException {
		// TODO Auto-generated method stub

		
		List<HashMap<String, Object>> resultSet = null;
		List<String> values = new ArrayList<String>();
		values.add(locid);
		values.add(actionuser);
		
		ArrayList<OrganizationVO> organizationList = new ArrayList<OrganizationVO>();
		
		try 
		{
			
			resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.get_All_OrganizationListByLocationId, values);
	
		} catch (StoredProcException e) 
		{
			throw new ServiceException(e.getMessage(), e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (resultSet != null) {
			
			
				try
				{
            	
            	for (Iterator<HashMap<String, Object>> iterator = resultSet
						.iterator(); iterator.hasNext();) {
					HashMap<?, ?> map = iterator.next();
				
					OrganizationVO info = new OrganizationVO();
						
			
					info.setOrganizationame(String.valueOf(map.get("organizationame")));
					info.setOrId(String.valueOf(map.get("orId")));
					info.setLicenseno(String.valueOf(map.get("licenseno")));
					info.setOrgstatus(String.valueOf(map.get("orgstatus")));
					info.setArea(String.valueOf(map.get("area")));
					info.setAddress(String.valueOf(map.get("address")));
					
					info.setRef_customerid(String.valueOf(map.get("ref_customerid")));
					info.setCreatedby(String.valueOf(map.get("createdby")));
					info.setCreatedate(String.valueOf(map.get("createdate")));
					info.setLasteditby(String.valueOf(map.get("lasteditby")));
					info.setLasteditdate(String.valueOf(map.get("lasteditdate")));
					
					info.setTableno(String.valueOf(map.get("tableno")));
					info.setCapacity(String.valueOf(map.get("capacity")));
					
					organizationList.add(info);

            	}
            } catch (Exception e) {
            	logger.info("Error"+e.getMessage());
                throw new ServiceException(e.getMessage(), e);
            }

		
		}

		logger.info("Returing organization query List : " + organizationList);

		System.out.println("Organization List:"+organizationList);

		return organizationList;
		
	}


	
	

	@Override
	public ArrayList<OrganizationVO> Get_BookingDetails(String customerid)
			throws ServiceException {
		// TODO Auto-generated method stub

		
		List<HashMap<String, Object>> resultSet = null;
		List<String> values = new ArrayList<String>();
		values.add(customerid);
		
		ArrayList<OrganizationVO> organizationList = new ArrayList<OrganizationVO>();
		
		try 
		{
			
			resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_All_BookingDetails, values);
	
		} catch (StoredProcException e) 
		{
			throw new ServiceException(e.getMessage(), e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (resultSet != null) {
			
			
				try
				{
            	
            	for (Iterator<HashMap<String, Object>> iterator = resultSet
						.iterator(); iterator.hasNext();) {
					HashMap<?, ?> map = iterator.next();
				
					OrganizationVO info = new OrganizationVO();
					
					info.setBookingdate(String.valueOf(map.get("bookingdate")));
					info.setStartime(String.valueOf(map.get("bookingstarttime")));
					info.setEndtime(String.valueOf(map.get("bookingendtime")));
					info.setName(String.valueOf(map.get("requestorname")));
					info.setMobileno(String.valueOf(map.get("requestormobile")));
					info.setId(String.valueOf(map.get("bookingId")));
					info.setTablename(String.valueOf(map.get("tablename")));
					info.setStatus(String.valueOf(map.get("status")));
					info.setCustomerid(String.valueOf(map.get("customerid")));
					info.setTableno(String.valueOf(map.get("tableno")));
					info.setCapacity(String.valueOf(map.get("capacity")));
					
					organizationList.add(info);

            	}
            } catch (Exception e) {
            	logger.info("Error"+e.getMessage());
                throw new ServiceException(e.getMessage(), e);
            }

		
		}

		logger.info("Returing organization query List : " + organizationList);

		System.out.println("Returning  Booking List:"+organizationList);

		return organizationList;
		
	}


	
	
	

	@Override
	public ArrayList<OrganizationVO> GetCustomerOrganizationList(String customerid,String actionuser)
			throws ServiceException {
		// TODO Auto-generated method stub

		
		List<HashMap<String, Object>> resultSet = null;
		List<String> values = new ArrayList<String>();
		values.add(actionuser);
		values.add(customerid);
		
		ArrayList<OrganizationVO> organizationList = new ArrayList<OrganizationVO>();
		
		try 
		{
			
			resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_All_CustomerOrganizationList, values);
	
		} catch (StoredProcException e) 
		{
			throw new ServiceException(e.getMessage(), e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (resultSet != null) {
			
			
				try
				{
            	
            	for (Iterator<HashMap<String, Object>> iterator = resultSet
						.iterator(); iterator.hasNext();) {
					HashMap<?, ?> map = iterator.next();
				
					OrganizationVO info = new OrganizationVO();
						
			
					info.setOrganizationame(String.valueOf(map.get("organizationame")));
					info.setOrId(String.valueOf(map.get("orId")));
					info.setLicenseno(String.valueOf(map.get("licenseno")));
					info.setOrgstatus(String.valueOf(map.get("orgstatus")));
					info.setAreaid(String.valueOf(map.get("area")));
					info.setAddress(String.valueOf(map.get("address")));
					info.setArea(String.valueOf(map.get("areaname")));
					
					info.setRef_customerid(String.valueOf(map.get("ref_customerid")));
					info.setCreatedby(String.valueOf(map.get("createdby")));
					info.setCreatedate(String.valueOf(map.get("createdate")));
					info.setLasteditby(String.valueOf(map.get("lasteditby")));
					info.setLasteditdate(String.valueOf(map.get("lasteditdate")));
					
					organizationList.add(info);

            	}
            } catch (Exception e) {
            	logger.info("Error"+e.getMessage());
                throw new ServiceException(e.getMessage(), e);
            }

		
		}

		logger.info("Returing organization query List : " + organizationList);

		System.out.println("Organization List:"+organizationList);

		return organizationList;
		
	}

	



	@Override
	public ArrayList<TableVO> GetCustomerTableList(String customerid, String  resturtantid, String actionuser)
			throws ServiceException {
		// TODO Auto-generated method stub

		
		List<HashMap<String, Object>> resultSet = null;
		List<String> values = new ArrayList<String>();
		values.add(actionuser);
		values.add(customerid);
		values.add(resturtantid);
		
		
		ArrayList<TableVO> organizationList = new ArrayList<TableVO>();
		
		try 
		{
			if ("null".equals(resturtantid))
					{
				

					resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_All_Table_By_CustomerIdWithoutresturantid, values);
		
					}else
					{

						resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_All_Table_By_CustomerIdWithResturantID, values);
							
					}
			
		} catch (StoredProcException e) 
		{
			throw new ServiceException(e.getMessage(), e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (resultSet != null) {
			
			
				try
				{
            	
            	for (Iterator<HashMap<String, Object>> iterator = resultSet
						.iterator(); iterator.hasNext();) {
					HashMap<?, ?> map = iterator.next();
				
					TableVO info = new TableVO();
						
			
					info.setOrganizationame(String.valueOf(map.get("organizationame")));
					info.setOrgId(String.valueOf(map.get("resturantid")));
			
					info.setTablename(String.valueOf(map.get("tablename")));
					info.setCapacity(String.valueOf(map.get("capacity")));
					info.setDeviceId(String.valueOf(map.get("deviceid")));
					info.setTableno(String.valueOf(map.get("tId")));
					info.setCustomerId(String.valueOf(map.get("customerid")));
					info.setCreatedby(String.valueOf(map.get("createdby")));
					info.setCreatedate(String.valueOf(map.get("createdate")));
					info.setLasteditby(String.valueOf(map.get("lasteditby")));
					info.setLasteditdate(String.valueOf(map.get("lasteditdate")));
					info.setDevicemac(String.valueOf(map.get("deviceid")));
					
					organizationList.add(info);

            	}
            } catch (Exception e) {
            	logger.info("Error"+e.getMessage());
                throw new ServiceException(e.getMessage(), e);
            }

		
		}

		logger.info("Returing table query List : " + organizationList);

		System.out.println("table List:"+organizationList);

		return organizationList;
		
	}



	
	@Override
	public ArrayList<TableVO> GetCustomerDeviceList(String customerid, String  resturtantid, String actionuser)
			throws ServiceException {
		// TODO Auto-generated method stub

		
		List<HashMap<String, Object>> resultSet = null;
		List<String> values = new ArrayList<String>();
		values.add(actionuser);
		values.add(customerid);
		values.add(resturtantid);
		
		
		ArrayList<TableVO> organizationList = new ArrayList<TableVO>();
		
		try 
		{
			if ("null".equals(resturtantid))
					{
					System.out.println("Entering here");
					
						resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_All_Devicees_By_CustomerIdWithoutresturantid, values);

					}else
					{
						resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_All_Devices_By_CustomerIdWithResturantID, values);
					}
			
		} catch (StoredProcException e) 
		{
			throw new ServiceException(e.getMessage(), e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (resultSet != null) {
			
			
				try
				{
            	
            	for (Iterator<HashMap<String, Object>> iterator = resultSet
						.iterator(); iterator.hasNext();) {
					HashMap<?, ?> map = iterator.next();
				

					
					TableVO info = new TableVO();
					
					//dId,macid,registrationdate,supplier,warrenty,createby,createdate,lasteditby,lasteditdate,devicemodel,customerid,resturantid,devicestatus
			
					info.setOrganizationame(String.valueOf(map.get("organizationame")));
					
					info.setOrgId(String.valueOf(map.get("resturantid")));
			
					info.setDevicemac(String.valueOf(map.get("macid")));
					info.setDevicemodel(String.valueOf(map.get("devicemodel")));
					info.setDevicesupplier(String.valueOf(map.get("supplier")));
					info.setRegistrationdate(String.valueOf(map.get("registrationdate")));
					info.setWarrenty(String.valueOf(map.get("warrenty")));
					
					
					info.setDeviceId(String.valueOf(map.get("dId")));
					info.setDevicerowid(String.valueOf(map.get("dId")));
					
					info.setCustomerId(String.valueOf(map.get("customerid")));
					info.setCreatedby(String.valueOf(map.get("createdby")));
					info.setCreatedate(String.valueOf(map.get("createdate")));
					info.setLasteditby(String.valueOf(map.get("lasteditby")));
					info.setLasteditdate(String.valueOf(map.get("lasteditdate")));
					
					organizationList.add(info);

            	}
            } catch (Exception e) {
            	logger.info("Error"+e.getMessage());
                throw new ServiceException(e.getMessage(), e);
            }

		
		}

		logger.info("Returing table query List : " + organizationList);

		System.out.println("Device List:"+organizationList);

		return organizationList;
		
	}



	
	

	
	@Override
	public ArrayList<TableVO> ListOfGlobalAdminDevices( String actionuser)
			throws ServiceException {
		// TODO Auto-generated method stub

		
		List<HashMap<String, Object>> resultSet = null;
		List<String> values = new ArrayList<String>();
		values.add(actionuser);
		
		
		ArrayList<TableVO> organizationList = new ArrayList<TableVO>();
		
		try {
							resultSet = storedProcJdbcDao.callStoredProcForMap(Constants.Get_All_Devices_ForAll_Customer, values);
			
		} catch (StoredProcException e) 
		{
			throw new ServiceException(e.getMessage(), e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (resultSet != null) {
			
			
				try
				{
            	
            	for (Iterator<HashMap<String, Object>> iterator = resultSet
						.iterator(); iterator.hasNext();) {
					HashMap<?, ?> map = iterator.next();
				

					
					TableVO info = new TableVO();
					
					//dId,macid,registrationdate,supplier,warrenty,createby,createdate,lasteditby,lasteditdate,devicemodel,customerid,resturantid,devicestatus
			
					info.setOrganizationame(String.valueOf(map.get("organizationame")));
					
					info.setOrgId(String.valueOf(map.get("resturantid")));
			
					info.setDevicemac(String.valueOf(map.get("macid")));
					info.setDevicemodel(String.valueOf(map.get("devicemodel")));
					info.setDevicesupplier(String.valueOf(map.get("supplier")));
					info.setRegistrationdate(String.valueOf(map.get("registrationdate")));
					info.setWarrenty(String.valueOf(map.get("warrenty")));
					
					
					info.setDeviceId(String.valueOf(map.get("dId")));
					info.setDevicerowid(String.valueOf(map.get("dId")));
					
					info.setCustomerId(String.valueOf(map.get("customerid")));
					info.setCreatedby(String.valueOf(map.get("createdby")));
					info.setCreatedate(String.valueOf(map.get("createdate")));
					info.setLasteditby(String.valueOf(map.get("lasteditby")));
					info.setLasteditdate(String.valueOf(map.get("lasteditdate")));
					
					organizationList.add(info);

            	}
            } catch (Exception e) {
            	logger.info("Error"+e.getMessage());
                throw new ServiceException(e.getMessage(), e);
            }

		
		}

		logger.info("Returing table query List : " + organizationList);

		System.out.println("Device List:"+organizationList);

		return organizationList;
		
	}


	
	///////////////////////////////////////////////////Table Management ///////////////////////////////
	
	
	@Override
	public String createTable(TableVO tableVO, String customerId,
			String actionuser) throws ServiceException {
		// TODO Auto-generated method stub
		
	List<String> values = new ArrayList<String>();
		
		values.add(tableVO.getOrgId());
		values.add(tableVO.getTablename());
		values.add(tableVO.getCapacity());
		values.add(customerId.trim());
		values.add(actionuser);
		
		
		String res=null;
		try {
			  	res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Create_TAble, values);
			  	
			  	System.out.println("create customer result"+res);
			  	
			  	logger.info("Table Created Successfully by:"+actionuser +"with customer ID:"+customerId);

		} catch (StoredProcException e) {
			
			System.out.println("error while create customer"+ e.getMessage());
			
			
			throw new ServiceException(e.getMessage(), e);
		}

		return res;

		
		
	}


	
	

	
	@Override
	public String UpdateTable(TableVO tableVO, String customerId,
			String actionuser) throws ServiceException {
		// TODO Auto-generated method stub
		
	List<String> values = new ArrayList<String>();
		
		values.add(tableVO.getTableno());
		values.add(tableVO.getOrgId());
		values.add(tableVO.getTablename());
		values.add(tableVO.getCapacity());
		values.add(customerId);
		values.add(actionuser);
		
		
		String res=null;
		try {
			  	res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Update_Table, values);
			  	
			  	System.out.println("create customer result"+res);
			  	
			  	logger.info("Table Created Successfully by:"+actionuser +"with customer ID:"+customerId);

		} catch (StoredProcException e) {
			
			System.out.println("error while create customer"+ e.getMessage());
			
			
			throw new ServiceException(e.getMessage(), e);
		}

		return res;

		
		
	}


	@Override
	public String createDevices(TableVO tableVO, String customerId,
			String actionuser) throws ServiceException {
		// TODO Auto-generated method stub
		
	List<String> values = new ArrayList<String>();
		
		values.add(tableVO.getOrgId());
		values.add(tableVO.getDevicemac());
		values.add(tableVO.getDevicemodel());
		values.add(tableVO.getDevicesupplier());
		values.add(tableVO.getWarrenty());
		values.add(customerId.trim());
		values.add(actionuser);
		
		
		String res=null;
		try {
			  	res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Create_Devices, values);
			  	
			  	System.out.println("create customer result"+res);
			  	
			  	logger.info("Device Created Successfully by:"+actionuser +"with customer ID:"+customerId);

		} catch (StoredProcException e) {
			
			System.out.println("error while create customer"+ e.getMessage());
			
			
			throw new ServiceException(e.getMessage(), e);
		}

		return res;

		
		
	}


	

	@Override
	public String ManageDeviceOperationForTable(TableVO tableVO, String customerId,
			String actionuser) throws ServiceException {
		// TODO Auto-generated method stub
		
	List<String> values = new ArrayList<String>();
		
		values.add(tableVO.getTableno());
		values.add(tableVO.getDevicemac());
		values.add(customerId);
		values.add(actionuser);
		
		
		String res=null;
		try {
			  	res=storedProcJdbcDao.callStoredProcForListInsertUpdate(Constants.Update_DeviceAssignement, values);
			  	
			  	System.out.println("create customer result"+res);
			  	
			  	logger.info("Device Created Successfully by:"+actionuser +"with customer ID:"+customerId);

		} catch (StoredProcException e) {
			
			System.out.println("error while create customer"+ e.getMessage());
			
			
			throw new ServiceException(e.getMessage(), e);
		}

		return res;

		
		
	}

	
	
	
	@Override
	public String GetRandomNumber() throws ServiceException {
		// TODO Auto-generated method stub

		String randomnumber=Null;
		
		Ranumber.GetRanOne();
		Ranumber.GetRanTwo();
		Ranumber.GetRanThree();
		Ranumber.GetRanFour();
				
		return (Ranumber.GetRanOne()+Ranumber.GetRanTwo()+Ranumber.GetRanThree()+Ranumber.GetRanFour());
	}
	
		


}


