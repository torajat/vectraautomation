package com.mlab.endpoint.util;

import java.util.HashMap;
import java.util.Map;

import org.owasp.esapi.ESAPI;
import org.springframework.ui.Model;

public class Menumodel {
	
	

	
public Model getConfigMenuItem( Model model)

{
		
	model.addAttribute(ESAPI.encoder().encodeForHTML("createsiteservers"),ESAPI.encoder().encodeForHTML("Create Site Server"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("createsiteserversaction"),ESAPI.encoder().encodeForHTML("/global/siteserver/createsiteserver"));
	

	model.addAttribute(ESAPI.encoder().encodeForHTML("editsiteservers"),ESAPI.encoder().encodeForHTML("List of Site Server"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("editsiteserversaction"),ESAPI.encoder().encodeForHTML("/global/siteserver/editsiteserver"));



	model.addAttribute(ESAPI.encoder().encodeForHTML("customerlist"),ESAPI.encoder().encodeForHTML("/global/customerlist"));
	
	
	model.addAttribute(ESAPI.encoder().encodeForHTML("createcustomer"),ESAPI.encoder().encodeForHTML("Create Customer"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("createcustomeraction"),ESAPI.encoder().encodeForHTML("/global/customer/createcustomer"));



	model.addAttribute(ESAPI.encoder().encodeForHTML("editcustomer"),ESAPI.encoder().encodeForHTML("Edit Customer"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("editcustomeraction"),ESAPI.encoder().encodeForHTML("/global/customer/listofcustomer"));


	model.addAttribute(ESAPI.encoder().encodeForHTML("createadmin"),ESAPI.encoder().encodeForHTML("Create Customer Admin"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("createadminaction"),ESAPI.encoder().encodeForHTML("/global/customer/createadmin"));

	model.addAttribute(ESAPI.encoder().encodeForHTML("editadmin"),ESAPI.encoder().encodeForHTML("Edit Customer Admin"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("editadminaction"),ESAPI.encoder().encodeForHTML("/global/customer/editadmin"));

	model.addAttribute(ESAPI.encoder().encodeForHTML("resetadminpass"),ESAPI.encoder().encodeForHTML("Reset Password"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("resetadminpassaction"),ESAPI.encoder().encodeForHTML("/global/customer/resetpassword"));

	model.addAttribute(ESAPI.encoder().encodeForHTML("organization"),ESAPI.encoder().encodeForHTML("List of Restaurants"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("organizationaction"),ESAPI.encoder().encodeForHTML("/global/restaurantsList"));

	
	model.addAttribute(ESAPI.encoder().encodeForHTML("createorganization"),ESAPI.encoder().encodeForHTML("Create Restaurants"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("createorganizationaction"),ESAPI.encoder().encodeForHTML("/global/manage/createorganization"));
	
	
	model.addAttribute(ESAPI.encoder().encodeForHTML("managerestaurants"),ESAPI.encoder().encodeForHTML("My Restaurant"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("managerestaurantsaction"),ESAPI.encoder().encodeForHTML("/restaurants/myrestaurants/list"));
	


	model.addAttribute(ESAPI.encoder().encodeForHTML("managerestaurantstable"),ESAPI.encoder().encodeForHTML("Table"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("managerestaurantstableaction"),ESAPI.encoder().encodeForHTML("/restaurants/myrestaurants/createtable"));


	model.addAttribute(ESAPI.encoder().encodeForHTML("tablelist"),ESAPI.encoder().encodeForHTML("List of Table"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("tablelistaction"),ESAPI.encoder().encodeForHTML("/restaurants/myrestaurants/tablellist"));

	
	

	model.addAttribute(ESAPI.encoder().encodeForHTML("managerestaurantsconfig"),ESAPI.encoder().encodeForHTML("Configuration"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("managerestaurantsconfigaction"),ESAPI.encoder().encodeForHTML("/restaurants/myrestaurants/configuration"));


	model.addAttribute(ESAPI.encoder().encodeForHTML("devices"),ESAPI.encoder().encodeForHTML("Devices"));

	model.addAttribute(ESAPI.encoder().encodeForHTML("devicesaction"),ESAPI.encoder().encodeForHTML("/restaurants/myrestaurants/devices"));

	
	model.addAttribute(ESAPI.encoder().encodeForHTML("deviceslistmenu"),ESAPI.encoder().encodeForHTML("Device List"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("deviceslistmenuaction"),ESAPI.encoder().encodeForHTML("/restaurants/myrestaurants/mydeviceslist"));

	
	
	model.addAttribute(ESAPI.encoder().encodeForHTML("pendingorder"),ESAPI.encoder().encodeForHTML("Today's Pending Order"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("pendingorderaction"),ESAPI.encoder().encodeForHTML("/restaurants/orderdetailslist"));

	model.addAttribute(ESAPI.encoder().encodeForHTML("rulelistaction"),ESAPI.encoder().encodeForHTML("/global/rule/list"));

	model.addAttribute(ESAPI.encoder().encodeForHTML("menurulelist"),ESAPI.encoder().encodeForHTML("Edit Rule"));

	
	
	model.addAttribute(ESAPI.encoder().encodeForHTML("createrule"),ESAPI.encoder().encodeForHTML("Create Rule"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("createruleaction"),ESAPI.encoder().encodeForHTML("/global/rule/create"));



	model.addAttribute(ESAPI.encoder().encodeForHTML("editrule"),ESAPI.encoder().encodeForHTML("Edit Rule"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("editruleaction"),ESAPI.encoder().encodeForHTML("/global/rule/edit"));
	
	

	model.addAttribute(ESAPI.encoder().encodeForHTML("createrulemap"),ESAPI.encoder().encodeForHTML("Create Rule mapping"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("createrulemapaction"),ESAPI.encoder().encodeForHTML("/global/rule/map/create"));

	model.addAttribute(ESAPI.encoder().encodeForHTML("editrulemap"),ESAPI.encoder().encodeForHTML("Edit Rule mapping"));
	model.addAttribute(ESAPI.encoder().encodeForHTML("editrulemapaction"),ESAPI.encoder().encodeForHTML("/global/rule/map/edit"));

	
	
	
	
	return model;
	
}





public  Map<String,String> getStatusList()
{
	

Map<String,String> statusList=new HashMap<>();

statusList.put("Active","Active");
statusList.put("Inactive", "Inactive");


return statusList;
}



}
