package com.mlab.endpoint.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class Constants {
	
	public static final String create_user="{call create_user(?,?,?,?,?,?,?,?)}";
	public static final String Update_User_Info="{call Update_User_Info(?,?,?,?,?,?,?)}";
	public static final String Update_User_Type="{call Update_User_Type(?,?,?,?)}";

	/*public static final String Update_User_Password="{call Update_User_Password(?,?,?,?,?)}";
	*/
	public static final String get_role="{call get_role()}";
	public static final String get_password="{call get_password(?)}";
	public static final String get_user_by_name="{call get_user_by_name(?)}";
	public static final String validate_user_login="{call validate_user_login(?,?)}";
	public static final String reset_password="{call reset_password(?,?)}";
	public static final String get_All_users="{call get_AllUsers()}";  
	
	public static final String get_All_Customar_Admin_Users="{call get_All_Customar_Admin_Users()}";  
	
	

	public static final String Create_CustomerAdmin_user="{call Create_CustomerAdmin_user(?,?,?,?,?,?,?,?,?)}";
	public static final String Update_CustomerAdmin_user="{call Update_CustomerAdminUser(?,?,?,?,?,?,?,?,?,?)}";
	
	
	
	
	//##############################
	
	public static final String Get_ProductCategory="{call get_productcategory(?)}";
	public static final String Create_ProductCategory="{call CreateCategory(?,?,?,?,?)}";
	public static final String Create_Product="{call CreateProduct(?,?,?,?,?,?,?,?,?,?,?)}";
	
	public static final String Get_ProductList="{call get_productlist(?)}";
	
	public static final String UpdateStockIn_Product="{call UpdateStockIn_Product(?,?,?,?,?,?)}";
	public static final String Customer_By_Mobile_ID="{call get_CustomerbyMobile_ID(?,?,?)}";
	public static final String Get_LIST_OF_CUSTOMER="{call get_CustomerList(?)}";
	public static final String Create_Customer="{call Create_Customer(?,?,?,?,?,?,?,?)}";
	
	public static final String Upload_Picture="{call Upload_productpicture(?,?,?,?,?,?,?)}";
	
	public static final String Get_LIST_OF_ProductImage="{call Get_ProductimageByProductId(?,?)}";
	public static final String Get_DayLanuchMenu="{call get_productlist(?)}";
	
	public static final String Update_Customer="{call Update_Customer(?,?,?,?,?,?,?,?)}";
	
	public static final String Get_All_OrganizationList="{call get_All_OrganizationList(?)}";
	
	public static final String Get_List_All_Location_Details="{call get_All_Locations_Name(?)}";
	
	
	public static final String Create_Organization="{call Create_OrganizationName(?,?,?,?,?,?)}";
	
	public static final String Update_Organization="{call Update_OrganizationName(?,?,?,?,?,?,?)}";
	
	public static final String Update_Organization_WithoutName="{call Update_OrganizationName_WithoutName(?,?,?,?,?,?,?)}";

	public static final String Get_All_CustomerOrganizationList="{call get_All_Customer_OrganizationListBy_CustomerId(?,?)}";
	
	public static final String Create_TAble="{call Create_Table(?,?,?,?,?)}";
	
	public static final String Create_Devices="{call Create_Devices(?,?,?,?,?,?,?)}";
	
	
	

	public static final String Get_All_Table_By_CustomerIdWithResturantID="{call get_All_Table_By_CustomerId(?,?,?)}";
		
	public static final String Get_All_Table_By_CustomerIdWithoutresturantid="{call get_All_Table_By_CustomerIdWithoutResturantID(?,?,?)}";
	
	
	public static final String Get_All_Devices_By_CustomerIdWithResturantID="{call Get_All_Devices_WithRestaurantId(?,?,?)}";
	public static final String Get_All_Devicees_By_CustomerIdWithoutresturantid="{call Get_All_Devices_withoutRestaurantId(?,?,?)}";
	
	public static final String Update_ProductByFieldValue="{call Update_ProductByFieldNameVlaue(?,?,?,?,?)}";
	

	public static final String Update_Product_UnitPrice="{call Update_Product_UnitPrice(?,?,?,?,?)}";
	
	public static final String Update_Product_Description="{call Update_Product_Description(?,?,?,?,?)}";
	
	public static final String Update_Product_Name="{call Update_Product_Name(?,?,?,?,?)}";
	
	public static final String Update_Product_Code="{call Update_Product_Code(?,?,?,?,?)}";
	
	public static final String Delet_Product="{call Delete_Product(?,?,?,?,?)}";
	
	public static final String Delete_Image="{call Delete_Image(?,?,?,?)}";
	
	public static final String Update_DeviceAssignement="{call Update_DeviceAssignement(?,?,?,?)}";
	
	public static final String Update_Table="{call Update_Table(?,?,?,?,?,?)}";

	public static final String Get_ProductListWithImage="{call get_productlist(?)}";
	
	public static final String Get_OrderDetails="{call Get_OrderDetails(?)}";
	
	public static final String Update_OrderStatus="{call UpdateOrderStatus(?,?,?,?,?,?)}";
	
	public static final String Get_OrderDetailsByFlg="{call Get_OrderDetailsByFlg(?,?)}";
	
	
	public static final String Get_All_ResturantUsers="{call GetAllResturantUsersList(?)}";  

	public static final String Get_Analytics="{call Get_OrderDetails_Analytics(?)}";
	
	

	public static final String Get_All_Devices_ForAll_Customer="{call Get_All_DeviceList_All_Customer(?)}";
	
	public static final String get_All_OrganizationListByLocationId="{call get_All_OrganizationListByLocationId(?,?)}";
	
	
	public static final String Get_All_BookingDetails="{call Get_All_BookingDetails(?)}";
	
	public static final String Create_Booking="{call Create_Booking(?,?,?,?,?,?,?,?)}";
	
	
	/////////Manage Vetra project

	public static final String Get_InstanceList="{call get_All_Instance_Id()}";
	public static final String Get_All_Customer_List="{call get_All_Customer_Information()}";
	
	public static final String get_instance_HostName="{call get_instance_HostName(?)}";

	public static final String get_All_Customer_InformationWebUri="{call get_All_Customer_InformationWebUri()}";
	
	
	public static final String Update_Instance_Status="{call Update_Instance_Status(?,?,?,?,?)}";
	public static final String CreateJobQue="{call CreateJobQue(?,?,?,?,?,?,?)}";
	
	public static final String InsertJobList="{call InsertJobList(?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	
	public static final String get_All_Jobs="{call get_All_Jobs()}";
	
	public static final String get_All_Jobs_Expand="{call get_All_Jobs_Expand()}";
	
	public static final String get_All_Collection_Jobs="{call get_All_Collection_Jobs()}";
	
	
	
	public static final String get_All_Jobs_Instance_ready="{call get_All_Jobs_Allocation(?)}";
	
	
	public static final String UpdateStatus_Que="{call Update_Job_QueStatus(?,?,?,?,?)}";
	

	public static final String UpdateJobListStatusQue="{call UpdateJobListStatusQue(?,?,?,?)}";
	
	
	public static final String Update_Job_OperationStatus="{call Update_Job_OperationStatus(?,?,?,?,?)}";
	

	public static final String Update_Job_Final="{call Update_Job_Final(?,?,?,?,?)}";
	
	
	
	public static final String Get_AllJobsForCopy="{call get_All_CopyContent_Jobs()}";
	
	public static final String Get_All_Monitoring_Job="{call Get_All_Monitoring_Job(?)}";
	public static final String Get_All_collectionJobLisStatus="{call Get_All_collectionJobLisStatus(?,?,?)}";
	
	
	
	
	
	
	public static final String get_All_WaitingForCompute="{call get_All_WaitingForCompute()}";
	
	public static final String Get_Job_Details_By_Job_Id="{call Get_Job_Details_By_Job_Id(?,?,?,?)}";

	public static final String Update_Job_Status="{call Update_Job_Status(?,?,?,?,?,?)}";
	
	public static final String Update_Job_Copy_Status="{call Update_Job_Copy_Status(?,?,?,?)}";
	
	public static final String Get_All_InstanceList="{call get_All_Instance_InWebUi()}";
	
	public static final String EditInstanceList="{call Edit_Instance(?,?,?,?)}";
	
	public static final String AddInstance="{call Create_Instance(?,?,?)}";
	
	public static final String AddCustomer="{call Create_Customer(?,?,?)}";

	public static final String Edit_Customer="{call Edit_Customer(?,?,?,?)}";
	
	public static final String Get_All_Users="{call Get_AllUsers_Information()}";  

	public static final String Update_User_Password="{call Update_User_Password(?,?,?,?)}";
	
	public static final String Get_Dashboard_JobList="{call Get_AllDashBoardJobList()}";

	public static final String Get_Dashboard_JobListWithCompanyName="{call Get_AllDashBoardJobListWithCompanyName(?)}";

	
	public static final String PreparedJobListQue="{call Create_JobListExpandQue(?,?,?,?)}";
	
	public static final String updatejobquevalue="{call Update_Job_QueExpand(?,?)}";
	
	public static final String Get_All_Running_Job="{call Get_All_Running_Job()}";
	
	
	public static final String Update_ada_alg="{call UpdateTime_ADA_alg(?,?)}";
	
	
	public static final String Update_Unit_alg="{call UpdateTime_unit_alg(?,?)}";


	public static final String Update_core_alg="{call UpdateTime_core_alg(?,?)}";

	public static final String Update_Auto2D_alg="{call UpdateTime_Auto2dServer(?,?)}";

	public static final String Update_NullProce_alg="{call UpdateTime_nullproc(?,?)}";

	
	//public static final String SCHEDULING_START_COMPLETION_FILE = "Transfer_Complete - Start Compute Sequence.txt";
	public static final String SCHEDULING_START_COMPLETION_FILE = "Transfer_Complete - Start Compute Sequence.txt";
	public static final String SCHEDULING_RUN_COMPLETION_FILE = "Transfer_Complete - Start Compute Sequence - Running.txt";
	public static final String SCHEDULING_FINISHED_COMPLETION_FILE  = "Transfer_Complete - Start Compute Sequence - Finished.txt";
	public static final String SCHEDULING_COPY_COMPLETION_FILE  = "Transfer_Complete - Copy Completed.txt";
	public static final String Update_Job_ManuallyStop="{call Update_Job_ManuallyStop(?,?)}";
	public static final String Transfer_Job_Manually="{call Transfer_Job_Manually(?,?,?,?,?,?,?)}";
	
	
	//Transfer_Complete - Start Compute Sequence - Running
	
	

	
	public static String GetElispTime(String recevieddate)
	{
		

		if("".equals(recevieddate)){return "";}
		
		if("null".equals(recevieddate)){return "";}
		
		
		Date now = new Date();
		
		SimpleDateFormat currentdf = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss");
		String currentdateString=currentdf.format(now);
		
		//System.out.println("MyCurrent datetime-->"+currentdateString);
		
		String fm_recevieddate = recevieddate;
		
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		
		String finalrecevieddate = null;
		try {
			finalrecevieddate = sdf2.format(sdf1.parse(fm_recevieddate));
			//System.out.println(finalrecevieddate); //will be 30/06/2007

		
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		String result="";
		String dateStart = finalrecevieddate;
		String dateStop = currentdateString;

		//HH converts hour in 24 hours format (0-23), day calculation
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		Date d1 = null;
		Date d2 = null;

		try {
			d1 = format.parse(dateStart);
			d2 = format.parse(dateStop);
			
			
			//in milliseconds
			long diff = d2.getTime() - d1.getTime();

			long diffSeconds = diff / 1000 % 60;
			long diffMinutes = diff / (60 * 1000) % 60;
			long diffHours = diff / (60 * 60 * 1000) % 24;
			long diffDays = diff / (24 * 60 * 60 * 1000);
		
			long diffMonths = diffDays / 30;
			long diffYears=diffMonths/12;
			
		result=diffHours + " hrs " + diffMinutes + " Min ";
			
		
		
		//	System.out.print(diffDays + " days, ");
			//System.out.print(diffHours + " hours, ");
			//System.out.print(diffMinutes + " minutes, ");
			//System.out.print(diffSeconds + " seconds.");

//			result=getDateDifferenceInDDMMYYYY(d1, d2);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
/*
		//datetime management.............................
		
		DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		
		LocalDate ldt = LocalDate.parse(recevieddate, dateformatter);
		
		LocalDate today = LocalDate.now();
		//LocalDate birthday = LocalDate.of(1960, Month.JANUARY, 1);
		
		LocalDate birthday =ldt ;//LocalDate.of(ldt);

		Period p = Period.between(birthday, today);
		long p2 = ChronoUnit.DAYS.between(birthday, today);
		
		 result="You are " + p.getYears() + " years, " + p.getMonths() + " months, and " + p.getDays() + " days old. (" + p2 + " days total)";
		*/
		return result;
	}
	
	



	public static String GetElispTimeNotification(String recevieddate)
	{
		

		if("".equals(recevieddate)){return "";}
		
		if("null".equals(recevieddate)){return "";}
		
		
		Date now = new Date();
		
		SimpleDateFormat currentdf = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss");
		String currentdateString=currentdf.format(now);
		
		//System.out.println("MyCurrent datetime-->"+currentdateString);
		
		String fm_recevieddate = recevieddate;
		
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		
		String finalrecevieddate = null;
		try {
			finalrecevieddate = sdf2.format(sdf1.parse(fm_recevieddate));
			//System.out.println(finalrecevieddate); //will be 30/06/2007

		
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		String result="";
		String dateStart = finalrecevieddate;
		String dateStop = currentdateString;

		//HH converts hour in 24 hours format (0-23), day calculation
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		Date d1 = null;
		Date d2 = null;

		try {
			d1 = format.parse(dateStart);
			d2 = format.parse(dateStop);
			
			
			//in milliseconds
			long diff = d2.getTime() - d1.getTime();

			long diffSeconds = diff / 1000 % 60;
			long diffMinutes = diff / (60 * 1000) % 60;
			long diffHours = diff / (60 * 60 * 1000) % 24;
			long diffDays = diff / (24 * 60 * 60 * 1000);
		
			long diffMonths = diffDays / 30;
			long diffYears=diffMonths/12;
			
		result=diffHours + "." + diffMinutes;
			
		
		
		//	System.out.print(diffDays + " days, ");
			//System.out.print(diffHours + " hours, ");
			//System.out.print(diffMinutes + " minutes, ");
			//System.out.print(diffSeconds + " seconds.");

//			result=getDateDifferenceInDDMMYYYY(d1, d2);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
/*
		//datetime management.............................
		
		DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		
		LocalDate ldt = LocalDate.parse(recevieddate, dateformatter);
		
		LocalDate today = LocalDate.now();
		//LocalDate birthday = LocalDate.of(1960, Month.JANUARY, 1);
		
		LocalDate birthday =ldt ;//LocalDate.of(ldt);

		Period p = Period.between(birthday, today);
		long p2 = ChronoUnit.DAYS.between(birthday, today);
		
		 result="You are " + p.getYears() + " years, " + p.getMonths() + " months, and " + p.getDays() + " days old. (" + p2 + " days total)";
		*/
		return result;
	}
	
	

	

	public static String GetElispeTwoDare(String recevieddate, String enddate)
	{
		

		if("".equals(recevieddate)){return "";}
		
		if("null".equals(recevieddate)){return "";}
		
		
		Date now = new Date();
		
		SimpleDateFormat currentdf = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss");
		String currentdateString=currentdf.format(now);
		
		
		
		String fm_recevieddate = recevieddate;
		
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		
		String finalrecevieddate = null;
		try {
			finalrecevieddate = sdf2.format(sdf1.parse(fm_recevieddate));
			currentdateString = sdf2.format(sdf1.parse(enddate));
			
			
			//System.out.println(finalrecevieddate); //will be 30/06/2007

		
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		String result="";
		String dateStart = finalrecevieddate;
		String dateStop = currentdateString;

		//HH converts hour in 24 hours format (0-23), day calculation
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		Date d1 = null;
		Date d2 = null;

		try {
			d1 = format.parse(dateStart);
			d2 = format.parse(dateStop);
			
			
			//in milliseconds
			long diff = d2.getTime() - d1.getTime();

			long diffSeconds = diff / 1000 % 60;
			long diffMinutes = diff / (60 * 1000) % 60;
			long diffHours = diff / (60 * 60 * 1000) % 24;
			long diffDays = diff / (24 * 60 * 60 * 1000);
		
			long diffMonths = diffDays / 30;
			long diffYears=diffMonths/12;
			
		result=diffHours + " hrs " + diffMinutes + " Min ";
			
		
		
		//	System.out.print(diffDays + " days, ");
			//System.out.print(diffHours + " hours, ");
			//System.out.print(diffMinutes + " minutes, ");
			//System.out.print(diffSeconds + " seconds.");

//			result=getDateDifferenceInDDMMYYYY(d1, d2);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
/*
		//datetime management.............................
		
		DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		
		LocalDate ldt = LocalDate.parse(recevieddate, dateformatter);
		
		LocalDate today = LocalDate.now();
		//LocalDate birthday = LocalDate.of(1960, Month.JANUARY, 1);
		
		LocalDate birthday =ldt ;//LocalDate.of(ldt);

		Period p = Period.between(birthday, today);
		long p2 = ChronoUnit.DAYS.between(birthday, today);
		
		 result="You are " + p.getYears() + " years, " + p.getMonths() + " months, and " + p.getDays() + " days old. (" + p2 + " days total)";
		*/
		return result;
	}
	
	

	

	public static String GetElispeTwoDateFormNotifcation(String recevieddate, String enddate)
	{
		

		if("".equals(recevieddate)){return "";}
		
		if("null".equals(recevieddate)){return "";}
		
		
		Date now = new Date();
		
		SimpleDateFormat currentdf = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss");
		String currentdateString=currentdf.format(now);
		
		
		
		String fm_recevieddate = recevieddate;
		
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		
		String finalrecevieddate = null;
		try {
			finalrecevieddate = sdf2.format(sdf1.parse(fm_recevieddate));
			currentdateString = sdf2.format(sdf1.parse(enddate));
			
			
			//System.out.println(finalrecevieddate); //will be 30/06/2007

		
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		String result="";
		String dateStart = finalrecevieddate;
		String dateStop = currentdateString;

		//HH converts hour in 24 hours format (0-23), day calculation
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		Date d1 = null;
		Date d2 = null;

		try {
			d1 = format.parse(dateStart);
			d2 = format.parse(dateStop);
			
			
			//in milliseconds
			long diff = d2.getTime() - d1.getTime();

			long diffSeconds = diff / 1000 % 60;
			long diffMinutes = diff / (60 * 1000) % 60;
			long diffHours = diff / (60 * 60 * 1000) % 24;
			long diffDays = diff / (24 * 60 * 60 * 1000);
		
			long diffMonths = diffDays / 30;
			long diffYears=diffMonths/12;
			
		result=diffHours +"."+diffMinutes;
			
		
		
		//	System.out.print(diffDays + " days, ");
			//System.out.print(diffHours + " hours, ");
			//System.out.print(diffMinutes + " minutes, ");
			//System.out.print(diffSeconds + " seconds.");

//			result=getDateDifferenceInDDMMYYYY(d1, d2);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
/*
		//datetime management.............................
		
		DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		
		LocalDate ldt = LocalDate.parse(recevieddate, dateformatter);
		
		LocalDate today = LocalDate.now();
		//LocalDate birthday = LocalDate.of(1960, Month.JANUARY, 1);
		
		LocalDate birthday =ldt ;//LocalDate.of(ldt);

		Period p = Period.between(birthday, today);
		long p2 = ChronoUnit.DAYS.between(birthday, today);
		
		 result="You are " + p.getYears() + " years, " + p.getMonths() + " months, and " + p.getDays() + " days old. (" + p2 + " days total)";
		*/
		return result;
	}
	

	
	public static String Get_Elisp_Hours(String recevieddate)
	{
		

		if("".equals(recevieddate)){return "";}
		
		if("null".equals(recevieddate)){return "";}
		
		
		Date now = new Date();
		
		SimpleDateFormat currentdf = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss");
		String currentdateString=currentdf.format(now);
		
		
		
		String fm_recevieddate = recevieddate;
		
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		
		String finalrecevieddate = null;
		try {
			finalrecevieddate = sdf2.format(sdf1.parse(fm_recevieddate));
			//System.out.println(finalrecevieddate); //will be 30/06/2007

		
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		String result="";
		String dateStart = finalrecevieddate;
		String dateStop = currentdateString;

		//HH converts hour in 24 hours format (0-23), day calculation
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		Date d1 = null;
		Date d2 = null;

		try {
			
			//System.out.println("Afert zero start date-->"+dateStart);
			d1 = format.parse(dateStart);
			d2 = format.parse(dateStop);
			
			
			//in milliseconds
			long diff = d2.getTime() - d1.getTime();

			long diffSeconds = diff / 1000 % 60;
			long diffMinutes = diff / (60 * 1000) % 60;
			long diffHours = diff / (60 * 60 * 1000) % 24;
			long diffDays = diff / (24 * 60 * 60 * 1000);
		
			long diffMonths = diffDays / 30;
			long diffYears=diffMonths/12;
			
		result=String.valueOf(diffHours);
			
		
		
		//	System.out.print(diffDays + " days, ");
			//System.out.print(diffHours + " hours, ");
			//System.out.print(diffMinutes + " minutes, ");
			//System.out.print(diffSeconds + " seconds.");

//			result=getDateDifferenceInDDMMYYYY(d1, d2);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
/*
		//datetime management.............................
		
		DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		
		LocalDate ldt = LocalDate.parse(recevieddate, dateformatter);
		
		LocalDate today = LocalDate.now();
		//LocalDate birthday = LocalDate.of(1960, Month.JANUARY, 1);
		
		LocalDate birthday =ldt ;//LocalDate.of(ldt);

		Period p = Period.between(birthday, today);
		long p2 = ChronoUnit.DAYS.between(birthday, today);
		
		 result="You are " + p.getYears() + " years, " + p.getMonths() + " months, and " + p.getDays() + " days old. (" + p2 + " days total)";
		*/
		return result;
	}


}
