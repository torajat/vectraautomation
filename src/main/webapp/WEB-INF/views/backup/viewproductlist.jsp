<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<meta charset="utf-8">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page session="false"%>
<html>
<head>
<title><spring:message code="title.app"/></title>

<%@ include file="css.jsp"%>

</head>
<%@ include file="headerAndSideManu.jsp"%>


        <div class="wrapper">
            <div class="container">

               
                <!-- Page-Title -->
                <h3 class="text-success counter">${title}</h3>
                 	          <%--  <a href="${pageContext.request.contextPath}/product/createproductcategory.do" class="btn btn-primary waves-effect waves-light m-t-10">
					               Create Category
					              </a>  
					     --%>
  				
                <div class="row">
           <%--     
                           <img src="${pageContext.request.contextPath}/resources/theme/productimage/1.jpg" alt=""/>
       <img src="${pageContext.request.contextPath}/resources/theme/productimage/8ff0c46f-50e7-4c29-baff-38e31b6a7bbe2.jpg" alt=""/>
  
   --%>
  
  
                    <div class="col-sm-12">
                    
  
                        <div class="card-box">
                        
                        <div class="box-body">
			<div class="form-group">
			
			<c:if test="${not empty msg}">
		<div class="alert alert-${css} alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" 
                                aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<strong>${msg}</strong>
		</div>
		</c:if>
		</div>
		</div>
                        
                        
                       <table id="productcategory" class="table table-bordered table-hover">
								<thead >
									<tr>
										<th>Product Code</th>
										<th>Product Name</th>
										<th>Description</th>
										<th>Unit Price</th>
										<th>Action</th>
										
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${productlist}" var="report" varStatus="vs">
										<tr>
												<td>


												
					<button type="button" class="btn btn-outline btn-link"  data-animation="fadein" data-toggle="modal" data-target="#myModalCode${vs.index}" id="viewDetailButton${vs.index}">  ${report.productCode}</button>
                  <!-- Modal -->
                  <div class="modal fade" id="myModalCode${vs.index}" role="dialog">
                    <div class="modal-dialog modal-full" >

                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Product Code</h4>
                        </div>
                        <div class="modal-body">
                    
                     <form:form action="${pageContext.request.contextPath}${codeaction}"  method="POST"	commandName="${modelVOval}">
                      			
                      			<div class="form-group">
									    <label for="product code">Product Code</label>
									    <form:input type="text" class="form-control" path="productDescription" value="${report.productCode}"></form:input>
									</div>
									
                      			 
                      			 <form:hidden path="productNo" value="${report.productNo}" />
                      			 
                      			 <form:hidden path="customerid" value="${report.customerid}" />
                      			 
                      			 
							 <button  type="submit" class="btn btn-primary btn-sm"  value="Validate">Update</button>
							
                      		</form:form>
                    
                        </div>
                   
                      </div>

                    </div>
                  </div>
												


												
												
												
												</td>
												
												<td>
												
					<button type="button" class="btn btn-outline btn-link"  data-animation="fadein" data-toggle="modal" data-target="#myModalname${vs.index}" id="viewDetailButton${vs.index}"> ${report.productname} </button>
                  <!-- Modal -->
                  <div class="modal fade" id="myModalname${vs.index}" role="dialog">
                    <div class="modal-dialog modal-full" >

                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Product Name</h4>
                        </div>
                        <div class="modal-body">
                    
                     <form:form action="${pageContext.request.contextPath}${namaction}"  method="POST"	commandName="${modelVOval}">
                      			
                      			<div class="form-group">
									    <label for="product code">Product Name</label>
									    <form:input type="text" class="form-control" path="productname" value="${report.productname}"></form:input>
									</div>
								
								 <form:hidden path="productNo" value="${report.productNo}" />
                      			 <form:hidden path="customerid" value="${report.customerid}" />
                      			 
                      			 
							 <button  type="submit" class="btn btn-primary btn-sm"  value="Validate">Update</button>
							
                      		</form:form>
                    
                        </div>
                   
                      </div>

                    </div>
                  </div>
												
												
												
												</td>
												
												<td>
												
												
												
					<button type="button" class="btn btn-outline btn-link"  data-animation="fadein" data-toggle="modal" data-target="#myModalDescription${vs.index}" id="viewDetailButton${vs.index}">${report.productDescription} </button>
                  <!-- Modal -->
                  <div class="modal fade" id="myModalDescription${vs.index}" role="dialog">
                    <div class="modal-dialog modal-full" >

                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Description</h4>
                        </div>
                        <div class="modal-body">
                    
                     <form:form action="${pageContext.request.contextPath}${descriptionaction}"  method="POST"	commandName="${modelVOval}">
                      			
                      			<div class="form-group">
									    <label for="product code">Description</label>
									    <form:input type="text" class="form-control" path="productDescription" value="${report.productDescription}"></form:input>
									</div>
									
                      			 
                      			 <form:hidden path="productNo" value="${report.productNo}" />
                      			 
                      			 <form:hidden path="customerid" value="${report.customerid}" />
                      			 
                      			 
							 <button  type="submit" class="btn btn-primary btn-sm"  value="Validate">Update</button>
							
                      		</form:form>
                    
                        </div>
                   
                      </div>

                    </div>
                  </div>
												
												
												
												
												
												
												</td>
												
												<td>
												
					<button type="button" class="btn btn-outline btn-link"  data-animation="fadein" data-toggle="modal" data-target="#myModal${vs.index}" id="viewDetailButton${vs.index}">${report.unitPrice}</button>
                  <!-- Modal -->
                  <div class="modal fade" id="myModal${vs.index}" role="dialog">
                    <div class="modal-dialog modal-full" >

                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Change Price</h4>
                        </div>
                        <div class="modal-body">
                    
                     <form:form action="${pageContext.request.contextPath}${unitaction}"  method="POST"	commandName="${modelVOval}">
                      			
                      			<div class="form-group">
									    <label for="product code">Unit Price</label>
									    <form:input type="text" class="form-control" path="unitPrice" value="${report.unitPrice}"></form:input>
									</div>
									
                      			 
                      			 <form:hidden path="productNo" value="${report.productNo}" />
                      			 
                      			 <form:hidden path="customerid" value="${report.customerid}" />
                      			 
                      			 
							 <button  type="submit" class="btn btn-primary btn-sm"  value="Validate">Update</button>
							
                      		</form:form>
                    
                        </div>
                   
                      </div>

                    </div>
                  </div>
                  
												</td>
												<td>
												
<div class="btn-group">
  <a class="btn btn-info dropdown-toggle" data-toggle="dropdown" href="#">
    Action
    <span class="caret"></span>
  </a>
  <ul class="dropdown-menu">
  
     <li><a href="${pageContext.request.contextPath}/product/uploadimage.Do?productno=${report.productNo}&flg=#upload">Manage Product Image</a></li>
      <li><a href="${pageContext.request.contextPath}/product/manageproduct/${report.productNo}">Delete</a></li>
   </ul>
</div>								
			
												
												
												</td>
										</tr>

									</c:forEach>

								</tbody>
							</table>
							
                             </div>
                    </div>
                    
                </div>
                
                    

            
                </div>
                </div>
  
  
  <div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content p-0 b-0">
                                        <div class="panel panel-color panel-primary">
                                            <div class="panel-heading">
                                                <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h3 class="panel-title">Create Category</h3>
                                            </div>
                                            <div class="panel-body">
                                   <form:form action="${pageContext.request.contextPath}/product/createproductcategory.Do" method="post"	commandName="productVO">
                                   
                                       <div class="form-group">
                                           </div> 
									    <label for="inputdefault">Category Name</label>
									    
  								<form:input type="text" class="form-control" path="categoryName"
								placeholder="Category Name" />	
								
									  
  									<div class="form-group">
									  <label for="sel1">Status</label>
									  <form:select class="form-control" path="catstatus">
									    <option value="1">Active</option>
									    <option value="0">Disable</option>
									  </form:select>
								</div>	
								
                                            
                                            
  									<div class="form-group">
									    <label for="inputdefault">Description</label>
									    <form:input type="text" class="form-control" path="categoryDescription" ></form:input>
									  </div>
  								
  									<input type="submit" class="btn btn-primary" value="Save" />	
  									
  									
        						</form:form>
                                       </div>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div>              
  
<%@ include file="javascript.jsp"%>
	 
</html>

   <script type="text/javascript">
            $(function () {
            	$('.table').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'print'
                    ]
                });
            });

        </script>
