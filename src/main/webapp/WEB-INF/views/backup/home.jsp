<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="false"%>
<html>
    <head>
        <script>
            var myContextPath = "${pageContext.request.contextPath}"

        </script>
        <title>RAB-Add a New Case</title>



        <%@ include file="css.jsp"%>

        <script src="${pageContext.request.contextPath}/resources/javascript/home.js" type="text/javascript"></script>
        <script type="text/javascript"
        src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    </head>
    <body class="skin-blue sidebar-mini">


        <div class="wrapper">

            <%@ include file="headerAndSideManu.jsp"%>

            <div class="content-wrapper">


                <section class="content">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="box-body ">

                                <form:form id="newCaseForm" action="${pageContext.request.contextPath}/case/showdetailsinputform" modelAttribute="caseBasicInfo"
                                           method="post" 
                                           enctype="multipart/form-data" autocomplete="off" >

                                    <div class="col-md-12">

                                        <div class="box box-primary ">
                                            <div class="box-header with-border">
                                              
                                                <h2 >প্রাথমিক তথ্য</h2>

                                            </div>
                                            <div class="box-body">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="battalionName"> ব্যাটালিয়নের নাম</label>
                                                        <form:select class="form-control" id="battalionName" path="battalionName">
                                                            <form:options items="${battalionNames}" />
                                                        </form:select>

                                                    </div>

                                                    <div class="form-group">
                                                        <label for="companyName">কোম্পানির নাম</label>

                                                        <form:select class="form-control"   path="companyName">
                                                            <form:options items="${companyNames}" />
                                                        </form:select>


                                                    </div>

                                                    <div class="form-group">
                                                        <label for="district"> জেলা </label>

                                                        <form:select  class="selectpicker"  data-live-search="true" title="জেলা " path="district">
                                                            <form:options items="${district}" />
                                                        </form:select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="subDistrict">থানা</label>
                                                            <form:select class="form-control" path="subDistrict">
                                                                <form:options items="${policeStations}" />
                                                            </form:select>

                                                    </div>

                                                    <div class="form-group">
                                                        <label for="serailNo">মামলা নাম্বার </label>
                                                        <form:input type="text" class="form-control" path="serialNumber" id="serialNumber"
                                                                    placeholder="মামলা নাম্বার  " onchange="checkCaseNumber" />

                                                    </div>

                                                    <div class="form-group">
                                                        <label for="caseDate">মামলা রুজূর তারিখ</label>
                                                        <form:input type="text" class="form-control commonDatepicker" onkeydown="return false"
                                                                    path="caseDate" placeholder="মামলা  রুজূর তারিখ " />

                                                    </div>



                                                    <div class="form-group">
                                                        <label for="typeOfCrime">অপরাধের ধরন</label>

                                                        <form:select class="form-control" path="typeOfCrime">
                                                            <form:options items="${typeOfCrimes}" />
                                                        </form:select>

                                                    </div>


                                                </div>



                                                <div class="col-md-6">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="dateOfCrime">ঘটনার তারিখ</label>
                                                            <form:input type="text" class="form-control commonDatepicker" onkeydown="return false"
                                                                        path="dateOfCrime" placeholder="ঘটনার  তারিখ " />

                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="timeOfCrime">ঘটনার সময় </label>
                                                            <form:input type="text" class="form-control"
                                                                        path="timeOfCrime" placeholder="ঘটনার  সময় " />

                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="placeOfCrime">ঘটনাস্থল</label>
                                                        <form:input type="text" class="form-control"
                                                                    path="placeOfCrime" placeholder="ঘটনাস্থল " />

                                                    </div>

                                                    <div class="form-group">
                                                        <label for="crimeDescription">ঘটনার সংক্ষিপ্ত বিবরণ</label>
                                                        <form:textarea rows="8"   class="form-control"
                                                                       path="crimeDescription" placeholder="ঘটনার সংক্ষিপ্ত বিবরণ  " /> 

                                                    </div>


                                                </div>
                                           <!--      <input type="hidden" id="lawNameCount" name="lawNameCount" value="1"/>	
                                                <div class="col-md-12">
                                                    <div class="input_fields_wrap add-block-more">
                                                        <span class="first-part-case-dhara" >
                                                            <div id="lawDataRow1">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="lawName">আইন </label>
                                                                        <input id="lawNameId1" name="lawName[0]" placeholder="আইন " type="text" class="form-control law-name-auto" autocomplete="off">

                                                                    </div>
                                                                </div>


                                                                <div class="col-md-5">
                                                                    <div class="form-group">
                                                                        <label for="caseDhara">মামলার ধারা</label>																															
                                                                        <input   id ="dh1" name="caseDahara[0]" placeholder="মামলার ধারা  " type="text" class="form-control dhara-name-auto" autocomplete="off">
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-1">
                                                                    <div class="form-group">
                                                                        <div>&nbsp</div>
                                                                        <a class="btn btn-primary add-more-case-dhara"><i class="glyphicon glyphicon-plus"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </span>
                                                    </div>
                                                </div> 
 -->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="officeName">এজাহার সংযুক্তি </label>
                                                        <form:input type="file" class="form-control" path="file" />
                                                    </div>
                                                </div>
                                                <input type="hidden" name="${_csrf.parameterName}"
                                                       value="${_csrf.token}" />

                                                <div class="col-md-12">
                                                    <div class="box-footer text-center">
                                                        <input type="submit" class="btn btn-primary"
                                                               value="সেভ " />
                                                    </div>
                                                </div>

                                            </div>

                                        </div>


                                    </div>

                                    <!-- <div class="col-md-2 "></div>
                                            <div class="col-md-8"></div> -->
                                </form:form>

                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>

        <%@ include file="javascript.jsp"%>
    </body>
</html>

<!-- <script type="text/javascript">
        $(document).ready(function() {

                $("#newCaseForm").validate({
                        rules : {
                                battalionName : {
                                        required : true
                                },

                                companyName : {
                                        required : true
                                },

                                serailNo : {
                                        required : true
                                },

                                subDistrict : {
                                        required : true
                                },

                                caseDhara : {
                                        required : true
                                },
                                lawName : {
                                        required : true
                                },

                        },
                        messages : {
                                battalionName : {
                                        required : "তথ্য আবশ্যক "
                                },

                                companyName : {
                                        required : "তথ্য আবশ্যক "
                                },

                                serailNo : {
                                        required : "তথ্য আবশ্যক "
                                },

                                subDistrict : {
                                        required : "তথ্য আবশ্যক "
                                },

                                caseDhara : {
                                        required : "তথ্য আবশ্যক "
                                },
                                lawName : {
                                        required : "তথ্য আবশ্যক "
                                },

                        }
                });

        })
</script>
-->
<!--  <script type="text/javascript">

$( "#dhara-name-auto" ).click(function() {
	  alert( "Handler for .click() called." );
	});​
</script>  -->
<script type="text/javascript">
$("#serialNumber").change( function() {
    var casenumber = $("#serialNumber").val();
    var battalionname = $("#battalionName").val();
    alert("Seacrh suggestion satrting with - " + casenumber+battalionname);


	  
	  var frm = $('#newCaseForm').serialize();
		var data = {}
		var Form = this;
		console.log(frm);

		var data_to_send = JSON.stringify(frm);
		console.log("JSON STNIGFY" + data_to_send);
		//alert("JSON STNIGFY" + data_to_send);
		 $.ajax({
			type : 'POST',
			url : myContextPath + "/case/CheckCaseExistOrNoT",
			data : frm,
			success : function(callback) {
				alert("Law Successfully Added");
				//$(this).html("Success!");
				// $('#investigationresult').hide();
				//$("#Defendant").show();
				
			},
			error : function() {
				alert("Failed");
			}
		});
  // var jsonString = JSON.stringify(array);



});


	   

</script>