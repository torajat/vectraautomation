<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<meta charset="utf-8">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page session="false"%>
<html>
<head>
<title><spring:message code="title.app"/></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Include Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  
<%@ include file="../../css.jsp"%>

</head>
<%@ include file="../../headerAndSideManu.jsp"%>


        <div class="wrapper">
            <div class="container">

               
                <!-- Page-Title -->
                <h3 class="text-success counter">${title}</h3>
                 	          <%--  <a href="${pageContext.request.contextPath}/product/createproductcategory.do" class="btn btn-primary waves-effect waves-light m-t-10">
					               Create Category
					              </a>  
					     --%>
  				
                <div class="row">
           <%--     
                           <img src="${pageContext.request.contextPath}/resources/theme/productimage/1.jpg" alt=""/>
       <img src="${pageContext.request.contextPath}/resources/theme/productimage/8ff0c46f-50e7-4c29-baff-38e31b6a7bbe2.jpg" alt=""/>
  
   --%>
   
  
  
                    <div class="col-sm-12">
                    
  
                        <div class="card-box">
                        
                        <div class="box-body">
			<div class="form-group">
			
			<c:if test="${not empty msg}">
		<div class="alert alert-${css} alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" 
                                aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<strong>${msg}</strong>
		</div>
		</c:if>
		</div>
		
		
		
		</div>
        
        
        
        
        
        
        <div class="row">
        
        <div class="col-sm-6 col-lg-3">
                        <div class="widget-simple-chart text-right card-box">
                            <div class="circliful-chart circliful" data-dimension="90" data-text="35%" data-width="5" data-fontsize="14" data-percent="35" data-fgcolor="#5fbeaa" data-bgcolor="#ebeff2" style="width: 90px;"><span class="circle-text" style="line-height: 90px; font-size: 14px;"></span><canvas width="90" height="90"></canvas></div>
                            <h3 class="text-success counter">${pending}</h3>
                            <p class="text-muted text-nowrap">Total Pending Order</p>
                        </div>
                    </div>
                    
                    
                          
        <div class="col-sm-6 col-lg-3">
                        <div class="widget-simple-chart text-right card-box">
                            <div class="circliful-chart circliful" data-dimension="90" data-text="35%" data-width="5" data-fontsize="14" data-percent="35" data-fgcolor="#5fbeaa" data-bgcolor="#ebeff2" style="width: 90px;"><span class="circle-text" style="line-height: 90px; font-size: 14px;"></span><canvas width="90" height="90"></canvas></div>
                            <h3 class="text-success counter">${pstart}</h3>
                            <p class="text-muted text-nowrap">Total Preparation Ongoing</p>
                        </div>
                    </div>
        
        
        
              
        <div class="col-sm-6 col-lg-3">
                        <div class="widget-simple-chart text-right card-box">
                            <div class="circliful-chart circliful" data-dimension="90" data-text="35%" data-width="5" data-fontsize="14" data-percent="35" data-fgcolor="#5fbeaa" data-bgcolor="#ebeff2" style="width: 90px;"><span class="circle-text" style="line-height: 90px; font-size: 14px;"> </span><canvas width="90" height="90"></canvas></div>
                            <h3 class="text-success counter">${completed}</h3>
                            <p class="text-muted text-nowrap">Total Completed Order</p>
                        </div>
                    </div>
        
        
              
        <div class="col-sm-6 col-lg-3">
                        <div class="widget-simple-chart text-right card-box">
                            <div class="circliful-chart circliful" data-dimension="90" data-text="35%" data-width="5" data-fontsize="14" data-percent="35" data-fgcolor="#5fbeaa" data-bgcolor="#ebeff2" style="width: 90px;"><span class="circle-text" style="line-height: 90px; font-size: 14px;"></span><canvas width="90" height="90"></canvas></div>
                            <h3 class="text-success counter">${total}</h3>
                            <p class="text-muted text-nowrap">Total Order</p>
                        </div>
                    </div>
        
        </div>
        
        
        
        
        
                        
                        
                       <table id="productcategory" class="table table-bordered table-hover">
								<thead >
									<tr>
									    <th>Order Date</th>
										<th>Product Code</th>
										<th>Product Name</th>
										<th>Order Quantity</th>
										<th>Unit Price</th>
										<th>Total Price</th>
										<th>Table No</th>
										<th>Status</th>
										<th>Image</th>
										
										
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${orderlist}" var="report" varStatus="vs">
										<tr>
												<td>${report.orderdate}</td>
												<td>
												${report.productCode}
												</td>
												
												<td>
												${report.productname}
												</td>
												
										
												<td>
												${report.quantity}
												
												</td>
																
												<td>
												
												${report.unitPrice}
												
                  
												</td>
												
												
												<td>
												
												${report.totalprice}
												
                  
												</td>
											<td>
												
												${report.tableno}
												
                  
												</td>
												
												<td>
												${report.status}
												</td>
												<td>
												
												<img src="${pageContext.request.contextPath}/resources/theme/productimage/${report.filename}" alt=""/>
													
												</td>
										</tr>

									</c:forEach>

								</tbody>
							</table>
							
                             </div>
                    </div>
                </div>
                </div>
                </div>
  
  	
<%@ include file="../../javascript.jsp"%>
	 
</html>


   <script type="text/javascript">
            $(function () {
            	$('.table').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'print'
                    ]
                });
            });

        </script>