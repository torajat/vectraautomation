<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<meta charset="utf-8">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page session="false"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<spring:htmlEscape defaultHtmlEscape="true" />

<html>
<head>
<title><spring:message code="title.app"/></title>

<%@ include file="../../css.jsp"%>

</head>
<%@ include file="../../headerAndSideManu.jsp"%>


        <div class="wrapper">
            <div class="container">

                <!-- Page-Title -->
                <h4 class="text-success counter">${title}</h4>
                


				<div class="row">
                
               
  <div class="col-sm-6 col-lg-2 pad-right">
                        <div class="widget-simple text-left card-box">                                    
                <h4 class="m-t-0 header-title"><b>Manage Restaurant</b></h4>
    <hr>
                               <ul class="nav">
					                    
					                  
					                          <li class="active" ><a href="${pageContext.request.contextPath}${managerestaurantsaction}">${managerestaurants}</a></li>
					                        <li ><a href="${pageContext.request.contextPath}${managerestaurantstableaction}">${managerestaurantstable}</a></li>
					                        <li ><a href="${pageContext.request.contextPath}${managerestaurantsconfigaction}">${managerestaurantsconfig}</a></li>
					                        <li ><a href="${pageContext.request.contextPath}${tablelistaction}">${tablelist}</a></li>
					                        <li ><a href="${pageContext.request.contextPath}${devicesaction}">${devices}</a></li>
					                        <li ><a href="${pageContext.request.contextPath}${deviceslistmenuaction}">${deviceslistmenu}</a></li>
					                 
					                       
					                       
					                       
					                       </ul>
                   
                        </div>
                    </div>
                    
               

   <div class="col-sm-6 col-lg-10 pad-left">
                 
                        <div class="widget-simple text-left card-box">
                            <div>

<h4 class="m-t-0 header-title"><b>${menutitle}</b></h4>
<hr>
                                        
 <div class="box-body">
			<div class="form-group">
			
			<c:if test="${not empty msg}">
		<div class="alert alert-${css} alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" 
                                aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<strong>${msg}</strong>
		</div>
		</c:if>
		</div>
		</div>
		
		
		<div class="row"> 
		
	   <form:form action="${pageContext.request.contextPath}${actionval}"  method="POST"	commandName="${modelVOval}">
   			
					<div class="col-lg-12">
					
							<div class="form-group">
                                <label for="assetfamily">Restaurant Name</label> 
                                
                                <form:select class="form-control selectpicker" data-show-subtext="true" data-live-search="true"  path="orgId">
            
            						<form:options items="${organizationlist}" />
                    
           						</form:select>
                            </div>
			
		
					
							<div class="form-group">
								<label>Device MAC address</label> <form:input 
									class="form-control" placeholder="Device Mac Address" path="devicemac" required="true"></form:input>
									<label id="form-msg-label_name" class="my-error-class">${erroragname}</label>
							</div>
							
							<div class="form-group">
								<label>Device Model</label> <form:input 
									class="form-control" placeholder="Device Model" path="devicemodel" required="true" ></form:input>
									<label id="form-msg-label_name" class="my-error-class">${erroragname}</label>
							</div>
							
							<div class="form-group">
								<label>Device Supplier</label> <form:input 
									class="form-control" placeholder="Device Supplier" path="devicesupplier" required="true" ></form:input>
									<label id="form-msg-label_name" class="my-error-class">${erroragname}</label>
							</div>
							
							
							
							<div class="form-group">
								<label>Device Warranty Period</label> <form:input 
									class="form-control" placeholder="Device Warranty Period" path="warrenty" required="true" ></form:input>
									<label id="form-msg-label_name" class="my-error-class">${erroragname}</label>
							</div>
							
							
							
								
							<%-- 
							<div class="form-group">
								<label>Device ID</label> 
								
								<form:select class="form-control selectpicker" data-show-subtext="true" data-live-search="true"  path="deviceId">
            
            						<form:options items="${citymap}" />
                    
           						</form:select>
            
								
									</div>
			 --%>				
					
            
							</div>
					
							
							<div class="row">
							<div class="col-lg-12 text-center">
							<button id="newCustomerBtn" type="submit"
								class="btn btn-primary btn-lg" value="Validate"  >Create</button>
							<button type="reset" onclick="ResetCustomer();" class="btn btn-primary btn-lg">Cancel</button>
		
							</div>
							</div>
		</form:form>
			 
		
 
		</div>
												
                  </div>
                        </div>
                    </div>


                </div>
                
      	
      	
      	<!-- -End of container class- -->
			</div>
													              
       </div>
      


<%@ include file="../../javascript.jsp"%>
	 
</html>

   <script type="text/javascript">
            $(function () 
            		{
            	

            
            });
            
</script>            

        