<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<meta charset="utf-8">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page session="false"%>
<html>
<head>
<title><spring:message code="title.app"/></title>

<%@ include file="../../css.jsp"%>

</head>
<%@ include file="../../headerAndSideManu.jsp"%>


        <div class="wrapper">
            <div class="container">

               
                <!-- Page-Title -->
                <h3 class="text-success counter">${title}</h3>
                 	          <%--  <a href="${pageContext.request.contextPath}/product/createproductcategory.do" class="btn btn-primary waves-effect waves-light m-t-10">
					               Create Category
					              </a>  
					     --%>
  				
  				
                <div class="row">
               
         <div class="box-body">
			<div class="form-group">
			
			<c:if test="${not empty msg}">
		<div class="alert alert-${css} alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" 
                                aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<strong>${msg}</strong>
		</div>
		</c:if>
		</div>
		</div>
		
		
                 <div class="col-sm-6 col-lg-2 pad-right">
                        <div class="widget-simple text-left card-box">                                    
                <h4 class="m-t-0 header-title"><b>Customer Setting</b></h4>
    <hr>
                               <ul class="nav">
					                    
					                     <li class="active" ><a href="${pageContext.request.contextPath}${managerestaurantsaction}">${managerestaurants}</a></li>
					                        <li ><a href="${pageContext.request.contextPath}${managerestaurantstableaction}">${managerestaurantstable}</a></li>
					                        <li ><a href="${pageContext.request.contextPath}${managerestaurantsconfigaction}">${managerestaurantsconfig}</a></li>
					                        <li ><a href="${pageContext.request.contextPath}${tablelistaction}">${tablelist}</a></li>
					                        <li ><a href="${pageContext.request.contextPath}${devicesaction}">${devices}</a></li>
					                  <li ><a href="${pageContext.request.contextPath}${deviceslistmenuaction}">${deviceslistmenu}</a></li>
					                 
					                       
					                       </ul>
                   
                        </div>
                    </div>
                    
                    

   <div class="col-sm-6 col-lg-10 pad-left">
                 
                        <div class="widget-simple text-left card-box">
                            <div>

<h4 class="m-t-0 header-title"><b>${menutitle}</b></h4>
<hr>

                       <table id="users" class="table table-bordered table-hover">
								<thead >
									<tr>
										<th>Restaurant Name</th>
										<th>Table Name</th>
										<th>Capacity</th>
										 <th>Device MAC Address</th>
										 <th>Device Ops</th>
										 <th>Action</th>
												
									</tr>
								</thead>
								<tbody>
								
								<c:forEach items="${tablevaluelist}" var="report" varStatus="vs">
										<tr>
											
												<td>
												${report.organizationame}
												
												</td>											
											
												<td>${report.tablename}</td>
												<td>${report.capacity}</td>
												<td>${report.devicemac}</td>
												<td>


<button type="button" class="btn btn-outline btn-link"  data-animation="fadein" data-toggle="modal" data-target="#myModaladddevice${vs.index}" id="viewDetailButton${vs.index}">Add Device</button>
                  <!-- Modal -->
                  <div class="modal fade" id="myModaladddevice${vs.index}" role="dialog">
                    <div class="modal-dialog modal-full" >

                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Add Device</h4>
                        </div>
                        <div class="modal-body">
                         <form:form action="${pageContext.request.contextPath}${addactionval}"  method="POST"	commandName="${modelVOval}">
                      	
                        	<div class="form-group">
                                <label for="assetfamily">Device Name</label> 
                                
                                <form:select class="form-control selectpicker" data-show-subtext="true" data-live-search="true"  path="devicemac">
            
            						<form:options items="${mydevicelist}" />
                    
           						</form:select>
                            </div>
                            
                            <form:hidden path="opsflg" value="Add"></form:hidden>
                            
                             <form:hidden path="tableno" value="${report.tableno}"></form:hidden>
                            	 	 
							 <button  type="submit" class="btn btn-primary btn-sm"  value="Validate">Add</button>
							
                            
                            </form:form>
			
                         </div>
                   
                      </div>

                    </div>
                  </div>
												

												
<button type="button" class="btn btn-outline btn-link"  data-animation="fadein" data-toggle="modal" data-target="#myModalname${vs.index}" id="viewDetailButton${vs.index}"> Remove Device</button>
                  <!-- Modal -->
                  <div class="modal fade" id="myModalname${vs.index}" role="dialog">
                    <div class="modal-dialog modal-full" >

                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Remove Device </h4>
                        </div>
                        <div class="modal-body">
                    
                     <form:form action="${pageContext.request.contextPath}${addactionval}"  method="POST"	commandName="${modelVOval}">
                      			
                      			Are you sure, want to remove the device ${report.devicemac} from the table : <p class="text-warning"><span>${report.tablename}</span></p>
                      			<br/>
                      			
                      			<form:hidden path="tableno" value="${report.tableno}" />
                      			 <form:hidden path="opsflg" value="Remove"></form:hidden>
                      			
                      			<%-- 
                      			<div class="form-group">
									    <label for="product code">Product Name</label>
									    <form:input type="text" class="form-control" path="devicemac" value="${report.productname}"></form:input>
									</div>
								
								 <form:hidden path="devicemac" value="${report.customerid}" />
                      			 <form:hidden path="customerid" value="${report.customerid}" />
                      			 --%> 
                      			 
							 <button  type="submit" class="btn btn-primary btn-sm"  value="Validate">Remove</button>
							
                      		</form:form>
                    
                        </div>
                   
                      </div>

                    </div>
                  </div>
												
							

													
													
																									
												</td>
												<td>
												<form:form action="${pageContext.request.contextPath}${editactionval}"  method="GET"	commandName="${modelVOval}">
                							     <form:hidden path="customerId" value="${id}" />
			       							     <form:hidden path="tablename" value="${report.tablename}" />
											     <form:hidden path="tableno" value="${report.tableno}" />
											     <form:hidden path="capacity" value="${report.capacity}" />
											       <form:hidden path="organizationame" value="${report.organizationame}" />
											  
                   								<input type="submit" class="btn btn-outline btn-link"  value="Edit"/>
                        	
                   								
                   								  </form:form>
												
												
												</td>
	
</tr>
</c:forEach>

								
								
								</tbody>
</table>
							
                             </div>
                    </div>
                    
                </div>
                
</div>                    

            
                </div>
                </div>
  
  
<%--   <div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content p-0 b-0">
                                        <div class="panel panel-color panel-primary">
                                            <div class="panel-heading">
                                                <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h3 class="panel-title">Create Category</h3>
                                            </div>
                                            <div class="panel-body">
                                 
<!--                                  
                                 private String productNo;
private String productCode;
private String productDescription;
private String barcode;
private String unitPrice;
private String stocksOnHand;
private String categoryNo;
private String categoryName;
private String catstatus;
private String categoryDescription; -->
           
                                   <form:form action="${pageContext.request.contextPath}/product/createproductcategory.Do" method="post"	commandName="productVO">
                                   
                                       <div class="form-group">
                                           </div> 
									    <label for="inputdefault">Category Name</label>
									    
  								<form:input type="text" class="form-control" path="categoryName"
								placeholder="Category Name" />	
								
									  
  									<div class="form-group">
									  <label for="sel1">Status</label>
									  <form:select class="form-control" path="catstatus">
									    <option value="1">Active</option>
									    <option value="0">Disable</option>
									  </form:select>
								</div>	
								
                                            
                                            
  									<div class="form-group">
									    <label for="inputdefault">Description</label>
									    <form:input type="text" class="form-control" path="categoryDescription" ></form:input>
									  </div>
  								
  									<input type="submit" class="btn btn-primary" value="Save" />	
  									
  									
        						</form:form>
                                       </div>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div>        --%>       
  
<%@ include file="../../javascript.jsp"%>
	 
</html>

   <script type="text/javascript">
            $(function () {
            	$('.table').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'print'
                    ]
                });
            });

        </script>
