<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<meta charset="utf-8">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page session="false"%>
<html>
<head>
<title><spring:message code="title.app"/></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Include Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  
<%@ include file="../css.jsp"%>

</head>
<%@ include file="../headerAndSideManu.jsp"%>


        <div class="wrapper">
            <div class="container">

               
                <!-- Page-Title -->
                <h3 class="text-success counter">${title}</h3>
                 	          <%--  <a href="${pageContext.request.contextPath}/product/createproductcategory.do" class="btn btn-primary waves-effect waves-light m-t-10">
					               Create Category
					              </a>  
					     --%>
  				
                <div class="row">
           <%--     
                           <img src="${pageContext.request.contextPath}/resources/theme/productimage/1.jpg" alt=""/>
       <img src="${pageContext.request.contextPath}/resources/theme/productimage/8ff0c46f-50e7-4c29-baff-38e31b6a7bbe2.jpg" alt=""/>
  
   --%>
   
     <div>
            <ul id="show-cart">
                <li>???????</li>
            </ul>
            <div>You have <span id="count-cart">X</span> items in your cart</div>
            <div>Total Cart:$<span id="total-cart"></span></div>
        </div>
    <button id="clear-cart">Clear Cart</button>
  
  
                    <div class="col-sm-12">
                    
  
                        <div class="card-box">
                        
                        <div class="box-body">
			<div class="form-group">
			
			<c:if test="${not empty msg}">
		<div class="alert alert-${css} alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" 
                                aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<strong>${msg}</strong>
		</div>
		</c:if>
		</div>
		</div>
       
       										
											<form:form action="${pageContext.request.contextPath}${actionval}"  method="GET"	commandName="${modelval}">
                							
                						<div class="form-group">
                                <label for="assetfamily">Location</label> 
                                
                                <form:select class="form-control selectpicker" data-show-subtext="true" data-live-search="true"  path="locid">
            
            						<form:options items="${citymap}" />
                    
           						</form:select>
                            </div>
			
                							  
                   								<input type="submit"  class="btn btn-primary" value="Search"/>
                        	
                        	
                   								
    										</form:form>
     								
       
                        
                       <table id="productcategory" class="table table-bordered table-hover">
								<thead >
									<tr>
										<th>Restaurant Name </th>
										<th>Address</th>
										<th>Booking Option</th>
										
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${autogen}" var="report" varStatus="vs">
										<tr>
												<td>
												${report.organizationame}
												</td>
												
												<td>
												${report.address}
												</td>
												
												<td>
												
												
       										
											<form:form action="${pageContext.request.contextPath}${actionval}"  method="POST"	commandName="${modelval}">
                							
                						<div class="form-group">
                                <label for="assetfamily">Table Capacity</label> 
                                
                                <select id="capacity" name="capacity" data-live-search="true" class="form-control selectpicker" data-show-subtext="true">
            	
												${report.capacity}
						        
           						</select>
                							  
                            </div>
			
				<div class="form-group">
								<label> Booking Date</label> <form:input 
									class="form-control" placeholder="Booking Date" path="bookingdate" required="true"></form:input>
									<label id="form-msg-label_name" class="my-error-class">${erroragname}</label>
							</div>
							
							
								<div class="form-group">
								<label> Start Time</label> <form:input 
									class="form-control" placeholder="Start Time" path="startime" required="true"></form:input>
									<label id="form-msg-label_name" class="my-error-class">${erroragname}</label>
							</div>
							
							<div class="form-group">
								<label> End Time</label> <form:input 
									class="form-control" placeholder="End Time" path="endtime" required="true"></form:input>
									<label id="form-msg-label_name" class="my-error-class">${erroragname}</label>
							</div>
			
			
				<div class="form-group">
								<label>Your Name</label> <form:input 
									class="form-control" placeholder="Your Name" path="name" required="true"></form:input>
									<label id="form-msg-label_name" class="my-error-class">${erroragname}</label>
							</div>
			
			
				<div class="form-group">
								<label>Your Mobile Number</label> <form:input 
									class="form-control" placeholder="Your Mobile Number" path="mobileno" required="true"></form:input>
									<label id="form-msg-label_name" class="my-error-class">${erroragname}</label>
							</div>
			
							
							<form:hidden path="customerid" value="${report.customerid}"/>
							
							
							<form:hidden path="orId" value="${report.orId}"/>
												
												
                   								<input type="submit"  class="btn btn-primary" value="Book Now"/>
                        	
                        	
                        	
                   								
    										</form:form>
     								
       
												
												
											
												
												</td>
												
														<%-- 	<td>
												
												<img src="${pageContext.request.contextPath}/resources/theme/productimage/${report.filename}" alt=""/>
													
												</td> --%>
										
										</tr>

									</c:forEach>

								</tbody>
							</table>
							
                             </div>
                    </div>
                </div>
                </div>
                </div>
  
  	
<%@ include file="../javascript.jsp"%>
	 
</html>

   <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
   
   
   
        <script>

            $(".add-to-cart").click(function(event){
                event.preventDefault();
                var name = $(this).attr("data-name");
                var price = Number($(this).attr("data-price"));

                shoppingCart.addItemToCart(name, price, 1);
                displayCart();
            });

            $("#clear-cart").click(function(event){
                shoppingCart.clearCart();
                displayCart();
            });

            function displayCart() {
                var cartArray = shoppingCart.listCart();
                console.log(cartArray);
                var output = "";

                for (var i in cartArray) {
                    output += "<li>"
                        +cartArray[i].name
                        +" <input class='item-count' type='number' name='item' data-name='"
                        +cartArray[i].name
                        +"' value='"+cartArray[i].count+"' >"
                        +" x "+cartArray[i].price
                        +" = "+cartArray[i].total
                        +" <button class='plus-item' data-name='"
                        +cartArray[i].name+"'>+</button>"
                        +" <button class='subtract-item' data-name='"
                        +cartArray[i].name+"'>-</button>"
                        +" <button class='delete-item' data-name='"
                        +cartArray[i].name+"'>X</button>"
                        +"</li>";
                }

                $("#show-cart").html(output);
                $("#count-cart").html( shoppingCart.countCart() );
                $("#total-cart").html( shoppingCart.totalCart() );
            }

            $("#show-cart").on("click", ".delete-item", function(event){
                var name = $(this).attr("data-name");
                shoppingCart.removeItemFromCartAll(name);
                displayCart();
            });

            $("#show-cart").on("click", ".subtract-item", function(event){
                var name = $(this).attr("data-name");
                shoppingCart.removeItemFromCart(name);
                displayCart();
            });

            $("#show-cart").on("click", ".plus-item", function(event){
                var name = $(this).attr("data-name");
                shoppingCart.addItemToCart(name, 0, 1);
                displayCart();
            });

            $("#show-cart").on("change", ".item-count", function(event){
                var name = $(this).attr("data-name");
                var count = Number($(this).val());
                shoppingCart.setCountForItem(name, count);
                displayCart();
            });


            displayCart();

        </script>
        
        
        
        
    <script type="text/javascript">
        $(document).ready(function(){
            // Initialize Smart Cart    	
            $('#smartcart').smartCart();
		});
    </script>

   <script type="text/javascript">
            $(function () {
            	$('.table').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'print'
                    ]
                });
            });

        </script>
