<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<meta charset="utf-8">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page session="false"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<spring:htmlEscape defaultHtmlEscape="true" />

<html>
<head>
<title><spring:message code="title.app"/></title>

<%@ include file="../../css.jsp"%>

</head>
<%@ include file="../../headerAndSideManu.jsp"%>


        <div class="wrapper">
            <div class="container">

                <!-- Page-Title -->
                <h4 class="text-success counter">${title}</h4>
                


				<div class="row">
                
               
  <div class="col-sm-6 col-lg-2 pad-right">
                        <div class="widget-simple text-left card-box">                                    
                <h4 class="m-t-0 header-title"><b>Manage Restaurant</b></h4>
    <hr>
                               <ul class="nav">
					                    
					                        <li class="active" ><a href="${pageContext.request.contextPath}${organizationaction}">${organization}</a></li>
					                        <li ><a href="${pageContext.request.contextPath}${createorganizationaction}">${createorganization}</a></li>
					                    <li ><a href="${pageContext.request.contextPath}${organizationaction}">Edit Restaurants</a></li>
					                     
					                       
					                       </ul>
                   
                        </div>
                    </div>
                    
               

   <div class="col-sm-6 col-lg-10 pad-left">
                 
                        <div class="widget-simple text-left card-box">
                            <div>

<h4 class="m-t-0 header-title"><b>${menutitle}</b></h4>
<hr>
                                        
                                            <div class="box-body">
			<div class="form-group">
			
			<c:if test="${not empty msg}">
		<div class="alert alert-${css} alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" 
                                aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<strong>${msg}</strong>
		</div>
		</c:if>
		</div>
		</div>
		
		
		<div class="row"> 
		
	   <form:form action="${pageContext.request.contextPath}${actionval}"  method="POST"	commandName="${modelVOval}">
   			
					<div class="col-lg-12">
					
					
						<div class="form-group">
                                <label for="assetfamily">Customer Name</label> 
                                
                                <form:select class="form-control selectpicker" data-show-subtext="true" data-live-search="true"  path="customerid">
            
            						<form:options items="${customerids}" />
                    
           						</form:select>
                            </div>
			
			
					
							<div class="form-group">
								<label>Restaurant Name</label> <form:input 
									class="form-control" placeholder="Restaurant Name" path="organizationame" required="true"></form:input>
									<label id="form-msg-label_name" class="my-error-class">${erroragname}</label>
							</div>
							
							<div class="form-group">
								<label>Number of License</label> <form:input 
									class="form-control" placeholder="Number of license" path="licenseno" ></form:input>
									<label id="form-msg-label_name" class="my-error-class">${erroragname}</label>
							</div>
							
							<div class="form-group">
								<label>Area</label> 
								
								
								
                                <form:select class="form-control selectpicker" data-show-subtext="true" data-live-search="true"  path="locid">
            
            						<form:options items="${citymap}" />
                    
           						</form:select>
            
								
									</div>
							
											
							<div class="form-group">
								<label>Address</label> <form:input 
									class="form-control" placeholder="Address" path="address" ></form:input>
									<label id="form-msg-label_name" class="my-error-class">${erroragname}</label>
							</div>
			
            
							</div>
					
							
							<div class="row">
							<div class="col-lg-12 text-center">
							<button id="newCustomerBtn" type="submit"
								class="btn btn-primary btn-lg" value="Validate"  >Create</button>
							<button type="reset" onclick="ResetCustomer();" class="btn btn-primary btn-lg">Cancel</button>
		
							</div>
							</div>
		</form:form>
			 
		
 
		</div>
												
                  </div>
                        </div>
                    </div>


                </div>
                
      	
      	
      	<!-- -End of container class- -->
			</div>
													              
       </div>
      


<%@ include file="../../javascript.jsp"%>
	 
</html>

   <script type="text/javascript">
            $(function () 
            		{
            	

            
            });
            
</script>            

        