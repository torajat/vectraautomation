<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<meta charset="utf-8">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page session="false"%>
<html>
<head>
<title><spring:message code="title.app"/></title>

<%@ include file="../css.jsp"%>

</head>
<%@ include file="../headerAndSideManu.jsp"%>


        <div class="wrapper">
            <div class="container">

               
                <!-- Page-Title -->
                <h3 class="text-success counter">${title}</h3>
                 	          <%--  <a href="${pageContext.request.contextPath}/product/createproductcategory.do" class="btn btn-primary waves-effect waves-light m-t-10">
					               Create Category
					              </a>  
					     --%>
  				
  				
                <div class="row">
               
         <div class="box-body">
			<div class="form-group">
			
			<c:if test="${not empty msg}">
		<div class="alert alert-${css} alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" 
                                aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<strong>${msg}</strong>
		</div>
		</c:if>
		</div>
		</div>
		
		
                 <div class="col-sm-6 col-lg-2 pad-right">
                        <div class="widget-simple text-left card-box">                                    
                <h4 class="m-t-0 header-title"><b>Customer Setting</b></h4>
    <hr>
                               <ul class="nav">
					                    
					                        <li class="active" ><a href="${pageContext.request.contextPath}${customerlist}">List of Customer</a></li>
					                        <li ><a href="${pageContext.request.contextPath}${createcustomeraction}">${createcustomer}</a></li>
					                        <li ><a href="${pageContext.request.contextPath}${createadminaction}">${createadmin}</a></li>
					                        <li ><a href="${pageContext.request.contextPath}${editadminaction}">${editadmin}</a></li>
					                        <li ><a href="${pageContext.request.contextPath}${editadminaction}">${resetadminpass}</a></li>
					                       
					                       
					                       </ul>
                   
                        </div>
                    </div>
                    
                    
                  
            

   <div class="col-sm-6 col-lg-10 pad-left">
                 
                        <div class="widget-simple text-left card-box">
                            <div>

<h4 class="m-t-0 header-title"><b>${menutitle}</b></h4>
<hr>

                       <table id="productcategory" class="table table-bordered table-hover">
								<thead >
									<tr>
										<th>Customer ID</th>
										<th>Organization Name</th>
										<th>Mobile No</th>
										<th>Email</th>
										<th>Address</th>
										<th>Status</th>
										 <th>Action</th>
												
									</tr>
								</thead>
								<tbody>
								
								<c:forEach items="${customerinfolist}" var="report">
										<tr>
											
												<td>${report.cid}</td>											
												<td>${report.customerName}</td>
												<td>${report.mobileNumber}</td>
												<td>${report.email}</td>
												<td>${report.customerAddress}</td>
												<td>${report.status}</td>
												
												<td>


	<form:form action="${pageContext.request.contextPath}${actionvaledit}"  method="GET"	commandName="${modelVOval}">
                							
                							     <form:hidden path="customerid" value="${report.cid}" />
			       							     <form:hidden path="customerName" value="${report.customerName}" />
											     <form:hidden path="mobileNumber" value="${report.mobileNumber}" />
											     <form:hidden path="email" value="${report.email}" />
											     <form:hidden path="customerAddress" value="${report.customerAddress}" />
											    <form:hidden path="customeridentificationo" value="${report.customeridentificationo}" />
												<form:hidden path="status" value="${report.status}"/>	  
											  
                   								<input type="submit"  class="btn btn-outline btn-link" value="Edit"/>
                        	
                   								
                   								  </form:form>
									
												
												<%-- 
												
<div class="btn-group">
  <a class="btn btn-info dropdown-toggle" data-toggle="dropdown" href="#">
    Action
    <span class="caret"></span>
  </a>
  <ul class="dropdown-menu">
 
     <li><a href="${pageContext.request.contextPath}/POS/Transection.do">New Order</a></li>
     
    <li><a href="${pageContext.request.contextPath}/usermanagement/EditUserDetails?uuid=${data.uuid}&username=${data.username}&battalion=${data.batId}&ruletype=${data.roleId}&firstname=${data.firstName}&middlename=${data.middleName}&lastname=${data.lastName}&phone=${data.phoneNo}&flg=userbattalionupdate">Update Customer Info</a></li>
    <li><a href="${pageContext.request.contextPath}/usermanagement/EditUserDetails?uuid=${data.uuid}&username=${data.username}&battalion=${data.batId}&ruletype=${data.roleId}&firstname=${data.firstName}&middlename=${data.middleName}&lastname=${data.lastName}&phone=${data.phoneNo}&flg=usertypeupdate">View Order History</a></li>
   </ul>
</div>								
 --%>
 
 
 </td>
</tr>
</c:forEach>

								
								
								</tbody>
</table>
							
                             </div>
                    </div>
                    
                </div>
                
</div>                    

            
                </div>
                </div>
  
  
<%--   <div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content p-0 b-0">
                                        <div class="panel panel-color panel-primary">
                                            <div class="panel-heading">
                                                <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h3 class="panel-title">Create Category</h3>
                                            </div>
                                            <div class="panel-body">
                                 
<!--                                  
                                 private String productNo;
private String productCode;
private String productDescription;
private String barcode;
private String unitPrice;
private String stocksOnHand;
private String categoryNo;
private String categoryName;
private String catstatus;
private String categoryDescription; -->
           
                                   <form:form action="${pageContext.request.contextPath}/product/createproductcategory.Do" method="post"	commandName="productVO">
                                   
                                       <div class="form-group">
                                           </div> 
									    <label for="inputdefault">Category Name</label>
									    
  								<form:input type="text" class="form-control" path="categoryName"
								placeholder="Category Name" />	
								
									  
  									<div class="form-group">
									  <label for="sel1">Status</label>
									  <form:select class="form-control" path="catstatus">
									    <option value="1">Active</option>
									    <option value="0">Disable</option>
									  </form:select>
								</div>	
								
                                            
                                            
  									<div class="form-group">
									    <label for="inputdefault">Description</label>
									    <form:input type="text" class="form-control" path="categoryDescription" ></form:input>
									  </div>
  								
  									<input type="submit" class="btn btn-primary" value="Save" />	
  									
  									
        						</form:form>
                                       </div>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div>        --%>       
  
<%@ include file="../javascript.jsp"%>
	 
</html>

   <script type="text/javascript">
            $(function () {
            	$('.table').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'print'
                    ]
                });
            });

        </script>
