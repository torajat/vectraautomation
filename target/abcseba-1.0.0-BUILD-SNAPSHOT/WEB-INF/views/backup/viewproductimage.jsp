<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<meta charset="utf-8">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page session="false"%>
<html>
<head>
<title><spring:message code="title.app"/></title>

<%@ include file="css.jsp"%>

</head>
<%@ include file="headerAndSideManu.jsp"%>


        <div class="wrapper">
            <div class="container">

               
                <!-- Page-Title -->
                <h3 class="text-success counter">${title}</h3>
                 	          <%--  <a href="${pageContext.request.contextPath}/product/createproductcategory.do" class="btn btn-primary waves-effect waves-light m-t-10">
					               Create Category
					              </a>  
					     --%>
  				
                <div class="row">
               
                    <div class="col-sm-12">
  
                        <div class="card-box">
                       <table id="productcategory" class="table table-bordered table-hover">
								<thead >
									<tr>
										<th>Product Code</th>
										<th>Action</th>
										
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${productimagelist}" var="report">
										<tr>
												<td> <img src="${pageContext.request.contextPath}/resources/theme/productimage/${report.filename}" alt=""/></td>
												<td>${report.filename}</td>
																			</tr>

									</c:forEach>

								</tbody>
							</table>
							
                             </div>
                    </div>
                    
                </div>
                
                    

            
                </div>
                </div>
  
  
        
  
<%@ include file="javascript.jsp"%>
	 
</html>

   <script type="text/javascript">
            $(function () {
            	$('.table').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'print'
                    ]
                });
            });

        </script>
