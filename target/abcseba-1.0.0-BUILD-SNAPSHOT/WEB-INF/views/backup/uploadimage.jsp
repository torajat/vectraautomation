<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<meta charset="utf-8">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page session="false"%>
<html>
<head>
<title><spring:message code="title.app"/></title>

<%@ include file="css.jsp"%>

</head>
<%@ include file="headerAndSideManu.jsp"%>


        <div class="wrapper">
            <div class="container">

               
                <!-- Page-Title -->
                <h3 class="text-success counter">${title}</h3>
                 	          <%--  <a href="${pageContext.request.contextPath}/product/createproductcategory.do" class="btn btn-primary waves-effect waves-light m-t-10">
					               Create Category
					              </a>  
					     --%>
  				
               
                  
                  	<div class="row" >
	                     
	                     
	                      <div class="col-sm-12">
  
                        <div class="card-box">
  
	                     
                                       <form:form action="${pageContext.request.contextPath}/product/uploadimage.Do" method="post"	commandName="productVO" enctype="multipart/form-data">
                                   
               
               
               
              					<div class="form-group col-md-12">
                                    
                                    <label for="nextdate" class="col-md-3 control-label">Choose File</label>
                                    
                                    <div class="col-md-9 frm-input">
                                    <form:input type="file" class="form-control" path="file" required="true"/>
                                       
                                    </div>
                                    
                                </div>
        
              				 <form:input type="hidden" path="productNo"  value='${productno}'/>
        						
               
                 								
  									<input type="submit" class="btn btn-primary" value="Save" />	
  									
  									
        						</form:form>
      </div>
      </div>
      
        						
	</div>
         
         
                  
     <div class="row">
               
                    <div class="col-sm-12">
  
                        <div class="card-box">
                       <table id="productcategory" class="table table-bordered table-hover">
								<thead >
									<tr>
										<th>Images</th>
										<th>File Name</th>
										<th>Action</th>
										
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${productimagelist}" var="report" varStatus="vs">
										<tr>
												<td> 
												
												<div style="width: 300px; height: 200px;">
												<img src="${pageContext.request.contextPath}/resources/theme/productimage/${report.filename}" width="100%" height="100%" alt=""/>
												
												
												</div>

</td>
<td>
${report.filename}
</td>
												<td>
												
												
																			
					<button type="button" class="btn btn-primary btn-sm"  data-animation="fadein" data-toggle="modal" data-target="#myModalCode${vs.index}" id="viewDetailButton${vs.index}">  Delete</button>
                  <!-- Modal -->
                  <div class="modal fade" id="myModalCode${vs.index}" role="dialog">
                    <div class="modal-dialog modal-full" >

                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Delete Image</h4>
                        </div>
                        <div class="modal-body">
                    
                    Are you sure, want to delete this image
                    		
							<form:form action="${pageContext.request.contextPath}${actiondeleteval}"  method="POST"	commandName="${modelVOval}">
                      			 
                      			 <form:hidden path="productNo" value="${report.productNo}" />
                      			 
                      			 <form:hidden path="filename" value="${report.filename}" />
                      			 
                      			 <form:hidden path="id" value="${report.id}" />
                      			 
                      			 
							 <button  type="submit" class="btn btn-primary btn-sm"  value="Validate">Delete</button>
							
                      		</form:form>
                      		
                        </div>
                   
                      </div>

                    </div>
                  </div>
												</td>
											
											
											
											
																			</tr>

									</c:forEach>

								</tbody>
							</table>
							
                             </div>
                    </div>
                    
                </div>
                
    

            
                </div>
                </div>
  
  


  
  
<%@ include file="javascript.jsp"%>
	 
</html>

   <script type="text/javascript">
            $(function () {
            	$('.table').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'print'
                    ]
                });
            });

        </script>
