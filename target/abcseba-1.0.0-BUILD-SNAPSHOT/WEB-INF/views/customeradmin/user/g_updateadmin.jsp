<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<meta charset="utf-8">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page session="false"%>
<%@page import="org.owasp.esapi.ESAPI"%>
<spring:htmlEscape defaultHtmlEscape="true" />

<html>
<head>
<title><spring:message code="title.app"/></title>

<%@ include file="../../css.jsp"%>

</head>
<%@ include file="../../headerAndSideManu.jsp"%>


        <div class="wrapper">
            <div class="container">

                <!-- Page-Title -->
                <h4 class="text-success counter">${title}</h4>
                


				<div class="row">
                
               
  <div class="col-sm-6 col-lg-2 pad-right">
                        <div class="widget-simple text-left card-box">                                    
                <h4 class="m-t-0 header-title"><b>Customer Setting</b></h4>
    <hr>
                               <ul class="nav">
					                    
					     <li class="active" ><a href="${pageContext.request.contextPath}/vc/userlist">List of User</a></li>
					                        <li ><a href="${pageContext.request.contextPath}/vc/createuser">Create User</a></li>
					                                         
					                       
					                       </ul>
                   
                        </div>
                    </div>
                    
               

   <div class="col-sm-6 col-lg-10 pad-left">
                 
                        <div class="widget-simple text-left card-box">
                            <div>

<h4 class="m-t-0 header-title"><b>${menutitle}</b></h4>
<hr>
                                        
                                            <div class="box-body">
			<div class="form-group">
			
			<c:if test="${not empty msg}">
		<div class="alert alert-${css} alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" 
                                aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<strong>${msg}</strong>
		</div>
		</c:if>
		</div>
		</div>
		
		
		<div class="row"> 
		
	   <form:form action="${pageContext.request.contextPath}${actionval}"  method="POST"	commandName="${modelVOval}">
   			
					<div class="col-lg-6">
					
					
					
							<div class="form-group">
								<label> First Name</label> <form:input 
									class="form-control" placeholder="First Name" path="firstName" required="true"></form:input>
									<label id="form-msg-label_name" class="my-error-class">${erroragname}</label>
							</div>
							
							<div class="form-group">
								<label> Middle Name</label> <form:input 
									class="form-control" placeholder="Middle Name" path="middleName" ></form:input>
									<label id="form-msg-label_name" class="my-error-class">${erroragname}</label>
							</div>
							
							<div class="form-group">
								<label> Last Name</label> <form:input 
									class="form-control" placeholder="Last Name" path="lastName" required="true"></form:input>
									<label id="form-msg-label_name" class="my-error-class">${erroragname}</label>
							</div>
							
							
							
							</div>
							<div class="col-lg-6">
					
					
							<div class="form-group">
								<label>Email </label> <form:input 
									class="form-control" placeholder="Email" path="email" required="true"></form:input>
									<label id="form-msg-label_email" class="my-error-class">${erremail}</label>
							</div>
							<div class="form-group">
								<label>Mobile</label> <form:input 
									class="form-control" placeholder="Mobile Number" path="phoneNo" required="true"></form:input>
									<label id="form-msg-label_phone" class="my-error-class">${errphone}</label>
							</div>

	

<%-- 
				<div class="form-group">
								<label>Username</label> <form:input 
									class="form-control" placeholder="Username" path="username" required="true"></form:input>
									<label id="form-msg-label_phone" class="my-error-class">${errphone}</label>
							</div>


 --%>
						<div class="form-group">
                                <label for="assetfamily">Status</label> 
                                
                                <form:select class="form-control selectpicker" data-show-subtext="true" data-live-search="true"  path="status">
            
            						<form:options items="${statuslist}" />
                    
           						</form:select>
                            </div>


							</div>
							
			  
											   <form:hidden path="uuid" />	  		
							
							<div class="row">
							<div class="col-lg-12 text-center">
							<button id="newCustomerBtn" type="submit" class="btn btn-primary btn-lg" value="Validate"  >Update</button>
							<a href="${pageContext.request.contextPath}${editadminaction}" class="btn btn-primary btn-lg">Cancel</a>
							</div>
							</div>
		</form:form>
			 
		
 
		</div>
												
                  </div>
                        </div>
                    </div>


                </div>
                
      	
      	
      	<!-- -End of container class- -->
			</div>
													              
       </div>
      


<%@ include file="../../javascript.jsp"%>
	 
</html>

   <script type="text/javascript">
            $(function () 
            		{
            	

            	var createddate;
            	 createddate = new Date();
            	 createddate = createddate.getUTCFullYear() + '-' +
            	     ('00' + (createddate.getUTCMonth()+1)).slice(-2) + '-' +
            	     ('00' + createddate.getUTCDate()).slice(-2) + ' ' + 
            	     ('00' + createddate.getUTCHours()).slice(-2) + ':' + 
            	     ('00' + createddate.getUTCMinutes()).slice(-2) + ':' + 
            	     ('00' + createddate.getUTCSeconds()).slice(-2);
            	     
            	     $('createddate').val(createddate);
            });
            
</script>            

        