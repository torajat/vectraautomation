<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<meta charset="utf-8">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page session="false"%>
<html>
<head>
<title><spring:message code="title.app"/></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Include Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  
<%@ include file="../../css.jsp"%>

</head>
<%@ include file="../../headerAndSideManu.jsp"%>


        <div class="wrapper">
            <div class="container">

               
                <!-- Page-Title -->
                <h3 class="text-success counter">${title}</h3>
                 	          <%--  <a href="${pageContext.request.contextPath}/product/createproductcategory.do" class="btn btn-primary waves-effect waves-light m-t-10">
					               Create Category
					              </a>  
					     --%>
  				
                <div class="row">
           <%--     
                           <img src="${pageContext.request.contextPath}/resources/theme/productimage/1.jpg" alt=""/>
       <img src="${pageContext.request.contextPath}/resources/theme/productimage/8ff0c46f-50e7-4c29-baff-38e31b6a7bbe2.jpg" alt=""/>
  
   --%>
   
  
  
                    <div class="col-sm-12">
                    
  
                        <div class="card-box">
                        
                        <div class="box-body">
			<div class="form-group">
			
			<c:if test="${not empty msg}">
		<div class="alert alert-${css} alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" 
                                aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<strong>${msg}</strong>
		</div>
		</c:if>
		</div>
		</div>  
		
		<h4>${tabletitle}</h4>
                        
                        
                       <table id="productcategory" class="table table-bordered table-hover">
								<thead >
									<tr>
									    <th>Order Date</th>
										<th>Product Code</th>
										<th>Product Name</th>
										<th>Order Quantity</th>
										<th>Unit Price</th>
										<th>Total Price</th>
										<th>Table No</th>
										<th>Status</th>
										<th>Image</th>
									<!-- 	<th>Action</th> -->
										
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${orderlist}" var="report" varStatus="vs">
										<tr>
												<td>${report.orderdate}</td>
												<td>
												${report.productCode}
												</td>
												
												<td>
												${report.productname}
												</td>
												
										
												<td>
												${report.quantity}
												
												</td>
																
												<td>
												
												${report.unitPrice}
												
                  
												</td>
												
												
												<td>
												
												${report.totalprice}
												
                  
												</td>
											<td>
												
												${report.tableno}
												
                  
												</td>
												
												<td>
												${report.status}
												</td>
												<td>
												
												<img src="${pageContext.request.contextPath}/resources/theme/productimage/${report.filename}" alt=""/>
													
												</td>
												<%-- <td>
								
												
<button type="button" class="btn btn-outline btn-link"  data-animation="fadein" data-toggle="modal" data-target="#myModalname${vs.index}" id="viewDetailButton${vs.index}">Change Status</button>
                 
                 
                 
                  <!-- Modal -->
                  <div class="modal fade" id="myModalname${vs.index}" role="dialog">
                    <div class="modal-dialog modal-full" >

                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Change Order Status</h4>
                        </div>
                        <div class="modal-body">
                    
                     <form:form action="${pageContext.request.contextPath}${actionval}"  method="POST"	commandName="${modelVOval}">
                      			
                      			Are you sure want to start the preparation: <p class="text-warning"><span>${report.tableno}</span></p>
                      			<br/>
                      			
                      			<form:hidden path="tableno" value="${report.tableno}" />
                      		
								 <form:hidden path="id" value="${report.id}" />
                      			 <form:hidden path="productNo" value="${report.productNo}" />
                      			  <form:hidden path="status" value="${report.status}" />
                      			
							 <button  type="submit" class="btn btn-primary btn-sm"  value="Validate">Start</button>
							
                      		</form:form>
                    
                        </div>
                   
                      </div>

                    </div>
                  </div>
							
												
							

													
												
												
												
													</td> --%>
										</tr>

									</c:forEach>

								</tbody>
							</table>
							
                             </div>
                    </div>
                </div>
                </div>
                </div>
  
  	
<%@ include file="../../javascript.jsp"%>
	 
</html>


   <script type="text/javascript">
            $(function () {
            	$('.table').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'print'
                    ]
                });
            });

        </script>