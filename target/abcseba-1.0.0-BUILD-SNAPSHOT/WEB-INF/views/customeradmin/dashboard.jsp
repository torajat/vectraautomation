<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<meta charset="utf-8">
<meta http-equiv="refresh" content="300">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page session="false"%>
<html>
<head>
<title><spring:message code="title.app"/></title>

<%@ include file="../css.jsp"%>

</head>
<%@ include file="../headerAndSideManu.jsp"%>
<body onload="load()">

        <div class="wrapper">
            <div class="container">

                <!-- Page-Title -->
        
<%--               <div class="row">
        
       <div class="col-sm-6 col-lg-3">
                        <div class="widget-simple-chart text-right card-box">
                            <div class="circliful-chart circliful" data-dimension="90" data-text="35%" data-width="5" data-fontsize="14" data-percent="35" data-fgcolor="#5fbeaa" data-bgcolor="#ebeff2" style="width: 90px;"><span class="circle-text" style="line-height: 90px; font-size: 14px;"></span><canvas width="90" height="90"></canvas></div>
                            <h3 class="text-success counter">${pending}</h3>
                            <p class="text-muted text-nowrap">Running Job</p>
                        </div>
                    </div>
                    
                    
                          
        <div class="col-sm-6 col-lg-3">
                        <div class="widget-simple-chart text-right card-box">
                            <div class="circliful-chart circliful" data-dimension="90" data-text="35%" data-width="5" data-fontsize="14" data-percent="35" data-fgcolor="#5fbeaa" data-bgcolor="#ebeff2" style="width: 90px;"><span class="circle-text" style="line-height: 90px; font-size: 14px;"></span><canvas width="90" height="90"></canvas></div>
                            <h3 class="text-success counter">${pstart}</h3>
                            <p class="text-muted text-nowrap">Will Start Soon</p>
                        </div>
                    </div>
        
        
        
              
        <div class="col-sm-6 col-lg-3">
                        <div class="widget-simple-chart text-right card-box">
                            <div class="circliful-chart circliful" data-dimension="90" data-text="35%" data-width="5" data-fontsize="14" data-percent="35" data-fgcolor="#5fbeaa" data-bgcolor="#ebeff2" style="width: 90px;"><span class="circle-text" style="line-height: 90px; font-size: 14px;"> </span><canvas width="90" height="90"></canvas></div>
                            <h3 class="text-success counter">${completed}</h3>
                            <p class="text-muted text-nowrap">Total Completed Order</p>
                        </div>
                    </div>
        
        
              
        <div class="col-sm-6 col-lg-3">
                        <div class="widget-simple-chart text-right card-box">
                            <div class="circliful-chart circliful" data-dimension="90" data-text="35%" data-width="5" data-fontsize="14" data-percent="35" data-fgcolor="#5fbeaa" data-bgcolor="#ebeff2" style="width: 90px;"><span class="circle-text" style="line-height: 90px; font-size: 14px;"></span><canvas width="90" height="90"></canvas></div>
                            <h3 class="text-success counter">${total}</h3>
                            <p class="text-muted text-nowrap">Total Order</p>
                        </div>
                    </div>
        
        
        </div>
        
         --%>
          <div class="col-sm-6 col-lg-3">
       
				 <form:form action="${pageContext.request.contextPath}/vc/job/dashboard"  method="GET"	commandName="${modelVOval}">
   
								<div class=row>
                      			 <div class="col-lg-6">
                      				<div class="form-group">
                                <label for="assetfamily">Select OEM</label> 
                                
                                <form:select class="form-control selectpicker" data-show-subtext="true" data-live-search="true"  path="companyname">
            
            						<form:options items="${companyname}" />
                    
           						</form:select>
           					
                    		
                            </div>
                            
                            </div>
                             <div class="col-lg-6">
                      				
           						<div class="col-lg-3">
           						<div class="form-group">
           						 <label for="assetfamily"> &nbsp </label> 
                               <button  type="submit" class="btn btn-info" >Apply</button>
           						</div>
           						</div>
           						</div>
           						</div>
                            </form:form>
                            </div>
                            
                            
<div class="col-sm-6 col-lg-4">
	<h4>Number of running Instance: <span class="badge badge-pink">${totalr}</span></h4>
	
	<h4>Number of Waiting Instance: <span class="badge badge-danger">${pendingrun}</span></h4>		
	
	Username: auto2dcompute
	Password: Vect2Dcompu163
		</div>
                  
                  
                  <div class="col-sm-6 col-lg-4">
	<h4>FTP: <span class="badge badge-pink">${ftp}</span></h4>
	
	<h4>${files}</h4>		
	
		</div>
                                          
<div class="row">


                      
                        <div class="box-body">
			<div class="form-group">
			
			<c:if test="${not empty msg}">
		<div class="alert alert-${css} alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" 
                                aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<strong>${msg}</strong>
		</div>
		</c:if>
		</div>
		</div>
<div class="col-lg-12">
                        <div class="card-box">
                            
                            <h4 class="text-dark  header-title m-t-0">Dashboard <a href="${pageContext.request.contextPath}/vc/job/dashboard"  class="btn btn-primary">Refresh</a></h4>
                            <p class="text-muted m-b-25 font-13">
                                Working dashboard
                            </p>

                            <div class="table-responsive">
                                <table class="table table-hover m-b-0">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Client Name</th>
                                        <th>Designer Workstation</th>
                                        <th>Tool</th>
                                        <th>Feeder File Index</th>
                                        <th>Present Working Status</th>
                                       <!--    <th>In Queue</th> -->
                                           <!--  <th>Scheduled</th> -->
                                            <th>Start Time</th>
                                            <th>End Time</th>
                                              <th>Running Process</th>
                                                <th>Operational Status</th>
                                                <th>Instance Operation</th>
                                                <th> Elapsed Time</th>
                                              <th>Public Ip</th>
                                                
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${assetconfigs}" var="report" varStatus="vs">
									
                                    <tr>
                                        <td>${vs.index}</td>
                                        <td>${report.companyname}</td>
                                        <td>${report.designerworkstation}
                                        </td>
                                        <td>${report.toolname}
                                        </td>
                                        <td>${report.datafilename}</td>
                                        <td> Instance Id: ${report.instanceid}
                                         <c:if test="${report.operationflg eq 'Running'}">  <span class="badge badge-pink">${report.operationflg}</span></c:if>
                                         <c:if test="${report.operationflg eq 'ProcessCompleted'}">  <span class="badge badge-success">${report.operationflg}</span></c:if>
                                         <c:if test="${report.processingstatus eq 'NeedToCopy'}">  <span class="badge badge-info">${report.processingstatus}</span></c:if>
                                  
                                  
                                     </td>
                                         <%--  <td>${report.jobdate}
                                          
                                           --%><td>${report.starttime}
	
	
                                          <td>${report.endtime}
	
										</td>
                             
                               <td>
                               ${report.running}
								</td>
                             
                               <td>
                                 <c:if test="${report.operationflg eq 'Running'}">  <span class="badge badge-pink">${report.operationflg}</span></c:if>
                                         <c:if test="${report.operationflg eq 'ProcessCompleted'}">  <span class="badge badge-success">${report.operationflg}</span></c:if>
                                    		
                                    		${report.privateip}
                                    		</td>
                             
                             <td>
                             
                             <c:choose>
    <c:when test="${report.operationflg eq 'Running'}">
    	
    			<button type="button" class="btn btn-outline btn-link"  data-animation="fadein" data-toggle="modal" data-target="#myModal${vs.index}" id="viewDetailButton${vs.index}">  <span class="badge badge-warning">Stop</span> </button>
        
     <!-- Modal -->
                  <div class="modal fade" id="myModal${vs.index}" role="dialog">
                    <div class="modal-dialog modal-full" >

                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h3 class="modal-title">Stopping Instance  </h3>
                        </div>
                        <div class="modal-body">
                    
					
					
					
       
                              <form:form action="${pageContext.request.contextPath}${actionval}"  method="POST"	commandName="${modelVOval}">
                      			
                      			<h2>Are you sure want to stop the instance: ${report.instanceid}</h2>
                      			<form:hidden path="instanceid" value="${report.instanceid}"  />
                      			
                      			<form:hidden path="jobrowid" value="${report.jobrowid}"  />
                      			
                    
			 					<div class="col-lg-12">
			
							 <button  type="submit" class="btn btn-danger btn-sm"  value="Validate">Stop</button>
							</div>
                      		</form:form>
                
                
                
                		 </div>
                   
                      </div>

                    </div>
                  </div>
    
    
    
    
    </c:when>
    <c:otherwise>
        
    </c:otherwise>
</c:choose>

                    
                     <c:choose>
    <c:when test="${report.transferbtn eq '1'}">
    	
    		 <button type="button" class="btn btn-outline btn-link"  data-animation="fadein" data-toggle="modal" data-target="#myModalTransfer${vs.index}" id="viewDetailButton${vs.index}">  <span class="badge badge-warning">Transfer Job to new Instance</span> </button>
        
     <!-- Modal -->
                  <div class="modal fade" id="myModalTransfer${vs.index}" role="dialog">
                    <div class="modal-dialog modal-full" >

                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h3 class="modal-title">Transfer Job  </h3>
                        </div>
                        <div class="modal-body">
                    
					
					
					
       
                              <form:form action="${pageContext.request.contextPath}${transferaction}"  method="POST"	commandName="${modelVOval}">
                      			
                      			<h2>Do you wish to transfer this server data to a new instance?</h2>
                      			<form:hidden path="instanceid" value="${report.instanceid}"  />
                      			
                      			<form:hidden path="companyname" value="${report.companyname}"  />
                      			
                  				<form:hidden path="designerworkstation" value="${report.designerworkstation}"  />
                      		
                      			<form:hidden path="toolname" value="${report.toolname}"  />
                      			
                      			<form:hidden path="jobrowid" value="${report.jobrowid}"  />
                      			
                      			
                      			<form:hidden path="csdestionation" value="${report.textfilepath}"  />
                      					
			 					<div class="col-lg-12">
			
							 <button  type="submit" class="btn btn-success btn-sm"  value="Validate">Transfer</button>
							 <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
							</div>
                      		</form:form>
                
                
                
                		 </div>
                   
                      </div>

                    </div>
                  </div>
    
    
    
    
    </c:when>
    <c:otherwise>
        
    </c:otherwise>
</c:choose>
                    
                             </td>
                             
                             <td>
                             ${report.passtime}
                             </td>
                             
                             <td>
                             ${report.publicip}
                             </td>
                             
                                    </tr>
                                    </c:forEach>
                                                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    
                      </div>                  

            
<!--             
<div class="col-lg-4">
                        <div class="portlet">
                            <div class="portlet-heading bg-inverse">
                                <h3 class="portlet-title">
                                    Inverse Heading
                                </h3>
                                <div class="portlet-widgets">
                                    <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                                    <span class="divider"></span>
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#bg-inverse"><i class="ion-minus-round"></i></a>
                                    <span class="divider"></span>
                                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="bg-inverse" class="panel-collapse collapse show">
                                <div class="portlet-body">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce elementum, nulla vel pellentesque consequat, ante nulla hendrerit arcu, ac tincidunt mauris lacus sed leo. vamus suscipit molestie vestibulum.
                                </div>
                            </div>
                        </div>
                    </div>
                             -->
            
            
            
            
            
            
            
                </div>
                </div>
</body>
  
<%@ include file="../javascript.jsp"%>
	 
</html>
<script type="text/javascript">
function load()
{
	setTimeout("window.open(self.location, '_self');", 300000);
}
</script>